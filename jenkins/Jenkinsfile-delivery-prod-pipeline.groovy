@Library('jenkins-sharedlib@master')
import sharedlib.FreeStyleJenkinsUtil
def utils = new FreeStyleJenkinsUtil(this)
/* Project settings */
def project="INCT"
/* Mail configuration*/
// If recipients is null the mail is sent to the person who start the job
// The mails should be separated by commas(',')
def recipients=""
def deploymentEnvironment="prod"
def artifactoryUrl="https://artifactory.lima.bcp.com.pe/artifactory"
try {
   node {
      stage('Preparation') {
            utils.notifyByMail('START',recipients)
            env.project="${project}"
            checkout scm
            utils.prepare()
      }

      stage('Start Release') {
        echo "inicio release"
        echo "params.RELEASE_TAG_NAME ${params.RELEASE_TAG_NAME}"
        utils.promoteRelease(params.RELEASE_TAG_NAME,params.FORCE_RELEASE)
      }

      stage('Save Results') {
        utils.saveResult('tar')
      }

      stage("Deploy to" +deploymentEnvironment){
         // Your Deploy method goes here
      }

      stage('Post Execution') {
        utils.executePostExecutionTasks()
        utils.notifyByMail('SUCCESS',recipients)
      }

   }
} catch(Exception e) {
   node{
      utils.executeOnErrorExecutionTasks()
      utils.notifyByMail('FAIL',recipients)
    throw e
   }
}
