@Library('jenkins-sharedlib@master')
import sharedlib.FreeStyleJenkinsUtil
def utils = new FreeStyleJenkinsUtil(this)
/* Project settings */
def project="INCT"
/* Mail configuration*/
// If recipients is null the mail is sent to the person who start the job
// The mails should be separated by commas(',')
def recipients=""
def deploymentEnvironment="cert"

try {
   node {
      stage('Preparation') {
         utils.notifyByMail('START',recipients)
         checkout scm
         utils.prepare()
         //Setup parameters
         env.project="${project}"
        utils.setRelativeSrcPath(pwd())
        utils.setPackageType('tar')
      }
      stage('Start Release') {
        utils.startRelease()
      }
      stage('Save Results') {
        utils.saveResult('tar')
      }
      stage("Deploy to" +deploymentEnvironment){
         // Your Deploy method goes here
      }
	  stage('Post Execution') {
        utils.executePostExecutionTasks()
        utils.notifyByMail('SUCCESS',recipients)
      }

   }
} catch(Exception e) {
   node{
      utils.executeOnErrorExecutionTasks()
      utils.notifyByMail('FAIL',recipients)
    throw e
   }
}
