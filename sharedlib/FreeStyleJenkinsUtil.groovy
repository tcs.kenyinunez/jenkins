package sharedlib;

import org.jenkinsci.plugins.docker.workflow.*;
import groovy.json.JsonSlurper;
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*

class FreeStyleJenkinsUtil extends BaseUtil implements Serializable, DevopsUtil {
  private String relativeSrcPath = ''
  private String packageType = 'zip'
  private String environment = ''

  private FreeStyleJenkinsUtil() {}

  public FreeStyleJenkinsUtil(
    script,
    Docker docker,
    buildTimestamp,
    currentGitURL,
    currentCredentialsId,
    branchName,
    currentGitCommit,
    String buildUser,
    String buildUserId
  ) {
    this.script = script
    this.docker = docker
    this.buildUser = buildUser
    this.buildUserId = buildUserId
    this.buildTimestamp = buildTimestamp
    this.currentGitURL = currentGitURL
    this.gitlabUrl = "${script.env.GITLAB_PROTOCOL}://${script.env.GITLAB_HOST}:${script.env.GITLAB_PORT}"
    this.bitbucketUrl = gitlabUrl
    this.currentCredentialsId = currentCredentialsId
    this.artefactoryUrl = script.env.ARTIFACT_SERVER_URL
    this.branchName = branchName
    this.currentGitCommit = currentGitCommit;
  }

  public FreeStyleJenkinsUtil(script, String type = ''){
    super(script, type)

    this.script.steps.echo "[BCP-DevSecOps] script: ${script}"
    script.properties.each { println "$it.key -> $it.value" }
  }

  public setRelativeSrcPath(String relativeSrcPath = ''){
    this.relativeSrcPath = relativeSrcPath
  }

  public setPackageType(String packageType = 'zip'){
    this.packageType = packageType
  }

  public String getApplicationVersion(){
    return this.script.steps.sh(script: "cat version", returnStdout: true).trim();
  }

  public String getApplicationName(){
    return this.getGitProjectName();
  }

  public void build(final String buildParams) {
    String applicationName = getApplicationName()
    String applicationVersion = getApplicationVersion()
    // Package the builds
    if(packageType=="zip"){
      this.printMessage("Zip artifact en construccion");
      def comment = "ic: JENKINS build-url: ${this.script.env.BUILD_URL} branch: ${branchName} commit: ${currentGitCommit} date: ${buildTimestamp}."
      this.script.steps.sh "echo ${comment} > comments-jks-${buildTimestamp}"
      this.script.steps.sh "zip -z -x comments-jks-${buildTimestamp} -r ${this.script.env.WORKSPACE}/${applicationName}-${applicationVersion}.zip ./${relativeSrcPath}* < comments-jks-${buildTimestamp}"
      this.script.steps.sh "ls -al ${applicationName}-${applicationVersion}.zip"
    } else if(packageType=="tar"){
      this.printMessage("Tar artifact en construccion");
      this.script.steps.sh "tar -cvf ${applicationName}-${applicationVersion}.tar ${relativeSrcPath}"
      this.script.steps.sh "ls -al ${applicationName}-${applicationVersion}.tar"
    }
  }

  public void executeQA(final String releaseName, final String encoding){
    printMessage("No implementado")
  }
  public void executeSast(final String buildParams){
    printMessage("No implementado")
  }

  public void saveResult(final String extension) {
  	this.script.steps.archiveArtifacts(artifacts:'**/**/*.'+extension,allowEmptyArchive:true)
  }

  public void uploadArtifact(final String releaseName){
    def buildUniqueId = "${buildTimestamp}"
    if(releaseName!=null&&releaseName!=''){
      this.printMessage("Deploy artifactory cert")
      this.script.steps.step([$class: 'WsCleanup'])
      buildUniqueId="${releaseName}"
      this.checkoutBranch(releaseName)
      this.build()
    }

    def repository
    def environmentUpper = this.environment.toUpperCase()

    def repoKey

    if ("${branchName}"=="master"){
      repoKey="${script.env.project}.Generic.Release"
    }else{
      repoKey="${script.env.project}.Generic.Snapshot"
    }
    if(environmentUpper){
      repoKey="${script.env.project}.Generic-${environmentUpper}"
    }
    def projectName = "${script.env.project}"
    def projectNameLowercase = projectName.toLowerCase()
    def applicationName = this.getApplicationName()
    def applicationVersion = this.getApplicationVersion()
    this.printMessage(this.getApplicationName())
    this.printMessage(this.getApplicationVersion())
    this.printMessage(this.getGitProjectName())
    def moduleName = "${projectName}".toLowerCase()
    def projectKeyName = "${script.env.project}".toLowerCase()+"-${applicationName}"
    def actionURL = "${artefactoryUrl}/${repoKey}/${moduleName}/${applicationName}/${applicationVersion}/"
    this.printMessage("Repository Path: ${actionURL}")
    def actionBuildURL = "${artefactoryUrl}/api/build"
    this.printMessage("Artifactory Buil Api URL: ${actionBuildURL}")
    this.script.steps.withCredentials([[
      $class: 'UsernamePasswordMultiBinding',
      credentialsId: projectNameLowercase+'-jenkins-artefactory-token',
      usernameVariable: 'ARTIFACT_USER_NAME',
      passwordVariable: 'ARTIFACT_USER_PASSWORD'
    ]]) {
      def artifactSha1 = this.script.steps.sh(
        script: "sha1sum ${applicationName}-${applicationVersion}.${packageType} | cut -d ' ' -f 1",
        returnStdout: true
      ).trim()
      def artifactMd5 = this.script.steps.sh(
        script: "md5sum ${applicationName}-${applicationVersion}.${packageType} | cut -d ' ' -f 1",
        returnStdout: true
      ).trim()
      this.printMessage("artifactSha1: ${artifactSha1}")
      this.printMessage("artifactMd5: ${artifactMd5}")

      def result = this.script.steps.sh (
        script: """
        curl -o response-build-info-${buildTimestamp}.txt -X PUT --header \"X-Checksum-MD5:${artifactMd5}\" --header \"X-Checksum-Sha1:${artifactSha1}\" -u ${script.env.ARTIFACT_USER_NAME}:${script.env.ARTIFACT_USER_PASSWORD} -T ${applicationName}-${applicationVersion}.${packageType} "${actionURL}${applicationName}-${applicationVersion}.${packageType};build.name=${projectKeyName};build.number=${buildUniqueId}"
        """,
        returnStdout: true
      )
      def callback = this.script.steps.sh(
        script: "cat response-build-info-${buildTimestamp}.txt",
        returnStdout: true
      ).trim()
      this.printMessage("Callback: ${callback}")

      def errorMessageArtifactory = this.script.steps.sh(
        script:  "cat response-build-info-${buildTimestamp}.txt | jq --raw-output '.errors | .[0] | .message'",
        returnStdout: true
      ).trim()

      if(errorMessageArtifactory!="null"){
        this.throwException("Error deploying the artifact to the artifact server")
      }

      def timestamp = this.script.steps.sh(script: "echo \$(date +\"%Y-%m-%dT%H:%M:%S.%3N%z\")", returnStdout: true).trim()
      def artifactoryBuildInfoJson = this.getArtifactoryBuildInfo(
        "${script.env.ARTIFACT_USER_NAME}",
        moduleName,
        applicationName,
        applicationVersion,
        buildUniqueId,
        timestamp,
        packageType,
        artifactSha1,
        artifactMd5
      );
      this.printMessage("artifactoryBuildInfoJson: ${artifactoryBuildInfoJson}")

      this.script.steps.writeFile(
        file: "${script.env.WORKSPACE}/artifactory-json-${buildTimestamp}.json",
        text: artifactoryBuildInfoJson
      )

      result = this.script.steps.sh(
        script: """
        curl -X PUT -I -i -u ${script.env.ARTIFACT_USER_NAME}:${script.env.ARTIFACT_USER_PASSWORD} -H \"Content-Type: application/json\" -T artifactory-json-${buildTimestamp}.json "${actionBuildURL}"
        """,
        returnStdout: true
      )
      this.printMessage("Result: ${result}")
    }
  }

  private String getArtifactoryBuildInfo(
  	String user,
  	String projectKeyName,
  	String applicationName,
  	String applicationVersion,
  	String buildtimestamp,
  	String startedTimeStamp,
  	String artifactType,
  	String artifactSha1,
  	String artifactMd5
  ){
    def result = """{
  "version" : "1.0",
  "name" : "${projectKeyName}-${applicationName}",
  "number" : "${buildtimestamp}",
  "type" : "GENERIC",
  "agent" : {
    "name" : "Jenkins"
  },
  "started" : "${startedTimeStamp}",
  "artifactoryPrincipal" : "${user}",
  "url" : "${this.script.env.BUILD_URL}",
  "vcsRevision" : "${currentGitCommit}",
  "vcsUrl" : "${currentGitURL}",
  "modules" : [ {
    "properties" : {
      "project.build.sourceEncoding" : "UTF-8"
    },
    "id" : "${applicationName}:${applicationVersion}",
    "artifacts" : [ {
      "type" : "${artifactType}",
      "sha1" : "${artifactSha1}",
      "md5" : "${artifactMd5}",
      "name" : "${applicationName}-${applicationVersion}.${artifactType}"
    }]
  } ]
}
""".stripIndent()
    return result
  }

  public void updateToReleaseVersionFreeStyle(String version){
    this.script.steps.sh (script: """echo ${version}> version""", returnStdout: true)
    this.updateToReleaseVersion(version)
  }

  public void startRelease(final String buildParams){
    if ("${branchName}" == "master"){
      this.throwException("Generating of release must not be done on master")
    }

    //Check generate new version
    def applicationVersion = this.getApplicationVersion()
    this.printMessage("Version de aplicacion: ${applicationVersion}")
    //The version is always major version
    def nextReleaseVersion = "${applicationVersion}"
    this.printMessage("Siguiente version release: ${nextReleaseVersion}")

    def releaseBranchName = "release/"+nextReleaseVersion
    def existBranchInRepository = this.existBranch(releaseBranchName)
    this.printMessage("Existe rama release: ${existBranchInRepository}")
    if(existBranchInRepository){
      //If the branch exist an error is throw
      this.printMessage("Eliminando rama release: ${releaseBranchName}")
      this.deleteNotMergeReleaseBranch(releaseBranchName)
    }

    this.createNewReleaseBranch(nextReleaseVersion)
    this.checkoutReleaseBranch(nextReleaseVersion)
    //updateToReleaseVersionFreeStyleProject(nextReleaseVersion)

    def tagStateComment = "Release Candicate"
    def currentDate = buildTimestamp
    this.environment = "CERT"
    def tagName = "RC-${applicationVersion}-${this.environment}-${currentDate}"

    tagName = this.updateTagNameWithACR(tagName)

    this.protectBranch("release/${nextReleaseVersion}")
    this.tagBranch(tagName,tagStateComment,this.environment)
    this.uploadArtifact(tagName)
  }

  public void promoteRelease(final String releaseTagName, final boolean forceRelease){
    if(!"${branchName}".startsWith("tags/RC-")){
      this.throwException("The release must be done on release")
    }

    def applicationVersion = this.getApplicationVersion()
    def existTag = existTag("${applicationVersion}")
    def tagStateComment = "Release"
    def tagName = this.getApplicationVersion()
    def applicationName = this.getApplicationName()

    if(existTag){
      if (!forceRelease){
        this.throwException("The tag for this release exists: ${applicationVersion}")
      }
      this.printMessage("Forcing removal and promote of release tag name: ${releaseTagName}")
      this.deleteTagBranch(tagName)
      this.printMessage("Tag removed: ${releaseTagName}")
      this.printMessage("${tagStateComment}-FORCED")
    }
    this.printMessage("Promote release tag name: ${releaseTagName}")
    promoteRepository(applicationName,releaseTagName,".Generic")
    //mergeReleaseToMaster(releaseTagName)
    mergeOverwriteBranch(releaseTagName,"master")
    tagBranch(tagName,tagStateComment)
  }
}
