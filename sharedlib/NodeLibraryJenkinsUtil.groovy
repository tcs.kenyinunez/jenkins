package sharedlib;

public class NodeLibraryJenkinsUtil extends JenkinsfileUtil implements DevopsUtil {
  private static final String NODE10_CHROME78_VERSION = "NODE10_CHROME78"
  private static final String DOCKER_NODE10_CHOME78 = "bcp/node10-chrome78:1.0"
  private String deployFolderNpmPack = '.'
  private String sourcesForSonar = 'src'
  private String reportPathsForSonar = 'coverage/lcov.info'
  private boolean isActiveDebug = false;
  private String artifactNpmPrivateRegistryReleaseUrl
  private String artifactNpmPrivateRegistryCertUrl
  private String artifactNpmPrivateRegistrySnapshotUrl
  private String dockerNodeEnviroment

  public NodeLibraryJenkinsUtil(steps,script,String type = '') {
    super(steps, script, type);

    artifactNpmPrivateRegistryReleaseUrl = "${script.env.ARTIFACT_SERVER_URL}/api/npm/public-release-node/"
    artifactNpmPrivateRegistryCertUrl = "${script.env.ARTIFACT_SERVER_URL}/api/npm/public-cert-node/"
    artifactNpmPrivateRegistrySnapshotUrl = "${script.env.ARTIFACT_SERVER_URL}/api/npm/public-snapshot-node/"
  }

  public void activeDebug() {
    this.isActiveDebug = true
  }

  public void setDeployFolderNpmPack(String deployFolder) {
    this.deployFolderNpmPack = deployFolder
  }

  public void setSourcesForSonar(String source) {
    this.sourcesForSonar = source
  }

  public void setReportPathsForSonar(String reportPaths) {
    this.reportPathsForSonar = reportPaths
  }

  public void setNodeVersion(String nodeVersion){
    if (NODE10_CHROME78_VERSION != nodeVersion) {
      this.throwException("Not supported version: ${nodeVersion}");
    }

    dockerNodeEnviroment = DOCKER_NODE10_CHOME78

    this.printMessage("La version de node es: ${nodeVersion}");
    this.printMessage("El entorno es: ${nodeVersion}");
  }

  public String getApplicationName(){
    def appName = steps.sh script: "jq --raw-output '.name' ./package.json", returnStdout: true
    return appName.trim()
  }

  public String getApplicationVersion(){
    def appVersion = steps.sh script: "jq --raw-output '.version' ./package.json", returnStdout: true
    return appVersion.trim()
  }

  public void build(final String buildParams){
    this.installDependencies()
    this.executeDockerCmd(["npm run test"]);
    this.executeDockerCmd(["npm run build ${buildParams ?: ''}"]);
  }

  protected void installDependencies() {
    def installFlags = "--package-lock=false"
    if (isActiveDebug) {
      installFlags += " --verbose"
    }

    this.executeDockerCmd(
      ["npm i ${installFlags}"],
      {
        def deploymentEnvironment = "${script.env.deploymentEnvironment}".toLowerCase();
        def npmUserConfig = getNpmConfiguration(deploymentEnvironment)

        this.steps.writeFile(
          file:"${script.env.WORKSPACE}/.npmrc",
          text: npmUserConfig
        );
      }
    );
  }

  protected void executeDockerCmd(scripts = [""], callback = null) {
    def listOfScript = scripts as String[];
    this.docker.withServer("${script.env.DOCKER_HOST}"){
      this.docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}"){
        def dockerParameters = "--network=host"
        def dockerVolumen = "-v ${script.env.WORKSPACE}:/project/data1 -w /project/data1 -v ${script.env.APPLICATION_CACHE}/node-local-repo:/opt/nodecache"
        def dockerCmd = "docker run --rm ${dockerVolumen} ${dockerParameters} ${this.dockerNodeEnviroment}"

        if(callback) callback();

        for (script in listOfScript) {
            this.steps.sh "${dockerCmd} ${script}"
        }
      }
    }
  }

  protected String getNpmConfiguration(String environment) {
    this.printMessage("Se ha ejecutado el getNpmConfiguracion");
    this.printMessage("environment: ${environment}");
    def artifactUrl = this.getUrlRepository(environment)
    this.printMessage("environment: ${artifactUrl}");
    def npmUserConfig = """
        registry="${artifact_npm_registry_url}"
        @bcp:registry="${artifactUrl}"
        strict-ssl=false
        cache=/opt/nodecache
        """.stripIndent()

    return npmUserConfig
  }

  protected String getUrlRepository(String environment) {
    def artifactNpmUrl
    switch(environment){
      case 'dev':
        artifactNpmUrl = artifactNpmPrivateRegistrySnapshotUrl
        break;
      case 'cert':
        artifactNpmUrl = artifactNpmPrivateRegistryCertUrl
        break;
      case 'prod':
        artifactNpmUrl = artifactNpmPrivateRegistryReleaseUrl
        break;
      default:
        this.throwException("Not supported environment: ${environment}");
    }

    return artifactNpmUrl
  }

  public void executeQA(final String releaseName="", final String encoding="UTF-8"){
    this.executeDockerCmd(["npm run lint"]);

    this.executeSonar(releaseName, encoding)
  }

  private void executeSonar(String releaseName, String encoding) {
    def appName = this.getApplicationName()
    appName = appName.replaceAll(/[^-_.:\s\w]/,"-");
    appName = appName.replaceAll("^-+", "");
    def appVersion  = this.getApplicationVersion()
    def projectName = "${this.script.env.project}".toLowerCase()
    def appId = "${appName}"

    executeSonarWithSonarRunner(projectName, appVersion, releaseName, encoding, appId)
  }

  private void executeSonarWithSonarRunner(String projectName, appVersion, String releaseName, String encoding, appId) {
    this.printMessage("executeSonarWithSonarRunner inicio");
    this.steps.withSonarQubeEnv {
      def dockerParameters = "--rm --network=host -v ${this.script.env.WORKSPACE}:/project/data -w /project/data"
      def dockerSonarCmd = "docker run ${dockerParameters} ${this.dockerSonarRunner}"
      def sonarProjectVersion=appVersion+"-${this.branchName}"
      if (releaseName != "") {
        sonarProjectVersion=appVersion+"-${releaseName}"
      }

      this.steps.writeFile(
        file:"${this.script.env.WORKSPACE}/sonar-project.properties",
        text: getSonarProperties(this.script.env.SONAR_HOST_URL, this.script.env.SONAR_AUTH_TOKEN, projectName, sonarProjectVersion, appVersion, "node", releaseName, encoding, appId)
      )

      this.steps.sh "${dockerSonarCmd} sonar-scanner"
    }
    this.printMessage("executeSonarWithSonarRunner fin");
  }

  private String getSonarProperties(String url ,String login ,String projectName, sonarProjectVersion, applicationVersion, tek, tagName, encoding, appId){
    def initScript = """
sonar.projectVersion=${sonarProjectVersion}
sonar.analysis.version=${applicationVersion}
sonar.analysis.userId=${this.buildUserId}
sonar.analysis.commitId=${this.currentGitCommit}
sonar.analysis.tek=${tek}
sonar.projectName=${projectName}-${tek}:${appId}
sonar.analysis.buildTimestamp=${this.buildTimestamp}
sonar.analysis.branch=${this.branchName}
sonar.links.scm=${this.currentGitURL}
sonar.analysis.jobname=${this.script.env.JOB_NAME}
sonar.analysis.tagName=${tagName}
sonar.links.ci=${this.script.env.BUILD_URL}
sonar.host.url=${url}
sonar.login=${login}
sonar.password=
sonar.projectKey=${projectName}-${tek}:${appId}
sonar.sourceEncoding=${encoding}
sonar.sources=${this.sourcesForSonar}
sonar.tests=${this.sourcesForSonar}
sonar.exclusions=**/node_modules/**, **/*.spec.ts
sonar.test.inclusions=**/*.spec.ts
sonar.typescript.lcov.reportPaths=${this.reportPathsForSonar}
""".stripIndent()

  return initScript
  }

  public void executeSast(final String buildParams=""){
    if(fortifyActivated){
      def fortifyVersion = getGitProjectName()
      def projectLowerCase="${script.env.project}".toLowerCase()
      def fortifyCloudScanCredId = "${projectLowerCase}-jenkins-fortify-cloudscan-token"
      def fortifyAuthCredId = "${projectLowerCase}-jenkins-fortify-ssc-token"
      def buildID = "${script.env.project}-${fortifyVersion}-${script.env.BUILD_NUMBER}"

      steps.withCredentials([
        [$class: 'StringBinding', credentialsId: "${fortifyCloudScanCredId}", variable: 'cloudScanToken' ],
        [$class: 'StringBinding', credentialsId: "${fortifyAuthCredId}", variable: 'authToken' ]
      ]){
        try{
          steps.sh """
            set -x
            docker run -d -it --cpus ${execFortifyCpu} --network=host -w /project/data ${dockerFortifyTypescript} bash > .dockerId
            docker cp ${script.env.WORKSPACE}/. `cat .dockerId`:/project/scan
            if [ -d ${script.env.WORKSPACE}/node_modules ]; then
              docker exec `cat .dockerId` rm -r /project/scan/node_modules
            fi
            if [ -d ${script.env.WORKSPACE}/platforms ]; then
              docker exec `cat .dockerId` rm -r /project/scan/platforms
            fi
            docker exec `cat .dockerId` sourceanalyzer -b "${buildID}" -verbose "/project/scan/**/*.ts" -clean
            docker exec `cat .dockerId` sourceanalyzer -b "${buildID}" -verbose "/project/scan/**/*.ts" -Dcom.fortify.sca.Phase0HigherOrder.Languages=typescript
            docker exec `cat .dockerId` cloudscan -sscurl ${fortifyUrl} -ssctoken ${script.env.cloudScanToken} start  -block -log typescript.log -upload --application "${script.env.project}" --application-version "${fortifyVersion}" -b "${buildID}" -uptoken ${script.env.cloudScanToken} -scan ${execFortifyMemory}
            docker rm -f `cat .dockerId`
            docker run -d -it --cpus ${execFortifyCpu} --network=host -v ${script.env.WORKSPACE}:/project/data -w /project/data ${dockerFortifyTypescript} bash > .dockerId
            docker exec `cat .dockerId` fortifyclient downloadFPR -file result.fpr -url ${fortifyUrl} -authtoken ${script.env.authtoken} -project "${script.env.project}" -version "${fortifyVersion}"
            docker exec `cat .dockerId` BIRTReportGenerator -template "OWASP Top 10" -source result.fpr -format HTML --Version "OWASP Top 10 2017" -output MyOWASP_Top10_Report.html
            docker exec `cat .dockerId` fortifyclient -url ${fortifyUrl} -authtoken ${script.env.authtoken} listApplicationVersions > .listProject.txt
          """
        }catch(Exception e){
          throw e
          steps.sh """
            set +e
            docker exec `cat .dockerId` cat /opt/jenkins/.fortify/cloudscan/log/cloudscan.log
          """
        }finally{
          steps.sh """
            docker rm -f `cat .dockerId`
          """
        }

        generateReportFortify()
      }
    }else{
      this.printMessage("Fortify esta deshabilitado.");
    }
  }

  public void uploadArtifact(final String releaseName){
    def buildTimestamp = getBuildTimestamp()
    def buildUniqueId = "${buildTimestamp}"
    def projectName = "${script.env.project}"
    def enviroment = "${script.env.deploymentEnvironment}".toUpperCase()

    if(releaseName!=null && releaseName!=''){
      steps.step([$class: 'WsCleanup'])
      buildUniqueId = steps.sh(script: "echo ${releaseName} | cut -d '/' -f2 ", returnStdout: true).trim()
      checkoutBranch(releaseName)
      build()
    }

    repositoryId = "${script.env.project}.npm.Snapshot"

    if ("${branchName}" == "master"){
      repositoryId = "${script.env.project}.npm.Release"
    }

    if(enviroment == "CERT"){
      repositoryId = "${script.env.project}.npm-${enviroment}"
    } else if(enviroment == 'PROD' && "${branchName}".startsWith("tags/RC-")){
      repositoryId = "${script.env.project}.npm.Release"
      buildUniqueId = buildUniqueId+"-PROMOTED"
    }

    this.printMessage("Despliegue al repositoryio: ${repositoryId}");
    def deployPackage;

    docker.withServer("${script.env.DOCKER_HOST}"){
      docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
        def dockerParameters = "--network=host --cpus 2"
        def dockerVolumen = "-v ${script.env.WORKSPACE}:${script.env.WORKSPACE} -v ${script.env.APPLICATION_CACHE}/node-local-repo:/opt/nodecache -w ${script.env.WORKSPACE}"
        def dockerCmd = "docker run --rm ${dockerVolumen} ${dockerParameters} ${dockerNodeEnviroment}"

        deployPackage=steps.sh(script: "${dockerCmd} npm pack ${this.deployFolderNpmPack}", returnStdout: true).trim()
      }
    }

    this.printMessage("Antes de buildInfo");
    this.printMessage("azureKeyVaultActivated: ${azureKeyVaultActivated}");

    buildInfo(deployPackage,buildUniqueId,repositoryId);
  }

  private void buildInfo(String deployPackage, String buildUniqueId = '', String repositoryId){
    def projectName = "${script.env.project}"
    def projectNameLowercase = projectName.toLowerCase()
    def appName = getApplicationName()
    def appVersion = getApplicationVersion()
    def buildTimestamp = getBuildTimestamp()
    def projectKeyName = "${script.env.project}".toLowerCase()+"-${appName}"
    def repositoryUrl = "${script.env.ARTIFACT_SERVER_URL}/${repositoryId}/${appName}/-/${deployPackage}"
    def buildUrl = "${script.env.ARTIFACT_SERVER_URL}/api/build"

    def artifactoryUserName
    def artifactoryPassword

    def timestamp    = steps.sh(script: "echo \$(date +\"%Y-%m-%dT%H:%M:%S.%3N%z\")", returnStdout: true).trim()
    def artifactSha1 = steps.sh(script: "sha1sum ${deployPackage} | cut -d ' ' -f 1", returnStdout: true).trim()
    def artifactMd5  = steps.sh(script: "md5sum ${deployPackage} | cut -d ' ' -f 1", returnStdout: true).trim()

    this.printMessage("ProjectName: ${projectNameLowercase}");
    this.printMessage("azureKeyVaultActivated: ${azureKeyVaultActivated}");
    if(!azureKeyVaultActivated){
      this.printMessage("Entro if: azureKeyVaultActivated");
      steps.withCredentials([[$class: 'UsernamePasswordMultiBinding',
        credentialsId: projectNameLowercase+'-jenkins-artefactory-token',
        usernameVariable: 'ARTIFACT_USER_NAME',
        passwordVariable: 'ARTIFACT_USER_PASSWORD']]) {
          artifactoryUserName="${script.env.ARTIFACT_USER_NAME}"
          artifactoryPassword="${script.env.ARTIFACT_USER_PASSWORD}"
        }
    }else{
      this.printMessage("Entro else: azureKeyVaultActivated");
      withAzureVaultCredentials([[azureCredentialVariable:"ARTIFACT_USER_NAME",azureCredentialId: projectNameLowercase+'-jenkins-artefactory-token-username'],
        [azureCredentialVariable:"ARTIFACT_USER_PASSWORD",azureCredentialId: projectNameLowercase+'-jenkins-artefactory-token-password']
      ]){
        artifactoryUserName="${script.env.ARTIFACT_USER_NAME}"
        artifactoryPassword="${script.env.ARTIFACT_USER_PASSWORD}"
      }
    }

    steps.wrap([$class: 'MaskPasswordsBuildWrapper',
      varPasswordPairs: [
        [password: artifactoryUserName, var: 'artifactoryUserNameVariable'],
        [password: artifactoryPassword, var: 'artifactoryPasswordVariable']
      ]]){
        steps.sh """
          curl --silent --show-error --fail -u ${artifactoryUserName}:${artifactoryPassword} -X PUT --header \"X-Checksum-MD5:${artifactMd5}\" --header \"X-Checksum-Sha1:${artifactSha1}\" -T ${deployPackage} '${repositoryUrl};build.name=${projectKeyName};build.number=${buildUniqueId}'
        """

        def buildInfo = """
        {
          "version": "1.0",
          "name"   : "${projectKeyName}",
          "number" : "${buildUniqueId}",
          "type": "GENERIC",
          "agent": {
            "name": "Jenkins"
          },
          "started"             : "${timestamp}",
          "artifactoryPrincipal": "${artifactoryUserName}",
          "url"                 : "${script.env.BUILD_URL}",
          "vcsRevision"         : "${currentGitCommit}",
          "vcsUrl"              : "${currentGitURL}",
          "modules"             : [ {
            "properties" : {
              "project.build.sourceEncoding": "UTF-8"
            },
            "id"       : "${appName}:${appVersion}",
            "artifacts": [ {
              "type": "tgz",
              "sha1": "${artifactSha1}",
              "md5" : "${artifactMd5}",
              "name": "${deployPackage}"
            }]
          }]
        }
        """.stripIndent();

        steps.writeFile(
          file: "${script.env.WORKSPACE}/artifactory-json-${buildTimestamp}.json",
          text: buildInfo
        );

        steps.sh "curl --silent --show-error --fail -u ${artifactoryUserName}:${artifactoryPassword} -X PUT -H \"Content-Type: application/json\" -T artifactory-json-${buildTimestamp}.json '${buildUrl}'"
        steps.sh "rm ${script.env.WORKSPACE}/artifactory-json-${buildTimestamp}.json"
      }
  }

  public void saveResult(final String extension=""){
    steps.archiveArtifacts(artifacts:'*.'+extension,allowEmptyArchive:true)
  }

  public void startRelease(final String buildParams=""){
    if ("${branchName}"=="master"){
      this.throwException("Generating of release must not be done on master");
    }

    if(fortifyActivated){
      executeSast(buildParams)
    }
    //Check generate new version
    def applicationVersion=getApplicationVersion()
    this.printMessage("Version de alicacion: ${applicationVersion}");
    //The version is always major version
    def nextReleaseVersion="${applicationVersion}"
    this.printMessage("Siguiente version de lanzamiento: ${nextReleaseVersion}");

    def releaseBranchName="release/"+nextReleaseVersion
    def existBranchInRepository=existBranch(releaseBranchName)
    this.printMessage("Existe la rama de lanzamiento: ${existBranchInRepository}");
    if(existBranchInRepository){
      this.printMessage("Eliminando la rama de lanzamiento: ${releaseBranchName}");
      deleteNotMergeReleaseBranch(releaseBranchName)
    }

    createNewReleaseBranch(nextReleaseVersion)
    checkoutReleaseBranch(nextReleaseVersion)

    def tagStateComment="Release Candicate"
    def currentDate = buildTimestamp
    def enviromentParam = "CERT"
    applicationVersion = getApplicationVersion()
    def releaseName="RC-"+applicationVersion+"-${enviromentParam}-${currentDate}"
    protectBranch("release/${nextReleaseVersion}")
    try{
      executeQA(releaseName)
    }catch(Exception e){
      this.printMessage("Advertencia: error ejecutando QA");
    }
    tagBranch(releaseName,tagStateComment,enviromentParam)

    uploadArtifact(releaseName)

    executeXraySCA(releaseName)
  }

  public void promoteRelease(final String releaseTagName, final boolean forceRelease=false){
    if(!"${this.branchName}".startsWith("tags/RC-")){
        this.throwException("The release must be done on release");
    }

    def applicationVersion = getApplicationVersion()
    def existTag = existTag("${applicationVersion}")
    def tagStateComment = "Release"
    def tagName = applicationVersion

    if(existTag){
        if (forceRelease){
          this.printMessage("Forzando la promocion del nombre de etiquete del relesase: ${releaseBranchName}");
          deleteTagBranch(tagName)
          this.printMessage("Tag eliminado: ${releaseTagName}");
          this.printMessage("${tagStateComment}-FORCED");
      } else{
        this.throwException("The tag for this release exists: ${applicationVersion}");
      }
    }

    this.printMessage("Promocion nombre de entrega: ${releaseTagName}");
    def applicationName = getApplicationName()

    this.printMessage("Promocion artifact a reositorio de entrega: ${releaseTagName}");
    promoteRepository(applicationName, releaseTagName, ".npm")
    mergeOverwriteBranch(releaseTagName,"master")
    tagBranch(tagName,tagStateComment)
  }
}