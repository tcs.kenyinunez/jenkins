package sharedlib

import org.jenkinsci.plugins.docker.workflow.*
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*

class GradleAndroidJenkinsUtil extends BaseUtil implements Serializable, DevopsUtil {

  private static final String dockerAndroid23Gradle35Environment      = "bcp/docker-gradle-android-sdk:0.2"
  private static final String dockerAndroid23Gradle41Environment      = "bcp/docker-sdk-android23-gradle41:1.0"
  private static final String dockerAndroid27Gradle41Environment      = "bcp/docker-sdk-android27-gradle41:1.0"
  private static final String dockerAndroid27Gradle41NDKEnvironment   = "bcp/docker-sdk-android27-gradle41.ndk:1.0"
  private static final String dockerAndroid27Gradle44Node8Environment = "bcp/docker-sdk-android27-gradle44.node8:1.0"
  private static final String dockerAndroid27Gradle44NDKEnvironment   = "bcp/docker-sdk-android27-gradle44.ndk:1.0"
  private static final String dockerAndroid28Gradle41Environment      = "bcp/docker-sdk-android28-gradle41:1.0"
  private static final String dockerAndroid28Gradle4101NDKEnvironment = "bcp/docker-sdk-android28-gradle4101.ndk:1.1"
  private static final String dockerAndroid28Gradle41NDKEnvironment   = "bcp/docker-sdk-android28-gradle41.ndk:1.2"
  private static final String dockerAndroid28Gradle4101Environment    = "bcp/docker-sdk-android28-gradle4101:1.1"
  private static final String dockerAndroid28Gradle54Environment      = "bcp/docker-sdk-android28-gradle54:1.1"

  private static final String android23Gradle35Version      = "ANDROID_23_GRADLE35"
  private static final String android23Gradle41Version      = "ANDROID_23_GRADLE41"
  private static final String android27Gradle41Version      = "ANDROID_27_GRADLE41"
  private static final String android27Gradle41NDKVersion   = "ANDROID_27_GRADLE41NDK"
  private static final String android27Gradle44Node8Version = "ANDROID_27_GRADLE44_NODE8"
  private static final String android27Gradle44NDKVersion   = "ANDROID_27_GRADLE44NDK"
  private static final String android28Gradle41Version      = "ANDROID_28_GRADLE41"
  private static final String android28Gradle4101NDKVersion = "ANDROID_28_GRADLE4101NDK"
  private static final String android28Gradle41NDKVersion   = "ANDROID_28_GRADLE41NDK"
  private static final String android28Gradle4101Version    = "ANDROID_28_GRADLE4101"
  private static final String android28Gradle54Version      = "ANDROID_28_GRADLE54"

  private static final String dockerFortifyAndroid23Gradle35Environment      = "bcp/fortify-android23-gradle35:2.0"
  private static final String dockerFortifyAndroid23Gradle41Environment      = "bcp/fortify-android23-gradle41:2.0"
  private static final String dockerFortifyAndroid27Gradle41Environment      = "bcp/fortify-android27-gradle41:2.0"
  private static final String dockerFortifyAndroid28Gradle41Environment      = "bcp/fortify-android28-gradle41:2.0"
  private static final String dockerFortifyAndroid28Gradle4101Environment    = "bcp/fortify-android28-gradle4101:2.0"
  private static final String dockerFortifyAndroid27Gradle41NDKEnvironment   = "bcp/fortify-android27-gradle41-ndk:2.0"
  private static final String dockerFortifyAndroid27Gradle44NDKEnvironment   = "bcp/fortify-android27-gradle44-ndk:2.0"
  private static final String dockerFortifyAndroid27Gradle44Node8Environment = "bcp/fortify-android27-gradle44-node8:2.0"
  private static final String dockerAndroidEmulator711Environment            = "bcp/android-emulator-x86-7.1.1"
  private static final String dockerFortifyAndroid28Gradle54Environment      = "bcp/fortify-android28-gradle54:1.1"

  private static final String dockerAzureClientEnvironment = "bcp/azure-cli-bcp:1.1"

  private String dockerAndroidGradleEnvironment            = "${dockerAndroid27Gradle41NDKEnvironment}"
  private String androidGradleVersion                      = "${android27Gradle41NDKVersion}"
  private String dockerFortifyAndroidGradleEnvironment     = "${dockerFortifyAndroid27Gradle41NDKEnvironment}"
  private String dockerFortifyAndroidGradleNodeEnvironment = "${dockerFortifyAndroid27Gradle44Node8Environment}"
  private String dockerAndroidEmulatorEnvironment          = "${dockerAndroidEmulator711Environment}"

  private boolean sonarUseBranch             = false
  private boolean azureKeyVaultActivated     = false
  private String environment                 = ''
  private String assembleFlavor              = ''
  private boolean generateSigningFile        = false
  private boolean enabledDexGuardObfuscation = false
  private boolean artefactory                = true
  private boolean gradleSnapshotActivated = true
  private boolean gradleCertActivated = false
  private String mappingFilePath

  private GradleAndroidJenkinsUtil() {}

  public GradleAndroidJenkinsUtil(
    script,
    Docker docker,
    buildTimestamp,
    currentGitURL,
    currentCredentialsId,
    branchName,
    fortifyHost,
    currentGitCommit,
    String buildUser,
    String buildUserId
  ) {
    this.script = script
    this.docker = docker
    this.buildUser = buildUser
    this.buildUserId = buildUserId
    this.buildTimestamp = buildTimestamp
    this.currentGitURL = currentGitURL
    this.gitlabUrl = "${script.env.GITLAB_PROTOCOL}://${script.env.GITLAB_HOST}:${script.env.GITLAB_PORT}"
    this.bitbucketUrl = gitlabUrl
    this.currentCredentialsId = currentCredentialsId
    this.branchName = branchName
    this.artefactoryUrl = "${script.env.ARTIFACT_SERVER_URL}"
    this.artifactM2RegistryUrl = "${script.env.ARTIFACT_SERVER_URL}/public/"
    this.repositoryURL = this.artefactoryUrl
    this.fortifyHost = fortifyHost
    this.currentGitCommit = currentGitCommit
    this.fortifyUrl = "${script.env.FORTIFY_SCC_URL}"
  }

  public GradleAndroidJenkinsUtil(script, String type = ''){
    super(script, type)

    this.artifactM2RegistryUrl = "${script.env.ARTIFACT_SERVER_URL}/public/"
    this.repositoryURL = this.artefactoryUrl

    script.properties.each {
      println "$it.key -> $it.value"
    }
  }

  public void setMappingFilePath(String path){
    this.mappingFilePath = path
  }

  public void setAndroidGradleVersion(String gradleVersion){
    switch(gradleVersion){
      case android23Gradle35Version:
        dockerAndroidGradleEnvironment = dockerAndroid23Gradle35Environment
        dockerFortifyAndroidGradleEnvironment = dockerFortifyAndroid23Gradle35Environment
        break;
      case android23Gradle41Version:
        dockerAndroidGradleEnvironment = dockerAndroid23Gradle41Environment
        dockerFortifyAndroidGradleEnvironment = dockerFortifyAndroid23Gradle41Environment
        break;
      case android27Gradle41Version:
        dockerAndroidGradleEnvironment = dockerAndroid27Gradle41Environment
        dockerFortifyAndroidGradleEnvironment = dockerFortifyAndroid27Gradle41Environment
        break;
      case android27Gradle41NDKVersion:
        dockerAndroidGradleEnvironment = dockerAndroid27Gradle41NDKEnvironment
        dockerFortifyAndroidGradleEnvironment = dockerFortifyAndroid27Gradle41NDKEnvironment
        break;
      case android27Gradle44Node8Version:
        dockerAndroidGradleEnvironment = dockerAndroid27Gradle44Node8Environment
        dockerFortifyAndroidGradleEnvironment = dockerFortifyAndroid27Gradle44Node8Environment
        break;
      case android27Gradle44NDKVersion:
        dockerAndroidGradleEnvironment = dockerAndroid27Gradle44NDKEnvironment
        dockerFortifyAndroidGradleEnvironment = dockerFortifyAndroid27Gradle44NDKEnvironment
        break;
      case android28Gradle41Version:
        dockerAndroidGradleEnvironment = dockerAndroid28Gradle41Environment
        dockerFortifyAndroidGradleEnvironment = dockerFortifyAndroid28Gradle41Environment
        break;
      case android28Gradle41NDKVersion:
        dockerAndroidGradleEnvironment = dockerAndroid28Gradle41NDKEnvironment
        dockerFortifyAndroidGradleEnvironment = dockerFortifyAndroid28Gradle41Environment
        break;
      case android28Gradle4101Version:
        dockerAndroidGradleEnvironment = dockerAndroid28Gradle4101Environment
        dockerFortifyAndroidGradleEnvironment = dockerFortifyAndroid28Gradle4101Environment
        break;
      case android28Gradle4101NDKVersion:
        dockerAndroidGradleEnvironment = dockerAndroid28Gradle4101NDKEnvironment
        dockerFortifyAndroidGradleEnvironment = dockerFortifyAndroid28Gradle4101Environment
        break;
      case android28Gradle54Version:
        dockerAndroidGradleEnvironment        = dockerAndroid28Gradle54Environment
        dockerFortifyAndroidGradleEnvironment = dockerFortifyAndroid28Gradle54Environment
        break;
      default:
        this.throwException("Not supported version: ${gradleVersion}")
    }

    this.printMessage("Configurador Gradle para android: ${gradleVersion}")
    this.printMessage("Configurado entorno: ${dockerAndroidGradleEnvironment}")
    this.printMessage("Configurado entorno fortify: ${dockerFortifyAndroidGradleEnvironment}")
  }

  public String getApplicationName(){
    def result = this.script.steps.sh(
      script: "cat gradle.properties | grep ^description= | awk -F= '\$1==\"description\" {print \$2}'",
      returnStdout: true
    )
    return result.trim()
  }

  public String getApplicationVersion(){
    def result = this.script.steps.sh(
      script: "cat gradle.properties | grep ^version= | awk -F= '\$1==\"version\" {print \$2}'",
      returnStdout: true
    )
    return result.trim()
  }

  public void build(final String buildParams = 'assembleDebug') {
    String httpProxyHost  = "${script.env.DOCKER_HTTP_PROXY_HOST}"
    String httpProxyPort  = "${script.env.DOCKER_HTTP_PROXY_PORT}"

    String environmentParameters = getDockerEnvironmentParameters()
    String temporalVolumes = getDockerTemporalVolumes()

    script.steps.sh "mkdir -p ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}"
    script.steps.sh "mkdir -p ${script.env.WORKSPACE}/gradle-local-repo"
    String dockerVolume  = "${temporalVolumes} -v ${script.env.WORKSPACE}/gradle-local-repo:/gradle-home -v ${script.env.WORKSPACE}:/project/data -w /project/data "
      dockerVolume += "-v ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}:/gradle-project-cache"

    String gradleInitScript = getGradleInitScript()

    this.script.steps.writeFile(
      file:"${script.env.WORKSPACE}/gradle-jenkins",
      text: gradleInitScript
    )

    docker.withServer("${script.env.DOCKER_HOST}"){
      docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
        String dockerParameters = "--network=host --cpus 2 ${environmentParameters}"
        String dockerCmd = "docker run --rm ${dockerVolume} ${dockerParameters} ${this.dockerAndroidGradleEnvironment}"
        String defaultParameters = "--project-cache-dir /gradle-project-cache --stacktrace -Dorg.gradle.daemon=false --gradle-user-home=/gradle-home --init-script"

        if(httpProxyHost && httpProxyPort){
          defaultParameters+=" -Dhttp.proxyHost=${httpProxyHost} -Dhttp.proxyPort=${httpProxyPort} -Dhttps.proxyHost=${httpProxyHost} -Dhttps.proxyPort=${httpProxyPort}"
        }
        this.script.steps.sh "${dockerCmd} gradle ${buildParams} ${defaultParameters} ./gradle-jenkins"
      }
    }

    try {
      this.script.steps.step( [ $class: 'JacocoPublisher' ] )
    }
    catch(Exception e) {
      //TODO - To be removed on the new version of plugin 2.2.1 of jacoco, verson 2.2.0 had issues with gitlba plugin
    }
    try {
      this.script.steps.junit allowEmptyResults: true, testResults: '**/**/build/test-results/TEST-*.xml'
    }catch(Exception e){
      //TODO: Remove when QA are implemented
    }
  }

  private void saveMapping() {
    if (mappingFilePath) {
      try {
        printMessage("guardando mappingFilePath ${mappingFilePath}")
        this.script.steps.archiveArtifacts(artifacts: '**/' + mappingFilePath, allowEmptyArchive: true)
      } catch (Exception e) {
        printMessage(e.getMessage())
      }
    }
  }

  private String getDockerEnvironmentParameters() {
    String environmentParameters= this.script.steps.sh(
      script: """set +x
KEYS=
for key in \$(printenv| cut -d= -f1 | grep -v \"SHELL\\|HOME\\|HOSTNAME\\|TERM\\|PATH\\|PWD\\|USER\\|SSH_CONNECTION\\|SHLVL\\|LANG\\|HUDSON_SERVER_COOKIE\\|SSH_CLIENT\\|MAIL\\|WORKSPACE\\|LOGNAME\\|library.\\|LESSOPEN\")
do
   KEYS=\"\${KEYS} -e \${key} \"
done
echo \"\${KEYS}\" """,
      returnStdout: true
    ).trim()
    return environmentParameters
  }

  private String getDockerTemporalVolumes() {
    String tentativeDockerTemporalVolumes = this.script.steps.sh(
      script: """set +x
VOLUMES=
for VOLUME in \$(printenv | grep -v "SHELL\\|HOME\\|HOSTNAME\\|TERM\\|PATH\\|PWD\\|USER\\|SSH_CONNECTION\\|SHLVL\\|LANG\\|HUDSON_SERVER_COOKIE\\|SSH_CLIENT\\|MAIL\\|WORKSPACE\\|LOGNAME\\|library.\\|LESSOPEN" | cut -d= -f2)
do
   echo "\${VOLUME}"
done """,
      returnStdout: true
    ).trim()
    def tentativeDockerVolumes = tentativeDockerTemporalVolumes.split('\\n')
    String workspacePrefix = this.script.env.WORKSPACE
    def secretsFiles = tentativeDockerVolumes.findAll{it.startsWith(workspacePrefix)&&it.contains("secretFiles")}
    def candidateDockerVolumes = secretsFiles.collect{it.substring(0,it.lastIndexOf("/"))}
    String dockerTemporalVolumes = ""
    if(candidateDockerVolumes){
      candidateDockerVolumes.each {
        dockerTemporalVolumes = "${dockerTemporalVolumes} -v ${it}:${it}"
      }
    }
    return dockerTemporalVolumes
  }

  private String getGradleInitScript(){
    String extraGradleSourceRepository = ""

    if("${branchName}" != "master" && !"${branchName}".startsWith("tags/RC-")){
      if (!"${branchName}".startsWith("release/") && gradleSnapshotActivated){
        printMessage("DEV")
        extraGradleSourceRepository = """
        maven {
          url = '${script.env.ARTIFACT_SERVER_URL}/public.snapshot'
        }
      """
      } else if ("${branchName}".startsWith("release/") && gradleCertActivated){
        printMessage("CERT")
        extraGradleSourceRepository = """
        maven {
          url = '${script.env.ARTIFACT_SERVER_URL}/public.cert'
        }
      """
      }
    }

    String initScript = """apply plugin:EnterpriseRepositoryPlugin

	allprojects {
    repositories {
        maven {
            url = '${artifactM2RegistryUrl}'
        }
        ${extraGradleSourceRepository}
    }
    buildscript {
        repositories {
            maven {
                url = '${artifactM2RegistryUrl}'
            }
        }
    }
	}

	class EnterpriseRepositoryPlugin implements Plugin<Gradle> {

		private static String ENTERPRISE_REPOSITORY_URL = "${artifactM2RegistryUrl}"

		void apply(Gradle gradle) {
			// ONLY USE ENTERPRISE REPO FOR DEPENDENCIES
			gradle.allprojects{ project ->
        project.logger.lifecycle "Applying proxy gradle"
				project.buildscript.repositories {

					// Remove all repositories not pointing to the enterprise repository url
					all { ArtifactRepository repo ->

						if ((repo instanceof MavenArtifactRepository) &&
							repo.url.toString() != ENTERPRISE_REPOSITORY_URL) {

                            if(!repo.url.toString().startsWith("file:/opt/maven-local-repo/")&&
                                 !repo.url.toString().startsWith("file:/project/")&&
                                 !repo.url.toString().startsWith("file:/opt/android-sdk-linux/")&&
                                 !repo.url.toString().startsWith("${script.env.ARTIFACT_SERVER_URL}")&&
                                 !repo.url.toString().startsWith("${artefactoryUrl}")
                                 ){
    							  project.logger.lifecycle "Repository \${repo.url} removed. Only \$ENTERPRISE_REPOSITORY_URL is allowed"
    							  remove repo
                            }

						}
					}


					// add the enterprise repository
					maven {
						name "STANDARD_ENTERPRISE_REPO"
						url ENTERPRISE_REPOSITORY_URL
					}
				}
				project.repositories {

					// Remove all repositories not pointing to the enterprise repository url
					all { ArtifactRepository repo ->
						if ((repo instanceof MavenArtifactRepository) &&
							repo.url.toString() != ENTERPRISE_REPOSITORY_URL) {

                            if(!repo.url.toString().startsWith("file:/opt/maven-local-repo/")&&
                                 !repo.url.toString().startsWith("file:/project/")&&
                                 !repo.url.toString().startsWith("file:/opt/android-sdk-linux/")&&
                                 !repo.url.toString().startsWith("${script.env.ARTIFACT_SERVER_URL}")&&
                                 !repo.url.toString().startsWith("${artefactoryUrl}")
                                 ){
    							  project.logger.lifecycle "Repository \${repo.url} removed. Only \$ENTERPRISE_REPOSITORY_URL is allowed"
    							  remove repo
                            }

						}
					}


					// add the enterprise repository
					maven {
						name "STANDARD_ENTERPRISE_REPO"
						url ENTERPRISE_REPOSITORY_URL
					}
				}
			}
		}
	}
	""".stripIndent()

    return initScript
  }

  public void executeQA(final String releaseName="", final String encoding="UTF-8"){
    def httpProxyHost = "${script.env.DOCKER_HTTP_PROXY_HOST}"
    def httpProxyPort = "${script.env.DOCKER_HTTP_PROXY_PORT}"
    String projectName = "${script.env.project}".toLowerCase()
    String groupIdName = getGroupId()
    String applicationName = getApplicationName()
    String applicationVersion = getApplicationVersion()
    String projectKey = "${projectName}-${groupIdName}:${applicationName}"
    String extraParams = ""

    if(this.sonarUseBranch){
      extraParams = "-Dsonar.branch=${this.branchName}"
    }
    def sonarProjectVersion="${applicationVersion}-${this.branchName}"
    if(releaseName!=""){
      sonarProjectVersion="${applicationVersion}-${releaseName}"
    }
    def traceabilityParams=" -Dsonar.projectVersion=\"${sonarProjectVersion}\" -Dsonar.analysis.version=\"${applicationVersion}\" \
-Dsonar.analysis.userId=\"${buildUserId}\" -Dsonar.analysis.commitId=\"${currentGitCommit}\" -Dsonar.analysis.tek=\"gradle-android-java\" \
-Dsonar.analysis.projectName=\"${projectName}\" -Dsonar.analysis.buildTimestamp=\"${buildTimestamp}\" -Dsonar.analysis.branch=\"${branchName}\" \
-Dsonar.links.scm=\"${currentGitURL}\" -Dsonar.analysis.jobname=\"${script.env.JOB_NAME}\" "
    if(releaseName!=""){
      traceabilityParams+=" -Dsonar.analysis.tagName=\"${releaseName}\" "
    }

    this.script.steps.sh "mkdir -p ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}"
    this.script.steps.sh "mkdir -p ${script.env.WORKSPACE}/gradle-local-repo"
    def dockerVolume = "-v ${script.env.APPLICATION_CACHE}/sonar-cache:/opt/android-sdk-linux/.sonar -v ${script.env.WORKSPACE}/gradle-local-repo:/gradle-home -v ${script.env.WORKSPACE}:/project/data -w /project/data"
      dockerVolume += " -v ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}:/gradle-project-cache"
    this.printMessage("dockerVolume: ${dockerVolume}")
    this.script.steps.sh "echo '' > ${script.env.WORKSPACE}/gradle-jenkins"

    this.script.steps.writeFile(
      file:"${script.env.WORKSPACE}/gradle-jenkins",
      text: getGradleInitScript()
    )

    docker.withServer("${script.env.DOCKER_HOST}"){
      docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
        String dockerParameters = "--network=host --cpus 2"
        String dockerCmd = "docker run --rm ${dockerVolume} ${dockerParameters} ${dockerAndroidGradleEnvironment}"
        this.script.steps.withSonarQubeEnv {
          String sonarParams = "--project-cache-dir /gradle-project-cache ${extraParams} -Dsonar.sourceEncoding=${encoding} -Dsonar.projectName=${projectKey} -Dsonar.projectKey=${projectKey} -Dsonar.login=${script.env.SONAR_AUTH_TOKEN} -Dsonar.password= -Dsonar.host.url=${script.env.SONAR_HOST_URL} -Dsonar.links.ci=${script.env.BUILD_URL} ${traceabilityParams} --stacktrace -Dorg.gradle.daemon=false --gradle-user-home=/gradle-home --init-script ./gradle-jenkins -Dsonar.junit.reportsPath=build/test-results"

          if(httpProxyHost && httpProxyPort){
            sonarParams += " -Dhttp.proxyHost=${httpProxyHost} -Dhttp.proxyPort=${httpProxyPort} -Dhttps.proxyHost=${httpProxyHost} -Dhttps.proxyPort=${httpProxyPort}";
          }
          this.script.steps.sh "${dockerCmd} gradle sonarqube ${sonarParams}"
        }
      }
    }
  }

  private String getGroupId(){
    def result = this.script.steps.sh(
      script: "cat gradle.properties | grep ^group= | awk -F= '\$1==\"group\" {print \$2}'",
      returnStdout: true
    )
    return result.trim()
  }

  public void setGradleSnapshotEnabled(boolean gradleSnapshotParam){
    this.gradleSnapshotActivated = gradleSnapshotParam
  }

  public void setGradleCertEnabled(boolean gradleCertParam){
    this.gradleCertActivated = gradleCertParam
  }

  public void executeSast(final String buildParams){
    if (fortifyActivated == false) {
      this.printMessage("Fortify esta deshabilitado")
      return
    }
    String fortifyVersion = getGitProjectName()
    String projectLowerCase = "${script.env.project}".toLowerCase()
    String fortifyCloudScanCredId = "${projectLowerCase}-jenkins-fortify-cloudscan-token"
    String fortifyAuthCredId = "${projectLowerCase}-jenkins-fortify-ssc-token"
    String buildID = "${script.env.project}-${fortifyVersion}-${script.env.BUILD_NUMBER}"

    this.script.steps.withCredentials([
      [$class: 'StringBinding', credentialsId: "${fortifyCloudScanCredId}", variable: 'cloudScanToken' ],
      [$class: 'StringBinding', credentialsId: "${fortifyAuthCredId}", variable: 'authToken' ]
    ]){
      String gradleInitScript = getGradleInitScript()

      this.script.steps.writeFile(
        file:"${script.env.WORKSPACE}/gradle-jenkins",
        text: gradleInitScript
      )

      try{
        //Docker execute
        this.script.steps.sh """
          set -x
          if [ ! -d ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER} ]; then
            mkdir -p ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}
          fi
          docker run -d -it --cpus ${execFortifyCpu} --network=host -v ${script.env.APPLICATION_CACHE}/gradle-local-repo:/gradle-home -v '${script.env.WORKSPACE}:/project/data' -w /project/data -v '${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}:/gradle-project-cache' ${dockerFortifyAndroidGradleEnvironment} bash > .dockerId
          docker exec `cat .dockerId` sourceanalyzer -b "${buildID}" gradle ${buildParams} --project-cache-dir /gradle-project-cache --stacktrace -Dorg.gradle.daemon=false --gradle-user-home=/gradle-home --init-script ./gradle-jenkins -PdisableUseProguard=true
          docker exec `cat .dockerId` cloudscan -sscurl ${fortifyUrl} -ssctoken ${script.env.cloudScanToken} start -block -log android_1.log -upload --application "${script.env.project}" --application-version "${fortifyVersion}" -b "${buildID}" -uptoken ${script.env.cloudScanToken} -scan ${execFortifyMemory}
          docker exec `cat .dockerId` fortifyclient downloadFPR -file result.fpr -url ${fortifyHost} -authtoken ${script.env.authtoken} -project "${script.env.project}" -version "${fortifyVersion}"
          docker exec `cat .dockerId` BIRTReportGenerator -template 'OWASP Top 10' -source result.fpr -format HTML --Version 'OWASP Top 10 2017' -output MyOWASP_Top10_Report.html
          docker exec `cat .dockerId` fortifyclient -url ${fortifyHost} -authtoken ${script.env.authtoken} listApplicationVersions > .listProject.txt
        """
      }catch(Exception e){
        this.script.steps.sh """
                set +e
                docker exec `cat .dockerId` cat /opt/android-sdk-linux/.fortify/cloudscan/log/cloudscan.log
        """
        throw e
      }finally{
        this.script.steps.sh """
          docker rm -f `cat .dockerId`
        """
      }

      generateReportFortify()
    }
  }

  protected void generateReportFortify() {
    this.script.steps.sh """
      set -x
      
      #Get High issues
      cat MyOWASP_Top10_Report.html | grep -A 4 -m 10 ">High<" | grep '<div class.*</div>' | sed 's%</div>%%g' | sed 's/<div class="style_92" style=" overflow:hidden;">//g' | awk '{\$1=\$1;print}' > .tmp
      total=0
      while IFS=' ' read -r valor
      do
        echo \$valor
        total=\$((\$total + \$valor))
      done < .tmp
      echo \$total > CANTIDAD_HIGH
      #remove temporal file
      rm -f .tmp
      
      #Get Critical issues
      cat MyOWASP_Top10_Report.html | grep -A 4 -m 10 ">Critical<" | grep '<div class.*</div>' | sed 's%</div>%%g' | sed 's/<div class="style_92" style=" overflow:hidden;">//g' | awk '{\$1=\$1;print}' > .tmp
      total=0
      while IFS=' ' read -r valor
      do
        echo \$valor
        total=\$((\$total + \$valor))
      done < .tmp
      echo \$total > CANTIDAD_CRITICAL
      #remove temporal file
      rm -f .tmp
    """
    def fortifyVersion = getGitProjectName()

    this.script.steps.sh """
      set -x
          if [ -f .version.txt ]; then
             rm .version.txt
          fi
          if [ -f .matchproject ]; then
             rm .matchproject
          fi

      cat .listProject.txt | while IFS=' ' read -r p1
         do
            echo \$p1 | awk '{print \$3}' > .repositories
            repos=`cat .repositories`
            echo \$repos
            if [ "${fortifyVersion}" == "\$repos" ]; then
              echo \$p1 >> .matchproject
            fi
         done
      cat .matchproject | grep ${script.env.project} | grep ${fortifyVersion} | awk '{print \$1}' > .version.txt
    """

    def idVerFortify = (this.script.steps.readFile('.version.txt').trim()).toInteger()
    def CANTIDAD_HIGH = (this.script.steps.readFile('CANTIDAD_HIGH').trim()).toInteger()
    def CANTIDAD_CRITICAL = (this.script.steps.readFile('CANTIDAD_CRITICAL').trim()).toInteger()

    this.printMessage("Analisis SAST Fortify")
    this.printMessage("Vulnerabilidades HIGH : ${CANTIDAD_HIGH}")
    this.printMessage("Vulnerabildiades CRITICAL : ${CANTIDAD_CRITICAL}")
    this.printMessage("URL : ${fortifyUrl}/html/ssc/version/${idVerFortify}/fix")

    this.script.steps.archiveArtifacts(artifacts: 'MyOWASP_Top10_Report.html')

    //Validar si contiene vulnerabilidades bloqueantes
    if (CANTIDAD_HIGH==0 && CANTIDAD_CRITICAL==0){
      this.printMessage("No posee vulnerabiblidades de criticidad Alta o Critica del OWASP TOP 10 - 2017")
    } else {
      this.script.steps.error 'Security validation failed when running tests on Fortify. To pass the validation your application must be free of Criticality Vulnerabilities High and/or Critical'
    }
  }

  //def deployArtifactWithGradleAndroid(String enviroment = '',String tagName = '',String assemblyFlavor='',boolean generateSigningFile=false,boolean enabledDexGuardOfuscation=false) {
  public void uploadArtifact(final String releaseName){
    String httpProxyHost = "${script.env.DOCKER_HTTP_PROXY_HOST}"
    String httpProxyPort = "${script.env.DOCKER_HTTP_PROXY_PORT}"
    String buildUniqueId

    String environmentParameters = getDockerEnvironmentParameters()
    String temporalVolumes = getDockerTemporalVolumes()

    buildUniqueId="${buildTimestamp}"
    if(releaseName != null && releaseName!=''){
      //Clean up to prevent unclean data
      this.script.steps.step([$class: 'WsCleanup'])
      buildUniqueId = this.script.steps.sh(script: "echo ${releaseName} | cut -d '/' -f2 ", returnStdout: true).trim()
      checkoutBranch(releaseName)
    }
    //Clean up to prevent unclean data
    def repository
    environment=environment.toUpperCase()
    def repoKey
    String repositoryPrefix=""

    if(this.assembleFlavor != ""){
      /* REMOVED 25/10/2018 - To support parameters
      if(assemblyFlavor.indexOf(" ")){
        throw new Exception("The assembly flavor should not cointains spaces.")
      }
      */
      if(!this.assembleFlavor.startsWith("assemble")){
        this.throwException("The assembly flavor should contains assemble.")
      }
      if(!this.assembleFlavor.contains("Release")){
        this.throwException("The assembly flavor should be release.")
      }
    }
    if(!this.artefactory){
      repositoryPrefix="/content/repositories"
    }

    repoKey="${script.env.project}.Snapshot"
    repository="${repositoryURL}${repositoryPrefix}/${repoKey}"
    if ("${this.branchName}"=="master"){
      repoKey="${script.env.project}.Release"
      repository="${repositoryURL}${repositoryPrefix}/${repoKey}"
    }
    //If the application is set for promotion to production
    if(generateSigningFile){
      if(environment==''){
        if ("${branchName}".startsWith("tags/RC-")){
          this.printMessage("PROD")
          generateSigningProperties("PROD")
        }
      }else{
        generateSigningProperties(environment)
      }
    }
    if(environment==''){
      if ("${branchName}".startsWith("tags/RC-")){
        repoKey="${script.env.project}.Release"
        repository="${repositoryURL}${repositoryPrefix}/${repoKey}"
        buildUniqueId=buildUniqueId+"-PROMOTED"
      }
    }
    if(environment){
      repoKey="${script.env.project}-${environment}"
      repository="${repositoryURL}${repositoryPrefix}/${repoKey}"
    }
    this.printMessage("Repository Key: ${repoKey}")
    this.printMessage("Repository: ${repository}")

    String projectName = "${script.env.project}".toLowerCase()
    String groupIdName = getGroupId()
    String applicationName = getApplicationName()
    String applicationVersion = getApplicationVersion()
    String projectKey = "${projectName}-${applicationName}"

    this.script.steps.sh "mkdir -p ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}"
    this.script.steps.sh "mkdir -p ${script.env.WORKSPACE}/gradle-local-repo"
    def dockerVolume = "${temporalVolumes} -v ${script.env.WORKSPACE}/gradle-local-repo:/gradle-home -v ${script.env.WORKSPACE}:/project/data -w /project/data"
      dockerVolume += " -v ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}:/gradle-project-cache"
    this.printMessage("dockerVolume: ${dockerVolume}")
    this.script.steps.sh "echo '' > ${script.env.WORKSPACE}/gradle-jenkins"

    this.script.steps.writeFile(
      file:"${script.env.WORKSPACE}/gradle-jenkins",
      text: getGradleInitScript()
    )

    String projectNameLowercase = projectName

    String gradleProxyParams = ""
    if(httpProxyHost && httpProxyPort){
      gradleProxyParams+=" -Dhttp.proxyHost=${httpProxyHost} -Dhttp.proxyPort=${httpProxyPort} -Dhttps.proxyHost=${httpProxyHost} -Dhttps.proxyPort=${httpProxyPort}";
    }

    this.printMessage("Enabled dexguard: ${enabledDexGuardObfuscation}")
    if(enabledDexGuardObfuscation){
      executeDexGuardObfuscation(applicationName)
    }

    String dexGuardConfigFile = "dexguard-license.txt"

    try {
      docker.withServer("${script.env.DOCKER_HOST}"){
        docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
          this.script.steps.withCredentials([
            [
              $class: 'UsernamePasswordMultiBinding',
              credentialsId: projectNameLowercase+'-jenkins-artefactory-token',
              usernameVariable: 'ARTIFACT_USER_NAME',
              passwordVariable: 'ARTIFACT_USER_PASSWORD'
            ]
          ]) {
            String dockerEnvironmentParameters="${environmentParameters} -e BUILD_USER_ID=${buildUserId} -e BUILD_GIT_URL=${currentGitURL} -e BUILD_URL=${script.env.BUILD_URL} -e BUILD_NAME=${projectKey} -e BUILD_INFO_ENVVARSEXCLUDEPATTERNS=*password*,*secret*,*PASSWORD*,*SECRET* -e ARTIFACT_USER_NAME=${script.env.ARTIFACT_USER_NAME} -e ARTIFACT_USER_PASSWORD=${script.env.ARTIFACT_USER_PASSWORD} -e PROJECT_NAME=${script.env.project} -e ARTIFACT_SERVER_URL=${repositoryURL} -e REPOKEY=${repoKey} -e BUILD_UNIQUE_ID=${buildUniqueId}"
            String dockerParameters="--network=host --cpus 2"
            String dockerCmd="docker run --rm ${dockerVolume} ${dockerEnvironmentParameters} ${dockerParameters} ${dockerAndroidGradleEnvironment}"
            this.script.steps.sh "${dockerCmd} gradle ${assembleFlavor} --project-cache-dir /gradle-project-cache --gradle-user-home=/gradle-home ${gradleProxyParams} --stacktrace -Dorg.gradle.daemon=false --init-script ./gradle-jenkins"
            this.script.steps.sh "${dockerCmd} gradle artifactoryPublish --project-cache-dir /gradle-project-cache --gradle-user-home=/gradle-home ${gradleProxyParams} --stacktrace -Dorg.gradle.daemon=false --init-script ./gradle-jenkins"
          }
        }
      }
    } catch(Exception e) {
      this.printMessage("echo \"${e}\"")
      throw e
    } finally {
      this.script.steps.sh """
          set +x
          rm -rf ${script.env.WORKSPACE}/${dexGuardConfigFile}
        """
    }
    if(generateSigningFile){
      this.script.steps.sh "rm -f jks-${buildTimestamp}"
    }
    saveMapping()
  }

  private void generateSigningProperties(String environmentParam){
    String currentDate = buildTimestamp
    String projectName="${script.env.project}".toLowerCase()
    String appName=getApplicationName()
    String environment=environmentParam.toLowerCase()
    String storeFileIdName="${projectName}-${appName}-android-trustore-${environment}"
    String storePasswordIdName="${projectName}-${appName}-android-storepassword-${environment}"
    String keyPasswordIdName="${projectName}-${appName}-android-keypassword-${environment}"
    String keyAliasIdName="${projectName}-${appName}-android-keyalias-${environment}"

    this.printMessage("Generating singing properties for APK - BEGIN")
    this.printMessage("NOTE: - The credentials should be populate on project folder credentials")
    this.printMessage("Keystore credentials Id(SecretFile): ${storeFileIdName}")
    this.printMessage("Keystore password credentials Id(SecretText): ${storePasswordIdName}")
    this.printMessage("Key Alias credentials Id(SecretText): ${keyAliasIdName}")
    this.printMessage("Key Password Credentials Id(SecretText): ${keyPasswordIdName}")

    if(!azureKeyVaultActivated){
      //STANDARD CREDENTIALS PLUGIN
      this.script.steps.withCredentials([
        [$class: "FileBinding", credentialsId: "${storeFileIdName}", variable: "storeFilePath"],
        [$class: "StringBinding", credentialsId: "${storePasswordIdName}", variable: "storePasswordValue"],
        [$class: "StringBinding", credentialsId: "${keyAliasIdName}", variable: "keyAliasValue"],
        [$class: "StringBinding", credentialsId: "${keyPasswordIdName}", variable: "keyPasswordValue"]
      ]){
        this.script.steps.sh """set +x
	      cp ${script.env.storeFilePath} ${script.env.WORKSPACE}/jks-${currentDate}
	      echo 'STORE_FILE=/project/data/jks-${currentDate}' > ${script.env.WORKSPACE}/signing.properties
	      echo 'STORE_PASSWORD=${script.env.storePasswordValue}' >> ${script.env.WORKSPACE}/signing.properties
	      echo 'KEY_ALIAS=${script.env.keyAliasValue}' >> ${script.env.WORKSPACE}/signing.properties
	      echo 'KEY_PASSWORD=${script.env.keyPasswordValue}' >> ${script.env.WORKSPACE}/signing.properties"""
      }
    }else{
      String vaultIdName = "${projectName}-vault-name"
      this.printMessage("Vault Name credentials Id: ${vaultIdName}")
      withAzureVaultCredentials([
        [azureCredentialVariable:"storeFilePath",azureCredentialId:"${storeFileIdName}",azureCredentialType:"file"],
        [azureCredentialVariable:"storePasswordValue",azureCredentialId:"${storePasswordIdName}"],
        [azureCredentialVariable:"keyAliasValue",azureCredentialId:"${keyAliasIdName}"],
        [azureCredentialVariable:"keyPasswordValue",azureCredentialId:"${keyPasswordIdName}"]]
      ){
        this.script.steps.sh """set +x
	      cp ${script.env.storeFilePath} ${script.env.WORKSPACE}/jks-${currentDate}
	      echo 'STORE_FILE=/project/data/jks-${currentDate}' > ${script.env.WORKSPACE}/signing.properties
	      echo 'STORE_PASSWORD=${script.env.storePasswordValue}' >> ${script.env.WORKSPACE}/signing.properties
	      echo 'KEY_ALIAS=${script.env.keyAliasValue}' >> ${script.env.WORKSPACE}/signing.properties
	      echo 'KEY_PASSWORD=${script.env.keyPasswordValue}' >> ${script.env.WORKSPACE}/signing.properties"""
      }
    }
    this.printMessage("Generating singing properties for APK - END")
  }

  public void executeDexGuardObfuscation(String appName) {
    String dexGuardConfigFile = "dexguard-license.txt"
    String projectName          = "${script.env.project}".toLowerCase()
    String dexGuardApplicationId = "dexguard-${projectName}-${appName}-license"

    this.printMessage("Configuring License file")
    this.printMessage("DexGuard License File: ${dexGuardApplicationId}")
    this.script.steps.withCredentials([
      [$class: "FileBinding", credentialsId: "${dexGuardApplicationId}", variable: "dexGuardApplicationId" ]
    ]) {
      this.script.steps.sh """set +x
            cp -f \"${script.env.dexGuardApplicationId}\" ${script.env.WORKSPACE}/${dexGuardConfigFile}
            """
    }
  }

  //def startReleaseAndroidGradle(String assembleFlavor = 'assembleRelease',boolean enabledDexGuardOfuscation=false,boolean generateSigningFile=true){
  public void startRelease(final String buildParams){
    if ("${branchName}"=="master"){
      this.throwException("Generating of release must not be done on master")
    }
    this.printMessage("Enabled dexguard: ${enabledDexGuardObfuscation}")
    //Check generate new version
    String applicationVersion = getApplicationVersion()
    this.printMessage("Application version: ${applicationVersion}")
    String nextReleaseVersion=getApplicationNextReleaseVersion(applicationVersion)
    this.printMessage("Next release version: ${nextReleaseVersion}")

    String releaseBranchName = "release/"+nextReleaseVersion
    boolean existBranchInRepository = existBranch(releaseBranchName)
    this.printMessage("Exist release branch: ${existBranchInRepository}")
    if(existBranchInRepository){
      //If the branch exist an error is throw
      this.printMessage("Deleting release branch: ${releaseBranchName}")
      deleteNotMergeReleaseBranch(releaseBranchName)
    }

    createNewReleaseBranch(nextReleaseVersion)
    checkoutReleaseBranch(nextReleaseVersion)
    updateToReleaseVersionGradle(nextReleaseVersion)

    String tagStateComment = "Release Candicate"
    String currentDate = buildTimestamp
    String environmentParam ="CERT"
    applicationVersion = getApplicationVersion()
    String tagName = "RC-"+applicationVersion+"-${environmentParam}-${currentDate}"

    tagName = updateTagNameWithACR(tagName)

    protectBranch("release/${nextReleaseVersion}")

    //Validate SAST Analisys
    executeSast(assembleFlavor)
    try{
      this.sonarUseBranch = false
      executeQA(tagName, "UTF-8")
    }catch(Exception e){
      this.printMessage("WARNING: Error executing Sonar!")
    }
    tagBranch(tagName,tagStateComment,environmentParam)
    this.environment = environmentParam
    uploadArtifact("tags/${tagName}")

    executeXraySCA(tagName)
  }

  private String getApplicationNextReleaseVersion(String version){
    String isSnapshotVersion=isSnapshotVersion(version)
    this.printMessage("Snapshot version: ${isSnapshotVersion}")
    String snapshotPattern="-SNAPSHOT"
    String newWorkingVersion
    if(isSnapshotVersion=="true"){
      this.printMessage("Version is snapshot")
      newWorkingVersion=this.script.steps.sh script: """currentVersion=${version}
        echo \${currentVersion%${snapshotPattern}}""", returnStdout: true
      newWorkingVersion=newWorkingVersion.trim()
      this.printMessage("New working version: ${newWorkingVersion}")
      return newWorkingVersion
    }

    this.throwException("Version is not snapshot. The release version should be done on snapshot version.")
  }

  private String isSnapshotVersion(String version){
    String snapshotPattern = "-- -SNAPSHOT"
    def command="echo \"${version}\" | grep ${snapshotPattern}"
    def statusCode =  this.script.steps.sh script:"${command}", returnStatus:true
    if(statusCode==1){
      return "false"
    }
    def containsSnapshot = this.script.steps.sh script: "${command}", returnStdout: true
    containsSnapshot=containsSnapshot.trim()
    this.printMessage("containsSnapshot: ${containsSnapshot}")

    def isSnapshot = this.script.steps.sh script: "[ \${#containsSnapshot} -ge 0 ] && echo 'true' || echo 'false'", returnStdout: true
    isSnapshot=isSnapshot.trim()
    return isSnapshot
  }

  public void updateToReleaseVersionGradle(String version){
    this.script.steps.sh (script: """
    sed -i -e "s;^version=.*;version=${version};g" gradle.properties
    """, returnStdout: true)
    updateToReleaseVersion(version)
  }

  //def promoteReleaseAndroidGradle(String releaseTagName,String assembleFlavor="",boolean enabledDexGuardOfuscation=false,boolean enabledSigning=true){
  public void promoteRelease(final String releaseTagName, final boolean forceRelease = false){
    if(!"${branchName}".startsWith("tags/RC-")){
      throw new Exception("The release must be done on release")
    }

    def applicationVersion = getApplicationVersion()
    def existTag = existTag("${applicationVersion}")
    def tagStateComment = "Release"
    def tagName = getApplicationVersion()
    def applicationName = getApplicationName()

    if(existTag){
      if (!forceRelease){
        this.throwException("The tag for this release exists: ${applicationVersion}")
      }
      this.printMessage("Forcing removal and promote of release tag name: ${releaseTagName}")
      this.deleteTagBranch(tagName)
      this.printMessage("Tag removed: ${releaseTagName}")
      this.printMessage("${tagStateComment}-FORCED")
    }

    this.printMessage("Promote release tag name: ${releaseTagName}")

    if(assembleFlavor!=null&&assembleFlavor!=""){
      this.printMessage("Deploy artifact to Release Repository: ${releaseTagName}")
      uploadArtifact(branchName)
    }else{
      this.printMessage("Promote artifact to Release Repository: ${releaseTagName}")
      this.throwException("Not supported")
      //TODO: FIX THIS METHOD
      //promoteRepository(applicationName, releaseTagName)
    }

    mergeOverwriteBranch(releaseTagName,"master")
    tagBranch(tagName,tagStateComment)
  }

  private void saveProguard() {
    this.script.steps.sh(script: """
     set +e
     tar -cvzf ${script.env.WORKSPACE}/proguard-${buildTimestamp}.tgz */build/outputs/mapping/*/release/*.txt
     if [ ! -f "${script.env.WORKSPACE}/proguard-${buildTimestamp}.tgz" ]
     then
     	tar -cvzf ${script.env.WORKSPACE}/proguard-${buildTimestamp}.tgz */build/outputs/mapping/release/*.txt
     fi
     """, returnStdout: true).trim()
    this.script.steps.archiveArtifacts(artifacts:'proguard-*.tgz',allowEmptyArchive:true)
  }

  public void saveResult(final String extension) {
    this.script.steps.archiveArtifacts(artifacts:'**/build/**/*.'+extension,allowEmptyArchive:true)
  }

  public void executeCucumber(String relativeApkFolderPath,String deviceName="Samsung Galaxy S6") {
    this.script.steps.node("android-emulator-slave"){
      //checkoutBranch(currentGitCommit)
      this.script.steps.sh "cp -R /opt/jenkins/bancamovil_android-auto/. ."

      def projectName="${script.env.project}".toLowerCase()
      def groupIdName=getGroupId()
      def applicationName=getApplicationName()
      def applicationVersion=getApplicationVersion()

      this.printMessage("projectName: ${projectName}")
      this.printMessage("groupIdName: ${groupIdName}")
      this.printMessage("applicationName: ${applicationName}")
      this.printMessage("applicationVersion: ${applicationVersion}")

      this.script.steps.sh "mkdir -p ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}"
      this.script.steps.sh "mkdir -p ${script.env.WORKSPACE}/gradle-local-repo"
      def dockerVolume = "${script.env.WORKSPACE}/gradle-local-repo:/gradle-home -v ${script.env.WORKSPACE}:/project/data -w /project/data"
        dockerVolume += " -v ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}:/gradle-project-cache"

      this.printMessage("dockerVolumen: ${dockerVolume}")
      this.script.steps.sh "echo '' > ${script.env.WORKSPACE}/gradle-jenkins"

      this.script.steps.writeFile(
        file:"${script.env.WORKSPACE}/gradle-jenkins",
        text: getGradleInitScript()
      )

      def hostIp = this.script.steps.sh(
        script: "echo \"\$(ip route show | grep default| awk '{ print \$3 }' | head -n 1)\"",
        returnStdout: true
      ).trim()
      def containerDockerHost = this.script.steps.sh script: "echo \"${hostIp}:2375\"", returnStdout: true
      def androidPlatformVersion="7.1.1"
      def containerId=createAndroidEmulatorInstance(relativeApkFolderPath,deviceName)
      try{
        this.printMessage("containerId: ${containerId}")

        docker.withServer("${containerDockerHost}"){
          //Get ports android emulator
          def dockerNoVNCPort = this.script.steps.sh(script: "docker inspect --format '{{ (index (index .NetworkSettings.Ports \"6080/tcp\") 0).HostPort }}' ${containerId}", returnStdout: true).trim()
          def dockerEmulatorDebugPort = this.script.steps.sh(script: "docker inspect --format '{{ (index (index .NetworkSettings.Ports \"5554/tcp\") 0).HostPort }}' ${containerId}", returnStdout: true).trim()
          def dockerAdbPort = this.steps.sh(script: "docker inspect --format '{{ (index (index .NetworkSettings.Ports \"5555/tcp\") 0).HostPort }}' ${containerId}", returnStdout: true).trim()
          def dockerAppiumPort = this.steps.sh(script: "docker inspect --format '{{ (index (index .NetworkSettings.Ports \"4723/tcp\") 0).HostPort }}' ${containerId}", returnStdout: true).trim()

          def dockerAndroidDevice="emulator-5554"
          def dockerAppiumHost="http://${hostIp}:${dockerAppiumPort}"

          this.printMessage("dockerAndroidDevice: ${dockerAndroidDevice}")
          this.printMessage("dockerAppiumHost: ${dockerAppiumHost}")
          this.printMessage("androidPlatformVersion: ${androidPlatformVersion}")

          docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
            def dockerParameters="--network=host --cpus 2"
            def dockerCmd="docker run --rm ${dockerVolume} ${dockerParameters} ${dockerAndroidGradleEnvironment}"
            try {
              this.script.steps.sh "echo \'ANDROID_PLATFORM_VERSION=${androidPlatformVersion}\' > ${script.env.WORKSPACE}/android-settings.properties"
              this.script.steps.sh "echo \'ANDROID_DEVICE=${dockerAndroidDevice}\' >> ${script.env.WORKSPACE}/android-settings.properties"
              this.script.steps.sh "echo \'APPIUM_HOST=${dockerAppiumHost}\' >> ${script.env.WORKSPACE}/android-settings.properties"
              this.script.steps.sh "cat ${script.env.WORKSPACE}/android-settings.properties"
              this.script.steps.sh "${dockerCmd} gradle cucumber -x assemble --project-cache-dir /gradle-project-cache --stacktrace -Dorg.gradle.daemon=false --gradle-user-home=/gradle-home --init-script ./gradle-jenkins"
              this.script.steps.cucumber '**/target/cucumber.json'
            }catch(Exception exception){
              this.script.steps.cucumber '**/target/cucumber.json'
              throw exception
            }
            def dockerRemoveStatus = this.script.steps.sh(script: "docker rm -f ${containerId}", returnStatus: true)
            this.printMessage("Remove container ${containerId} status: ${dockerRemoveStatus}")
          }
        }
      }catch(Exception exception){
        docker.withServer("${containerDockerHost}"){
          def dockerRemoveStatus = this.script.steps.sh(script: "docker rm -f ${containerId}", returnStatus: true)
          this.printMessage("Remove container ${containerId} status: ${dockerRemoveStatus}")
        }
        throw exception
      }

    }
  }

  def createAndroidEmulatorInstance(String relativeApkFolderPath,String deviceName="Samsung Galaxy S6") {
    def apkFolderPath="${script.env.WORKSPACE}:/project/data"
    def dockerVolumen="-v ${apkFolderPath}"

    this.printMessage("dockerVolumen: ${dockerVolumen}")
    this.script.steps.sh "echo '' > ${script.env.WORKSPACE}/gradle-jenkins"

    this.script.steps.writeFile(
      file:"${script.env.WORKSPACE}/gradle-jenkins",
      text: getGradleInitScript()
    )

    def containerId=""
    def containerDockerHost = this.script.steps.sh script: "echo \"tcp://\$(ip route show | grep default| awk '{ print \$3 }' | head -n 1):2375\"", returnStdout: true
    docker.withServer("${containerDockerHost}"){
      docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
        def dockerParameters="-e SAVE_BOOT_SNAPSHOT=False -e APPIUM=True -e DEVICE=\"${deviceName}\" -P"
        def dockerCmd="docker run --cpus 2 --privileged -d ${dockerVolumen} ${dockerParameters} ${dockerAndroidEmulatorEnvironment}"
        containerId=this.script.steps.sh (
          script: "${dockerCmd}",
          returnStdout: true
        ).trim()
        this.printMessage("Wait for emulator")
        this.script.steps.sh (
          script: "docker exec -t ${containerId} adb wait-for-device shell \'while [[ -z \$(getprop sys.boot_completed) ]]; do sleep 1; done; input keyevent 82\'",
          returnStdout: true
        ).trim()
        this.printMessage("Emulator up and running")
      }
    }
    return containerId
  }

  public void deployAppToHockeyAppStore(
    String appId,
    String apiTokenStringCredentialsId,
    String applicationRelativePath,
    String strategy="add"
  ){
    this.printMessage("appId: ${appId}")
    this.printMessage("apiTokenStringCredentialsId: ${apiTokenStringCredentialsId}")
    def comment="branch:${branchName}<br />commit:${currentGitCommit}<br />date:${buildTimestamp}<br />build-url:${script.env.BUILD_URL}<br />build-user-id: ${buildUserId}<br />build-user: ${buildUser}"
    this.printMessage("comment: ${comment}")
    this.script.steps.withCredentials([
      [$class: "StringBinding", credentialsId: "${apiTokenStringCredentialsId}", variable: "apiToken"]
    ]) {
      def result = this.script.steps.sh (
        script: """
       		# --proxy http://localhost:3128 \\
            curl -Lv \\
               -F "status=2" \\
               -F "notify=1" \\
               -F "notes=${comment}" \\
               -F "notes_type=1" \\
               -F "ipa=@${applicationRelativePath}" \\
               -F "commit_sha=${currentGitCommit}" \\
               -F "build_server_url=${script.env.BUILD_URL}" \\
               -F "repository_url=${currentGitURL}" \\
               -F "strategy=${strategy}" \\
               -H "X-HockeyAppToken: ${script.env.apiToken}" \\
               https://rink.hockeyapp.net/api/2/apps/${appId}/app_versions/upload
            """,
        returnStdout: true)
      this.printMessage("result: ${result}")
    }
  }

  public void setAssembleFlavor(String assembleFlavor) {
    this.assembleFlavor = assembleFlavor
  }

  public void setGenerateSigningFile(boolean generate) {
    this.generateSigningFile = generate
  }

  public void setEnabledDexGuardObfuscation(boolean enable) {
    this.enabledDexGuardObfuscation = enable
  }

  public void setAzureKeyVaultEnabled(boolean azureKeyVaultActivatedParam){
    this.azureKeyVaultActivated=azureKeyVaultActivatedParam
  }
}
