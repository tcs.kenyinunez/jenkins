package sharedlib

import org.jenkinsci.plugins.docker.workflow.*;
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*

class MavenJenkinsUtil extends BaseUtil implements Serializable, DevopsUtil {
  private static final String execMavenCpu="2"
  private static final String mavenTool="MAVEN_3.3.9"
  private boolean failNever = true
  private boolean azureKeyVaultActivated = false
  private boolean artefactory = false
  private boolean sonarUseBranch = false
  private String environment = ''
  private String maven339Java8Version = "MAVEN339_JAVA8"
  private String maven339Jdk8Version = "MAVEN339_JDK8_OPENJ9"
  private String maven339Jdk11Version = "MAVEN339_JDK11_OPENJ9"
  private String mavenJavaVersion = "${maven339Java8Version}"
  private String dockerMaven339jdk8Version232 = "bcp/maven339-openjdk180_232:1.0"
  private String dockerMaven339jdk11Version1105 = "bcp/maven339-openjdk11_05:1.0"
  private String mvnDirectory
  private String deploymentEnvironment
  private String repositoryId
  private String mvnHome

  public MavenJenkinsUtil(
    script,
    Docker docker,
    buildTimestamp,
    currentGitURL,
    currentCredentialsId,
    branchName,
    fortifyHost,
    currentGitCommit,
    String buildUser,
    String buildUserId
  ) {
    this.script = script
    this.buildUser = buildUser
    this.buildUserId = buildUserId
    this.buildTimestamp = buildTimestamp
    this.currentGitURL = currentGitURL
    this.gitlabUrl = "${script.env.GITLAB_PROTOCOL}://${script.env.GITLAB_HOST}:${script.env.GITLAB_PORT}"
    this.bitbucketUrl = gitlabUrl
    this.currentCredentialsId = currentCredentialsId
    this.branchName = branchName
    this.artefactoryUrl = script.env.ARTIFACT_SERVER_URL
    this.repositoryURL = this.artefactoryUrl
    this.artifactM2RegistryUrl = "${script.env.ARTIFACT_SERVER_URL}/public/"
    this.fortifyHost = fortifyHost
    this.docker = docker
    this.currentGitCommit = currentGitCommit
    this.fortifyUrl = "${script.env.FORTIFY_SCC_URL}"
  }

  public MavenJenkinsUtil(script, String type = ''){
    super(script, type)
    this.script.steps.echo "[BCP-DevSecOps] script: ${script}"
    script.properties.each { println "$it.key -> $it.value" }
  }

  public void prepare(){
    super.prepare()
    mvnHome = script.steps.tool "${mavenTool}"
    configureMavenSettings()
  }

  public String getApplicationName(){
    return getMvnSettings("project.artifactId")
  }

  private String getMvnSettings(String arg) {
    configureMavenSettings()
    def result
    docker.withServer("${script.env.DOCKER_HOST}"){
      docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
        def mvnExec="mvn -s ${mvnDirectory}/settings.xml -q -N org.codehaus.mojo:exec-maven-plugin:1.3.1:exec -Dexec.executable='echo' -Dexec.args='\${${arg}}'"
        result = this.script.steps.sh(
          script: "docker run --cpus ${execMavenCpu} --rm -v ${script.env.APPLICATION_CACHE}/maven-local-repo:/opt/jenkins/.m2/repository -v '${script.env.WORKSPACE}:/project/data' -v ${script.env.APPLICATION_TOOL}/oracle_java/jdk1.8.0_172/jre/lib/security/cacerts:/usr/lib/jvm/jdk1.8.0_172/jre/lib/security/cacerts -w /project/data --network=host ${dockerMavenjavaEnvironment} ${mvnExec}",
          returnStdout: true
        )
      }
    }
    return result.trim()
  }

  public String getApplicationVersion(){
    return getMvnSettings("project.version")
  }

  //def buildMaven(String buildParameters = '', boolean failNever=true) {
  public void build(final String buildParams){
    // Configure maven
    configureMavenSettings()

    // Run the maven build
    def httpProxyHost = "${script.env.DOCKER_HTTP_PROXY_HOST}"
    def httpProxyPort = "${script.env.DOCKER_HTTP_PROXY_PORT}"
    def dockerVolumen = "-v ${script.env.APPLICATION_CACHE}/maven-local-repo:/opt/jenkins/.m2/repository -v '${script.env.WORKSPACE}:/project/data' "
    //TODO - Fix path for cacerts
    dockerVolumen  += "-v ${script.env.APPLICATION_TOOL}/oracle_java/jdk1.8.0_172/jre/lib/security/cacerts:/usr/lib/jvm/jdk1.8.0_172/jre/lib/security/cacerts -w /project/data "

    String failNeverParam=""
    if(failNever){
      failNeverParam="--fail-never"
    }

    docker.withServer("${script.env.DOCKER_HOST}"){
      docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
        def dockerParameters = "--network=host"
        def dockerCmd = "docker run --cpus ${execMavenCpu} --rm ${dockerVolumen} ${dockerParameters} ${dockerMavenjavaEnvironment}"
        def mavenCompileCmd
        def mavenInstallCmd

        mavenCompileCmd = "${dockerCmd} mvn -s ${mvnDirectory}/settings.xml -PBCP_artifactory_dev compile test-compile "
        mavenInstallCmd = "${dockerCmd} mvn ${failNeverParam} -s ${mvnDirectory}/settings.xml -U -PBCP_artifactory_dev -DskipITs ${buildParams} install"
        if (deploymentEnvironment!="dev") {
          mavenCompileCmd = "${dockerCmd} mvn -s ${mvnDirectory}/settings.xml compile test-compile "
          mavenInstallCmd = "${dockerCmd} mvn ${failNeverParam} -s ${mvnDirectory}/settings.xml -DskipITs ${buildParams} install"
        }

        if(httpProxyHost && httpProxyPort){
          mavenCompileCmd+=" -Dhttp.proxyHost=${httpProxyHost} -Dhttp.proxyPort=${httpProxyPort} -Dhttps.proxyHost=${httpProxyHost} -Dhttps.proxyPort=${httpProxyPort}"
          mavenInstallCmd+=" -Dhttp.proxyHost=${httpProxyHost} -Dhttp.proxyPort=${httpProxyPort} -Dhttps.proxyHost=${httpProxyHost} -Dhttps.proxyPort=${httpProxyPort}"
        }

        this.script.steps.sh "${mavenCompileCmd}"
        this.script.steps.sh "${mavenInstallCmd}"
      }
    }

    try {
      this.script.steps.step( [ $class: 'JacocoPublisher' ] )
    }
    catch(Exception e) {
      //TODO - To be removed on the new version of plugin 2.2.1 of jacoco, verson 2.2.0 had issues with gitlba plugin
    }
    try {
      this.script.steps.junit allowEmptyResults: true, testResults: '**/**/target/surefire-reports/TEST-*.xml'
    }catch(Exception e){
      //TODO: Remove when QA are implemented
    }
  }

  public void configureMavenSettings(){
    mvnDirectory = "mvn-settings-${buildTimestamp}"
    this.script.steps.sh """
      set +x
      echo "mvnDirectory: ${mvnDirectory}"
      if [ -d "${mvnDirectory}" ]; then
          rm -r ${mvnDirectory}
      fi
      mkdir ${mvnDirectory}
      echo '' > ${script.env.WORKSPACE}/${mvnDirectory}/settings.xml
    """
    this.script.steps.writeFile(
      file:"${script.env.WORKSPACE}/${mvnDirectory}/settings.xml",
      text: getSettingsInitScript()
    )
  }

  private getSettingsInitScript(){
    def initScript
    def artifactURL
    setDeploymentEnvironment()
    def urlSnapshot  = "public.snapshot/"
    artifactURL = artifactM2RegistryUrl
    if(deploymentEnvironment == "dev"){
      artifactURL = "${script.env.ARTIFACT_SERVER_URL}/${urlSnapshot}"
    }
    this.printMessage("artifactURL: " + artifactURL)

    initScript = """<?xml version="1.0" encoding="UTF-8"?>
      <settings xmlns="http://maven.apache.org/SETTINGS/1.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
        <localRepository>/opt/jenkins/.m2/repository</localRepository>
        <mirrors>
          <mirror>
            <id>internal-mirror-repository</id>
            <name>Mirror Repository</name>
            <url>${artifactURL}</url>
            <mirrorOf>*</mirrorOf>
          </mirror>
        </mirrors>
        <profiles>

          <profile>
            <id>BCP_artifactory</id>
            <activation>
              <activeByDefault>true</activeByDefault>
            </activation>
            <repositories>
              <repository>
                <snapshots>
                  <enabled>false</enabled>
                </snapshots>
                <id>central</id>
                <name>public</name>
                <url>${artifactM2RegistryUrl}</url>
              </repository>
            </repositories>
            <pluginRepositories>
              <pluginRepository>
                <snapshots>
                <enabled>false</enabled>
                </snapshots>
                <id>central</id>
                <name>public</name>
                <url>${artifactM2RegistryUrl}</url>
              </pluginRepository>
            </pluginRepositories>
          </profile>

          <profile>
            <id>BCP_artifactory_dev</id>
            <repositories>
              <repository>
                <snapshots>
                  <enabled>false</enabled>
                </snapshots>
                <id>central</id>
                <name>public.snapshots</name>
                <url>${script.env.ARTIFACT_SERVER_URL}/${urlSnapshot}</url>
              </repository>
              <repository>
                <snapshots />
                <id>snapshots</id>
                <name>public.snapshots</name>
                <url>${script.env.ARTIFACT_SERVER_URL}/${urlSnapshot}</url>
              </repository>
            </repositories>
            <pluginRepositories>
              <pluginRepository>
                <snapshots>
                  <enabled>false</enabled>
                </snapshots>
                <id>central</id>
                <name>public.snapshots</name>
                <url>${script.env.ARTIFACT_SERVER_URL}/${urlSnapshot}</url>
              </pluginRepository>
              <pluginRepository>
                <snapshots />
                <id>snapshots</id>
                <name>public.snapshots</name>
                <url>${script.env.ARTIFACT_SERVER_URL}/${urlSnapshot}</url>
              </pluginRepository>
            </pluginRepositories>
          </profile>

          </profiles>

        </settings>
      </settings>
    """.stripIndent()
    return initScript
  }

  private void setDeploymentEnvironment(){
    String environment="${script.env.deploymentEnviroment}"

    if (environment==null) {
      environment = "prod"
    }

    this.deploymentEnvironment=environment
  }

  //def executeSonar(boolean useBranchName = false,String tagName="") {
  public void executeQA(final String releaseName="", final String encoding="UTF-8"){
    def projectName="${script.env.project}".toLowerCase()
    def groupIdName=getGroupId()
    def applicationName=getApplicationName()
    def applicationVersion=getApplicationVersion()
    def projectKey = "${projectName}-${groupIdName}:${applicationName}"
    def extraParams=""
    if(sonarUseBranch){
      extraParams="-Dsonar.branch=${branchName}"
    }
    def sonarProjectVersion="${applicationVersion}-${branchName}"
    if(releaseName!=""){
      sonarProjectVersion="${applicationVersion}-${releaseName}"
    }
    def traceabilityParams=" -Dsonar.projectVersion=\"${sonarProjectVersion}\" -Dsonar.analysis.version=\"${applicationVersion}\" \
-Dsonar.analysis.userId=\"${buildUserId}\" -Dsonar.analysis.commitId=\"${currentGitCommit}\" -Dsonar.analysis.tek=\"maven-java\" \
-Dsonar.analysis.projectName=\"${projectName}\" -Dsonar.analysis.buildTimestamp=\"${buildTimestamp}\" -Dsonar.analysis.branch=\"${branchName}\" \
-Dsonar.links.scm=\"${currentGitURL}\" -Dsonar.analysis.jobname=\"${script.env.JOB_NAME}\" "
    if(releaseName!=""){
      traceabilityParams+=" -Dsonar.analysis.tagName=\"${releaseName}\" "
    }

    def dockerVolumen  = "-v ${script.env.APPLICATION_CACHE}/maven-local-repo:/opt/jenkins/.m2/repository -v '${script.env.WORKSPACE}:/project/data' "
    dockerVolumen += "-v ${script.env.APPLICATION_TOOL}/oracle_java/jdk1.8.0_172/jre/lib/security/cacerts:/usr/lib/jvm/jdk1.8.0_172/jre/lib/security/cacerts -w /project/data "

    docker.withServer("${script.env.DOCKER_HOST}"){
      docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
        String dockerParameters = "--network=host"
        String dockerCmd = "docker run --cpus ${execMavenCpu} --rm ${dockerVolumen} ${dockerParameters} ${dockerMavenjavaEnvironment}"

        configureMavenSettings()

        this.script.steps.withSonarQubeEnv {
          this.script.steps.sh "${dockerCmd} mvn -s ${mvnDirectory}/settings.xml org.sonarsource.scanner.maven:sonar-maven-plugin:3.6.0.1398:sonar ${extraParams} -Dsonar.projectName=${projectKey} -Dsonar.projectKey=${projectKey} -Dsonar.login=${script.env.SONAR_AUTH_TOKEN} -Dsonar.host.url=${script.env.SONAR_HOST_URL} -Dsonar.junit.reportsPath=target/surefire-reports -Dsonar.jacoco.reportPath=target/jacoco.exec -Dsonar.links.ci=${script.env.BUILD_URL} ${traceabilityParams}"
        }
      }
    }
  }

  public String getGroupId(){
    return getMvnSettings("project.groupId")
  }

  //def deployArtifactWithMavenByURL(String project,String enviroment = '',String repositoryURL,String version = '',boolean artefactory=false) {
  public void uploadArtifact(final String releaseName = ''){
    this.printMessage("RELEASE NAME: ${releaseName}")
    this.printMessage("Current Git Commit: ${currentGitCommit}")
    def project = "${script.env.project}"
    this.script.steps.step([$class: 'WsCleanup'])
    def buildUniqueId
    if(releaseName!=null&&releaseName!=''){
      buildUniqueId="${releaseName}"
      checkoutBranch(releaseName)
    }else{
      buildUniqueId="${buildTimestamp}"
      checkoutBranch(currentGitCommit)
    }
    // Configure maven
    configureMavenSettings()

    def repository
    environment=environment.toUpperCase()
    def repoKey
    def repositoryPrefix=""
    if(!artefactory){
      repositoryPrefix="/content/repositories"
    }

    repositoryId = project+"_snapshot"
    repoKey="${script.env.project}.Snapshot"
    repository="${repositoryURL}${repositoryPrefix}/${repoKey} -Drelease=false"
    if ("${branchName}"=="master"){
      repositoryId = project+"_release"
      repoKey="${script.env.project}.Release"
      repository="${repositoryURL}${repositoryPrefix}/${repoKey}"
    }

    if(environment){
      repositoryId = project+"-${environment}"
      repoKey="${script.env.project}-${environment}"
      repository="${repositoryURL}${repositoryPrefix}/${repoKey} -Drelease=false"
    }

    def altDeploymentRepository="-DaltDeploymentRepository=${repositoryId}::default::${repository}"
    def projectName="${script.env.project}"
    def projectNameLowercase=projectName.toLowerCase()

    this.printMessage("Repository ID: ${repositoryId}")
    // Run the maven build
    def dockerVolume = "-v ${script.env.APPLICATION_CACHE}/maven-local-repo:/opt/jenkins/.m2/repository -v '${script.env.WORKSPACE}:/project/data' "
    //TODO - Fix path for cacerts
    dockerVolume += "-v ${script.env.APPLICATION_TOOL}/oracle_java/jdk1.8.0_172/jre/lib/security/cacerts:/usr/lib/jvm/jdk1.8.0_172/jre/lib/security/cacerts -w /project/data "

    docker.withServer("${script.env.DOCKER_HOST}"){
      docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
        def applicationName=getApplicationName()
        def projectKeyName="${script.env.project}".toLowerCase()+"-${applicationName}"
        def dockerParameters="--network=host"
        def dockerEnvironmentParameters="-e BUILD_USER_ID=${buildUserId} -e BUILD_GIT_URL=${currentGitURL} -e PROJECT_NAME=${script.env.project}  -e ARTIFACT_SERVER_URL=${repositoryURL} -e REPOKEY=${repoKey}  -e BUILD_UNIQUE_ID=${buildUniqueId} -e BUILD_NAME=${projectKeyName} "

        def executionCommand="mvn -s ${mvnDirectory}/settings.xml deploy -DdeployAtEnd=true -Dmaven.test.skip=true ${altDeploymentRepository} -DbuildInfoConfig.envVarsExcludePatterns=*password*,*secret*,*PASSWORD*,*SECRET*"
        if(!azureKeyVaultActivated){
          this.script.steps.withCredentials([
            [
              $class: 'UsernamePasswordMultiBinding',
              credentialsId: projectNameLowercase+'-jenkins-artefactory-token',
              usernameVariable: 'ARTIFACT_USER_NAME',
              passwordVariable: 'ARTIFACT_USER_PASSWORD'
            ]
          ]) {
            dockerEnvironmentParameters+="-e ARTIFACT_USER_NAME=${script.env.ARTIFACT_USER_NAME} -e ARTIFACT_USER_PASSWORD=${script.env.ARTIFACT_USER_PASSWORD}"
            def dockerCmd="docker run --cpus ${execMavenCpu} --rm ${dockerVolume} ${dockerEnvironmentParameters} ${dockerParameters} ${dockerMavenjavaEnvironment}"
            this.script.steps.sh "${dockerCmd} ${executionCommand}"
          }
        }else{
          withAzureVaultCredentials([
            [
              azureCredentialVariable:"ARTIFACT_USER_NAME",
              azureCredentialId: projectNameLowercase+'-jenkins-artefactory-token-username'
            ],
            [
              azureCredentialVariable:"ARTIFACT_USER_PASSWORD",
              azureCredentialId: projectNameLowercase+'-jenkins-artefactory-token-password'
            ]
          ]){
            dockerEnvironmentParameters+="-e ARTIFACT_USER_NAME=${script.env.ARTIFACT_USER_NAME} -e ARTIFACT_USER_PASSWORD=${script.env.ARTIFACT_USER_PASSWORD}"
            def dockerCmd="docker run --cpus ${execMavenCpu} --rm ${dockerVolume} ${dockerEnvironmentParameters} ${dockerParameters} ${dockerMavenjavaEnvironment}"
            this.script.steps.sh "${dockerCmd} ${executionCommand}"
          }
        }
      }
    }
  }

  //def executeFortifyMavenSast(String buildParameters = ''){
  public void executeSast(final String buildParams=''){
    if(!fortifyActivated){
      this.printMessage("Fortify esta deshabilitado")
      return
    }
    // Configure maven
    configureMavenSettings()
    String fortifyVersion = getGitProjectName()
    String projectLowerCase="${script.env.project}".toLowerCase()
    String fortifyCloudScanCredId = "${projectLowerCase}-jenkins-fortify-cloudscan-token"
    String fortifyAuthCredId = "${projectLowerCase}-jenkins-fortify-ssc-token"
    String buildID = "${script.env.project}-${fortifyVersion}-${script.env.BUILD_NUMBER}"

    this.script.steps.withCredentials([
      [$class: 'StringBinding', credentialsId: "${fortifyCloudScanCredId}", variable: 'cloudScanToken' ],
      [$class: 'StringBinding', credentialsId: "${fortifyAuthCredId}", variable: 'authToken' ]
    ]){
      try{
        //Docker execute
        this.script.steps.sh """
            set -x
            docker run -d -it --cpus ${execFortifyCpu} --network=host -v ${script.env.APPLICATION_CACHE}/maven-local-repo:/opt/jenkins/.m2/repository -v '${script.env.WORKSPACE}:/project/data' -w /project/data ${dockerFortifyRunner} bash > .dockerId
            docker exec `cat .dockerId` sourceanalyzer -b "${buildID}" -clean
            docker exec `cat .dockerId` sourceanalyzer -b "${buildID}" mvn -s ${mvnDirectory}/settings.xml -Dmaven.test.skip=true ${buildParams} com.fortify.sca.plugins.maven:sca-maven-plugin:19.2.0:translate
            docker exec `cat .dockerId` cloudscan -sscurl ${fortifyUrl} -ssctoken ${script.env.cloudScanToken} start -block -log resultado.log -upload --application "${script.env.project}" --application-version "${fortifyVersion}" -b "${buildID}" -uptoken ${script.env.cloudScanToken} -scan ${execFortifyMemory}
            docker exec `cat .dockerId` fortifyclient downloadFPR -file result.fpr -url ${fortifyHost} -authtoken ${script.env.authtoken} -project "${script.env.project}" -version "${fortifyVersion}"
            docker exec `cat .dockerId` BIRTReportGenerator -template "OWASP Top 10" -source result.fpr -format HTML --Version "OWASP Top 10 2017" -output MyOWASP_Top10_Report.html
            docker exec `cat .dockerId` fortifyclient -url ${fortifyHost} -authtoken ${script.env.authtoken} listApplicationVersions > .listProject.txt
          """
      }catch(Exception e){
        this.script.steps.sh """
            set +e
            docker exec `cat .dockerId` cat /opt/jenkins/data/.fortify/cloudscan/log/cloudscan.log
          """
        throw e
      }finally{
        this.script.steps.sh """
            docker rm -f `cat .dockerId`
          """
      }

      generateReportFortify()
    }
  }

  public void saveResult(final String extension){
    this.script.steps.archiveArtifacts(artifacts:'**/target/*.'+extension, allowEmptyArchive:true)
  }

  public void startRelease(final String buildParams){
    if ("${branchName}"=="master"){
      this.trowException("Generating of release must not be done on master")
    }

    //Check generate new version
    String applicationVersion=getApplicationVersion()
    this.printMessage("Application version: ${applicationVersion}")
    String nextReleaseVersion=getApplicationNextReleaseVersion(applicationVersion)
    this.printMessage("Next release version: ${nextReleaseVersion}")

    String releaseBranchName="release/"+nextReleaseVersion
    def existBranchInRepository=existBranch(releaseBranchName)
    this.printMessage("Exist release branch: ${existBranchInRepository}")
    if(existBranchInRepository){
      //If the branch exist an error is throw
      this.printMessage("Deleting release branch: ${releaseBranchName}")
      deleteNotMergeReleaseBranch(releaseBranchName)
    }

    createNewReleaseBranch(nextReleaseVersion)
    checkoutReleaseBranch(nextReleaseVersion)
    updateToReleaseVersionMaven(nextReleaseVersion)

    def tagStateComment="Release Candidate"
    def currentDate = buildTimestamp
    def environmentParam="CERT"
    applicationVersion=getApplicationVersion()
    def tagName="RC-"+applicationVersion+"-${environmentParam}-${currentDate}"

    tagName=updateTagNameWithACR(tagName)

    protectBranch("release/${nextReleaseVersion}")
    //Validate SAST Analisys
    executeSast()
    try{
      this.sonarUseBranch = false
      executeQA(tagName)
    }catch(Exception e){
      this.printMessage("WARNING: Error executing Sonar!")
    }
    tagBranch(tagName,tagStateComment,environmentParam)
    this.environment = environmentParam
    uploadArtifact(tagName)
    executeXraySCA(tagName)
  }

  private String getApplicationNextReleaseVersion(version){
    String isSnapshotVersion=isSnapshotVersion(version)
    this.printMessage("Snapshot version: ${isSnapshotVersion}")
    String snapshotPattern="-SNAPSHOT"
    def newWorkingVersion
    if (isSnapshotVersion != "true") {
      this.throwException("Version is not snapshot. The release version should be done on snapshot version.")
    }

    this.printMessage("Version is snapshot")
    newWorkingVersion = this.script.steps.sh(script: """currentVersion=${version}
      echo \${currentVersion%${snapshotPattern}}""", returnStdout: true).trim()

    this.printMessage("New working version: ${newWorkingVersion}")
    return newWorkingVersion
  }

  private String isSnapshotVersion(String version){
    String snapshotPattern = "-- -SNAPSHOT"
    String command = "echo \"${version}\" | grep ${snapshotPattern}"
    def statusCode =  this.script.steps.sh script:"${command}", returnStatus:true
    if(statusCode==1){
      return "false"
    }
    String containsSnapshot = this.script.steps.sh(script: "${command}", returnStdout: true).trim()
    this.printMessage("containsSnapshot: ${containsSnapshot}")

    return this.script.steps.sh(
      script: "[ \${#containsSnapshot} -ge 0 ] && echo 'true' || echo 'false'",
      returnStdout: true
    ).trim()
  }


  /*
  * This method will update version to a release version for maven
  */
  def updateToReleaseVersionMaven(String version){
    configureMavenSettings()

    docker.withServer("${script.env.DOCKER_HOST}"){
      docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
        def mvnExec="mvn -s ${mvnDirectory}/settings.xml --batch-mode versions:set -DnewVersion=${version}"
        this.script.steps.sh "docker run --cpus ${execMavenCpu} --rm -v ${script.env.APPLICATION_CACHE}/maven-local-repo:/opt/jenkins/.m2/repository -v '${script.env.WORKSPACE}:/project/data' -v ${script.env.APPLICATION_TOOL}/oracle_java/jdk1.8.0_172/jre/lib/security/cacerts:/usr/lib/jvm/jdk1.8.0_172/jre/lib/security/cacerts -w /project/data --network=host ${dockerMavenjavaEnvironment} ${mvnExec}"
      }
    }
    updateToReleaseVersion(version)
  }

  //def promoteReleaseMaven(String releaseTagName, boolean forceRelease=false){
  public void promoteRelease(final String releaseTagName, final boolean forceRelease=false){
    if(!"${branchName}".startsWith("tags/RC-")){
      this.throwException("The release must be done on release")
    }
    def applicationVersion=getApplicationVersion()
    this.printMessage("Forcing Promote release tag name: ${forceRelease}")
    boolean existTag=existTag("${applicationVersion}")
    String tagStateComment="Release"
    if(existTag){
      if (!forceRelease){
        this.throwException("The tag for this release exists: ${applicationVersion}")
      }
      this.printMessage("Forcing removal and promote of release tag name: ${releaseTagName}")
      String tagName=getApplicationVersion()
      deleteTagBranch(tagName)
      this.printMessage("Tag removed: ${releaseTagName}")
      this.printMessage("${tagStateComment}-FORCED")
    }
    this.printMessage("Promote release tag name: ${releaseTagName}")
    String applicationName=getApplicationName()
    promoteRepository(applicationName,releaseTagName)
    // Removing from stage modified files during npm publish
    mergeOverwriteBranch(releaseTagName,"master")
    def tagName=getApplicationVersion()
    tagBranch(tagName,tagStateComment)
  }

  public void setFailNever(boolean failNeverParam) {
    this.failNever = failNeverParam
  }

  public void setSonarUseBranch(boolean sonarUseBranchParam) {
    this.sonarUseBranch = sonarUseBranchParam
  }

  public void setEnvironment(String environment) {
    this.environment = environment
  }

  public String getMavenArtifactPath(String pomFolderPath){
    // Configure maven
    configureMavenSettings()

    def result
    docker.withServer("${script.env.DOCKER_HOST}"){
      docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
        def mvnExec="mvn -s ${mvnDirectory}/settings.xml -f ${script.env.WORKSPACE}/${pomFolderPath} -q -N org.codehaus.mojo:exec-maven-plugin:1.3.1:exec -Dexec.executable='echo' -Dexec.args='\${project.directory}/${script.env.project.artifactId}-\${project.version}.jar'"
        result = this.script.steps.sh script: "docker run --cpus ${execMavenCpu} --rm -v ${script.env.APPLICATION_CACHE}/maven-local-repo:/opt/jenkins/.m2/repository -v '${script.env.WORKSPACE}:/project/data' -v ${script.env.APPLICATION_TOOL}/oracle_java/jdk1.8.0_172/jre/lib/security/cacerts:/usr/lib/jvm/jdk1.8.0_172/jre/lib/security/cacerts -w /project/data --network=host ${dockerMavenjavaEnvironment} ${mvnExec}", returnStdout: true
      }
    }
    return result.trim()
  }

  public String getMavenArtifactReleaseVersion() {
    configureMavenSettings()

    def result
    docker.withServer("${script.env.DOCKER_HOST}"){
      docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
        def mvnExec="mvn -s ${mvnDirectory}/settings.xml -f ${script.env.WORKSPACE}/target/checkout/pom.xml -q -N org.codehaus.mojo:exec-maven-plugin:1.3.1:exec -Dexec.executable='echo' -Dexec.args='\${project.version}'"
        result = this.script.steps.sh script: "docker run --cpus ${execMavenCpu} --rm -v ${script.env.APPLICATION_CACHE}/maven-local-repo:/opt/jenkins/.m2/repository -v '${script.env.WORKSPACE}:/project/data' -v ${script.env.APPLICATION_TOOL}/oracle_java/jdk1.8.0_172/jre/lib/security/cacerts:/usr/lib/jvm/jdk1.8.0_172/jre/lib/security/cacerts -w /project/data --network=host ${dockerMavenjavaEnvironment} ${mvnExec}", returnStdout: true
      }
    }
    return result.trim()
  }

  public void setMavenJavaVersion(String mavenJavaVersion){
    if("${maven339Java8Version}"=="${mavenJavaVersion}"){
      dockerMavenjavaEnvironment = dockerMaven339java8Version
    }else if("${maven339Jdk8Version}"=="${mavenJavaVersion}"){
      dockerMavenjavaEnvironment = dockerMaven339jdk8Version232
    }else if("${maven339Jdk11Version}"=="${mavenJavaVersion}"){
      dockerMavenjavaEnvironment = dockerMaven339jdk11Version1105
    }else{
      throw new Exception("Not supported version: "+mavenJavaVersion)
    }
    printMessage("version maven java: "+mavenJavaVersion)
    printMessage("docker maven java environment: "+dockerMavenjavaEnvironment)
  }
}
