package sharedlib;

class SalesforceJenkinsfileUtil extends JenkinsfileUtil implements Serializable {
    
  SalesforceJenkinsfileUtil(steps,script,String type = '') {
    super(steps, script,type);
  }
  
  def executeAntTask(antTask,antTaskParameters){
    def dockerVolumen    = "-v ${script.env.WORKSPACE}:${script.env.WORKSPACE} -w ${script.env.WORKSPACE}"
    def dockerParameters = "-d -it --network=host"
    def containerId = null;
      
    try{
      docker.withServer("${script.env.DOCKER_HOST}"){
        docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
          containerId = steps.sh(script:"docker run ${dockerParameters} ${dockerVolumen} ${dockerSalesforceTool41Ant110Cs385Enviroment} bash",returnStdout:true).trim();
          steps.sh "docker exec ${containerId} ant ${antTaskParameters} ${antTask}"
        }
      }

    }catch(e){
      throw e;
    }finally{
      if(containerId){
          steps.sh "docker rm -f ${containerId}"
      }
    }
  }

  def withSalesforceAntProperties(deploymentTarget="develop",Closure body){
    def deploymentEnviroment = script.env.deploymentEnvironment;
    def branchToDeploy       = branchName;
    def credentialPrefix     = deploymentEnviroment
    def filePath             = "${script.env.WORKSPACE}/build.properties"
    
    steps.echo """
      [BCP-DevSecOps] Generacion de archivo de configuracion para Salesforce
    """

    if(deploymentEnviroment=="dev"){
      credentialPrefix = "dev"
    }

    if(deploymentEnviroment == "dev" && branchToDeploy!="develop"){
      credentialPrefix = deploymentTarget;
    }

    def projectLowerCase      = script.env.project.toLowerCase();
    def usernameCredentialsId = "${projectLowerCase}-salesforce-username-${credentialPrefix}"
    def passwordCredentialsId = "${projectLowerCase}-salesforce-password-${credentialPrefix}"
    def tokenCredentialsId    = "${projectLowerCase}-salesforce-token-${credentialPrefix}"
    def existsTokenCredential;

    steps.echo """
      [BCP-DevSecOps] Credenciales para autenticacion en Salesforce 
        * Username credential ID: ${usernameCredentialsId}
        * Password credential ID ${passwordCredentialsId}
        * Token Credential ID: ${tokenCredentialsId}
    """

    def credentialsMapping = [
      [$class: "StringBinding", credentialsId: "${usernameCredentialsId}", variable: "sfUsernameValue"],
      [$class: "StringBinding", credentialsId: "${passwordCredentialsId}", variable: "sfPasswordValue"],
    ]

    try{
      steps.withCredentials([
        [$class: "StringBinding", credentialsId: "${tokenCredentialsId}", variable: "sfTokenValue"]
      ]){
         existsTokenCredential = true
         steps.echo """
          [BCP-DevSecOps] Token de autenticacion para Salesforce existe.
         """ 
       }
    }catch(e){
      existsTokenCredential = false
      steps.echo """
        [BCP-DevSecOps] Token de autenticacion para Salesforce NO existe.
      """
    }

    if(existsTokenCredential){
      credentialsMapping.add([$class: "StringBinding", credentialsId: "${tokenCredentialsId}", variable: "sfTokenValue"])
    }

    try{
      steps.withCredentials(credentialsMapping){
        def buildProperties = makeSalesforcePropertiesFile(script.env.sfUsernameValue,script.env.sfPasswordValue,script.env.sfTokenValue)
        
        steps.writeFile(
          file: filePath,
          text: buildProperties
        );
        
        body();
                
      }
    }catch(Exception e){
        throw e;
    }finally{
      if(steps.fileExists("${filePath}")){
        steps.sh "rm -f ${filePath}"  
      }
      
      steps.echo """
        [BCP-DevSecOps] Fin de proceso de generacion de archivo de configuracion para Salesforce
      """
    }
  }

  def withSalesforceAntPropertiesFromParameters(sandboxUsername="",sandboxPassword="",sandboxToken="",Closure body){  
    def filePath = "${script.env.WORKSPACE}/build.properties"
    
    steps.echo """
      [BCP-DevSecOps] Generacion de archivo de configuracion para Salesforce
    """
    
    try{
      steps.wrap([
        $class: 'MaskPasswordsBuildWrapper', varPasswordPairs: [
          [password: sandboxUsername, var: 'sandboxUsernameVariable'],														
          [password: sandboxPassword, var: 'sandboxPasswordVariable'],
          [password: sandboxToken, var: 'sandboxTokenVariable']
        ]
      ]){
        def buildProperties = makeSalesforcePropertiesFile(sandboxUsername,sandboxPassword,sandboxToken);
        
        steps.writeFile(
          file: filePath,
          text: buildProperties
        );
        
        body();
      }
    }catch(Exception e){
        throw e;
    }finally{
      if(steps.fileExists("${filePath}")){
        steps.sh "rm -f ${filePath}"  
      }

      steps.echo """
        [BCP-DevSecOps] Fin de proceso de generacion de archivo de configuracion para Salesforce
      """
    }
  }

  def makeSalesforcePropertiesFile(sandboxUsername="",sandboxPassword="",sandboxToken=""){
    def MAX_POLL      = 2000
    def SERVER_URL    = "https://test.salesforce.com/"
    def PKG_NAME      = "package.xml"
    def PROXY_ENABLED = false

    if(isDeployToProd()){
      SERVER_URL = "https://login.salesforce.com/"
    }
    
    steps.echo """
      [BCP-DevSecOps] Salesforce Url de conexion: ${SERVER_URL}
    """

    def buildProperties = """
        sf.username=${sandboxUsername}
        sf.password=${sandboxPassword}
        sf.token=${sandboxToken}
        sf.pkgName=${PKG_NAME}
        sf.serverurl=${SERVER_URL}
        sf.maxPoll=${MAX_POLL}
        sf.proxy.enabled=${PROXY_ENABLED}
        sf.proxy.host=
        sf.proxy.port=
    """.stripIndent().trim();
    
    return buildProperties;
  }

  def isDeployToProd(){
    def environment = "${script.env.deploymentEnvironment}".toUpperCase();

    if(environment=='PROD'){
      return true;
    }

    if ("${branchName}".startsWith("tags/RC-")){
      return true;
    }

    if("${branchName}" == "master"){
      return true;
    }

    return false;
  }

  def executeSonarForSalesForce() {
      //Pending for plugin codescan
  }

  def executeFortifySalesforce(){
    if(!fortifyActivated){
      steps.echo "********************* FORTIFY IS DISABLED!!!!! *********************"
      return;
    }

    def fortifyVersion         = getGitProjectName()
    def projectLowerCase       = "${script.env.project}".toLowerCase()
    def fortifyCloudScanCredId = "${projectLowerCase}-jenkins-fortify-cloudscan-token"
    def fortifyAuthCredId      = "${projectLowerCase}-jenkins-fortify-ssc-token"
    def buildID                = "${script.env.project}-${fortifyVersion}-${script.env.BUILD_NUMBER}"

    def imageId = ""
    def dockerParams = "--cpus ${execFortifyCpu} -d -it --network=host -v ${script.env.WORKSPACE}:/project/data -w /project/data ${dockerFortifyRunner}"
        
    steps.withCredentials(
      [
        [$class: 'StringBinding', credentialsId: "${fortifyCloudScanCredId}", variable: 'cloudScanToken' ],
        [$class: 'StringBinding', credentialsId: "${fortifyAuthCredId}", variable: 'authToken' ]
      ]){
        try{
          
          imageId = steps.sh(script:"docker run ${dockerParams} bash",returnStdout:true).trim();

          steps.sh "docker exec ${imageId} sourceanalyzer -b ${buildID} -clean"
          steps.sh "docker exec ${imageId} sourceanalyzer -b ${buildID} -Dcom.fortify.sca.LogLevel=\"FATAL\" -apex */**/*.cls */**/*.page */**/*.trigger */**/*.component"
          steps.sh "docker exec ${imageId} cloudscan -sscurl ${script.env.FORTIFY_SCC_HOST} -ssctoken ${script.env.cloudScanToken} start -block -log resultado.log -upload --application ${script.env.project} --application-version ${fortifyVersion} -b ${buildID} -uptoken ${script.env.cloudScanToken} -scan ${execFortifyMemory}"
          steps.sh "docker exec ${imageId} fortifyclient downloadFPR -file result.fpr -url ${script.env.FORTIFY_SCC_HOST} -authtoken ${script.env.authtoken} -project ${script.env.project} -version ${fortifyVersion}"
          steps.sh "docker exec ${imageId} BIRTReportGenerator -template \"OWASP Top 10\" -source result.fpr -format HTML --Version \"OWASP Top 10 2017\" -output MyOWASP_Top10_Report.html"
          steps.sh "docker exec ${imageId} fortifyclient -url ${script.env.FORTIFY_SCC_HOST} -authtoken ${script.env.authtoken} listApplicationVersions > .listProject.txt"
        
        }catch(Exception e){
          if(imageId){
            steps.sh """                      
              docker exec ${imageId} cat /opt/jenkins/.fortify/cloudscan/log/cloudscan.log 
            """ 
          }

          throw e
        }finally{
          if(imageId){
            steps.sh """
              docker rm -f ${imageId}
            """
          }
        }
        generateReportFortify()	
    }
  }
}