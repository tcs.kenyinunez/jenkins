package sharedlib;
/***
*
* JenkinsfileUtil - version 2.19.0
* Author: Miguel Angel Martinez Espichan
*/
import org.jenkinsci.plugins.docker.workflow.*;
import com.cloudbees.plugins.credentials.CredentialsProvider;
import com.cloudbees.plugins.credentials.common.StandardUsernameCredentials;
import com.cloudbees.plugins.credentials.domains.URIRequirementBuilder;
import com.cloudbees.plugins.credentials.common.UsernamePasswordCredentials;
import hudson.security.ACL;
import groovy.json.JsonSlurper;
import groovy.json.JsonOutput;
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*
import com.cloudbees.plugins.credentials.domains.DomainRequirement;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

class JenkinsfileUtil extends BaseUtil implements Serializable {
  /* General Properties */
  def mvnHome
  def gradleHome
  def nodeHome
  def sonarRunnerHome="${script.env.APPLICATION_TOOL}/sonar-scanner-3.2.0.1227"
  def icNameUser="jenkins"
  def icMailUser="jenkins@bcp.com.pe"
  def mavenTool="MAVEN_3.3.9"
  def gradleTool2="GRADLE_2.14.1"
  def gradleTool3="GRADLE_3.5"
  def gradleTool=gradleTool3
  def nodeJSTool="NODEJS_6.9.2"
  def gradle351Java8Version="GRADLE351_JAVA8"
  def gradle351Java8VersionShort="GRADLE_3.5"
  def gradle481Java8Version="GRADLE481_JAVA8"
  def gradle481Java8VersionShort="GRADLE_4.5"
  def gradle56Java8Version="GRADLE56_JAVA8"
  def gradle56Java8VersionShort="GRADLE_5.6"
  def gradleJavaVersion="${gradle481Java8Version}"

  def node6Angular4Version="NODE6_ANGULAR4"
  def node6Angular5Version="NODE6_ANGULAR5"
  def node6Angular6Version="NODE6_ANGULAR6"
  def node10Angular7Version="NODE10_ANGULAR7"
  def nativeScript343Gradle410Android28Node8Version="NATIVESCRIPT_343_GRADLE410_ANDROID28_NODE8"
  def nativeScript511Gradle410Android28Node8Version="NATIVESCRIPT_511_GRADLE410_ANDROID28_NODE8"
  def nativeScript542Gradle410Android28Node8Version="NATIVESCRIPT_542_GRADLE410_ANDROID28_NODE8"
  def nativeScriptGradleAndroidVersion="${nativeScript343Gradle410Android28Node8Version}"

  def dockerGradle351Java8Enviroment="bcp/gradle351-jdk8:1.0"

  def dockerGradle481Java8Enviroment="bcp/gradle481-jdk8:1.0"
  def dockerGradle56Java8Enviroment="bcp/gradle56-jdk8:1.0"
  def dockerGradleJavaEnviroment="${dockerGradle481Java8Enviroment}"

  def dockerNode6Angular4Enviroment="bcp/node6-angular4-phantomjs2:0.1"
  def dockerNode6Angular5Enviroment="bcp/node6-angular5:0.2"
  def dockerNode6Angular6Enviroment="bcp/node6-angular6:0.2"
  def dockerNode10Angular7Enviroment = "bcp/node10-angular7:1.1"
  def dockerNodeAngularEnviroment="${dockerNode6Angular4Enviroment}"
  def dockerAndroidEmulator711Enviroment="bcp/android-emulator-x86-7.1.1"
  def dockerPython27GoogleApiEnviroment="bcp/python27-google-api-python-client:1.2"
  def dockerSalesforceTool41Ant110Cs385Enviroment="bcp/docker-salesforce-tool-4_1-ant-1_10-cs-3_8_5:1.0"
  def dockerAndroidEmulatorEnviroment="${dockerAndroidEmulator711Enviroment}"
  def dockerNativeScript343Gradle410Android28Node8Enviroment="bcp/ns3_4_3-gradle4_10_2-android28-node8:1.13"
  def dockerNativeScript511Gradle410Android28Node8Enviroment="bcp/ns5_1_1-gradle4_10_2-android28-node8:1.2"
  def dockerNativeScript542Gradle410Android28Node8Enviroment="bcp/ns5_4_2-gradle4_10_2-android28-node8:1.0"
  def dockerNativeScriptEnviroment="${dockerNativeScript343Gradle410Android28Node8Enviroment}"
  def dockerSonarRunner = "bcp/sonar-scanner:v3.2.0-jdk8"
  def dockerAzureClientEnviroment = "bcp/azure-cli-bcp:1.1"

  def dockerFastlane2126Enviroment="bcp/fastlane:2.129.0"
  def dockerFastlaneEnviroment="${dockerFastlane2126Enviroment}"

  def dockerFortifyGradle = "bcp/fortify-gradle:2.0"
  def dockerFortifyGradle56 = "bcp/fortify1920-gradle56:1.0"
  def dockerFortifyGradleRunner = "${dockerFortifyGradle}"
  def dockerFortifyTypescript = "bcp/fortify-typescript:2.0"

  def artifact_m2_registry_url
  def artifact_npm_registry_url
  def repositoryId
  def container_name
  def newApp
  protected def buildTimestamp
  def macServerAgent
  private def iosTeamId
  private def iosTeamName
  private def iosAppIdentifier
  private def iosProvisionalProfileName
  private def iosAppSchema
  private def iosPlistPath
  protected def azureKeyVaultActivated=false
  def sshAgentauthSocketVar = "SSH_AUTH_SOCK"
  def sshAgentagentPidVar = "SSH_AGENT_PID"
  def idVerFortify = ""
  def folderAnalyze = ""
  def angularEnvVault = null
  def angularEnvVaultPathFile = null

  private def nativeScriptIOSBundleName
  private def xcodeWorkspace=""
  private def macServerNode8113Path="~/.nvm/versions/node/v8.11.3/bin"
  private def macServerNodePath="${macServerNode8113Path}"
  private def macServerNativeScript511Path="~/.nsvm/versions/5.1.1/bin"
  private def macServerNativeScript542Path="~/.nsvm/versions/5.4.2/bin"
  private def macServerNativeScript343Path="~/.nsvm/versions/3.4.3/node_modules/.bin"
  private def macServerRvmPath
  private def macServerRubyPath="/usr/bin:/usr/local/bin"
  private def macServerGemPath
  private def macServerGemExecutablePath="/usr/bin:/usr/local/bin"
  private def nativeScriptVersionSet=false
  private def macServerNativeScriptPath="${macServerNativeScript343Path}"
  private def nativeScriptIOSEnabled=true
  private def nativeScriptAndroidEnabled=true
  private def nativeScriptMutualPluginVersion="1.0.0"

  private def podsXCodeProject = 'Pods/Pods.xcodeproj'

  def deploymentEnviroment

  def dockerBuildEnvironmentVariables    = [];
  def webAppSettingsEnvironmentVariables = [];
  def azureContainerRegistries           = [
    "dev" : "acrgeu2inctd01.azurecr.io",
    "cert": "acrgeu2inctc01.azurecr.io",
    "prod": "acrgeu2inctp01.azurecr.io"
  ];

  def execMavenCpu="2"
  private FreeStyleJenkinsUtil freeStyleJenkinsUtil;
  private GradleAndroidJenkinsUtil gradleAndroidJenkinsUtil;
  private MavenJenkinsUtil mavenJenkinsUtil;

  /*
  * Get information from the main pipeline
  */
  def steps
  def sharedlibVersion

  private JenkinsfileUtil() {}

  public JenkinsfileUtil(steps,script,String type = '') {
    super(script, type)
    this.steps = steps

    steps.echo "steps: ${steps}"
    steps.echo "script: ${script}"
    def sharedlibVersionInfo=script.env.getEnvironment().find{ key, value  -> key.equals("library.jenkins-sharedlib.version")}
    sharedlibVersion=sharedlibVersionInfo.value
    steps.echo "Shared library version: ${sharedlibVersion}"
    if(sharedlibVersion!="master"){
     steps.echo """
     *********************************************************
     ********* SHARED LIBRARY BRANCH MUST BE MASTER ! ********
     *********************************************************"""
    }
    script.properties.each { println "$it.key -> $it.value" }
    steps.properties.each { println "$it.key -> $it.value" }
  }

  /*
  * This method checkout the code from git and configure settings for the project
  */
  public void prepare(){
    super.prepare()
    artifact_m2_registry_url  = "${script.env.ARTIFACT_SERVER_URL}/public/"
    artifact_npm_registry_url = "${script.env.ARTIFACT_SERVER_URL}/api/npm/public-node/"

    // Get the Maven tool.
    // ** NOTE: This 'M3' Maven tool must be configured
    // **       in the global configuration.
    setGradleVersion(gradle351Java8Version)
    mvnHome = steps.tool "${mavenTool}"
    steps.echo "MAVEN_HOME: ${mvnHome}"
    steps.echo "NODE_HOME: ${nodeHome}"
  }

  /* DEPRECATED method, use getApplicationVersion */
  def getProjectVersion(){
    def result = steps.sh script: "'${mvnHome}/bin/mvn' -q -N org.codehaus.mojo:exec-maven-plugin:1.3.1:exec -Dexec.executable='echo' -Dexec.args='\${project.version}'", returnStdout: true
    result=result.trim()
    return result
  }

  def configureMavenSettings(){
    this.getInstanceMavenJenkinsUtil().configureMavenSettings()
  }

  def setDeploymentEnviroment(){

    def enviroment="${script.env.deploymentEnviroment}"

    if (enviroment==null)
    {
      enviroment = "prod"
    }

    this.deploymentEnviroment=enviroment
  }

  def setNativeScriptIOSBundleName(String iOSBundleName){
    steps.echo "Set nativeScript IOS BundleName: ${iOSBundleName}"
    this.nativeScriptIOSBundleName=iOSBundleName
  }

  def forceTeamNameForTestFlight(String iosTeamNameParam){
    steps.echo "Forcing iosTeamNameParam: ${iosTeamNameParam}"
    this.iosTeamName=iosTeamNameParam
  }

  def setNativeScriptIOSEnabled(boolean nativeScriptIOSEnabledParam){
    this.nativeScriptIOSEnabled=nativeScriptIOSEnabledParam
  }

  def setNativeScriptAndroidEnabled(boolean nativeScriptAndroidEnabledParam){
    this.nativeScriptAndroidEnabled=nativeScriptAndroidEnabledParam
  }

  def setAzureKeyVaultEnabled(boolean azureKeyVaultActivatedParam){
    this.azureKeyVaultActivated=azureKeyVaultActivatedParam
  }

  def getBuildTimestamp(){
    return buildTimestamp
  }

  public String getApplicationVersion(){
    return this.getInstanceMavenJenkinsUtil().getApplicationVersion()
  }

  private MavenJenkinsUtil getInstanceMavenJenkinsUtil() {
    if (this.mavenJenkinsUtil == null) {
      this.mavenJenkinsUtil = new MavenJenkinsUtil(
        script,
        docker,
        buildTimestamp,
        currentGitURL,
        currentCredentialsId,
        branchName,
        fortifyHost,
        currentGitCommit,
        buildUser,
        buildUserId
      )
    }

    return this.mavenJenkinsUtil;
  }

  def getApplicationVersionIOS(){
    def result = steps.sh script: "awk -F\"[<>]\" '/CFBundleShortVersionString/ {getline;print \$3;exit}' ${script.env.WORKSPACE}/${iosPlistPath}", returnStdout: true
    result=result.trim()
    return result
  }

  def getApplicationBuildNumberIOS(){
    def result = steps.sh script: "awk -F\"[<>]\" '/CFBundleVersion/ {getline;print \$3;exit}' ${script.env.WORKSPACE}/${iosPlistPath}", returnStdout: true
    result=result.trim()
    return result
  }

  def getApplicationVersionNode(){
    def result = steps.sh script: "jq --raw-output '.version' ${script.env.WORKSPACE}/package.json", returnStdout: true
    result=result.trim()
    return result
  }

  def getApplicationVersionGradle(){
    def result = steps.sh script: "cat gradle.properties | grep ^version= | awk -F= '\$1==\"version\" {print \$2}'", returnStdout: true
    result=result.trim()
    return result
  }

  def getApplicationVersionFreeStyle(){
    return this.getInstanceFreeStyleJenkinsUtil().getApplicationVersion();
  }

  private FreeStyleJenkinsUtil getInstanceFreeStyleJenkinsUtil() {
    if (this.freeStyleJenkinsUtil == null) {
      this.freeStyleJenkinsUtil = new FreeStyleJenkinsUtil(
        script,
        docker,
        buildTimestamp,
        currentGitURL,
        currentCredentialsId,
        branchName,
        currentGitCommit,
        buildUser,
        buildUserId
      )
    }

    return this.freeStyleJenkinsUtil;
  }

  def getApplicationVersionForSalesForce(){
    def result = steps.sh script: "cat version", returnStdout: true
    result=result.trim()
    return result
  }

  def getApplicationVersionForNativeScript(){
    def result = steps.sh script: "jq --raw-output '.appVersion' ${script.env.WORKSPACE}/package.json", returnStdout: true
    result=result.trim()
    return result
  }

  def getApplicationBuildVersionForNativeScript(){
    def result = steps.sh script: "jq --raw-output '.buildVersion' ${script.env.WORKSPACE}/package.json", returnStdout: true
    result=result.trim()
    return result
  }

  def getApplicationNextReleaseVersion(version){
    def isSnapshotVersion=isSnapshotVersion(version)
    steps.echo "Snapshot version: ${isSnapshotVersion}"
    def result
    def snapshotPattern="-SNAPSHOT"
    def newWorkingVersion
    def nextVersion
    if(isSnapshotVersion=="true"){
        steps.echo "Version is snapshot"
        newWorkingVersion=steps.sh script: """currentVersion=${version}
        echo \${currentVersion%${snapshotPattern}}""", returnStdout: true
        newWorkingVersion=newWorkingVersion.trim()
        nextVersion=newWorkingVersion
        steps.echo "New working version: ${newWorkingVersion}"
    }else{
        throw new Exception("Version is not snapshot. The release version should be done on snapshot version.")
    }
    return nextVersion
  }

  def getApplicationNextDevelopmentVersion(version){
    def isSnapshotVersion=isSnapshotVersion(version)
    steps.echo "Snapshot version: ${isSnapshotVersion}"
    def result
    def snapshotPattern="-SNAPSHOT"
    def newWorkingVersion
    def nextVersion
    if(isSnapshotVersion=="true"){
        throw new Exception("Version is snapshot. The development version should be done on release version.")
    }else{
        nextVersion=increaseVersion(version)
        nextVersion=nextVersion+snapshotPattern
    }
    return nextVersion
  }

  def increaseVersion(String currentVersion){
    def newReleaseVersion=steps.sh script: """
    # Accepts a version string and prints it incremented by one.
    # Usage: increment_version <version> [<position>] [<leftmost>]
    increment_version() {
       local usage=" USAGE: \$FUNCNAME [-l] [-t] <version> [<position>] [<leftmost>]
               -l : remove leading zeros
               -t : drop trailing zeros
        <version> : The version string.
       <position> : Optional. The position (starting with one) of the number
                    within <version> to increment.  If the position does not
                    exist, it will be created.  Defaults to last position.
       <leftmost> : The leftmost position that can be incremented.  If does not
                    exist, position will be created.  This right-padding will
                    occur even to right of <position>, unless passed the -t flag."

       # Get flags.
       local flag_remove_leading_zeros=0
       local flag_drop_trailing_zeros=0
       while [ "\${1:0:1}" == "-" ]; do
          if [ "\$1" == "--" ]; then shift; break
          elif [ "\$1" == "-l" ]; then flag_remove_leading_zeros=1
          elif [ "\$1" == "-t" ]; then flag_drop_trailing_zeros=1
          else echo -e "Invalid flag: \${1}\n\$usage"; return 1; fi
          shift; done

       # Get arguments.
       if [ \${#@} -lt 1 ]; then echo "\$usage"; return 1; fi
       local v="\${1}"             # version string
       local targetPos=\${2-last}  # target position
       local minPos=\${3-\${2-0}}   # minimum position

       # Split version string into array using its periods.
       local IFSbak; IFSbak=IFS; IFS='.' # IFS restored at end of func to
       read -ra v <<< "\$v"               #  avoid breaking other scripts.

       # Determine target position.
       if [ "\${targetPos}" == "last" ]; then
          if [ "\${minPos}" == "last" ]; then minPos=0; fi
          targetPos=\$((\${#v[@]}>\${minPos}?\${#v[@]}:\$minPos)); fi
       if [[ ! \${targetPos} -gt 0 ]]; then
          echo -e "Invalid position: '\$targetPos'\n\$usage"; return 1; fi
       (( targetPos--  )) || true # offset to match array index

       # Make sure minPosition exists.
       while [ \${#v[@]} -lt \${minPos} ]; do v+=("0"); done;

       # Increment target position.
       v[\$targetPos]=`printf %0\${#v[\$targetPos]}d \$((10#\${v[\$targetPos]}+1))`;

       # Remove leading zeros, if -l flag passed.
       if [ \$flag_remove_leading_zeros == 1 ]; then
          for (( pos=0; \$pos<\${#v[@]}; pos++ )); do
             v[\$pos]=\$((\${v[\$pos]}*1)); done; fi

       # If targetPosition was not at end of array, reset following positions to
       #   zero (or remove them if -t flag was passed).
       if [[ \${flag_drop_trailing_zeros} -eq "1" ]]; then
            for (( p=\$((\${#v[@]}-1)); \$p>\$targetPos; p-- )); do unset v[\$p]; done
       else for (( p=\$((\${#v[@]}-1)); \$p>\$targetPos; p-- )); do v[\$p]=0; done; fi

       echo "\${v[*]}"
       IFS=IFSbak
       return 0
    }
    increment_version ${currentVersion}
    """, returnStdout: true
    newReleaseVersion=newReleaseVersion.trim()
    steps.echo "New release version ${newReleaseVersion}"
    return newReleaseVersion
  }

  def isSnapshotVersion(String version){
    def snapshotPattern="-- -SNAPSHOT"
    def command="echo \"${version}\" | grep ${snapshotPattern}"
    def statusCode =  steps.sh script:"${command}", returnStatus:true
    if(statusCode==1){
        return "false"
    }
    def containsSnapshot = steps.sh script: "${command}", returnStdout: true
    containsSnapshot=containsSnapshot.trim()
    steps.echo "containsSnapshot: ${containsSnapshot}"

    def isSnapshot = steps.sh script: "[ \${#containsSnapshot} -ge 0 ] && echo 'true' || echo 'false'", returnStdout: true
    isSnapshot=isSnapshot.trim()
    return isSnapshot
  }

  /* DEPRECATED method, use getApplicationName */
  def getProjectName(){
    def result = steps.sh script: "'${mvnHome}/bin/mvn' -q -N org.codehaus.mojo:exec-maven-plugin:1.3.1:exec -Dexec.executable='echo' -Dexec.args='\${project.artifactId}'", returnStdout: true
    result=result.trim()
    return result
  }

  public String getApplicationName(){
    return this.getInstanceMavenJenkinsUtil().getApplicationName()
  }

  // Get name for Android/Gradle project
  def getApplicationNameGradle(){
    def result = steps.sh script: "cat gradle.properties | grep ^description= | awk -F= '\$1==\"description\" {print \$2}'", returnStdout: true
    result=result.trim()
    return result
  }

  // Get name for Java/Gradle project
  def getApplicationNameJavaGradle(){
    def result = steps.sh script: "cat gradle.properties | grep ^artifactId= | awk -F= '\$1==\"artifactId\" {print \$2}'", returnStdout: true
    result=result.trim()
    return result
  }

  def getApplicationNameFreeStyle(){
    return this.getInstanceFreeStyleJenkinsUtil().getApplicationName();
  }

  def getApplicationNameForSalesForce(){
    def result = getGitProjectName()
    return result
  }

  def getApplicationNameNode(){
    def result = steps.sh script: "jq --raw-output '.name' ${script.env.WORKSPACE}/package.json", returnStdout: true
    result=result.trim()
    return result
  }

  // Get group id for NativeScript project
  def getGroupIdNativeScript(){
    def result = steps.sh script: "jq --raw-output '.groupId' ${script.env.WORKSPACE}/package.json", returnStdout: true
    result=result.trim()
    return result
  }

  // Get name for NativeScript project
  def getApplicationNameNativeScript(){
    def result = steps.sh script: "jq --raw-output '.name' ${script.env.WORKSPACE}/package.json", returnStdout: true
    result=result.trim()
    return result
  }

  // Get Id for NativeScript project
  def getApplicationIdNativeScript(){
    def result = steps.sh script: "jq --raw-output '.nativescript.id' ${script.env.WORKSPACE}/package.json", returnStdout: true
    result=result.trim()
    return result
  }

  def getApplicationNameIOS(){
    def result = steps.sh script: "echo '${iosAppIdentifier}'", returnStdout: true
    result=result.trim()
    return result
  }

  def getGroupId(){
    return getInstanceMavenJenkinsUtil().getGroupId()
  }

  def getGroupIdGradle(){
    def result = steps.sh script: "cat gradle.properties | grep ^group= | awk -F= '\$1==\"group\" {print \$2}'", returnStdout: true
    result=result.trim()
    return result
  }

  def getMavenArtifactPath(String pomFolderPath){
    return getInstanceMavenJenkinsUtil().getMavenArtifactPath(pomFolderPath)
  }

  def getMavenArtifactReleasePath(String pomFolderPath){
    return getMavenArtifactPath("/target/checkout/${pomFolderPath}")
  }

  /*
  * Method to Fix or Review
  */
  def getMavenArtifactReleaseVersion(){
    return getInstanceMavenJenkinsUtil().getMavenArtifactReleaseVersion()
  }

  def getDexGuardParameters(String inJars,String outJars,String libraryJars){
    def result = """-injars ${inJars}(!META-INF/*)
-outjars ${outJars}
-libraryjars ${libraryJars}
-include /lib/dexguard-release.pro
-printusage   dexguard-${script.env.BUILD_NUMBER}/mapping/usage.txt
-printmapping dexguard-${script.env.BUILD_NUMBER}/mapping/mapping.txt
-printseeds   dexguard-${script.env.BUILD_NUMBER}/mapping/seeds.txt
-include dexguard-configuration.txt"""
    result=result.trim()
    return result
  }

  def getSalesforceAntBuildXml(){
    def result = """<project name=\"Deploy Salesforce Ant tasks\" default=\"deployZip\" basedir=\".\" xmlns:sf=\"antlib:com.salesforce\">

    <property file=\"build.properties\"/>
    <property environment=\"env\"/>

    <!-- Setting default value for username, password and session id properties to empty string
         so unset values are treated as empty. Without this, ant expressions such as \${sf.username}
         will be treated literally.
    -->
    <condition property=\"sf.username\" value=\"\"> <not> <isset property=\"sf.username\"/> </not> </condition>
    <condition property=\"sf.password\" value=\"\"> <not> <isset property=\"sf.password\"/> </not> </condition>
    <condition property=\"sf.sessionId\" value=\"\"> <not> <isset property=\"sf.sessionId\"/> </not> </condition>

    <taskdef resource=\"com/salesforce/antlib.xml\" uri=\"antlib:com.salesforce\">
        <classpath>
            <pathelement location=\"/opt/salesforces/ant-salesforce.jar\" />
        </classpath>
    </taskdef>

    <!-- Test out deploy and retrieve verbs for package 'mypkg' -->
      <!-- Deploy a zip of metadata files to the org -->
    <target name=\"deployZip\" depends=\"proxy\">
      <sf:deploy username=\"\${sf.username}\" password=\"\${sf.password}\" sessionId=\"\${sf.sessionId}\" serverurl=\"\${sf.serverurl}\"
        maxPoll=\"\${sf.maxPoll}\" zipFile=\"\${sf.zipFile}\" pollWaitMillis=\"1000\" deployRoot=\"src\" rollbackOnError=\"true\" testLevel=\"RunLocalTests\"/>
    </target>

    <target name=\"cancelDeploy\" depends=\"proxy\">
      <sf:cancelDeploy username=\"\${sf.username}\" password=\"\${sf.password}\"
          sessionId=\"\${sf.sessionId}\" serverurl=\"\${sf.serverurl}\" maxPoll=\"\${sf.maxPoll}\"
          requestId=\"\${sf.deployRequestId}\"/>
    </target>

	<target name=\"proxy\">
			<property name=\"proxy.host\" value=\"127.0.0.1\" />
			<property name=\"proxy.port\" value=\"3128\" />
			<property name=\"proxy.user\" value=\"\" />
			<property name=\"proxy.pwd\" value=\"\" />
			<setproxy proxyhost=\"\${proxy.host}\" proxyport=\"\${proxy.port}\" proxyuser=\"\${proxy.user}\" proxypassword=\"\${proxy.pwd}\" />
	</target>

  </project>"""
    return result
  }

  def getArtifactoryBuildInfoByList(String user,String projectKeyName,String applicationName,String applicationVersion,String buildtimestamp,String startedTimeStamp,def artifactPackageList,def artifactSha1List,def artifactMd5List){
    def artifactRequest=""
    int artifactListSize=artifactPackageList.size()
    artifactPackageList.eachWithIndex { it, i ->
      def artifact = """
      {
        "type" : "${artifactPackageList[i]}",
        "sha1" : "${artifactSha1List[i]}",
        "md5" : "${artifactMd5List[i]}",
        "name" : "${applicationName}-${applicationVersion}.${artifactPackageList[i]}"
      }"""
      if(artifactListSize>0&&artifactListSize-1!=i){
        artifact+=","
      }
      artifactRequest+=artifact
    }

    def result = """{
  "version" : "1.0",
  "name" : "${projectKeyName}-${applicationName}",
  "number" : "${buildtimestamp}",
  "type" : "GENERIC",
  "agent" : {
    "name" : "Jenkins"
  },
  "started" : "${startedTimeStamp}",
  "artifactoryPrincipal" : "${user}",
  "url" : "${script.env.BUILD_URL}",
  "vcsRevision" : "${currentGitCommit}",
  "vcsUrl" : "${currentGitURL}",
  "modules" : [ {
    "properties" : {
      "project.build.sourceEncoding" : "UTF-8"
    },
    "id" : "${applicationName}:${applicationVersion}",
    "artifacts" : [ ${artifactRequest} ]
  } ]
}
""".stripIndent()
    return result
  }

  def getArtifactoryBuildInfo(String user,String projectKeyName,String applicationName,String applicationVersion,String buildtimestamp,String startedTimeStamp,String artifactType,String artifactSha1,String artifactMd5){
    def result = """{
  "version" : "1.0",
  "name" : "${projectKeyName}-${applicationName}",
  "number" : "${buildtimestamp}",
  "type" : "GENERIC",
  "agent" : {
    "name" : "Jenkins"
  },
  "started" : "${startedTimeStamp}",
  "artifactoryPrincipal" : "${user}",
  "url" : "${script.env.BUILD_URL}",
  "vcsRevision" : "${currentGitCommit}",
  "vcsUrl" : "${currentGitURL}",
  "modules" : [ {
    "properties" : {
      "project.build.sourceEncoding" : "UTF-8"
    },
    "id" : "${applicationName}:${applicationVersion}",
    "artifacts" : [ {
      "type" : "${artifactType}",
      "sha1" : "${artifactSha1}",
      "md5" : "${artifactMd5}",
      "name" : "${applicationName}-${applicationVersion}.${artifactType}"
    }]
  } ]
}
""".stripIndent()
    return result
  }

    def getArtifactoryBuildInfoEscaped(String user,String schema,String projectKeyName,String applicationName,String applicationVersion,String buildtimestamp,String startedTimeStamp,String artifactType,String artifactSha1,String artifactMd5){
    def result = """{
  \\\"version\\\" : \\\"1.0\\\",
  \\\"name\\\" : \\\"${projectKeyName}-${applicationName}\\\",
  \\\"number\\\" : \\\"${buildtimestamp}\\\",
  \\\"type\\\" : \\\"GENERIC\\\",
  \\\"agent\\\" : {
    \\\"name\\\" : \\\"Jenkins\\\"
  },
  \\\"started\\\" : \\\"${startedTimeStamp}\\\",
  \\\"artifactoryPrincipal\\\" : \\\"${user}\\\",
  \\\"url\\\" : \\\"${script.env.BUILD_URL}\\\",
  \\\"vcsRevision\\\" : \\\"${currentGitCommit}\\\",
  \\\"vcsUrl\\\" : \\\"${currentGitURL}\\\",
  \\\"modules\\\" : [ {
    \\\"properties\\\" : {
      \\\"project.build.sourceEncoding\\\" : \\\"UTF-8\\\",
      \\\"project.ios.build.schema\\\" : \\\"${schema}\\\"
    },
    \\\"id\\\" : \\\"${applicationName}:${applicationVersion}\\\",
    \\\"artifacts\\\" : [ {
      \\\"type\\\" : \\\"${artifactType}\\\",
      \\\"sha1\\\" : \\\"${artifactSha1}\\\",
      \\\"md5\\\" : \\\"${artifactMd5}\\\",
      \\\"name\\\" : \\\"${applicationName}-${applicationVersion}.${artifactType}\\\"
    }]
  } ]
}
""".stripIndent()
    return result
  }

  def setGradleVersion(String gradleVersion){
    if("${gradle351Java8Version}"=="${gradleVersion}" || "${gradle351Java8VersionShort}"=="${gradleVersion}" ){
	    dockerGradleJavaEnviroment=dockerGradle351Java8Enviroment
    }else if("${gradle481Java8Version}"=="${gradleVersion}" || "${gradle481Java8VersionShort}"=="${gradleVersion}" ){
      dockerGradleJavaEnviroment=dockerGradle481Java8Enviroment
    }else if("${gradle56Java8Version}"=="${gradleVersion}" || "${gradle56Java8VersionShort}"=="${gradleVersion}" ){
      dockerGradleJavaEnviroment=dockerGradle56Java8Enviroment
      dockerFortifyGradleRunner=dockerFortifyGradle56
    }else{
      throw new Exception("Not supported version: "+gradleVersion)
    }

    steps.echo "******** GRADLE IS SET TO GRADLE: "+gradleVersion+" ********"
    steps.echo "******** ENVIROMENT SET TO: "+dockerGradleJavaEnviroment+" ********"
  }

  def setAndroidGradleVersion(String gradleVersion){
    getInstanceGradleAndroidJenkinsUtil().setAndroidGradleVersion(gradleVersion)
  }

  private GradleAndroidJenkinsUtil getInstanceGradleAndroidJenkinsUtil() {
    if (this.gradleAndroidJenkinsUtil == null) {
      this.gradleAndroidJenkinsUtil = new GradleAndroidJenkinsUtil(
        script,
        docker,
        buildTimestamp,
        currentGitURL,
        currentCredentialsId,
        branchName,
        fortifyHost,
        currentGitCommit,
        buildUser,
        buildUserId
      )
    }

    return this.gradleAndroidJenkinsUtil;
  }

  def setMacServerAgent(String macServerAgent){
    steps.echo "******** MAC SERVER AGENT: "+macServerAgent+" ********"
    this.macServerAgent=macServerAgent
  }

  def setXcodeWorkspace(String xcodeWorkspace){
    steps.echo "******** XCODE WORKSPACE: "+xcodeWorkspace+" ********"
    this.xcodeWorkspace=xcodeWorkspace
  }

  def setNodeAngularVersion(String nodeAngularVersion){
    if("${node6Angular4Version}"=="${nodeAngularVersion}"){
	  dockerNodeAngularEnviroment = dockerNode6Angular4Enviroment
	}else if("${node6Angular5Version}"=="${nodeAngularVersion}"){
	  dockerNodeAngularEnviroment = dockerNode6Angular5Enviroment
	}else if("${node6Angular6Version}"=="${nodeAngularVersion}"){
	  dockerNodeAngularEnviroment = dockerNode6Angular6Enviroment
  }else if("${node10Angular7Version}"=="${nodeAngularVersion}"){
    dockerNodeAngularEnviroment = dockerNode10Angular7Enviroment
	}else{
	  throw new Exception("Not supported version: "+nodeAngularVersion)
	}
    steps.echo "******** ANGULAR VERSION IS SET TO: "+nodeAngularVersion+" ********"
    steps.echo "******** ENVIROMENT SET TO: "+dockerNodeAngularEnviroment+" ********"
  }

  def setMavenJavaVersion(String mavenJavaVersion){
    getInstanceMavenJenkinsUtil().setMavenJavaVersion(mavenJavaVersion)
  }


  def setIdVersionFortify(String versionFortify){
	  idVerFortify = versionFortify
  }

  def setFolderToAnalyze(String toAnalyze){
	  folderAnalyze = toAnalyze
  }

  def setNativeScriptVersion(String nativeScriptVersion){
    if("${nativeScript343Gradle410Android28Node8Version}"=="${nativeScriptVersion}"){
      dockerNativeScriptEnviroment=dockerNativeScript343Gradle410Android28Node8Enviroment
      macServerNodePath=macServerNode8113Path
      macServerNativeScriptPath=macServerNativeScript343Path
    }else if("${nativeScript511Gradle410Android28Node8Version}"=="${nativeScriptVersion}"){
      dockerNativeScriptEnviroment=dockerNativeScript511Gradle410Android28Node8Enviroment
      macServerNodePath=macServerNode8113Path
      macServerNativeScriptPath=macServerNativeScript511Path
    }else if("${nativeScript542Gradle410Android28Node8Version}"=="${nativeScriptVersion}"){
      dockerNativeScriptEnviroment=dockerNativeScript542Gradle410Android28Node8Enviroment
      macServerNodePath=macServerNode8113Path
      macServerNativeScriptPath=macServerNativeScript542Path
    }else{
      throw new Exception("Not supported version: "+nativeScriptVersion)
    }
    macServerRvmPath="~/.rvm/bin"
    macServerRubyPath="~/.rvm/rubies/ruby-2.3.0/bin"
    macServerGemPath="~/.rvm/gems/ruby-2.3.0"

    steps.echo "******** MAC SERVER RVM SET TO: "+macServerRvmPath+" ********"
    steps.echo "******** MAC SERVER RUBY SET TO: "+macServerRubyPath+" ********"
    steps.echo "******** MAC SERVER GEM SET TO: "+macServerGemPath+" ********"

    steps.echo "******** NATIVESCRIPT VERSION IS SET TO: "+nativeScriptVersion+" ********"
    steps.echo "******** ENVIROMENT CONTAINER SET TO: "+dockerNativeScriptEnviroment+" ********"
    steps.echo "******** ENVIROMENT NODE MAC SET TO: "+macServerNodePath+" ********"
    steps.echo "******** ENVIROMENT NATIVESCRIPT MAC SET TO: "+macServerNativeScriptPath+" ********"
    nativeScriptVersionSet=true
  }

  def getSettingsMavenInitScript(){
    return this.getInstanceMavenJenkinsUtil().getSettingsInitScript()
  }

  def getGradleInitScript(){
    def initScript = """apply plugin:EnterpriseRepositoryPlugin

	allprojects {
    repositories {
        maven {
            url = '${artifact_m2_registry_url}'
        }
    }
    buildscript {
        repositories {
            maven {
                url = '${artifact_m2_registry_url}'
            }
        }
    }
	}

	class EnterpriseRepositoryPlugin implements Plugin<Gradle> {

		private static String ENTERPRISE_REPOSITORY_URL = "${artifact_m2_registry_url}"

		void apply(Gradle gradle) {
			// ONLY USE ENTERPRISE REPO FOR DEPENDENCIES
			gradle.allprojects{ project ->
        project.logger.lifecycle "Applying proxy gradle"
				project.buildscript.repositories {

					// Remove all repositories not pointing to the enterprise repository url
					all { ArtifactRepository repo ->

						if ((repo instanceof MavenArtifactRepository) &&
							repo.url.toString() != ENTERPRISE_REPOSITORY_URL) {

                            if(!repo.url.toString().startsWith("file:/opt/maven-local-repo/")&&
                                 !repo.url.toString().startsWith("file:/project/")&&
                                 !repo.url.toString().startsWith("file:/opt/android-sdk-linux/")&&
                                 !repo.url.toString().startsWith("${script.env.ARTIFACT_SERVER_URL}")&&
                                 !repo.url.toString().startsWith("${artefactoryUrl}")
                                 ){
    							  project.logger.lifecycle "Repository \${repo.url} removed. Only \$ENTERPRISE_REPOSITORY_URL is allowed"
    							  remove repo
                            }

						}
					}


					// add the enterprise repository
					maven {
						name "STANDARD_ENTERPRISE_REPO"
						url ENTERPRISE_REPOSITORY_URL
					}
				}
				project.repositories {

					// Remove all repositories not pointing to the enterprise repository url
					all { ArtifactRepository repo ->
						if ((repo instanceof MavenArtifactRepository) &&
							repo.url.toString() != ENTERPRISE_REPOSITORY_URL) {

                            if(!repo.url.toString().startsWith("file:/opt/maven-local-repo/")&&
                                 !repo.url.toString().startsWith("file:/project/")&&
                                 !repo.url.toString().startsWith("file:/opt/android-sdk-linux/")&&
                                 !repo.url.toString().startsWith("${script.env.ARTIFACT_SERVER_URL}")&&
                                 !repo.url.toString().startsWith("${artefactoryUrl}")
                                 ){
    							  project.logger.lifecycle "Repository \${repo.url} removed. Only \$ENTERPRISE_REPOSITORY_URL is allowed"
    							  remove repo
                            }

						}
					}


					// add the enterprise repository
					maven {
						name "STANDARD_ENTERPRISE_REPO"
						url ENTERPRISE_REPOSITORY_URL
					}
				}
			}
		}
	}
	""".stripIndent()
	return initScript
  }

  def getKarmaConfJS(){
    def initScript = """// Karma configuration file, see link for more information
// https://karma-runner.github.io/0.13/config/configuration-file.html

module.exports = function (config) {
  let options = {
    basePath: '',
    frameworks: ['jasmine', '@angular/cli'],
    plugins: [
      require('karma-jasmine'),
      require('karma-phantomjs-launcher'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-spec-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular/cli/plugins/karma'),

      require('karma-junit-reporter')

    ],
    client:{
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    files: [
      { pattern: './src/test.ts', watched: false }
    ],
    preprocessors: {
      './src/test.ts': ['@angular/cli']
    },
    mime: {
      'text/x-typescript': ['ts','tsx']
    },
    coverageIstanbulReporter: {
      reports: [ 'html', 'lcovonly' ],
      fixWebpackSourcePaths: true
    },
    angularCli: {
      environment: 'dev'
    },
    reporters: config.angularCli && config.angularCli.codeCoverage
              ? ['spec','junit', 'coverage-istanbul']
              : ['spec','junit', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['PhantomJS'],
    singleRun: false,


    customLaunchers: {
          ChromeNoSandbox: {
            base: 'Chrome',
            flags: ['--no-sandbox', '--headless', '--disable-gpu', '--remote-debugging-port=9222']
          }
        }

    ,junitReporter: {
      outputDir: 'tests_out', // results will be saved as \$outputDir/\$browserName.xml
      outputFile: 'test_jasmine_js_unit.xml', // if included, results will be saved as \$outputDir/\$browserName/\$outputFile
      suite: '', // suite will become the package name attribute in xml testsuite element
      useBrowserName: true, // add browser name to report and classes names
      nameFormatter: undefined, // function (browser, result) to customize the name attribute in xml testcase element
      classNameFormatter: undefined, // function (browser, result) to customize the classname attribute in xml testcase element
      properties: {} // key value pair of properties to add to the section of the report
    }

  };
  if (process.env.TRAVIS) {
    options.singleRun = true;
    options.browsers = [ 'Chrome' ];
  }
  config.set(options);
};
""".stripIndent()
	return initScript
  }

  def configureProvisionalProfile(String teamId, String appIdentifierParam,String plistPath,String provisionalProfileName="") {
  	 iosTeamId=teamId
	 iosAppIdentifier=appIdentifierParam
	 iosProvisionalProfileName=provisionalProfileName
	 iosPlistPath=plistPath
	 steps.echo "iosTeamId: ${iosTeamId}"
	 steps.echo "iosAppIdentifier: ${iosAppIdentifier}"
	 steps.echo "iosProvisionalProfileName: ${iosProvisionalProfileName}"
	 steps.echo "iosPlistPath: ${iosPlistPath}"
  }

  /*
  * This method configure provisional profile using fastlane
  */
  def configureProvisionalProfileExecution(boolean development=true, String applicationName, String enviroment="", boolean updateTeamId=true) {
     def proxy=steps.sh(script: """echo \${http_proxy}""", returnStdout: true).trim()
     def proxyEnviroment=""
     if(proxy!=null&&proxy!=""){
      proxyEnviroment="export HTTP_PROXY=${proxy} && HTTPS_PROXY=${proxy} && http_proxy=${proxy} && htts_proxy=${proxy} && "
     }

     def macServerRvmPath="${macServerRvmPath}"
     def macServerGemPath="${macServerGemPath}"
     def macServerGemExecutablePath="${macServerGemExecutablePath}"
     def macServerRubyPath="${macServerRubyPath}"
     steps.echo "macServerGemExecutablePath: ${macServerGemExecutablePath}"
     steps.echo "macServerRubyPath: ${macServerRubyPath}"
     def temporalBinPath = ""
     if(macServerRvmPath!=null&&!(macServerRvmPath==""||macServerRvmPath=="null")){
       steps.echo "macServerRvmPath: ${macServerRvmPath}"
       temporalBinPath+="${macServerRvmPath}:"
     }
     temporalBinPath+="${macServerRubyPath}:${macServerGemExecutablePath}"
     steps.echo "temporalBinPath: ${temporalBinPath}"
     def paths=""
     if(macServerGemPath!=null&&!(macServerGemPath==""||macServerGemPath=="null")){
       steps.echo "macServerGemPath: ${macServerGemPath}"
       paths+="export GEM_PATH=${macServerGemPath} && "
     }
     paths+="export PATH=${temporalBinPath}:\$PATH &&"
     steps.echo "paths: ${paths}"
     steps.echo "proxyEnviroment: ${proxyEnviroment}"

     steps.sh "printenv"
     steps.sh "id"
     def buildTimestamp=getBuildTimestamp()
     def iosDistributionP12SecretFileKey="${script.env.project}".toLowerCase()+"-${applicationName}-ios-codesign-private-key-p12-file"
     def iosDistributionP12PasswordSecretKey="${script.env.project}".toLowerCase()+"-${applicationName}-ios-codesign-private-key-p12-file-password"
     def appleIdCredentials="${script.env.project}".toLowerCase()+"-${applicationName}-appleid-credentials"

     steps.echo """************ Configuring Provisional Profile ************
NOTE: - The credentials should be populate on project folder credentials
IOS CodeSign Private Key P12(SecretFile): ${iosDistributionP12SecretFileKey}
IOS CodeSign Private Key P12 Password(SecretText): ${iosDistributionP12PasswordSecretKey}
Apple Id Credentials(UserPassword): ${appleIdCredentials}

For unlock keychain:
Mac Server Keychain password(SecretText): macserver-keychain-password
"""

     steps.withCredentials([
          [$class: "FileBinding", credentialsId: "${iosDistributionP12SecretFileKey}", variable: "iosDistributionP12SecretFilePath"],
          [$class: "StringBinding", credentialsId: "macserver-keychain-password", variable: "keychainPassword" ],
          [$class: "StringBinding", credentialsId: "${iosDistributionP12PasswordSecretKey}", variable: "iosDistributionP12PasswordSecretValue" ],
          [$class: 'UsernamePasswordMultiBinding', credentialsId: "${appleIdCredentials}",
                          usernameVariable: 'appleIdUser', passwordVariable: 'appleIdPassword']
          ]) {

        steps.sh "mkdir -p /opt/jenkins-slave/certs"
        def keychainPath="/opt/jenkins-slave/certs/jks-${script.env.project}-${applicationName}-${enviroment}.keychain-db".toLowerCase()
        def certPath="/opt/jenkins-slave/certs"

        steps.sh "security list-keychains"
        steps.sh "ls \"\${HOME}/Library/MobileDevice/Provisioning Profiles\""
        def fliendlyName=steps.sh(script: """set +x
openssl pkcs12 -info -in "${script.env.iosDistributionP12SecretFilePath}" -password pass:${script.env.iosDistributionP12PasswordSecretValue} -nodes | grep friendlyName | head -n1
""", returnStdout: true).trim()
        def credentialId=""
        fliendlyName=fliendlyName.split(':')
        steps.echo "fliendlyName: ${fliendlyName}"
        if(fliendlyName.size()>1){
          def credential = "${fliendlyName[2]}"
          steps.echo "credential: ${credential}"
          def credentialIdTemp = (credential =~ /\(\w*\)/)
          steps.echo "credentialIdTemp: ${credentialIdTemp}"
          if(credentialIdTemp.size()>0){
            credentialIdTemp = "${credentialIdTemp[0]}"
            steps.echo "credentialIdTemp: ${credentialIdTemp}"
            if(credentialIdTemp.indexOf("(")>0){
              credentialId = credentialIdTemp.replace("(","")
              credentialId = credentialId.replace(")","")
              steps.echo "credentialId: ${credentialId}"
            }
          }
        }

        def credentialExistTemp=""
        if(credentialId!=""){
          credentialExistTemp=steps.sh(script: "security find-identity -v -p codesigning | grep ${credentialId} | true", returnStdout: true).trim()
        }
        steps.echo "credentialExistTemp: ${credentialExistTemp}"

        def keychainExists=steps.sh(script: """if [ -f "${keychainPath}" ]; then
echo "exist"
fi""", returnStdout: true).trim()

        if(keychainExists!="exist"){
        	steps.sh "${paths} ${proxyEnviroment} export LC_ALL=en_US.UTF-8 && export LANG=en_US.UTF-8 && fastlane run create_keychain password:\"${script.env.keychainPassword}\" path:\"${keychainPath}\" unlock:true timeout:false"
        }else{
          steps.sh "security -v unlock-keychain -p \"${script.env.keychainPassword}\" \"${keychainPath}\""
        }

        if(credentialExistTemp==""){
          steps.sh "security import \"${script.env.iosDistributionP12SecretFilePath}\" -k \"${keychainPath}\" -P \"${script.env.iosDistributionP12PasswordSecretValue}\" -A "
        }

        def developmentParam=" "
        if(development|enviroment=="CERT"){
          //developmentParam=" --development "
          developmentParam=" --adhoc "
 		}

        script.echo(script.env.getEnvironment().collect({environmentVariable ->  "${environmentVariable.key} = ${environmentVariable.value}"}).join("\n"))
        def passwordEscaped="${script.env.appleIdPassword}"

        def credentialIdCmd=""
        if(credentialId!=""){
          credentialIdCmd=" --cert_id ${credentialId}"
        }
        //TOFIX: Check support for GEM/Python/RVM for IOS Native
        steps.sh "${paths} ${proxyEnviroment} export LC_ALL=en_US.UTF-8 && export LANG=en_US.UTF-8 && export FASTLANE_TEAM_ID=${iosTeamId} && export FASTLANE_PASSWORD=${passwordEscaped} && fastlane sigh  --skip_certificate_verification ${credentialIdCmd} --readonly ${developmentParam} -u ${script.env.appleIdUser} --app_identifier \"${iosAppIdentifier}\" --provisioning_name \"${iosProvisionalProfileName}\" -q \"jks-${buildTimestamp}.mobileprovision\" "
        steps.sh "ls -al \"${script.env.WORKSPACE}/jks-${buildTimestamp}.mobileprovision\""
        steps.echo "**** IF MORE THAN 2 CERTIFICATES CHECK DUPLICATE PRIVATE KEY ON lOGIN KEYCHAIN OR SYSTEM ****"
        if(credentialId!=""){
		      steps.sh "security find-identity -v -p codesigning | grep ${credentialId}"
        }else{
        	  steps.sh "security find-identity -v -p codesigning"
        }

        steps.sh "security set-key-partition-list -S apple-tool:,apple:,codesign:,xcodebuild: -s -k ${script.env.keychainPassword} \"${keychainPath}\" "

        if(updateTeamId){
            def xcodeProjectName=steps.sh(script: "ls -d *.xcodeproj", returnStdout: true).trim()
            steps.sh "${paths} ${proxyEnviroment} export LC_ALL=en_US.UTF-8 && export LANG=en_US.UTF-8 && fastlane run update_project_team teamid:\"${iosTeamId}\" path:\"${script.env.WORKSPACE}/${xcodeProjectName}\" "
        }
     }
  }

  /*
  * This method compile the doce from maven
  */
  def buildFreeStyle(String relativeSrcPath = '',String packageType='zip') {
    FreeStyleJenkinsUtil freeStyleInstance = this.getInstanceFreeStyleJenkinsUtil()
    freeStyleInstance.setRelativeSrcPath(relativeSrcPath)
    freeStyleInstance.setPackageType(packageType)
    freeStyleInstance.build();
  }

  /*
  * This method compile the artifact for salesforces projects
  */
  def buildForSalesForce(){
     def applicationName=getApplicationNameForSalesForce()
     def applicationVersion=getApplicationVersionForSalesForce()
     // Package the builds
     steps.echo "Building a zip artifact"
     def comment="ic: JENKINS build-url: ${script.env.BUILD_URL} branch: ${branchName} commit: ${currentGitCommit} date: ${buildTimestamp}."
     steps.sh "echo ${comment} > comments-jks-${buildTimestamp}"
     //-x .git/\\* -x devops/\\* -x version -J comments-jks-*
     steps.sh "zip -v -x .git/\\* -x devops/\\* -x version -z -x comments-jks-${buildTimestamp} -r ${script.env.WORKSPACE}/${applicationName}-${applicationVersion}.zip . < comments-jks-${buildTimestamp}"
     steps.sh "ls -al ${applicationName}-${applicationVersion}.zip"
  }

  /*
  * This method compile the doce from maven
  */
  def buildMaven(String buildParameters = '', boolean failNever=true) {
    MavenJenkinsUtil mavenJenkinsUtil = this.getInstanceMavenJenkinsUtil()
    mavenJenkinsUtil.setFailNever(failNever)
    mavenJenkinsUtil.build(buildParameters)
  }

  /***** AGREGADO ATLAS downloadGitRepo *****/
    def downloadGitRepo(String project,String repoName,String repoId,String gitAlternativeCredential,String repoType="branch"){
          if("${project}"=="" || "${repoName}"=="" || "${repoId}"=="" ||  "${gitAlternativeCredential}"==""){
              steps.error("""
                  **************************************************************************************************************

                  * Parameter project must be a string representing a project from bitbucket (atla, ciam).
                  * Parameter repoName must be a string representing a bitbucket repository inside a project.
                  * Parameter repoId must be a string representing a Branch (mybranch) or a String representing a tag (1.5.1).

                  **************************************************************************************************************
              """)
          }

          def deliveryRepo = "${bitbucketUrl}/rest/api/latest/projects/${project}/repos/${repoName}/archive?at=refs%2Fheads%2F${repoId}"

          if("${repoType}"=="tag"){
              deliveryRepo = "${bitbucketUrl}/rest/api/latest/projects/${project}/repos/${repoName}/archive?at=refs%2Ftags%2F${repoId}"
          } else if("${repoType}"!="branch"){
              steps.error("""
              **************************************************************************************************************

                  * Parameter repoType must be 'branch' or 'tag'

              **************************************************************************************************************
              """)
          }

          steps.withCredentials([
              [$class: 'UsernamePasswordMultiBinding', credentialsId: "${gitAlternativeCredential}", usernameVariable: 'GIT_USERNAME', passwordVariable: 'GIT_PASSWORD']
          ]) {
              steps.sh "curl -s -k -H \"Authorization: Bearer ${script.env.GIT_PASSWORD}\" -o \$(echo ${repoId} | cut -d '/' -f2).zip ${deliveryRepo}"
              steps.sh "unzip -o -q \$(echo ${repoId} | cut -d '/' -f2).zip -d tmp-repository"
          }
      }
  /***** FIN ATLAS downloadGitRepo *****/

  /***** AGREGADO ATLAS getCredentialMap *****/
    def getCredentialMap (Map credentials){
  def credentialBinding = []
  credentials.each {
      key,value ->
        if (value.id != null && value.id.length() > 0 && value.id != ''){
        if(value.type == 'UsernamePasswordMultiBinding'){
            credentialBinding << [
                $class: value.type,
                credentialsId: value.id,
                passwordVariable: value.varpassword,
                usernameVariable: value.varuser
            ]
        } else if(value.type == 'SSHUserPrivateKeyBinding'){
            credentialBinding << [
                $class: value.type,
                credentialsId: value.id,
                keyFileVariable: value.varkey,
                passphraseVariable: value.varpassword,
                usernameVariable: value.varuser
            ]
        } else if(value.type == 'StringBinding' || value.type == 'FileBinding'){
            credentialBinding << [
                $class: value.type,
                credentialsId: value.id,
                variable: value.varkey
            ]
        }
    }
  }
  return credentialBinding
  }
  /***** FIN ATLAS getCredentialMap *****/

  /*
  * This method compile the gradle
  */
  def buildGradle(String buildParameters = '') {
    steps.sh "mkdir -p ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}"
    def httpProxyHost  = "${script.env.DOCKER_HTTP_PROXY_HOST}"
    def httpProxyPort  = "${script.env.DOCKER_HTTP_PROXY_PORT}"
    def dockerVolumen   = "-v ${script.env.APPLICATION_CACHE}/gradle-local-repo:/gradle-home -v ${script.env.WORKSPACE}:/project/data -w /project/data "

    dockerVolumen  += "-v ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}:/gradle-project-cache "

    steps.echo "dockerVolumen: ${dockerVolumen}"
    steps.sh "echo '' > ${script.env.WORKSPACE}/gradle-jenkins"

    steps.writeFile(
      file:"${script.env.WORKSPACE}/gradle-jenkins",
      text: getGradleInitScript()
    )

    docker.withServer("${script.env.DOCKER_HOST}"){
      docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
          def dockerParameters = "--network=host"
          def dockerCmd        = "docker run --rm ${dockerVolumen} ${dockerParameters} ${dockerGradleJavaEnviroment}"
          def gradleCall       = "${dockerCmd} gradle ${buildParameters} install --project-cache-dir /gradle-project-cache --stacktrace -Dorg.gradle.daemon=false --gradle-user-home=/gradle-home --init-script ./gradle-jenkins"

          if(httpProxyHost && httpProxyPort){
            gradleCall+=" -Dhttp.proxyHost=${httpProxyHost} -Dhttp.proxyPort=${httpProxyPort} -Dhttps.proxyHost=${httpProxyHost} -Dhttps.proxyPort=${httpProxyPort}"
          }

          steps.sh "${gradleCall}"
      }
    }

     try {
         steps.step( [ $class: 'JacocoPublisher' ] )
     }
     catch(Exception e) {
         //TODO - To be removed on the new version of plugin 2.2.1 of jacoco, verson 2.2.0 had issues with gitlba plugin
     }
     try {
         steps.junit allowEmptyResults: true, testResults: '**/**/build/test-results/TEST-*.xml'
     }catch(Exception e){
        //TODO: Remove when QA are implemented
     }
  }

  def buildNativeScript(String enviroment,String buildParams = "",String assemblyFlavor = "",boolean signing=false,boolean enabledDexGuardOfuscation=false,boolean enabledJscrambler=false,boolean enabledMutualAuth = false, boolean enabledDynatrace=true, boolean enabledFirebase=true, boolean enabledPseudoMutualAuth=true) {
    if(!nativeScriptVersionSet){
        throw new Exception("NativeScript version have to be set")
    }

    steps.echo "Enviroment: ${enviroment}"
    steps.echo "Build Params: ${buildParams}"
    steps.echo "Assembly Flavor: ${assemblyFlavor}"
    steps.echo "Signing: ${signing}"
    steps.echo "DexGuardOfuscation enabled: ${enabledDexGuardOfuscation}"
    steps.echo "Jscrambler enabled: ${enabledJscrambler}"
    steps.echo "Mutual enabled: ${enabledMutualAuth}"
    steps.echo "Pseudo Mutual enabled: ${enabledPseudoMutualAuth}"

    steps.echo "NativeScript IOS enabled: ${nativeScriptIOSEnabled}"
    steps.echo "NativeScript Android enabled: ${nativeScriptAndroidEnabled}"

    if(enabledMutualAuth==true&&enabledPseudoMutualAuth==true){
        throw new Exception("Two exclusive flags are activated at same time: Pseudomutual and Mutual authentication ")
    }

    if(nativeScriptIOSEnabled){
      steps.echo "Build NativeScript IOS"
      buildNativeScriptIOS(enviroment, buildParams, assemblyFlavor, enabledJscrambler, enabledMutualAuth, enabledFirebase, enabledPseudoMutualAuth)
    }
    if(nativeScriptAndroidEnabled){
      steps.echo "Build NativeScript Android"
      if(signing){
        def projectName="${script.env.project}"
        def projectNameLowercase = projectName.toLowerCase()
        def enviromentLowerCase=enviroment.toLowerCase()
        def applicationName = getApplicationNameNativeScript()
        String keystoreFileIdName="${projectNameLowercase}-${applicationName}-android-keystore-${enviromentLowerCase}"
        String keystorePasswordIdName="${projectNameLowercase}-${applicationName}-android-keystorepassword-${enviromentLowerCase}"
        String keyAliasIdName="${projectNameLowercase}-${applicationName}-android-keyalias-${enviromentLowerCase}"
        String keyPasswordIdName="${projectNameLowercase}-${applicationName}-android-keypassword-${enviromentLowerCase}"
        steps.echo """************ Generating singing properties for APK - BEGIN ************
NOTE: - The credentials should be populate on project folder credentials
Keystore credentials Id(SecretFile): ${keystoreFileIdName}
Trustore password credentials Id(SecretText): ${keystorePasswordIdName}
Key Alias credentials Id(SecretText): ${keyAliasIdName}
Key Password Credentials Id(SecretText): ${keyPasswordIdName}"""
	      steps.withCredentials([
          [$class: "FileBinding", credentialsId: "${keystoreFileIdName}", variable: "keystoreFilePath"],
	    		[$class: "StringBinding", credentialsId: "${keystorePasswordIdName}", variable: "keystorePasswordValue"],
	    		[$class: "StringBinding", credentialsId: "${keyAliasIdName}", variable: "keyAliasValue"],
	    		[$class: "StringBinding", credentialsId: "${keyPasswordIdName}", variable: "keyPasswordValue"]]){
          buildNativeScriptAndroid(enviroment, buildParams, assemblyFlavor,signing,enabledDexGuardOfuscation,enabledJscrambler,enabledMutualAuth, enabledDynatrace, enabledFirebase,enabledPseudoMutualAuth)
        }
      }else{
        buildNativeScriptAndroid(enviroment, buildParams, assemblyFlavor,signing,enabledDexGuardOfuscation,enabledJscrambler,enabledMutualAuth, enabledDynatrace, enabledFirebase,enabledPseudoMutualAuth)
      }
    }
  }

  def buildNativeScriptIOS(String enviroment, String buildParams = "", String assemblyFlavor = "", boolean enabledJscrambler=false, boolean enabledMutualAuth = false, boolean enabledFirebase = true, boolean enabledPseudoMutualAuth = true) {
    def buildTimestamp=getBuildTimestamp()
    steps.stash includes: '**/*', name: 'workspace'
    steps.echo "Mac Server Agent: ${macServerAgent}"
    if(macServerAgent==null){
      throw new Exception("The Mac Server Agent is not set")
    }
    def applicationName = getApplicationNameNativeScript()
    def applicationVersion = getApplicationVersionForNativeScript()
    def buildNumber = getApplicationBuildVersionForNativeScript()
    def projectName="${script.env.project}".toLowerCase()

    /*Config proxy */
    def proxyCredentials="${script.env.project}-${applicationName}-user-proxy-mac-server".toLowerCase()
    steps.echo "Please register the proxy credentials(Username/Password): ${proxyCredentials}"
    def noProxy="${script.env.HTTP_PROXY_NO_PROXY_NATIVESCRIPT}"
    def proxyIp="${script.env.HTTP_PROXY_IP}"
    def proxyPort="${script.env.HTTP_PROXY_PORT}"
    steps.echo "No proxy: ${noProxy}"
    steps.echo "Proxy Ip: ${proxyIp}"
    steps.echo "Proxy port: ${proxyPort}"

    def macServerNodePath="${macServerNodePath}"
    def macServerNativeScriptPath="${macServerNativeScriptPath}"
    def macServerRvmPath="${macServerRvmPath}"
    def macServerGemPath="${macServerGemPath}"
    def macServerGemExecutablePath="${macServerGemExecutablePath}"
    def macServerRubyPath="${macServerRubyPath}"
    steps.echo "macServerNodePath: ${macServerNodePath}"
    steps.echo "macServerNativeScriptPath: ${macServerNativeScriptPath}"
    steps.echo "macServerRvmPath: ${macServerRvmPath}"
    steps.echo "macServerGemPath: ${macServerGemPath}"
    steps.echo "macServerGemExecutablePath: ${macServerGemExecutablePath}"
    steps.echo "macServerRubyPath: ${macServerRubyPath}"


    def temporalBinPath = "${macServerRvmPath}:${macServerRubyPath}:${macServerGemExecutablePath}:${macServerNodePath}:${macServerNativeScriptPath}"
    def paths="export GEM_PATH=${macServerGemPath} && export PATH=${temporalBinPath}:\$PATH &&"

    steps.withCredentials([
      [$class: 'UsernamePasswordMultiBinding', credentialsId:"${proxyCredentials}", usernameVariable: 'proxyUser',passwordVariable:'proxyPassword']
    ]){
        steps.echo "proxyUser: ${script.env.proxyUser}"
        def proxy = "http://${script.env.proxyUser}:${script.env.proxyPassword}@${proxyIp}:${proxyPort}"
        def proxyEnviroment=""
        if(proxyIp!=null&&proxyIp!=""){
          proxyEnviroment="export no_proxy=${noProxy} && export NO_PROXY=${noProxy} && export HTTP_PROXY=${proxy} && HTTPS_PROXY=${proxy} && http_proxy=${proxy} && htts_proxy=${proxy} && "
        }
        if(proxyIp!=null&&proxyIp=="null"){
          proxyEnviroment=""
          proxy=""
          noProxy=""
        }

        paths="${proxyEnviroment} ${paths}"
        steps.echo "paths: ${paths}"

        steps.node(macServerAgent) {
          def fireBaseToken="${script.env.WORKSPACE}/jks-firebase-${buildTimestamp}"
          def firebaseGoogleServicesPath
          try{
            steps.step([$class: 'WsCleanup'])
            steps.echo "Checking Clean Up"
            steps.sh "bash -l -c 'pwd'"
            steps.sh "ls"
            steps.sh "bash -l -c 'ls -la'"
            if(enabledFirebase){
                steps.echo "Please register the Firebase credential (Credential File): ${applicationName}-nativescript-firebase-google-services-plist-token"
                steps.withCredentials([
                  [$class: 'FileBinding', credentialsId: "${applicationName}-nativescript-firebase-google-services-plist-token", variable: 'storeFilePath'],
                ]){
                steps.sh """
                set +x
                cp ${script.env.storeFilePath} ${fireBaseToken}
                """
              }
            }
            steps.withEnv(["LC_ALL=en_US.UTF-8","LANG=en_US.UTF-8"]) {
              steps.unstash 'workspace'
              boolean isDevelopment=true
              def keychainPath="/opt/jenkins-slave/certs/jks.keychain-db"
              def certPath="/opt/jenkins-slave/certs"

              if(enabledFirebase){
                if (steps.fileExists("${script.env.WORKSPACE}/app/App_Resources/iOS")) {
                    firebaseGoogleServicesPath="${script.env.WORKSPACE}/app/App_Resources/iOS/GoogleService-Info.plist"
                } else {
                    firebaseGoogleServicesPath="${script.env.WORKSPACE}/App_Resources/iOS/GoogleService-Info.plist"
                }
                steps.sh """cp "${fireBaseToken}" ${firebaseGoogleServicesPath}"""
              }

              if(enviroment!="dev"){
                  isDevelopment=false
              }

              steps.echo "Set current CFBundleShortVersionString: ${applicationVersion}"
              steps.echo "Set current CFBundleVersion: ${buildNumber}"
              steps.sh "/usr/libexec/PlistBuddy -c \"Set :CFBundleShortVersionString ${applicationVersion}\" ${script.env.WORKSPACE}/${iosPlistPath}"
              steps.sh "/usr/libexec/PlistBuddy -c \"Set :CFBundleVersion ${buildNumber}\" ${script.env.WORKSPACE}/${iosPlistPath}"
              if(nativeScriptIOSBundleName!=null){
                steps.echo "Set current nativeScriptIOSBundleName: ${nativeScriptIOSBundleName}"
                steps.sh "/usr/libexec/PlistBuddy -c \"Set :CFBundleName ${nativeScriptIOSBundleName}\" ${script.env.WORKSPACE}/${iosPlistPath}"
              }

              //Apply this change because buildId from develop and prod are not equal
              if(nativeScriptIOSBundleName!=null && enviroment!="prod"){
                String applicationId=getApplicationIdNativeScript();
                def  packageJsonFilePath="${script.env.WORKSPACE}/package.json"
                def  tmpPackageJsonFilePath="${script.env.WORKSPACE}/package.json.tmp"
                steps.echo "*********** Set NativeScript Id package.json old - new: ${applicationId} - ${nativeScriptIOSBundleName} ************"
                steps.sh  "jq \'.nativescript.id = \"${nativeScriptIOSBundleName}\"\' ${script.env.WORKSPACE}/package.json > ${tmpPackageJsonFilePath} && mv ${tmpPackageJsonFilePath} ${packageJsonFilePath}"
                steps.sh "cat ${packageJsonFilePath}"
			        }

              if(enabledPseudoMutualAuth){
                applyPseudomutual();
              }

              steps.sh "bash -l -c 'pwd'"
              steps.sh "bash -l -c 'ls -la'"

              steps.echo "Prepare node proxy"
              def bcp_virtual_registry = "${script.env.ARTIFACT_SERVER_NAME_URL}/api/npm/public-release-node/"
              def artifact_npm_registry_url = "${script.env.ARTIFACT_SERVER_NAME_URL}/api/npm/public-node/"

              def npmUserConfig = """
                  registry=${artifact_npm_registry_url}
                  @bcp:registry=${bcp_virtual_registry}
                  strict-ssl=false
                  cache=/opt/nodecache
                  noproxy=${noProxy}
                  proxy=${proxy}
                  https-proxy=${proxy}
                  unsafe-perm=true
                  update-notifier=false
                  """.stripIndent()

              steps.writeFile(
                file:"${script.env.WORKSPACE}/.npmrc",
                text: npmUserConfig
              )

              steps.sh "${paths} npm --version"
              steps.sh "${paths} npm config list"
              steps.sh "${paths} npm cache verify"
              steps.sh "mkdir -p ${script.env.WORKSPACE}/jks-${buildTimestamp}"

              def parameters=""

              steps.echo "Always compile for devices"
              assemblyFlavor="release"
              if(assemblyFlavor!=null && assemblyFlavor!=""){
                parameters="-- --"+ assemblyFlavor+" "
              }

              if(enabledJscrambler){
                steps.echo "Jscrambler enabled"
                buildParams=" --env.jscrambler "+ buildParams
              }

              buildParams+=" --copy-to platforms/ios/build/device/${applicationName}-${applicationVersion}.ipa --provision \"${iosProvisionalProfileName}\" --for-device "

              if(buildParams!=null && (buildParams!="") ){
                if(parameters!=null && (parameters!="")){
                  parameters+= buildParams
                }else{
                  parameters= "-- "+buildParams
                }
              }

              steps.echo "parameters: ${parameters}"
              steps.sh "${paths} gem env"
              steps.sh "${paths} pod --version"

              steps.withEnv(["GIT_HTTP_PROXY_AUTHMETHOD=basic","GIT_SSL_NO_VERIFY=true","SWIFT_VERSION=4","NO_PROXY=${noProxy}","no_proxy=${noProxy}","npm_config_noproxy=${noProxy}","npm_config_https_proxy=${proxy}","npm_config_proxy=${proxy}","HTTP_PROXY=${proxy}","HTTPS_PROXY=${proxy}","http_proxy=${proxy}","https_proxy=${proxy}","LC_ALL=en_US.UTF-8","LANG=en_US.UTF-8"]) {
                withProvisionalProfile(isDevelopment,applicationName,enviroment,false){
                    steps.sh "${paths} npm config ls -l"
                    steps.sh "${paths} gem env"
                    steps.echo " ** Please check that the ruby is point to the current ruby - BEGIN ** "
                    steps.sh "${paths} which ruby"
                    steps.sh "head -n 1 /Applications/Xcode.app/Contents/Developer/usr/bin/ipatool"
                    steps.echo " ** Please check that the ruby is point to the current ruby - END ** "
                    steps.sh "${paths} which tns"
                    steps.sh "${paths} tns --version"
                    if(enabledMutualAuth){
                      configureNativeScriptMutualAuth(applicationName,applicationVersion,enviroment,nativeScriptMutualPluginVersion,"ios"){
                        if(enabledJscrambler){
                          configureJSCrambler(applicationName){
                              steps.sh "${paths} npm run build:ios ${parameters}"
                          }
                        }else{
                          steps.sh "${paths} npm run build:ios ${parameters}"
                        }
                      }
                    }else{
                        if(enabledJscrambler){
                          configureJSCrambler(applicationName){
                              steps.sh "${paths} npm run build:ios ${parameters}"
                          }
                        }else{
                          steps.sh "${paths} npm run build:ios ${parameters}"
                        }
                    }
                    steps.sh "bash -l -c 'ls -la platforms/ios/build/device | grep ipa'"
                }
              }
            }
            steps.echo "IPA: platforms/ios/build/device/"+applicationName+"-*.ipa"
            steps.stash includes: "platforms/ios/build/device/"+applicationName+"-*.ipa", name: 'ios-artifacts'
          }catch(Exception e) {
            steps.sh "echo \"${e}\""
            throw e
          }finally {
            if(enabledFirebase){
              steps.echo "Cleaning firebase google services credentials"
              steps.sh """
                set +x
                rm -rf ${script.env.WORKSPACE}/${fireBaseToken}
                rm -rf ${script.env.WORKSPACE}/${firebaseGoogleServicesPath}
              """
            }
          }
        }
     }
     steps.unstash 'ios-artifacts'
  }

  def applyPseudomutual() {
      def project = "${script.env.project}";
      def pseudomutualFile = project.toLowerCase() + "-pseudomutual-library-file";
      def mutualFileDir= "${script.env.workspace}/src/app/libs";
      def mutualFilePath= "${mutualFileDir}/mutual.ts";
      steps.echo "mutualFilePath ${mutualFilePath}";
      steps.withCredentials([
        [$class: 'StringBinding', credentialsId: project.toLowerCase()+'-nativescript-pseudo-exp', variable: 'exp'],
        [$class: 'StringBinding', credentialsId: project.toLowerCase()+'-nativescript-pseudo-mod', variable: 'mod']
      ]) {
         steps.echo "Starting Pseudomutual Process"
         steps.sh "sed -e 's/rx - 3/${script.env.exp}/' ${mutualFilePath} > ${mutualFileDir}/mutual.ts.tmp && mv ${mutualFileDir}/mutual.ts.tmp ${mutualFilePath}"
         steps.sh "sed -e 's/rx + 45/${script.env.mod}/' ${mutualFilePath} > ${mutualFileDir}/mutual.ts.tmp && mv ${mutualFileDir}/mutual.ts.tmp ${mutualFilePath}"
         //steps.sh "bash -c -l 'cat ${mutualFilePath}'"
         steps.echo "End Pseudomutual Process"
      }
  }

  def withProvisionalProfile(boolean development=true, String applicationName, String enviroment="", boolean updateTeamId=true, Closure body){
    configureProvisionalProfileExecution(development, applicationName, enviroment, updateTeamId)
    try{
      body.call()
    }catch(Exception e) {
      steps.sh "echo \"${e}\""
      throw e
    }finally {
      def keychainPath="/opt/jenkins-slave/certs/jks-${script.env.project}-${applicationName}-${enviroment}.keychain-db".toLowerCase()
      steps.echo "Clearing keychains and provisioning"
      def provisioningProfileUUID=steps.sh(script: """grep UUID -A1 -a "jks-${buildTimestamp}.mobileprovision" | awk -F '[<>]' '/string/{print \$3}'""", returnStdout: true).trim()
      steps.sh """
        set +x
        set +e
        security delete-keychain ${keychainPath}
        rm -f "jks-${buildTimestamp}.mobileprovision"
        rm -f "\${HOME}/Library/MobileDevice/Provisioning Profiles/${provisioningProfileUUID}.mobileprovision"
      """
    }
  }

  def buildNativeScriptAndroid(String enviroment,String buildParams = "",String assemblyFlavor = "",boolean signing=false,boolean enabledDexGuardOfuscation=false,boolean enabledJscrambler=false,boolean enabledMutualAuth = false, boolean enabledDynatrace=true, boolean enabledFirebase = true, boolean enabledPseudoMutualAuth = true) {
    def enviromentParameters=getDockerEnviromentParameters()
    def temporalVolumes=getDockerTemporalVolumes()
    def projectName="${script.env.project}".toLowerCase()
    def applicationName      = getApplicationNameNativeScript()
    def applicationVersion   = getApplicationVersionForNativeScript()
    def buildVersion = getApplicationBuildVersionForNativeScript()

    steps.echo "Enviroment: ${enviroment}"
    steps.echo "Build Params: ${buildParams}"
    steps.echo "Assembly Flavor: ${assemblyFlavor}"
    steps.echo "Signing: ${signing}"
    steps.echo "DexGuardOfuscation enabled: ${enabledDexGuardOfuscation}"
    steps.echo "Jscrambler enabled: ${enabledJscrambler}"
    steps.echo "Mutual enabled: ${enabledMutualAuth}"
    steps.echo "Dynatrace enabled: ${enabledDynatrace}"
    steps.echo "Firebase enabled: ${enabledFirebase}"

    def buildTimestamp       = getBuildTimestamp()

    def bcp_virtual_registry = "${script.env.ARTIFACT_SERVER_URL}/api/npm/public-release-node/"
    def cacertsVolume        = "-v ${script.env.APPLICATION_TOOL}/oracle_java/jdk1.8.0_172/jre/lib/security/cacerts:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/security/cacerts"
    def initGradleVolume     = "-v ${script.env.WORKSPACE}/.gradle:/opt/android-sdk-linux/.gradle"
    def dockerVolumen       = "${temporalVolumes} -v ${script.env.WORKSPACE}:${script.env.WORKSPACE} -w ${script.env.WORKSPACE} ${initGradleVolume} ${cacertsVolume} -v ${script.env.APPLICATION_CACHE}/node-local-repo:/opt/nodecache"
    def httpProxy           = "${script.env.DOCKER_HTTP_PROXY_URL}"

    steps.sh """
      mkdir -p ${script.env.WORKSPACE}/.gradle/wrapper/dists
    """

    steps.writeFile(
      file:"${script.env.WORKSPACE}/.gradle/init.gradle",
      text: getGradleInitScript()
    )

    if (steps.fileExists("${script.env.WORKSPACE}/app/App_Resources/Android")) {
      steps.sh """sed -i -e '/android:versionName/ s/="[^"][^"]*"/="${applicationVersion}"/' ${script.env.WORKSPACE}/app/App_Resources/Android/AndroidManifest.xml"""
      steps.sh """sed -i -e '/android:versionCode/ s/="[^"][^"]*"/="${buildVersion}"/' ${script.env.WORKSPACE}/app/App_Resources/Android/AndroidManifest.xml"""
    }else{
      steps.sh """sed -i -e '/android:versionName/ s/="[^"][^"]*"/="${applicationVersion}"/' ${script.env.WORKSPACE}/App_Resources/Android/src/main/AndroidManifest.xml"""
      steps.sh """sed -i -e '/android:versionCode/ s/="[^"][^"]*"/="${buildVersion}"/' ${script.env.WORKSPACE}/App_Resources/Android/src/main/AndroidManifest.xml"""
    }

    def firebaseGoogleServicesPath=null
    try{
      if(enabledFirebase){
          steps.echo "Firebase token path(Set Credentials File:${applicationName}-nativescript-firebase-google-services-json-token)"
          steps.withCredentials([
              [$class: 'FileBinding', credentialsId: "${applicationName}-nativescript-firebase-google-services-json-token", variable: 'storeFilePath'],
          ]){
            if (steps.fileExists("${script.env.WORKSPACE}/app/App_Resources/Android")) {
              firebaseGoogleServicesPath="${script.env.WORKSPACE}/app/App_Resources/Android/google-services.json"
            } else {
              firebaseGoogleServicesPath="${script.env.WORKSPACE}/App_Resources/Android/google-services.json"
            }
            steps.sh """
            set +x
            cp ${script.env.storeFilePath} ${firebaseGoogleServicesPath}
            chmod 600 ${firebaseGoogleServicesPath}
            """
          }
      }

      def enviroment_parameters=""
      docker.withServer("${script.env.DOCKER_HOST}"){
        docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
            def dockerParameters = "--network=host ${enviromentParameters} -e PROJECT_ARTIFACTID=${applicationName} -e PROJECT_VERSION=${applicationVersion}"
            def dockerCmd        = "docker run --rm ${dockerVolumen} ${dockerParameters} ${dockerNativeScriptEnviroment}"
            def tnsVersion=steps.sh(script: "${dockerCmd} tns --version", returnStdout: true).trim()
            def tnsMajorVersionString=tnsVersion.substring(0,tnsVersion.indexOf('.'))
            int tnsMajorVersion = Integer.parseInt(tnsMajorVersionString)
            steps.echo "tns version: ${tnsVersion}"
            steps.echo "tns major version: ${tnsMajorVersion}"

            def npmUserConfig = """
              registry=${artifact_npm_registry_url}
              @bcp:registry=${bcp_virtual_registry}
              strict-ssl=false
              cache=/opt/nodecache
            """.stripIndent()

            steps.writeFile(
                file:"${script.env.WORKSPACE}/.npmrc",
                text: npmUserConfig
            )

            def parameters="-- "

            if(assemblyFlavor!=null && assemblyFlavor!=""){
              parameters+=" --"+ assemblyFlavor+" "
            }
            if(enabledJscrambler){
              steps.echo "Jscrambler enabled"
              parameters+=" --env.jscrambler "
            }
            if(signing){
              buildParams+=" --key-store-path ${script.env.keystoreFilePath} --key-store-password ${script.env.keystorePasswordValue} --key-store-alias ${script.env.keyAliasValue} --key-store-alias-password ${script.env.keyPasswordValue} "
            }

            buildParams+=" --copy-to platforms/android/app/build/outputs/apk/${applicationName}-${applicationVersion}.apk "

            if(buildParams!=null && (buildParams!="") ){
              parameters+= buildParams
            }

            if(assemblyFlavor!=null && assemblyFlavor!="" && assemblyFlavor=="release"){
              steps.echo "Snapshot enabled"
              parameters+=" --env.snapshot "
            }

            steps.echo "parameters: ${parameters}"

            if(enabledMutualAuth){
              configureNativeScriptMutualAuth(applicationName,applicationVersion,enviroment,nativeScriptMutualPluginVersion,"android"){
                if(enabledJscrambler){
                  configureJSCrambler{
                      steps.sh "${dockerCmd} npm run build:android ${parameters}"
                  }
                }else{
                  steps.sh "${dockerCmd} npm run build:android ${parameters}"
                }
              }
            }else{

                if(enabledPseudoMutualAuth){
                  applyPseudomutual();
                }
                if(enabledJscrambler){
                  configureJSCrambler{
                      steps.sh "${dockerCmd} npm run build:android ${parameters}"
                  }
                }else{
                  steps.sh "${dockerCmd} npm run build:android ${parameters}"
                }
            }

            def compileSdkVersion=getCompileSdkVersionNativeScript("platforms/android/app")
          	if(compileSdkVersion==null){
          		compileSdkVersion="25"
            }

            if(enabledDexGuardOfuscation){
              String applicationId=getApplicationIdNativeScript()
              applicationId=applicationId.substring(applicationId.lastIndexOf(".") + 1)
              steps.sh "cat platforms/android/app/build.gradle"
              if(tnsMajorVersion>3){
              	applicationId="app"
              }
                            def inJars = '';
              if(enabledDynatrace) {
                if(tnsMajorVersion<=3){
                	inJars="platforms/android/app/build/outputs/apk/${applicationId}-${assemblyFlavor}/dist/${applicationId}-${assemblyFlavor}-zipaligned.apk"
                }else{
                    inJars="platforms/android/app/build/outputs/apk/${assemblyFlavor}/${applicationId}-${assemblyFlavor}/dist/${applicationId}-${assemblyFlavor}-zipaligned.apk"
                }
              } else {
                inJars="platforms/android/app/build/outputs/apk/${applicationName}-${applicationVersion}.apk"
              }
              def outJars="platforms/android/app/build/outputs/apk/${applicationName}-${applicationVersion}-withDexGuard.apk"
              def libraryJars="/opt/android-sdk-linux/platforms/android-${compileSdkVersion}/android.jar"
              steps.echo "inJars: ${inJars}"
              steps.echo "outJars: ${outJars}"
              steps.echo "libraryJars: ${libraryJars}"
              executeDexGuard(applicationName, inJars, outJars, libraryJars, signing,dockerNativeScriptEnviroment)
              if(enabledDynatrace) {
                if(tnsMajorVersion<=3){
                	steps.sh """
                	mv "platforms/android/app/build/outputs/apk/${applicationId}-${assemblyFlavor}/dist/${applicationId}-${assemblyFlavor}-zipaligned.apk" "platforms/android/app/build/outputs/apk/${applicationId}-${assemblyFlavor}/dist/${applicationId}-${applicationVersion}-zipaligned-withOutDexGuard.apk"
                	"""
                }else{
                	steps.sh """
                    ls -al platforms/android/app/build/outputs/apk/${assemblyFlavor}/${applicationId}-${assemblyFlavor}/dist
                    mv "platforms/android/app/build/outputs/apk/${assemblyFlavor}/${applicationId}-${assemblyFlavor}/dist/${applicationId}-${assemblyFlavor}-zipaligned.apk" "platforms/android/app/build/outputs/apk/${assemblyFlavor}/${applicationId}-${assemblyFlavor}/dist/${applicationId}-${applicationVersion}-zipaligned-withOutDexGuard.apk"
                    """
                }
              }
              steps.sh """
              mv "platforms/android/app/build/outputs/apk/${applicationName}-${applicationVersion}-withDexGuard.apk" "platforms/android/app/build/outputs/apk/${applicationName}-${applicationVersion}.apk"
              """
            }

            String applicationId=getApplicationIdNativeScript()
            applicationId=applicationId.substring(applicationId.lastIndexOf(".") + 1)
          	if(assemblyFlavor==""){
              assemblyFlavor="debug"
          	}
            steps.sh """
            cd "${script.env.WORKSPACE}/platforms/android/app/build/outputs/apk" && find . -maxdepth 1 -type f -name "${applicationId}-${assemblyFlavor}*" -print0 | xargs -r0 mv -t "${script.env.WORKSPACE}/platforms/android/app/build/outputs/apk/${applicationId}-${assemblyFlavor}/dist/"
            """
          }
        }
    }catch(Exception e) {
      steps.sh "echo \"${e}\""
      throw e
    }finally {
      if(enabledFirebase){
        steps.echo "Cleaning firebase google services credentials"
        steps.sh """
          set +x
          rm -f ${script.env.WORKSPACE}/${firebaseGoogleServicesPath}
        """
      }
    }
    steps.sh "rm ${script.env.WORKSPACE}/.npmrc"
    steps.junit(allowEmptyResults: true, testResults: 'test-results/**/*unit.xml')
  }

  /*
  * Get secure enviroment parameters
  */
  def getDockerEnviromentParameters() {
    def enviromentParameters=steps.sh(script: """set +x
KEYS=
for key in \$(printenv| cut -d= -f1 | grep -v \"SHELL\\|HOME\\|HOSTNAME\\|TERM\\|PATH\\|PWD\\|USER\\|SSH_CONNECTION\\|SHLVL\\|LANG\\|HUDSON_SERVER_COOKIE\\|SSH_CLIENT\\|MAIL\\|WORKSPACE\\|LOGNAME\\|library.\\|LESSOPEN\")
do
   KEYS=\"\${KEYS} -e \${key} \"
done
echo \"\${KEYS}\" """, returnStdout: true).trim()
    return enviromentParameters
  }

  /*
  * Get secure docker temporal parameters
  */
  def getDockerTemporalVolumes() {
    def tentativeDockerTemporalVolumens=steps.sh(script: """set +x
VOLUMES=
for VOLUME in \$(printenv | grep -v "SHELL\\|HOME\\|HOSTNAME\\|TERM\\|PATH\\|PWD\\|USER\\|SSH_CONNECTION\\|SHLVL\\|LANG\\|HUDSON_SERVER_COOKIE\\|SSH_CLIENT\\|MAIL\\|WORKSPACE\\|LOGNAME\\|library.\\|LESSOPEN" | cut -d= -f2)
do
   echo "\${VOLUME}"
done """, returnStdout: true).trim()
    def tentativeDockerVolumens=tentativeDockerTemporalVolumens.split('\\n')
    def workspacePrefix="${script.env.WORKSPACE}"
    def secretsFiles=tentativeDockerVolumens.findAll{it.startsWith(workspacePrefix)&&it.contains("secretFiles")}
    def candidateDockerVolumens=secretsFiles.collect{it.substring(0,it.lastIndexOf("/"))}
    def dockerTemporalVolumes=""
    if(candidateDockerVolumens){
      candidateDockerVolumens.each {
        dockerTemporalVolumes="${dockerTemporalVolumes} -v ${it}:${it}"
      }
    }
    return dockerTemporalVolumes
  }

  /*
  * This method compile the gradle
  */
  def buildGradleAndroid(String buildParameters = 'assembleDebug') {
    this.getInstanceGradleAndroidJenkinsUtil().build(buildParameters);
  }

  /*
  * This method build Native IOS project using fastlane
  */
  def buildIOS(String schema,String enviroment,String codeSignIdentity="iPhone Developer",boolean useAutomaticSigning=true,boolean includeSymbols=false, boolean failTestError=false) {
		//Pack data to send to slave node
		def buildTimestamp=getBuildTimestamp()
    def applicationName=getApplicationNameIOS()
		steps.stash includes: '**/*', name: 'workspace'
		steps.node(macServerAgent) {
		steps.step([$class: 'WsCleanup'])
		steps.unstash 'workspace'
		boolean isDevelopment=true
		def exportMethod="app-store"
		iosAppSchema=schema
		def keychainPath="/opt/jenkins-slave/certs/jks.keychain-db"
		def certPath="/opt/jenkins-slave/certs"

		steps.echo "CodeSign Identity: ${codeSignIdentity}"
		if(codeSignIdentity!="iPhone Developer"){
			isDevelopment=false
			useAutomaticSigning=false
		}else{
			exportMethod="development"
		}

		if(!isDevelopment){
			steps.sh "bash -l -c 'xcodebuild -alltargets clean'"
		}else {
			steps.sh """
				if [ -f \"${script.env.WORKSPACE}/Podfile\" ]; then
					echo "Clean cocoapods"
					rm -rf ${script.env.WORKSPACE}/Pods
				fi
					bash -l -c 'xcodebuild clean -workspace ${xcodeWorkspace} -scheme \"${schema}\" '
			"""
		}


		def proxyCredentials = "${script.env.project}-${applicationName}-user-proxy-mac-server".toLowerCase()
		def noProxy = "10.79.0.0/16,127.0.0.1,.lima.bcp.com.pe,localhost"
		def proxyIp = "10.80.129.173"
		def proxyPort = "8080"

		steps.withCredentials([
		  [$class: 'UsernamePasswordMultiBinding', credentialsId:"${proxyCredentials}", usernameVariable: 'proxyUser',passwordVariable:'proxyPassword']]){
           	steps.echo "proxyUser: ${script.env.proxyUser}"
			def proxy = "http://${script.env.proxyUser}:${script.env.proxyPassword}@${proxyIp}:${proxyPort}"

			steps.withEnv(["GITLAB_USER_VALUE=${script.env.proxyUser}","GITLAB_USER_PASSWORD=${script.env.proxyPassword}","SWIFT_VERSION=4","HTTP_PROXY=${proxy}","HTTPS_PROXY=${proxy}","http_proxy=${proxy}","https_proxy=${proxy}","no_proxy=${noProxy}","NO_PROXY=${noProxy}","LC_ALL=en_US.UTF-8","LANG=en_US.UTF-8"]) {

				withProvisionalProfile(isDevelopment,applicationName,enviroment,false){
				  def buildNumber=getApplicationBuildNumberIOS()
				  steps.sh "echo current buildNumber: ${buildNumber}"
				  steps.sh "/usr/libexec/PlistBuddy -c \"Set :CFBundleVersion ${buildTimestamp}\" ${iosPlistPath}"

				  buildNumber=getApplicationBuildNumberIOS()
				  steps.sh "echo new buildNumber: ${buildNumber}"

				  //steps.sh "bash -l -c 'fastlane run automatic_code_signing use_automatic_signing:\"${useAutomaticSigning}\"  targets:\"${schema}\" code_sign_identity:\"${codeSignIdentity}\"'"
				  steps.sh "bash -l -c 'security list-keychains'"

				  def xcodeProjectName=steps.sh(script: "ls -d *.xcodeproj", returnStdout: true).trim()
				  if(!isDevelopment){
						steps.sh "bash -l -c \'fastlane run update_project_provisioning target_filter:\".*${schema}.*\" profile:\"./jks-${buildTimestamp}.mobileprovision\" \"${script.env.WORKSPACE}/${xcodeProjectName}\"\'"
					}
					steps.sh "mkdir -p ${script.env.WORKSPACE}/jks-${buildTimestamp}/junit"
					steps.sh "mkdir -p ${script.env.WORKSPACE}/jks-${buildTimestamp}/gym-logs"
					steps.sh "mkdir -p ${script.env.WORKSPACE}/jks-${buildTimestamp}/buildpath"
					steps.sh "mkdir -p ${script.env.WORKSPACE}/jks-${buildTimestamp}/builds"

					def gymParameters=""
					if(!includeSymbols){
					//--compileBitcode false
						gymParameters+=" --include_symbols false"
					}

					steps.sh "bash -l -c 'ls'"

					def remoteKeychainPath = "/opt/jenkins-slave/certs/jks-${script.env.project}-${applicationName}-${enviroment}.keychain-db".toLowerCase()
					boolean include_bitcode = false

					//COCOA PODS WITH PROXY CONFIGURATION & CLEAN PROJECT SCHEMES
					steps.sh "bash -l -c  'rm -rf Pods'"
					steps.sh "bash -l -c  'pod install'"
					steps.sh "bash -l -c  'xcodebuild -alltargets clean'"

					steps.withCredentials([
						[$class: "StringBinding", credentialsId: "macserver-keychain-password", variable: "keychainPassword"]]) {

							//UNLOCK KEYCHAIN
							steps.sh "bash -l -c 'security -v unlock-keychain -p \"${script.env.keychainPassword}\" \"${remoteKeychainPath}\"'"

							//FASTLANE GYM
							steps.sh "export MATCH_KEYCHAIN_NAME=\"${remoteKeychainPath}\" && export MATCH_PASSWORD=\"${script.env.keychainPassword}\" && bash -l -c \'fastlane gym ${gymParameters} --codesigning_identity \"${codeSignIdentity}\" --scheme \"${schema}\" --build_path \"${script.env.WORKSPACE}/jks-${buildTimestamp}/buildpath\" --buildlog_path \"${script.env.WORKSPACE}/jks-${buildTimestamp}/gym-logs\" --workspace \"${xcodeWorkspace}\" --clean --export_method \"${exportMethod}\" --include_bitcode \"${include_bitcode}\" PROVISIONING_PROFILE = \"${iosProvisionalProfileName}\" \'"

					}

					//FASTLANE SCAN
					steps.sh "mkdir -p \"${script.env.WORKSPACE}/jks-${buildTimestamp}/coverage\" "
					try{
						steps.sh "export FASTLANE_EXPLICIT_OPEN_SIMULATOR=0 && bash -l -c \'fastlane scan --skip_build --code_coverage --output_types 'junit' --output_directory \"${script.env.WORKSPACE}/jks-${buildTimestamp}/junit\" --scheme \"${schema}\" \'"
						steps.sh "bash -l -c \'slather coverage --html --cobertura-xml --output-directory jks-${buildTimestamp}/coverage --scheme \"${schema}\" \"${xcodeProjectName}\" \'"
					}catch(Exception exception) {
						steps.sh "echo \"${exception}\""
					}

					def applicationVersion=getApplicationVersionIOS()

					def packageType="ipa"
					steps.sh (script: """
					export ipaFilename=\$(find \"\$(pwd)\" -name \"${iosAppSchema}.${packageType}\" -depth 1  -type f -exec basename {} \\;)
					export dsymFilename=\$(find \"\$(pwd)\" -name \"${iosAppSchema}.app.dSYM.zip\" -depth 1  -type f -exec basename {} \\;)
					echo \"Renaming artifact: \${ipaFilename}\"
					mkdir -p ${script.env.WORKSPACE}/jks-${buildTimestamp}
					cp \"\${ipaFilename}\" \"${script.env.WORKSPACE}/jks-${buildTimestamp}/builds\"
					cp \"\${dsymFilename}\" \"${script.env.WORKSPACE}/jks-${buildTimestamp}/builds\"
					cd \"${script.env.WORKSPACE}/jks-${buildTimestamp}/builds\"
					mv \"\${ipaFilename}\" \"${applicationName}-${applicationVersion}.${packageType}\"
					mv \"\${dsymFilename}\" \"${applicationName}-${applicationVersion}.app.dSYM.zip\"
					""", returnStdout: true)
					if(codeSignIdentity=="iPhone Distribution"){
						steps.stash includes: 'jks-*/builds/*.ipa, jks-*/builds/*.app.dSYM.zip', name: 'ios-artifacts'
					}else{
						steps.stash includes: 'jks-*/builds/*.ipa', name: 'ios-artifacts'
					}

					try {
						//TOFIX to analyze
						steps.step([$class: 'CoberturaPublisher', autoUpdateHealth: false, autoUpdateStability: false, coberturaReportFile: 'jks-${buildTimestamp}/coverage/cobertura.xml', failUnhealthy: false, failUnstable: false, maxNumberOfBuilds: 0, onlyStable: false, sourceEncoding: 'ASCII', zoomCoverageChart: false])
					}
					catch(Exception e) {
						//TODO - To be removed on the new version of plugin 2.2.1 of jacoco, verson 2.2.0 had issues with gitlba plugin
					}
					try {
					   steps.junit allowEmptyResults: true, testResults: '**/**/junit/report.junit'
					}catch(Exception e){
					   //TODO: Remove when QA are implemented
					}
				}
			}
		}
	 }
	 steps.unstash 'ios-artifacts'
  }

  /*
  * This method release the gradle project
  */
  def releaseGradle() {
     // Run the maven build
     steps.sh "echo '' > ${script.env.WORKSPACE}/gradle-jenkins"
    //def gradleInitScriptFile = new File("${script.env.WORKSPACE}/gradle-jenkins")
    //gradleInitScriptFile.append(getGradleInitScript())
    steps.writeFile(
      file:"${script.env.WORKSPACE}/gradle-jenkins",
      text: getGradleInitScript()
    )

    steps.sh "'${gradleHome}/bin/gradle' release --stacktrace -Dorg.gradle.daemon=false -Dhttp.proxyHost=127.0.0.1 -Dhttp.proxyPort=3128 -Dhttps.proxyHost=127.0.0.1 -Dhttps.proxyPort=3128 --init-script ./gradle-jenkins"
  }

  /*
  * Se NodeJS paths to enviroment
  */
  void withNodeEnv(Closure body) {
     // Run the NodeJS build
     steps.withEnv(["NODEJS_HOME=${nodeHome}","PATH=$script.env.PATH:${nodeHome}/bin"]){
        steps.sh "echo NODEJS_HOME: ${script.env.NODEJS_HOME}"
        body.call()
    }
  }

  /*
  * This method compile the angular
  */
  def buildNode(String buildParams = '', boolean failNever=true, boolean enableJscrambler=false) {
    def appName              = getApplicationNameNode()
    def project              = script.env.project
    def buildTimestamp       = getBuildTimestamp()
    def bcp_virtual_registry = "${script.env.ARTIFACT_SERVER_URL}/api/npm/public-release-node/"
    def dockerVolumen       = "-v ${script.env.WORKSPACE}:/project/data1 -w /project/data1 -v ${script.env.APPLICATION_CACHE}/node-local-repo:/opt/nodecache"

    configureAngularEnvironmentFromVault {
      docker.withServer("${script.env.DOCKER_HOST}"){
        docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
           def npm_registry_url  = "https://registry.npmjs.org/"
           def dockerParameters = "--network=host --cpus 2"                                                                         //"--cap-add=SYS_ADMIN -e DISPLAY --net=host -u root"
           def dockerCmd        = "docker run --rm ${dockerVolumen} ${dockerParameters} ${dockerNodeAngularEnviroment}"

           steps.sh """echo "registry=${artifact_npm_registry_url}\n@bcp:registry=${bcp_virtual_registry}" > ./npmrc-jenkins"""

           def http_proxy      = "${script.env.DOCKER_HTTP_PROXY_URL}"
           def npm_arguments   = "--no-progress --strict-ssl=false --userconfig=./npmrc-jenkins"
           def bower_arguments = "--config.strict-ssl=false"

           if(http_proxy){
             npm_arguments   += " --proxy ${http_proxy} --https-proxy ${http_proxy}"
             bower_arguments += " --config.proxy=${http_proxy} --config.https-proxy=${http_proxy}"
           }

           steps.echo "****Display Node and Angular Version****"
           steps.sh "${dockerCmd} npm -v"
           steps.sh "${dockerCmd} npm config set cache /opt/nodecache --global"
           steps.sh "${dockerCmd} npm config ls -l"
           steps.sh "${dockerCmd} node -v"
           steps.sh "${dockerCmd} ng -v"
           //steps.sh "${dockerCmd} npm i ${npm_arguments} -D @angular-devkit/core"
           steps.sh "${dockerCmd} npm install ${npm_arguments}"

           if ( steps.fileExists ('bower.json') ) {
              steps.sh "${dockerCmd} bower -v"
              steps.sh "${dockerCmd} bower install ${bower_arguments}"
           }

           if ( steps.fileExists ('Gruntfile.js') ) {
              steps.sh "${dockerCmd} grunt --version"
           }

           if(!failNever){

              steps.sh "echo '' > ${script.env.WORKSPACE}/karma.conf.js"
              steps.writeFile(
                file:"${script.env.WORKSPACE}/karma.conf.js",
                text: getKarmaConfJS()
              )

              try{
                  steps.sh "${dockerCmd} npm install ${npm_arguments} karma-spec-reporter karma-coverage-istanbul-reporter karma-phantomjs-launcher istanbul karma-jasmine-html-reporter karma-junit-reporter"
                  steps.sh "${dockerCmd} ng test --single-run -w false --browsers PhantomJS"
              }catch(Exception exception) {
                  steps.sh "echo \"${exception}\""
                  if(!failNever){
                    throw exception
                  }
              }finally{
                // Reverting modified karma.conf.js
                steps.sh "git checkout -- ${script.env.WORKSPACE}/karma.conf.js"
              }
           }

           steps.sh "${dockerCmd} ng build ${buildParams}"
        }
      }
    }

     if(enableJscrambler){
        configureJscramblerAngularApp();
     }

     try {
        steps.junit allowEmptyResults: true, testResults: '**/tests_out/**/*unit.xml'
     }catch(Exception e){
        //TODO: Remove when QA are implemented
     }

  }

  /*
  * DEPRECATED USE SAVE RESULT MAVEN OR GRADLE
  * This method save the results for test and generate and the deploy artifact by extension
  */
  def saveResult(extension) {
     steps.archiveArtifacts(artifacts:'**/target/*.'+extension,allowEmptyArchive:true)
     steps.archiveArtifacts(artifacts:'**/build/*.'+extension,allowEmptyArchive:true)
  }

  /*
  * This method save the results for test and generate and the deploy artifact by extension
  */
  def saveResultMaven(extension) {
    this.getInstanceMavenJenkinsUtil().saveResult(extension)
  }

  /*
  * This method save the results for test and generate and the deploy artifact by extension
  */
  def saveResultGradle(extension) {
     steps.archiveArtifacts(artifacts:'**/build/libs/*.'+extension,allowEmptyArchive:true)
  }

  /*
  * This method save the results for test and generate and the deploy artifact by extension
  */
  def saveResultGradleAndroid(extension) {
    getInstanceGradleAndroidJenkinsUtil().saveResult(extension)
  }

  /*
  * This method save the results for test and generate and the deploy artifact by extension
  */
  def saveResultNativeScriptAndroid(extension) {
     steps.archiveArtifacts(artifacts:'**/build/outputs/apk/*.'+extension,allowEmptyArchive:true)
  }

  /*
  * This method save the results for test and generate and the deploy artifact by extension
  */
  def saveResultNativeScriptIOS(extension) {
     steps.archiveArtifacts(artifacts:'**/platforms/ios/build/device/*.'+extension,allowEmptyArchive:true)
  }

  /*
  * This method save the progruards files for gradle android projects
  */
  def saveProguardGradleAndroid() {
    this.getInstanceGradleAndroidJenkinsUtil().saveProguard()
  }

  /*
  * This method save the progruards files for gradle android projects
  */
  def saveProguardNativeScriptAndroid() {
     try{
       steps.sh(script: """
       set +e
       tar -cvzf ${script.env.WORKSPACE}/proguard-${buildTimestamp}.tgz dexguard-${script.env.BUILD_NUMBER}/mapping/*.txt
       """, returnStdout: true).trim()
       steps.archiveArtifacts(artifacts:'proguard-*.tgz',allowEmptyArchive:true)
     }catch(Exception e) {
       steps.echo "DexGuard enabled?"
       steps.sh "echo \"${e}\""
     }
  }

  /*
  * This method save the results for test and generate and the deploy artifact by extension
  */
  def saveResultFreeStyle(extension) {
    this.getInstanceFreeStyleJenkinsUtil().saveResult(extension);
  }

  /*
  * This method save the results for test and generate and the deploy artifact by extension
  */
  def saveResultForSalesForce(extension) {
     steps.archiveArtifacts(artifacts:'*.'+extension,allowEmptyArchive:true)
  }

  /*
  * This method save the results for test and generate and the deploy artifact by extension
  */
  def saveResultNode(extension) {
     steps.archiveArtifacts(artifacts:'*.'+extension,allowEmptyArchive:true)
  }

  /*
  * This method save the results for test and generate and the deploy artifact by extension
  */
  def saveResultIOS(extension) {
     steps.archiveArtifacts(artifacts:'jks-*/builds/*.'+extension,allowEmptyArchive:true)
  }

   /*
  * This method execute sonar
  */
  def executeSonar(boolean useBranchName = false,String tagName="") {
    MavenJenkinsUtil mavenJenkinsUtil = this.getInstanceMavenJenkinsUtil()
    mavenJenkinsUtil.sonarUseBranch = useBranchName
    mavenJenkinsUtil.executeQA(tagName)
  }

  /*
  * This method execute sonar
  */
  def executeSonarWithGradle(boolean useBranchName = false,String sourceEncoding = "UTF-8",String tagName="") {
     def httpProxyHost  = "${script.env.DOCKER_HTTP_PROXY_HOST}"
     def httpProxyPort  = "${script.env.DOCKER_HTTP_PROXY_PORT}"
     def projectName="${script.env.project}".toLowerCase()
     def groupIdName=getGroupIdGradle()
     def applicationName=getApplicationNameJavaGradle()
     def applicationVersion=getApplicationVersionGradle()
     def projectKey = "${projectName}-${groupIdName}:${applicationName}"
     def extraParams=""
     if(useBranchName){
       extraParams="-Dsonar.branch=${branchName}"
     }
     def sonarProjectVersion="${applicationVersion}-${branchName}"
     if(tagName!=""){
       sonarProjectVersion="${applicationVersion}-${tagName}"
     }
     def traceabilityParams=" -Dsonar.projectVersion=\"${sonarProjectVersion}\" -Dsonar.analysis.version=\"${applicationVersion}\" \
-Dsonar.analysis.userId=\"${buildUserId}\" -Dsonar.analysis.commitId=\"${currentGitCommit}\" -Dsonar.analysis.tek=\"gradle-java\" \
-Dsonar.analysis.projectName=\"${projectName}\" -Dsonar.analysis.buildTimestamp=\"${buildTimestamp}\" -Dsonar.analysis.branch=\"${branchName}\" \
-Dsonar.links.scm=\"${currentGitURL}\" -Dsonar.analysis.jobname=\"${script.env.JOB_NAME}\" "
     if(tagName!=""){
       traceabilityParams+=" -Dsonar.analysis.tagName=\"${tagName}\" "
     }

     steps.sh "mkdir -p ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}"
     def dockerVolumen="-v ${script.env.APPLICATION_CACHE}/sonar-cache:/opt/android-sdk-linux/.sonar -v ${script.env.APPLICATION_CACHE}/gradle-local-repo:/gradle-home -v ${script.env.WORKSPACE}:/project/data -w /project/data"
     dockerVolumen+=" -v ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}:/gradle-project-cache"
     steps.echo "dockerVolumen: ${dockerVolumen}"
     steps.sh "echo '' > ${script.env.WORKSPACE}/gradle-jenkins"

     steps.writeFile(
        file:"${script.env.WORKSPACE}/gradle-jenkins",
        text: getGradleInitScript()
     )

     docker.withServer("${script.env.DOCKER_HOST}"){
        docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
           def dockerParameters="--network=host"
           def dockerCmd="docker run --rm ${dockerVolumen} ${dockerParameters} ${dockerGradleJavaEnviroment}"
           steps.withSonarQubeEnv {
              def defaultParameters = "-Dsonar.sourceEncoding=${sourceEncoding} -Dsonar.projectName=${projectKey} -Dsonar.projectKey=${projectKey} -Dsonar.login=${script.env.SONAR_AUTH_TOKEN} -Dsonar.password= -Dsonar.host.url=${script.env.SONAR_HOST_URL} -Dsonar.links.ci=${script.env.BUILD_URL} ${traceabilityParams} --stacktrace -Dorg.gradle.daemon=false --gradle-user-home=/gradle-home --init-script ./gradle-jenkins -Dsonar.junit.reportsPath=build/test-results"

              if(httpProxyHost && httpProxyPort){
                defaultParameters+=" -Dhttp.proxyHost=${httpProxyHost} -Dhttp.proxyPort=${httpProxyPort} -Dhttps.proxyHost=${httpProxyHost} -Dhttps.proxyPort=${httpProxyPort}"
              }
              steps.sh "${dockerCmd} gradle sonarqube --project-cache-dir /gradle-project-cache ${defaultParameters}"
           }
        }
     }
  }

  /*
  * This method execute sonar on android gradle projects
  */
  def executeSonarWithGradleAndroid(boolean useBranchName = false,String sourceEncoding = "UTF-8",String tagName="") {
    GradleAndroidJenkinsUtil gradleAndroidJenkinsUtil = this.getInstanceGradleAndroidJenkinsUtil()
    gradleAndroidJenkinsUtil.sonarUseBranch = useBranchName
    gradleAndroidJenkinsUtil.executeQA(tagName, sourceEncoding)
  }

  /*
  * This method execute sonar for node projects
  */
  def executeSonarForNode(String tagName="") {
    def appName     = steps.sh script: "jq '.name' ./package.json",    returnStdout: true
    def appVersion  = steps.sh script: "jq '.version' ./package.json", returnStdout: true
    def projectName = "${script.env.project}".toLowerCase()
    def appGroup    = projectName+"-node"
    def appId       = "${appName}"
    def appEncoding = "UTF-8"
    def appSources  = "src"
    executeSonarWithSonarRunner(appGroup,appId,appVersion,appName,appSources,appEncoding,"node",tagName)
  }

  def executeLintForNode(String lintParameters="--force --format checkstyle"){
    docker.withServer("${script.env.DOCKER_HOST}"){
      docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}"){
        def dockerVolumen    = "-v ${script.env.WORKSPACE}:${script.env.WORKSPACE} -w ${script.env.WORKSPACE} -v ${script.env.APPLICATION_CACHE}/node-local-repo:/opt/nodecache"
        def dockerParameters = "--network=host --cpus 2"
        def dockerCmd        = "docker run --rm ${dockerVolumen} ${dockerParameters} ${dockerNodeAngularEnviroment}"

        steps.sh "${dockerCmd} /bin/bash -c 'npm run --quiet lint -- ${lintParameters} > checkstyle-result.xml'"
      }
    }

    def checkStyleContent = steps.sh(script:"tail -n +5 checkstyle-result.xml",returnStdout:true)

    steps.writeFile(
      file:"${script.env.WORKSPACE}/checkstyle-result.xml",
      text:checkStyleContent
    )

    steps.checkstyle(pattern:'checkstyle-result.xml',canComputeNew:false)
  }

  def executeNativeScriptTSLint(String lintParameters="--force --format checkstyle"){
    docker.withServer("${script.env.DOCKER_HOST}"){
      docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}"){
        def dockerVolumen    = "-v ${script.env.WORKSPACE}:${script.env.WORKSPACE} -w ${script.env.WORKSPACE} -v ${script.env.APPLICATION_CACHE}/node-local-repo:/opt/nodecache"
        def dockerParameters = "--network=host"
        def dockerCmd        = "docker run --rm ${dockerVolumen} ${dockerParameters} ${dockerNativeScriptEnviroment}"

        steps.sh "${dockerCmd} /bin/bash -c 'npm run lint -- --project . ${lintParameters} > checkstyle-result.xml'"
      }
    }

    def checkStyleContent = steps.sh(script:"tail -n +5 checkstyle-result.xml",returnStdout:true)

    steps.writeFile(
      file:"${script.env.WORKSPACE}/checkstyle-result.xml",
      text:checkStyleContent
    )

    steps.checkstyle(pattern:'checkstyle-result.xml',canComputeNew:false)
  }

  /*
  * This method execute sonar runner
  */
  def executeSonarWithSonarRunner(appGroup,appId,appVersion,appName,appSources,appEncoding,String tek="other",String tagName="") {
    def buildTimestamp=buildTimestamp
    steps.withSonarQubeEnv {
      def dockerParameters      = "--rm --network=host -v ${script.env.WORKSPACE}:/project/data -w /project/data"
      def docker_sonar_cmd      = "docker run ${dockerParameters} ${dockerSonarRunner}"
      def projectName = "${script.env.project}".toLowerCase()
      def applicationVersion=appVersion.trim()
      def sonarProjectVersion=applicationVersion+"-${branchName}"
      if(tagName!=""){
       sonarProjectVersion=appVersion.trim()+"-${tagName}"
      }
      def traceabilityParams=" -Dsonar.projectVersion=\"${sonarProjectVersion}\" -Dsonar.analysis.version=\"${applicationVersion}\" \
-Dsonar.analysis.userId=\"${buildUserId}\" -Dsonar.analysis.commitId=\"${currentGitCommit}\" -Dsonar.analysis.tek=\"${tek}\" \
-Dsonar.analysis.projectName=\"${projectName}\" -Dsonar.analysis.buildTimestamp=\"${buildTimestamp}\" -Dsonar.analysis.branch=\"${branchName}\" \
-Dsonar.links.scm=\"${currentGitURL}\" -Dsonar.analysis.jobname=\"${script.env.JOB_NAME}\" "
      if(tagName!=""){
       traceabilityParams+=" -Dsonar.analysis.tagName=\"${tagName}\" "
      }
      def sonarrunner_arguments = "-Dsonar.login=${script.env.SONAR_AUTH_TOKEN} -Dsonar.password= -Dsonar.host.url=${script.env.SONAR_HOST_URL} -Dsonar.links.ci=${script.env.BUILD_URL} ${traceabilityParams}"

      steps.sh """echo "sonar.projectKey=${appGroup}:${appId}\nsonar.projectName=${appGroup}:${appId}\nsonar.projectVersion=${appVersion}\nsonar.sources=${appSources}\nsonar.sourceEncoding=${appEncoding}\n" > ./sonar-project.properties"""
      steps.sh "${docker_sonar_cmd} sonar-scanner ${sonarrunner_arguments}"
    }
  }

  /*
  * This method execute QA -- Only IOS 8 and above
  */
  def executeQAForIOS() {
      steps.sh "fastlane scan --scheme \\\"${schema}\\\" --address_sanitizer --thread_sanitizer"
  }

  /*
  * This method deploy artifact on Nexus
  */
  def deployArtifactWithMaven(String enviroment = '',String version = '') {
      deployNexusArtifact(enviroment,version)
  }

  /*
  * This method deploy artifact on Nexus
  */
  def deployNexusArtifact(String enviroment = '',String version = '') {
      deployArtifactWithMavenByURL("${script.env.project}",enviroment,"${script.env.ARTIFACT_SERVER_URL}",version)
  }

  /*
  * This method deploy artifact on Artefactory
  */
  def deployArtifactoryMaven(String enviroment = '',String version = '') {
      def repositoryURL="${artefactoryUrl}"
      deployArtifactWithMavenByURL("${script.env.project}",enviroment,repositoryURL,version,true)
  }

 /*
  * This method deploy artifact on Artifactory for FreeStyle projects
  */
  def deployArtifactoryForSalesForce(String enviroment = '',String tagName = ''){
  	def packageType="zip"

     //Clean up to prevent unclean data
    steps.step([$class: 'WsCleanup'])
    def buildUniqueId
    if(tagName!=null&&tagName!=''){
          buildUniqueId="${tagName}"
        checkoutBranch(tagName)
    }else{
        buildUniqueId="${buildTimestamp}"
        checkoutBranch(currentGitCommit)
    }

    buildForSalesForce()

    def repository
    enviroment=enviroment.toUpperCase()

    def repoKey

    if ("${branchName}"=="master"){
      repoKey="${script.env.project}.Generic.Release"
    }else{
      repoKey="${script.env.project}.Generic.Snapshot"
    }
    if(enviroment){
      repoKey="${script.env.project}.Generic-${enviroment}"
    }

    def projectName="${script.env.project}"
    def projectNameLowercase = projectName.toLowerCase()
    def applicationName=getApplicationNameForSalesForce()
    def buildTimestamp=getBuildTimestamp()
    def applicationVersion=getApplicationVersionForSalesForce()

    def moduleName = "${projectName}".toLowerCase()
    def projectKeyName="${script.env.project}".toLowerCase()+"-${applicationName}"
    def actionURL="${artefactoryUrl}/${repoKey}/${moduleName}/${applicationName}/${applicationVersion}/"
    steps.echo "Repository Path: ${actionURL}"
    def actionBuildURL="${artefactoryUrl}/api/build"
    steps.echo "Artifactory Buil Api URL: ${actionURL}"
    steps.withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: projectNameLowercase+'-jenkins-artefactory-token',
                        usernameVariable: 'ARTIFACT_USER_NAME', passwordVariable: 'ARTIFACT_USER_PASSWORD']]) {
         def artifactSha1=steps.sh(script: "sha1sum ${applicationName}-${applicationVersion}.${packageType} | cut -d ' ' -f 1", returnStdout: true).trim()
         def artifactMd5=steps.sh(script: "md5sum ${applicationName}-${applicationVersion}.${packageType} | cut -d ' ' -f 1", returnStdout: true).trim()
         steps.echo "artifactSha1: ${artifactSha1}"
         steps.echo "artifactMd5: ${artifactMd5}"

         def result = steps.sh (script: """
         curl -o response-build-info-${buildTimestamp}.txt -X PUT --header \"X-Checksum-MD5:${artifactMd5}\" --header \"X-Checksum-Sha1:${artifactSha1}\" -u ${script.env.ARTIFACT_USER_NAME}:${script.env.ARTIFACT_USER_PASSWORD} -T ${applicationName}-${applicationVersion}.${packageType} "${actionURL}${applicationName}-${applicationVersion}.${packageType};build.name=${projectKeyName};build.number=${buildUniqueId}"
         """, returnStdout: true)
         def callback = steps.sh(script: "cat response-build-info-${buildTimestamp}.txt", returnStdout: true).trim()
         steps.echo "Callback: ${callback}"

         def errorMessageArtifactory = steps.sh (script:  "cat response-build-info-${buildTimestamp}.txt | jq --raw-output '.errors | .[0] | .message'", returnStdout: true).trim()

         //Artefact has promoted or moved
         if(errorMessageArtifactory!="null"){
             throw new Exception("Error deploying the artifact to the artifact server")
         }

         def timestamp=steps.sh(script: "echo \$(date +\"%Y-%m-%dT%H:%M:%S.%3N%z\")", returnStdout: true).trim()
         def artifactoryBuildInfoJson=getArtifactoryBuildInfo("${script.env.ARTIFACT_USER_NAME}",moduleName,applicationName,applicationVersion,buildUniqueId,timestamp,packageType,artifactSha1,artifactMd5)
         steps.echo "artifactoryBuildInfoJson: ${artifactoryBuildInfoJson}"

         steps.writeFile(
          file: "${script.env.WORKSPACE}/artifactory-json-${buildTimestamp}.json",
          text: artifactoryBuildInfoJson
         )

         result = steps.sh (script: """
         curl -X PUT -I -i -u ${script.env.ARTIFACT_USER_NAME}:${script.env.ARTIFACT_USER_PASSWORD} -H \"Content-Type: application/json\" -T artifactory-json-${buildTimestamp}.json "${actionBuildURL}"
         """, returnStdout: true)
         steps.echo "Result: ${result}"
    }
  }

  /*
  * This method deploy artifact on Artifactory for FreeStyle projects
  */
  def deployArtifactoryFreeStyle(String relativeSrcPath = '',String packageType='zip',String enviroment = '',String tagName = ''){
    FreeStyleJenkinsUtil freeStyleInstance = this.getInstanceFreeStyleJenkinsUtil()
    freeStyleInstance.setRelativeSrcPath(relativeSrcPath)
    freeStyleInstance.setPackageType(packageType)
    freeStyleInstance.uploadArtifact(tagName);
  }

  /*
  * This method deploy artifact on Artifactory for Native IOS projects
  */
  def deployArtifactoryIOS(String enviroment = '',String tagName = '', String schema = '', String codeSignIdentity="iPhone Developer"){
	 def buildUniqueId
	 if(tagName!=null&&tagName!=''){
	    //Clean up to prevent unclean data
		steps.step([$class: 'WsCleanup'])
        buildUniqueId=steps.sh(script: "echo ${tagName} | cut -d '/' -f2 ", returnStdout: true).trim()
	    checkoutBranch(tagName)
	    buildIOS(schema,enviroment.toLowerCase(),codeSignIdentity)
	 }else{
	    buildUniqueId="${buildTimestamp}"
	 }

     def repository
     enviroment=enviroment.toUpperCase()

     def repoKey

     if ("${branchName}"=="master"){
        repoKey="${script.env.project}.Generic.Release"
     }else{
        repoKey="${script.env.project}.Generic.Snapshot"
     }
     if(enviroment&&enviroment!="PROD"){
        repoKey="${script.env.project}.Generic-${enviroment}"
     }
     steps.echo "Code Sign Identity: ${codeSignIdentity}"
     steps.echo "Enviroment: ${enviroment}"
     steps.echo "Branch name: ${branchName}"
     //If the application is set for promotion to production
     if(codeSignIdentity=="iPhone Distribution"){
       if(enviroment=='PROD'){
       		if ("${branchName}".startsWith("tags/RC-")){
       			repoKey="${script.env.project}.Generic.Release"
       			buildUniqueId=buildUniqueId+"-PROMOTED"
       		}
       }
     }

    def projectName="${script.env.project}"
    def projectNameLowercase=projectName.toLowerCase()
    def applicationName=getApplicationNameIOS()
    def buildTimestamp=getBuildTimestamp()
    def applicationVersion=getApplicationVersionIOS()

    def moduleName = "${projectName}".toLowerCase()
    def applicationGitName = getGitProjectName()
    def projectKeyName="${script.env.project}".toLowerCase()+"-${applicationName}"
    def actionURL="${artefactoryUrl}/${repoKey}/${moduleName}/${applicationGitName}/${applicationName}/${applicationVersion}/"
    steps.echo "Repository Path: ${actionURL}"
    def actionBuildURL="${artefactoryUrl}/api/build"
    steps.echo "Artifactory Buil Api URL: ${actionURL}"

    def packageType="ipa"

    steps.withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: projectNameLowercase+'-jenkins-artefactory-token',
                        usernameVariable: 'ARTIFACT_USER_NAME', passwordVariable: 'ARTIFACT_USER_PASSWORD']]) {

         def artifactSha1=steps.sh(script: "cd ${script.env.WORKSPACE}/jks-${buildTimestamp}/builds && sha1sum ${applicationName}-${applicationVersion}.${packageType} | cut -d ' ' -f 1 ", returnStdout: true).trim()
         def artifactMd5=steps.sh(script: "cd ${script.env.WORKSPACE}/jks-${buildTimestamp}/builds && md5sum ${applicationName}-${applicationVersion}.${packageType} | cut -d ' ' -f 1 ", returnStdout: true).trim()
         steps.echo "artifactSha1: ${artifactSha1}"
         steps.echo "artifactMd5: ${artifactMd5}"

         def iosAppSchemaEncoded = encodeHtml('$iosAppSchema')
         def result = steps.sh (script: """
         curl -o response-build-info-${buildTimestamp}.txt -X PUT --header \"X-Checksum-MD5:${artifactMd5}\" --header \"X-Checksum-Sha1:${artifactSha1}\" -u ${script.env.ARTIFACT_USER_NAME}:${script.env.ARTIFACT_USER_PASSWORD} -T \"${script.env.WORKSPACE}/jks-${buildTimestamp}/builds/${applicationName}-${applicationVersion}.${packageType}\" \"${actionURL}${applicationName}-${applicationVersion}.${packageType};build.name=${projectKeyName};build.number=${buildUniqueId};build.userId=${buildUserId};schema=${iosAppSchemaEncoded}\"
         """, returnStdout: true)
         def callback = steps.sh(script: "cat response-build-info-${buildTimestamp}.txt", returnStdout: true).trim()
         steps.echo "Callback: ${callback}"

         def errorMessageArtifactory = steps.sh (script:  "cat response-build-info-${buildTimestamp}.txt | jq --raw-output '.errors | .[0] | .message'", returnStdout: true).trim()

         //Artefact has promoted or moved
         if(errorMessageArtifactory!="null"){
             throw new Exception("Error deploying the artifact to the artifact server")
         }

         //Nanoseconds not supporte for MAC, set to 0
         def timestamp=steps.sh(script: "echo \$(date +\"%Y-%m-%dT%H:%M:%S.000%z\")", returnStdout: true).trim()
         def artifactoryBuildInfoJson=getArtifactoryBuildInfoEscaped("${script.env.ARTIFACT_USER_NAME}","${iosAppSchema}",moduleName,applicationName,applicationVersion,buildUniqueId,timestamp,packageType,artifactSha1,artifactMd5)
         steps.echo "artifactoryBuildInfoJson: ${artifactoryBuildInfoJson}"

	     steps.sh (script: """
	     touch ${script.env.WORKSPACE}/artifactory-json-${buildTimestamp}.json
	     echo \"\"\"${artifactoryBuildInfoJson}\"\"\" > ${script.env.WORKSPACE}/artifactory-json-${buildTimestamp}.json
	     """, returnStdout: true)

         result = steps.sh (script: """
         curl -X PUT -I -i -u ${script.env.ARTIFACT_USER_NAME}:${script.env.ARTIFACT_USER_PASSWORD} -H \"Content-Type: application/json\" -T artifactory-json-${buildTimestamp}.json "${actionBuildURL}"
         """, returnStdout: true)
         steps.echo "Result: ${result}"
    }
  }

  /*
  * This method deploy artifact on Nexus
  */
  def deployArtifactWithMavenByURL(String project,String enviroment = '',String repositoryURL,String version = '',boolean artefactory=false) {
    MavenJenkinsUtil mavenJenkinsUtil = this.getInstanceMavenJenkinsUtil()
    mavenJenkinsUtil.environment = enviroment
    mavenJenkinsUtil.repositoryURL = repositoryURL
    mavenJenkinsUtil.artefactory = artefactory
    mavenJenkinsUtil.uploadArtifact(version)
  }

  /*
  * This method deploy artifact on Nexus
  */
 def deployArtifactWithGradle(String enviroment = '',String tagName = '') {
      def repositoryURL="${artefactoryUrl}"
      deployArtifactWithGradleURL(enviroment,repositoryURL,tagName,true)
  }

  /*
  * This method deploy artifact with Android
  */
  def deployArtifactWithGradleURL(String enviroment = '',String repositoryURL="${artefactoryUrl}/",String tagName = '',boolean artefactory=true) {
     def httpProxyHost    = "${script.env.DOCKER_HTTP_PROXY_HOST}"
     def httpProxyPort    = "${script.env.DOCKER_HTTP_PROXY_PORT}"

     def buildUniqueId
     if(tagName!=null&&tagName!=''){
         //Clean up to prevent unclean data
         steps.step([$class: 'WsCleanup'])
         buildUniqueId=steps.sh(script: "echo ${tagName} | cut -d '/' -f2 ", returnStdout: true).trim()
         checkoutBranch(tagName)
     }else{
         buildUniqueId="${buildTimestamp}"
     }
      //Clean up to prevent unclean data
     def repository
     enviroment=enviroment.toUpperCase()
     def repoKey
     def repositoryPrefix=""

     if(!artefactory){
        repositoryPrefix="content/repositories"
     }
     if ("${branchName}"=="master"){
        repoKey="${script.env.project}.Release"
        repository="${repositoryURL}${repositoryPrefix}/${repoKey}"
     }else{
        repoKey="${script.env.project}.Snapshot"
        repository="${repositoryURL}${repositoryPrefix}/${repoKey}"
     }

     if(enviroment){
        repoKey="${script.env.project}-${enviroment}"
        repository="${repositoryURL}${repositoryPrefix}/${repoKey}"
     }
     steps.echo "Repository Key: ${repoKey}"
     steps.echo "Repository: ${repository}"

     def projectName="${script.env.project}".toLowerCase()
     def groupIdName=getGroupIdGradle()
     def applicationName=getApplicationNameJavaGradle()
     def applicationVersion=getApplicationVersionGradle()
     def projectKey = "${projectName}-${applicationName}"

     steps.sh "mkdir -p ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}"
     def dockerVolumen="-v ${script.env.APPLICATION_CACHE}/gradle-local-repo:/gradle-home -v ${script.env.WORKSPACE}:/project/data -w /project/data"
     dockerVolumen+=" -v ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}:/gradle-project-cache"
     steps.echo "dockerVolumen: ${dockerVolumen}"
     steps.sh "echo '' > ${script.env.WORKSPACE}/gradle-jenkins"

    steps.writeFile(
        file:"${script.env.WORKSPACE}/gradle-jenkins",
        text: getGradleInitScript()
    )

     def projectNameLowercase = projectName

     docker.withServer("${script.env.DOCKER_HOST}"){
        docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
            def gradle_proxy_params=""
            if(httpProxyHost && httpProxyPort){
		        gradle_proxy_params+=" -Dhttp.proxyHost=${httpProxyHost} -Dhttp.proxyPort=${httpProxyPort} -Dhttps.proxyHost=${httpProxyHost} -Dhttps.proxyPort=${httpProxyPort}";
		    }
		    def dockerParameters="--network=host"
		    def dockerEnviromentParameters="-e BUILD_USER_ID=${buildUserId} -e BUILD_GIT_URL=${currentGitURL} -e BUILD_URL=${script.env.BUILD_URL} -e BUILD_NAME=${projectKey} -e BUILD_INFO_ENVVARSEXCLUDEPATTERNS=*password*,*secret*,*PASSWORD*,*SECRET* -e PROJECT_NAME=${script.env.project} -e ARTIFACT_SERVER_URL=${repositoryURL} -e REPOKEY=${repoKey} -e BUILD_UNIQUE_ID=${buildUniqueId} "
            def executionCommand="gradle artifactoryPublish --project-cache-dir /gradle-project-cache --gradle-user-home=/gradle-home ${gradle_proxy_params} --stacktrace -Dorg.gradle.daemon=false --init-script ./gradle-jenkins"
			if(!azureKeyVaultActivated){
	            steps.withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: projectNameLowercase+'-jenkins-artefactory-token',
	                           usernameVariable: 'ARTIFACT_USER_NAME', passwordVariable: 'ARTIFACT_USER_PASSWORD']]) {
	                dockerEnviromentParameters+="-e ARTIFACT_USER_NAME=${script.env.ARTIFACT_USER_NAME} -e ARTIFACT_USER_PASSWORD=${script.env.ARTIFACT_USER_PASSWORD}"
	            	def dockerCmd="docker run --rm ${dockerVolumen} ${dockerEnviromentParameters} ${dockerParameters} ${dockerGradleJavaEnviroment}"
	            	steps.sh "${dockerCmd} ${executionCommand}"
	            }
			}else{
				withAzureVaultCredentials([	[azureCredentialVariable:"ARTIFACT_USER_NAME",azureCredentialId: projectNameLowercase+'-jenkins-artefactory-token-username'],
											[azureCredentialVariable:"ARTIFACT_USER_PASSWORD",azureCredentialId: projectNameLowercase+'-jenkins-artefactory-token-password']
										  ]){
	                dockerEnviromentParameters+="-e ARTIFACT_USER_NAME=${script.env.ARTIFACT_USER_NAME} -e ARTIFACT_USER_PASSWORD=${script.env.ARTIFACT_USER_PASSWORD}"
	            	def dockerCmd="docker run --rm ${dockerVolumen} ${dockerEnviromentParameters} ${dockerParameters} ${dockerGradleJavaEnviroment}"
	            	steps.sh "${dockerCmd} ${executionCommand}"
				}
			}
        }
     }
  }

  /*
  * This method deploy artifact on Artefactory with Android Gradle
  */
  def deployArtifactWithGradleAndroid(
    String enviroment = '',
    String tagName = '',
    String assemblyFlavor='',
    boolean generateSigningFile=false,
    boolean enabledDexGuardOfuscation=false
  ) {
    def repositoryURL="${artefactoryUrl}"
    deployArtifactWithGradleAndroidURL(
      enviroment,
      repositoryURL,
      tagName,
      true,
      assemblyFlavor,
      generateSigningFile,
      enabledDexGuardOfuscation
    )
  }

  /*
  * This method deploy artifact with Android Gradle
  */
  def deployArtifactWithGradleAndroidURL(
    String enviroment = '',
    String repositoryURL="${artefactoryUrl}/",
    String tagName = '',
    boolean artefactory=true,
    String assemblyFlavor='',
    boolean generateSigningFile=false,
    boolean enabledDexGuardOfuscation=false
  ) {
    GradleAndroidJenkinsUtil gradleAndroidJenkinsUtil = this.getInstanceGradleAndroidJenkinsUtil()
    gradleAndroidJenkinsUtil.environment = enviroment
    gradleAndroidJenkinsUtil.setAzureKeyVaultEnabled(azureKeyVaultActivated)
    gradleAndroidJenkinsUtil.setAssembleFlavor(assemblyFlavor)
    gradleAndroidJenkinsUtil.setGenerateSigningFile(generateSigningFile)
    gradleAndroidJenkinsUtil.setEnabledDexGuardObfuscation(enabledDexGuardOfuscation)
    gradleAndroidJenkinsUtil.artefactory = artefactory
    gradleAndroidJenkinsUtil.repositoryURL = repositoryURL
    gradleAndroidJenkinsUtil.uploadArtifact(tagName)
  }

  def getGradleDependencies(String relativeAppPath,String dockerEngine=dockerNativeScriptEnviroment) {
      steps.writeFile(
       file:"${script.env.WORKSPACE}/gradle-jenkins",
       text: getGradleInitScript()
      )
      def cacertsVolume="-v ${script.env.APPLICATION_TOOL}/oracle_java/jdk1.8.0_172/jre/lib/security/cacerts:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/security/cacerts"
      def dockerParameters="--network=host"
      steps.sh "mkdir -p ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}"
      def dockerVolumen=" -v ${script.env.APPLICATION_CACHE}/gradle-local-repo:/gradle-home -v ${script.env.WORKSPACE}:/project/data -w /project/data ${cacertsVolume} "
      dockerVolumen+=" -v ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}:/gradle-project-cache"
      steps.echo "dockerVolumen: ${dockerVolumen}"
      def dockerCmd="docker run --rm ${dockerVolumen} ${dockerParameters} ${dockerEngine}"
      steps.sh "${dockerCmd} bash -c \"cd ./${relativeAppPath} && gradle dependencies --project-cache-dir /gradle-project-cache --gradle-user-home=/gradle-home --stacktrace -Dorg.gradle.daemon=false --init-script /project/data/gradle-jenkins \""
  }

  def getCompileSdkVersionNativeScript(String relativeAppPath,String dockerEngine=dockerNativeScriptEnviroment) {
      steps.writeFile(
       file:"${script.env.WORKSPACE}/gradle-jenkins",
       text: getGradleInitScript()
      )
      def cacertsVolume="-v ${script.env.APPLICATION_TOOL}/oracle_java/jdk1.8.0_172/jre/lib/security/cacerts:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/security/cacerts"
      def dockerParameters="--network=host"
      steps.sh "mkdir -p ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}"
      def dockerVolumen=" -v ${script.env.APPLICATION_CACHE}/gradle-local-repo:/gradle-home -v ${script.env.WORKSPACE}:/project/data -w /project/data ${cacertsVolume} "
      dockerVolumen+=" -v ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}:/gradle-project-cache"
      steps.echo "dockerVolumen: ${dockerVolumen}"
      def dockerCmd="docker run --rm ${dockerVolumen} ${dockerParameters} ${dockerEngine}"
      try{
        def compileSdkVersion=steps.sh(script: "${dockerCmd} bash -c \"cd ./${relativeAppPath} && gradle getCompileSdkVersion --project-cache-dir /gradle-project-cache --gradle-user-home=/gradle-home -Dorg.gradle.daemon=false --init-script /project/data/gradle-jenkins | sed -n '/getCompileSdkVersion/{N; p}' | tail -n1 \"", returnStdout: true).trim()
        steps.echo "compileSdkVersion: ${compileSdkVersion}"
        if(compileSdkVersion.size()>1){
          return compileSdkVersion
        }
      }catch(Exception e) {
          steps.sh "echo \"${e}\""
      }
      return null
  }

  /*
  * This method deploy artifact with Android Gradle
  */
  def deployArtifactNativeScript(String enviroment = '',String buildParams = '',String tagName = '',String assemblyFlavor='debug',boolean signing=false,boolean enabledDexGuardOfuscation=false,boolean enabledJscrambler=false,boolean enabledMutualAuth=false, boolean enabledDynatrace=true, boolean enabledFirebase=true,enabledPseudoMutualAuth=true) {
    steps.echo "NativeScript IOS enabled: ${nativeScriptIOSEnabled}"
    steps.echo "NativeScript Android enabled: ${nativeScriptAndroidEnabled}"

    def buildUniqueId

    def projectName="${script.env.project}"
    def projectNameLowercase=projectName.toLowerCase()
    def buildTimestamp=getBuildTimestamp()
    def applicationName=getApplicationNameNativeScript()
    def applicationVersion=getApplicationVersionForNativeScript()

    if(tagName!=null&&tagName!=''){
        //Clean up to prevent unclean data
        steps.step([$class: 'WsCleanup'])
        buildUniqueId=steps.sh(script: "echo ${tagName} | cut -d '/' -f2 ", returnStdout: true).trim()
        checkoutBranch(tagName)
        buildNativeScript(enviroment, buildParams, assemblyFlavor, signing, enabledDexGuardOfuscation, enabledJscrambler, enabledMutualAuth, enabledDynatrace, enabledFirebase,enabledPseudoMutualAuth)
    }else{
        buildUniqueId="${buildTimestamp}"
    }

    def repository
    enviroment=enviroment.toUpperCase()

    def repoKey

    if ("${branchName}"=="master"){
       repoKey="${script.env.project}.Generic.Release"
    }else{
       repoKey="${script.env.project}.Generic.Snapshot"
    }
    if(enviroment&&enviroment!="PROD"){
       repoKey="${script.env.project}.Generic-${enviroment}"
    }
    //steps.echo "Code Sign Identity: ${codeSignIdentity}"
    steps.echo "Enviroment: ${enviroment}"
    steps.echo "Branch name: ${branchName}"
    //If the application is set for promotion to production
    if(enviroment=='PROD'){
      if ("${branchName}".startsWith("tags/RC-")){
      	repoKey="${script.env.project}.Generic.Release"
      	buildUniqueId=buildUniqueId+"-PROMOTED"
      }
    }

    def moduleName = "${projectName}".toLowerCase()
    def groupIdName=getGroupIdNativeScript()
    groupIdName=groupIdName.replace(".","/")
    def projectKeyName="${script.env.project}".toLowerCase()+"-${applicationName}"
    def actionURL="${artefactoryUrl}/${repoKey}/${moduleName}/${groupIdName}/${applicationName}/${applicationVersion}/"
    steps.echo "Repository Path: ${actionURL}"
    def actionBuildURL="${artefactoryUrl}/api/build"
    steps.echo "Artifactory Buil Api URL: ${actionURL}"

    steps.withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: projectNameLowercase+'-jenkins-artefactory-token',
                        usernameVariable: 'ARTIFACT_USER_NAME', passwordVariable: 'ARTIFACT_USER_PASSWORD']]) {

         def artifactSha1List = []
         def artifactMd5List = []
         def artifactPackageList = []

         def artifactSha1IPA
         def artifactMd5IPA

         if(nativeScriptIOSEnabled){
          def packageType="ipa"
          def artifactFolder="platforms/ios/build/device"
          artifactSha1IPA=steps.sh(script: "cd ${script.env.WORKSPACE}/${artifactFolder} && sha1sum ${applicationName}-${applicationVersion}.${packageType} | cut -d ' ' -f 1 ", returnStdout: true).trim()
          artifactMd5IPA=steps.sh(script: "cd ${script.env.WORKSPACE}/${artifactFolder} && md5sum ${applicationName}-${applicationVersion}.${packageType} | cut -d ' ' -f 1 ", returnStdout: true).trim()
          steps.echo "artifactSha1IPA: ${artifactSha1IPA}"
          steps.echo "artifactMd5IPA: ${artifactMd5IPA}"

          def result = steps.sh (script: """
          curl -o response-build-info-${buildTimestamp}.txt -X PUT --header \"X-Checksum-MD5:${artifactMd5IPA}\" --header \"X-Checksum-Sha1:${artifactSha1IPA}\" -u ${script.env.ARTIFACT_USER_NAME}:${script.env.ARTIFACT_USER_PASSWORD} -T ${artifactFolder}/${applicationName}-${applicationVersion}.${packageType} "${actionURL}${applicationName}-${applicationVersion}.${packageType};build.name=${projectKeyName};build.number=${buildUniqueId};build.userId=${buildUserId}"
          """, returnStdout: true)
          def callback = steps.sh(script: "cat response-build-info-${buildTimestamp}.txt", returnStdout: true).trim()
          steps.echo "Callback: ${callback}"

          def errorMessageArtifactory = steps.sh (script:  "cat response-build-info-${buildTimestamp}.txt | jq --raw-output '.errors | .[0] | .message'", returnStdout: true).trim()

          //Artefact has promoted or moved
          if(errorMessageArtifactory!="null"){
              throw new Exception("Error deploying the artifact to the artifact server")
          }
          artifactSha1List.add(artifactSha1IPA)
          artifactMd5List.add(artifactMd5IPA)
          artifactPackageList.add(packageType)
         }

         def artifactSha1APK
         def artifactMd5APK
         if(nativeScriptAndroidEnabled){
          def packageType="apk"
          def artifactFolder="platforms/android/app/build/outputs/apk"
          artifactSha1APK=steps.sh(script: "cd ${script.env.WORKSPACE}/${artifactFolder} && sha1sum ${applicationName}-${applicationVersion}.${packageType} | cut -d ' ' -f 1 ", returnStdout: true).trim()
          artifactMd5APK=steps.sh(script: "cd ${script.env.WORKSPACE}/${artifactFolder} && md5sum ${applicationName}-${applicationVersion}.${packageType} | cut -d ' ' -f 1 ", returnStdout: true).trim()
          steps.echo "artifactSha1APK: ${artifactSha1APK}"
          steps.echo "artifactMd5APK: ${artifactMd5APK}"

          def result = steps.sh (script: """
          curl -o response-build-info-${buildTimestamp}.txt -X PUT --header \"X-Checksum-MD5:${artifactMd5APK}\" --header \"X-Checksum-Sha1:${artifactSha1APK}\" -u ${script.env.ARTIFACT_USER_NAME}:${script.env.ARTIFACT_USER_PASSWORD} -T ${artifactFolder}/${applicationName}-${applicationVersion}.${packageType} "${actionURL}${applicationName}-${applicationVersion}.${packageType};build.name=${projectKeyName};build.number=${buildUniqueId};build.userId=${buildUserId}"
          """, returnStdout: true)
          def callback = steps.sh(script: "cat response-build-info-${buildTimestamp}.txt", returnStdout: true).trim()
          steps.echo "Callback: ${callback}"

          def errorMessageArtifactory = steps.sh (script:  "cat response-build-info-${buildTimestamp}.txt | jq --raw-output '.errors | .[0] | .message'", returnStdout: true).trim()

          //Artefact has promoted or moved
          if(errorMessageArtifactory!="null"){
              throw new Exception("Error deploying the artifact to the artifact server")
          }
          artifactSha1List.add(artifactSha1APK)
          artifactMd5List.add(artifactMd5APK)
          artifactPackageList.add(packageType)
         }

         if(nativeScriptIOSEnabled || nativeScriptAndroidEnabled){
            //Nanoseconds not supported for MAC, set to 0
            def timestamp=steps.sh(script: "echo \$(date +\"%Y-%m-%dT%H:%M:%S.%3N%z\")", returnStdout: true).trim()
            def artifactoryBuildInfoJson=getArtifactoryBuildInfoByList("${script.env.ARTIFACT_USER_NAME}",moduleName,applicationName,applicationVersion,buildUniqueId,timestamp,artifactPackageList,artifactSha1List,artifactMd5List)
            steps.echo "artifactoryBuildInfoJson: ${artifactoryBuildInfoJson}"

            steps.writeFile(
              file: "${script.env.WORKSPACE}/artifactory-json-${buildTimestamp}.json",
              text: artifactoryBuildInfoJson
            )

            def result = steps.sh (script: """
            curl -X PUT -I -i -u ${script.env.ARTIFACT_USER_NAME}:${script.env.ARTIFACT_USER_PASSWORD} -H \"Content-Type: application/json\" -T artifactory-json-${buildTimestamp}.json "${actionBuildURL}"
            """, returnStdout: true)
            steps.echo "Result: ${result}"
         }
    }
  }

  /*
  * This method deploy artifact for node projects
  */
  def deployArtifactForNode(String enviroment = '',String tagName = '',String buildParameters= '',enableJscrambler=false) {
    def buildUniqueId
    if(tagName!=null&&tagName!=''){
        //Clean up to prevent unclean data
      steps.step([$class: 'WsCleanup'])
        buildUniqueId=steps.sh(script: "echo ${tagName} | cut -d '/' -f2 ", returnStdout: true).trim()
        checkoutBranch(tagName)
        //TODO: ACTIVAR LOS TESTS UNITARIOS CON KARMA Y PHANTOMJS EN CERTI Y PROD
        buildNode(buildParameters,true,enableJscrambler)
    }else{
        buildUniqueId="${buildTimestamp}"
    }
     def dockerVolumen="-v ${script.env.WORKSPACE}:/project/data -w /project/data -v \${JENKINS_HOME}/nodecache:/opt/nodecache"
     steps.echo "dockerVolumen: ${dockerVolumen}"
     def appName =getApplicationNameNode()
     appName=appName.trim()
     def appVersion=getApplicationVersionNode()
     appVersion=appVersion.trim()
     def isPrivate = steps.sh script: "jq --raw-output '.private' ${script.env.WORKSPACE}/package.json", returnStdout: true
     isPrivate=appVersion.trim()
     def access="public"

     def currentDate = buildTimestamp
     enviroment=enviroment.toUpperCase()

     if ("${branchName}"=="master"){
        repositoryId = "${script.env.project}.npm.Release"
     }else{
        repositoryId = "${script.env.project}.npm.Snapshot"
     }

     if(enviroment&&enviroment!="PROD"){
      repositoryId = "${script.env.project}.npm-${enviroment}"
    }

     //If the application is set for promotion to production
     if(enviroment=='PROD'){
       		if ("${branchName}".startsWith("tags/RC-")){
       			repositoryId="${script.env.project}.npm.Release"
       			buildUniqueId=buildUniqueId+"-PROMOTED"
       		}
     }

     steps.echo "Deploy to repository: ${repositoryId}"

     steps.sh "mkdir -p ${script.env.WORKSPACE}/temp${script.env.BUILD_NUMBER}/package/"
     if(isPrivate){
       steps.sh "jq '.private = false' ${script.env.WORKSPACE}/package.json > ${script.env.WORKSPACE}/temp${script.env.BUILD_NUMBER}/package/package.json"
     }

     steps.sh "cp -R ${script.env.WORKSPACE}/dist/ ${script.env.WORKSPACE}/temp${script.env.BUILD_NUMBER}/package/"
     steps.sh "ls -la ${script.env.WORKSPACE}/temp${script.env.BUILD_NUMBER}"
     steps.sh "echo 'ic: JENKINS\nbuild-url: ${script.env.BUILD_URL}\nbranch: ${branchName}\ncommit: ${currentGitCommit}\ndate: ${currentDate}' > ${script.env.WORKSPACE}/temp${script.env.BUILD_NUMBER}/package/build.info"
     steps.sh "cd ${script.env.WORKSPACE}/temp${script.env.BUILD_NUMBER} && tar -czvf ../${appName}-${appVersion}.tgz *"

     def artifact="${appName}-${appVersion}"
     steps.echo "artifact: ${artifact}"
     def nodeDeployRegistryUrl="${artefactoryUrl}/${repositoryId}/"
     buildInfoNode(nodeDeployRegistryUrl,buildUniqueId)

     // Reverting modified package.json
     // NOTE: Workaround para que no considere estos archivos que se han modificado en la instalacion de los paquetes.
     try{
       steps.sh "git checkout -- ${script.env.WORKSPACE}/package.json"
       steps.sh "git checkout -- ${script.env.WORKSPACE}/package-lock.json"
     }catch(e){
       steps.echo "${e}"
     }

  }



  /*
  * Deploy Web App to Azure
  */
  def deployAngularToAzureWebapp(String azureResourceGroupName = '',String azureWebAppName = '',String azureSlot= '',String deployMode="sp") {
    if(!azureResourceGroupName || !azureWebAppName || !azureSlot){
      throw new Exception("azureResourceGroupName, azureWebAppName, azureSlot are required")
    }

    def deployFolder = "dist"
    def projectName  = "${script.env.project}".toLowerCase()
    def appName      = getApplicationNameNode()

    def azureSpId       = "${projectName}-${appName}-webapp-id"
    def azureSpPassword = "${projectName}-${appName}-webapp-password"
    def azureSpTenant   = "${projectName}-${appName}-webapp-tenant"

    def loginArgs  = ""
    def deployArgs = ""

    steps.echo """
      NOTE: - The credentials should be populate on project folder credentials
        * Service Principal ID: ${azureSpId}
        * Service Principal Password: ${azureSpPassword}
        * Service Principal Tenant: ${azureSpTenant}

      Azure Webapp Deployment INFO
        * Webapp Resource Group: ${azureResourceGroupName}
        * Webapp Name: ${azureWebAppName}
        * Webapp Slot: ${azureSlot}
        * Deployment Folder: ${deployFolder}
        * Deployment Mode: ${deployMode}
    """

    if(deployMode == "sp" || deployMode == "sp-cert"){
      loginArgs+="--service-principal"
    }

    if(!azureSlot.equals("production")){
      deployArgs+="--slot ${azureSlot}"
    }

    steps.sh """
      set +x
      cd ${script.env.WORKSPACE}/${deployFolder}
      zip -rq artifact.zip *
    """

    def PASSWORD_BINDING = "StringBinding";

    if(deployMode == "sp-cert"){
      PASSWORD_BINDING = "FileBinding"
    }

    docker.withRegistry(script.env.DOCKER_REGISTRY_URL) {
      docker.withServer(script.env.DOCKER_HOST){
        steps.withCredentials([
          [$class: "StringBinding", credentialsId: "${azureSpId}", variable: "azureMasterSPValue" ],
          [$class: PASSWORD_BINDING, credentialsId: "${azureSpPassword}", variable: "azureMasterSpPasswordValue" ],
          [$class: "StringBinding", credentialsId: "${azureSpTenant}", variable: "azureMasterTenantIdValue" ]
        ]){
          //script.env.azureMasterSPValue         = script.env.azureMasterSPValue.replaceAll( /([^a-zA-Z0-9])/, '\\\\$1' )
          //script.env.azureMasterTenantIdValue   = script.env.azureMasterTenantIdValue.replaceAll( /([^a-zA-Z0-9])/, '\\\\$1' )
          steps.sh """
          	set +x
          	if [[ `file ${script.env.azureMasterSpPasswordValue}` != *"CRLF"* ]]; then
                echo "Warning!!! the file of credential ${azureSpPassword} use a LF EOL (End of Line) and have to use a CRLF, maybe this format cause an error in the login of Azure"
            fi
          """

          if(deployMode == "sp" || deployMode == "user"){
            script.env.azureMasterSpPasswordValue = script.env.azureMasterSpPasswordValue.replaceAll( /([^a-zA-Z0-9])/, '\\\\$1' )
          }

          def dockerVolumen    = "-v ${script.env.WORKSPACE}:${script.env.WORKSPACE} -v ${script.env.WORKSPACE}@tmp:${script.env.WORKSPACE}@tmp"
          def dockerParameters = "--network=host --rm"

          try{
            steps.sh """
              docker run ${dockerParameters} ${dockerVolumen} ${dockerAzureClientEnviroment} bash -c 'export https_proxy=${script.env.DOCKER_HTTP_PROXY_URL} &&
              az login ${loginArgs} --output table --username \"${script.env.azureMasterSPValue}\" --password \"${script.env.azureMasterSpPasswordValue}\" --tenant \"${script.env.azureMasterTenantIdValue}\" &&
              az webapp deployment source config-zip --output table --resource-group ${azureResourceGroupName} --name ${azureWebAppName} --src ${script.env.WORKSPACE}/${deployFolder}/artifact.zip ${deployArgs} &&
              az webapp restart --output table --resource-group ${azureResourceGroupName} --name ${azureWebAppName} ${deployArgs}'
            """
          }catch(Exception e){
            if(deployMode == "sp-cert"){
              steps.sh """
                set +x
                rm -rf ${script.env.azureMasterSpPasswordValue}
              """
            }
            throw e;
          }
        }
      }
    }
  }

  def buildInfoNode(String nodeDeployRegistryUrl = '',String buildUniqueId = ''){
    def projectName="${script.env.project}"
    def projectNameLowercase=projectName.toLowerCase()
    def applicationName=getApplicationNameNode()
    def buildTimestamp=getBuildTimestamp()
    def applicationVersion=getApplicationVersionNode()
    def moduleName = "${projectName}".toLowerCase()
    def projectKeyName="${script.env.project}".toLowerCase()+"-${applicationName}"
    def actionURL="${nodeDeployRegistryUrl}"
    steps.echo "Repository Path: ${actionURL}"
    def actionBuildURL="${artefactoryUrl}/api/build"
    steps.echo "Artifactory Buil Api URL: ${actionURL}"
    def artifactSha1=steps.sh(script: "sha1sum ${applicationName}-${applicationVersion}.tgz | cut -d ' ' -f 1", returnStdout: true).trim()
    def artifactMd5=steps.sh(script: "md5sum ${applicationName}-${applicationVersion}.tgz | cut -d ' ' -f 1", returnStdout: true).trim()
    steps.echo "artifactSha1: ${artifactSha1}"
    steps.echo "artifactMd5: ${artifactMd5}"

    def artifactoryUserName
    def artifactoryPassword

    def azureKeyVaultForArtifactoryActivated = false;
    //def azureKeyVaultForArtifactoryActivated = azureKeyVaultActivated

    if(!azureKeyVaultForArtifactoryActivated){
	     steps.withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: projectNameLowercase+'-jenkins-artefactory-token',
	                        usernameVariable: 'ARTIFACT_USER_NAME', passwordVariable: 'ARTIFACT_USER_PASSWORD']]) {
			  artifactoryUserName="${script.env.ARTIFACT_USER_NAME}"
			  artifactoryPassword="${script.env.ARTIFACT_USER_PASSWORD}"
		  }
    }else{
        withAzureVaultCredentials([	[azureCredentialVariable:"ARTIFACT_USER_NAME",azureCredentialId: projectNameLowercase+'-jenkins-artefactory-token-username'],
                        [azureCredentialVariable:"ARTIFACT_USER_PASSWORD",azureCredentialId: projectNameLowercase+'-jenkins-artefactory-token-password']
        ]){
        artifactoryUserName="${script.env.ARTIFACT_USER_NAME}"
        artifactoryPassword="${script.env.ARTIFACT_USER_PASSWORD}"
      }
    }

    steps.wrap([$class: 'MaskPasswordsBuildWrapper', varPasswordPairs: [
														[password: artifactoryUserName, var: 'artifactoryUserNameVariable'],
														[password: artifactoryPassword, var: 'artifactoryPasswordVariable']
				 ]]) {
         def result = steps.sh (script: """
         curl -o response-build-info-${buildTimestamp}.txt -X PUT --header \"X-Checksum-MD5:${artifactMd5}\" --header \"X-Checksum-Sha1:${artifactSha1}\" -u ${artifactoryUserName}:${artifactoryPassword} -T ${applicationName}-${applicationVersion}.tgz "${actionURL}${applicationName}-${applicationVersion}.tgz;build.name=${projectKeyName};build.number=${buildUniqueId}"
         """, returnStdout: true)
         def callback = steps.sh(script: "cat response-build-info-${buildTimestamp}.txt", returnStdout: true).trim()
         steps.echo "Callback: ${callback}"

         def errorMessageArtifactory = steps.sh (script:  "cat response-build-info-${buildTimestamp}.txt | jq --raw-output '.errors | .[0] | .message'", returnStdout: true).trim()

         //Artefact has promoted or moved
         if(errorMessageArtifactory!="null"){
             throw new Exception("Error deploying the artifact to the artifact server")
         }

         def timestamp=steps.sh(script: "echo \$(date +\"%Y-%m-%dT%H:%M:%S.%3N%z\")", returnStdout: true).trim()
         def artifactoryBuildInfoJson=getArtifactoryBuildInfo("${script.env.ARTIFACT_USER_NAME}",moduleName,applicationName,applicationVersion,buildUniqueId,timestamp,'tgz',artifactSha1,artifactMd5)
         steps.echo "artifactoryBuildInfoJson: ${artifactoryBuildInfoJson}"

         steps.writeFile(
          file: "${script.env.WORKSPACE}/artifactory-json-${buildTimestamp}.json",
          text: artifactoryBuildInfoJson
         )

         result = steps.sh (script: """
         curl -X PUT -I -i -u ${artifactoryUserName}:${artifactoryPassword} -H \"Content-Type: application/json\" -T artifactory-json-${buildTimestamp}.json "${actionBuildURL}"
         """, returnStdout: true)
         steps.echo "Result: ${result}"
    }
  }

  /*
  * This method support BBD for funtional test
  */
  def executeCucumberWithGradleAndroid(String relativeApkFolderPath,String deviceName="Samsung Galaxy S6") {
    getInstanceGradleAndroidJenkinsUtil().executeCucumber(relativeApkFolderPath, deviceName)
  }

  /*
  * This method support BBD for funtional test
  */
  def createAndroidEmulatorInstance(String relativeApkFolderPath,String deviceName="Samsung Galaxy S6") {
      def apkFolderPath="${script.env.WORKSPACE}:/project/data"

      def dockerVolumen="-v ${apkFolderPath}"

      steps.echo "dockerVolumen: ${dockerVolumen}"
      steps.sh "echo '' > ${script.env.WORKSPACE}/gradle-jenkins"

      steps.writeFile(
        file:"${script.env.WORKSPACE}/gradle-jenkins",
        text: getGradleInitScript()
      )

      def containerId=""
      def containerDockerHost = steps.sh script: "echo \"tcp://\$(ip route show | grep default| awk '{ print \$3 }' | head -n 1):2375\"", returnStdout: true
      docker.withServer("${containerDockerHost}"){
          docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
            def dockerParameters="-e SAVE_BOOT_SNAPSHOT=False -e APPIUM=True -e DEVICE=\"${deviceName}\" -P"
            def dockerCmd="docker run --privileged -d ${dockerVolumen} ${dockerParameters} ${dockerAndroidEmulatorEnviroment}"
            containerId=steps.sh (
              script: "${dockerCmd}",
              returnStdout: true
            ).trim()
            steps.echo "Wait for emulator"
            steps.sh (
              script: "docker exec -t ${containerId} adb wait-for-device shell \'while [[ -z \$(getprop sys.boot_completed) ]]; do sleep 1; done; input keyevent 82\'",
              returnStdout: true
            ).trim()
            steps.echo "Emulator up and running"
         }
      }
      return containerId
  }

  /*
  * This method run the prepare release and release perform maven
  */
  def preReleaseMaven(String enviromentParam) {
    if ("${branchName}"=="master"){
        throw new Exception("The pre-release must not be done on master")
    }

    steps.step([$class: 'WsCleanup'])
    steps.checkout script.scm

    def tagStateComment="Pre-release"
    def currentDate = buildTimestamp
    def tagName=getApplicationVersion()+"-${enviromentParam}-${currentDate}"
    tagBranch(tagName,tagStateComment,enviromentParam)
    deployArtifactWithMaven(enviromentParam)
  }

  /*
  * This method run the prepare release and release perform maven
  */
  def releaseMaven() {
    if ("${branchName}"!="master"){
        throw new Exception("The release must be done on master")
    }
    steps.step([$class: 'WsCleanup'])
    steps.checkout script.scm

    def repository
    if ("${branchName}"=="master"){
       repositoryId = "${script.env.project}_release"
       repository="${script.env.ARTIFACT_SERVER_URL}/content/repositories/${script.env.project}.Release"
    }

    def altDeploymentRepository="-DaltDeploymentRepository=${repositoryId}::default::${repository}"

    steps.withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: currentCredentialsId,
                    usernameVariable: 'GITLAB_USER_VALUE', passwordVariable: 'GITLAB_USER_PASSWORD']]) {
        def projectName="${script.env.project}"
        def projectLowerName="${script.env.project}".toLowerCase()
        def currentDate = buildTimestamp
        def tagComment
        try{
            def gitlabCommentPrefix="[ci-${branchName}]"

            tagComment="${gitlabCommentPrefix} Release \\\${line.separator}branch: ${branchName}\\\${line.separator}commit: ${currentGitCommit}\\\${line.separator}date: ${currentDate}\\\${line.separator}build-url: ${script.env.BUILD_URL}\\\${line.separator}"
            steps.sh "'${mvnHome}/bin/mvn' --batch-mode release:clean release:prepare -Darguments=\"-DskipTests -Dmaven.javadoc.failOnError=false\" -DscmCommentPrefix=\"${tagComment}\" -DPROJECT_NAME=${projectName} -DPROJECT_LOWER_NAME=${projectLowerName} -DSCM_SERVER_URL=${gitlabUrl} -Dusername=${script.env.GITLAB_USER_VALUE} -Dpassword=${script.env.GITLAB_USER_PASSWORD}"
            steps.sh "'${mvnHome}/bin/mvn' --batch-mode release:perform -DdeployAtEnd=true -Darguments=\"-DdeployAtEnd=true -DskipTests -Dmaven.javadoc.failOnError=false -DPROJECT_NAME=${projectName} ${altDeploymentRepository}\" -DPROJECT_NAME=${projectName} -DPROJECT_LOWER_NAME=${projectLowerName} -DSCM_SERVER_URL=${gitlabUrl} -Dusername=${script.env.GITLAB_USER_VALUE} -Dpassword=${script.env.GITLAB_USER_PASSWORD}"

        } catch(Exception e) {
            //Remember to remove Maven tag: https://issues.apache.org/jira/browse/MRELEASE-229
            steps.sh "'${mvnHome}/bin/mvn' --batch-mode release:rollback ${altDeploymentRepository} -Darguments=\"-DskipTests\" -DPROJECT_NAME=${projectName} -DPROJECT_LOWER_NAME=${projectLowerName} -DSCM_SERVER_URL=${gitlabUrl} -DscmCommentPrefix=\"${tagComment}\" -Dusername=${script.env.GITLAB_USER_VALUE} -Dpassword=${script.env.GITLAB_USER_PASSWORD}"
            throw e

        }
    }
  }

  /*
  * This method to build docker image
  */
  def builSprintBootDockerFile(dockerHost,tag) {
     steps.withEnv(["DOCKER_HOST="+dockerHost]){
        steps.sh "cp ./target/*.jar ."
        steps.echo "Docker tag: ${tag}"
        def currentDate = buildTimestamp
        newApp = docker.build(tag, "-f ./src/main/docker/Dockerfile c branch=${branchName}  --label gitcommit=${currentGitCommit} --label builddate=${currentDate} --label projectname=${script.env.project} --label build-url=${script.env.BUILD_URL} .")
     }
  }

   /*
  * This method to build docker image
  */
  def buildDockerFileMaven(String enviroment = '') {
     enviroment=enviroment.toLowerCase()
     def currentDate = buildTimestamp
     steps.withEnv(["DOCKER_HOST=${script.env.DOCKER_HOST}"]){
         if ("${branchName}"=="master"&&(enviroment!=''||enviroment==null)){
            throw new Exception("You can not set enviroment to master branch")
         }

         steps.echo "Docker registryHost: ${script.env.DOCKER_REGISTRY_URL}"
         def projectLowerName="${script.env.project}".toLowerCase()

         def applicationName=getApplicationName()
         def applicationVersion=getApplicationVersion()
         if("${branchName}"=="master"){
             applicationVersion=getMavenArtifactReleaseVersion()
         }
         def tagName="${projectLowerName}/${applicationName}:${applicationVersion}"
         if("${enviroment}"!=null&&"${enviroment}"!=''){
             tagName="${tagName}-${enviroment}"
         }
         def dockerFilePath="./src/main/docker/Dockerfile"
         if ("${branchName}"!="master"){
            tagName="${tagName}-${currentDate}"
         }else{
            dockerFilePath="./target/checkout/src/main/docker/Dockerfile"
         }
         steps.echo "Docker tag: ${tagName}"
         steps.echo "Docker file path: ${dockerFilePath}"

         newApp = docker.build(tagName, "-f ${dockerFilePath} --label branch=${branchName} --label applicationName=${applicationName} --label applicationVersion=${applicationVersion} --label gitcommit=${currentGitCommit} --label builddate=${currentDate} --label projectname=${script.env.project} --label build-url=${script.env.BUILD_URL} .")
     }
  }

  /*
  * This method to push to Docker Registry
  */
  def pushDockerRegistryMaven(String enviroment = '') {
     enviroment=enviroment.toLowerCase()
     if ("${branchName}"=="master"&&(enviroment!=''||enviroment==null)){
        throw new Exception("You can not set enviroment to master branch")
     }

     steps.echo "Docker registryHost: ${script.env.DOCKER_REGISTRY_URL}"
     def projectName="${script.env.project}"
     def projectLowerName="${script.env.project}".toLowerCase()

     def applicationName=getApplicationName()
     def applicationVersion=getApplicationVersion()
     if("${branchName}"=="master"){
         applicationVersion=getMavenArtifactReleaseVersion()
     }
     def tagName="${projectLowerName}/${applicationName}:${applicationVersion}"
     if("${enviroment}"!=null&&"${enviroment}"!=''){
         tagName="${tagName}-${enviroment}"
     }
     def currentDate = buildTimestamp
     if ("${branchName}"!="master"){
         tagName="${tagName}-${currentDate}"
     }
     steps.echo "Docker tag: ${tagName}"
     pushDockerRegistry("${script.env.DOCKER_HOST}","${script.env.DOCKER_REGISTRY_URL}",tagName)
  }

  /*
  * This method to push to Docker Registry
  */
  def pushDockerRegistry(dockerHost,registryHost,tag) {
     steps.withEnv(["DOCKER_HOST="+dockerHost]){
        steps.echo "Docker registryHost: ${registryHost}"
        steps.echo "Docker tag: ${tag}"
        steps.sh "docker tag ${tag} ${registryHost}/${tag}"
        steps.sh "docker push ${registryHost}/${tag}"
     }
  }

  /*
  * This method run the build on docker
  */
  def runDocker(dockerHost,containerName,exposedPorts) {
     runDocker(dockerHost,containerName,exposedPorts,"${newApp.id}")
  }

  /*
  * This method run the build on docker
  */
  def runDocker(dockerHost,containerName,exposedPorts,image) {
     steps.withEnv(["DOCKER_HOST="+dockerHost]){
        def containerId = steps.sh (
          script: 'docker ps -q --filter "name='+containerName+'"',
          returnStdout: true
        ).trim()
        steps.echo "containerId: "+containerId
        if(containerId!=null&&!containerId.isEmpty()) {
            steps.sh (
              script: 'docker stop '+containerId,
              returnStdout: false
            )
        }
        containerId = steps.sh (
          script: 'docker ps -a -q --filter "name='+containerName+'"',
          returnStdout: true
        ).trim()
        if(containerId!=null&&!containerId.isEmpty()) {
            steps.sh (
              script: 'docker rm '+containerId,
              returnStdout: false
            )
        }
        steps.sh "docker run -d --name="+containerName+" "+exposedPorts+" "+image
        steps.sh "docker ps"
     }
  }

  /*
  * This method allow to checkout branch without clean out the workspace
  */
  def checkoutReleaseBranchWithoutWipeOut(String gitURLParam,String branchNameParam){
	steps.echo "branchNameParam: ${branchNameParam}"
    //steps.checkout script.scm
    steps.checkout([$class: 'GitSCM', branches: [[name: branchNameParam]],
    doGenerateSubmoduleConfigurations: false,
    extensions: [
        [$class: 'LocalBranch', localBranch: '**']
    ],
    submoduleCfg: [],
    userRemoteConfigs: [
        [   credentialsId: currentCredentialsId,
            url: gitURLParam]
        ]
    ])
    branchName = steps.sh (
        script: 'git name-rev --name-only HEAD | sed "s?.*remotes/origin/??"',
        returnStdout: true
        ).trim()
    currentGitCommit = steps.sh (
        script: 'git rev-parse HEAD',
        returnStdout: true
        ).trim()
    steps.echo "branchName: ${branchName}"
    steps.echo "currentGitCommit: ${currentGitCommit}"
  }

  /*
  * This method will update version to a release version for maven
  */
  def updateToReleaseVersionMaven(String version){
    getInstanceMavenJenkinsUtil().updateToReleaseVersionMaven(version)
  }

  /*
  * This method will update version to a release version for gradle
  */
  def updateToReleaseVersionGradle(String version){
    steps.sh (script: """
    sed -i -e "s;^version=.*;version=${version};g" gradle.properties
    """, returnStdout: true)
    updateToReleaseVersion(version)
  }

  /*
  * This method will update version to a release version for gradle
  */
  def updateToReleaseVersionFreeStyleProject(String version){
    this.getInstanceFreeStyleJenkinsUtil().updateToReleaseVersionFreeStyle(version);
  }

  /*
  * This method will return relationship
  */
  def getRelationship(String sourceBranch,String targetBranch){
    def relationship = steps.sh (
     	script: """
          if git merge-base --is-ancestor "${sourceBranch}" "${targetBranch}"; then
              echo 'ancestor'
          elif git merge-base --is-ancestor "${targetBranch}" "${sourceBranch}"; then
              echo 'descendant'
          else
              echo 'unrelated'
          fi
     	""",
     	returnStdout: true
     ).trim()
     return relationship
  }

  /*
  * This method will communicate a clean merge
  */
  def verifyMergeAgainstBranch(String sourceBranch,String targetBranch){
    steps.sh "git log --decorate --graph --oneline --all"
    boolean cleanMerge=false
    //only if not already at branchTo
    steps.sh "git checkout ${targetBranch}"
    steps.sh "git checkout -b branchTmp"
    steps.sh "git merge --no-ff --no-commit ${sourceBranch}"
    //see what happens
    steps.sh "git checkout ${sourceBranch}"
    steps.sh "git branch -d branchTmp"
    //TODO: act accordingly
  }

  /*
  * This method will generate a release version
  */
  def startReleaseMaven(){
    MavenJenkinsUtil mavenJenkinsUtil = this.getInstanceMavenJenkinsUtil()
    mavenJenkinsUtil.setFortifyEnabled(this.fortifyActivated)
    mavenJenkinsUtil.startRelease()
  }

  /*
  * This method will generate a SalesForce Deploy Info
  */
  def generateSalesForceInfoForDeploy(String enviromentParam, String serverURLParam, String maxPollParam, String zipFileParam){
    def currentDate = buildTimestamp
    def projectName="${script.env.project}".toLowerCase()
    def appName=getApplicationNameForSalesForce()
    def enviroment=enviromentParam.toLowerCase()
    def serverURL=serverURLParam
    def maxPoll=maxPollParam
    def zipFile=zipFileParam

    steps.echo "Enviroment: ${enviromentParam}"
    steps.echo "SalesForce ServerUrl: ${serverURL}"
    steps.echo "SalesForce MaxPoll: ${maxPoll}"
    steps.echo "SalesForce Zip File: ${zipFile}"

    String sfCredentials="${projectName}-${appName}-credentials-salesforce-${enviromentParam}"
    String sfTokenId="${projectName}-${appName}-tokenid-salesforce-${enviromentParam}"

    steps.echo """************ Generating Sales Force Info for Deployment - BEGIN ************
      NOTE: - The credentials should be populate on project folder credentials
      SalesForce Credentials (UsernamePassword): ${sfCredentials}
      SalesForce TokenId (SecretText): ${sfTokenId}
    """

    steps.withCredentials([
        [$class: 'UsernamePasswordMultiBinding', credentialsId: sfCredentials,
        usernameVariable: 'sfUserNameValue', passwordVariable: 'sfPasswordValue'],
        [$class: "StringBinding", credentialsId: "${sfTokenId}", variable: "sfTokenIdValue"]]) {
      steps.sh """set +x
      echo 'sf.username=${script.env.sfUserNameValue}' > ${script.env.WORKSPACE}/build.properties
      echo 'sf.password=${script.env.sfPasswordValue}${script.env.sfTokenIdValue}' >> ${script.env.WORKSPACE}/build.properties
      echo 'sf.serverurl=${serverURL}' >> ${script.env.WORKSPACE}/build.properties
      echo 'sf.maxPoll=${maxPoll}' >> ${script.env.WORKSPACE}/build.properties
      echo 'sf.zipFile=${zipFile}' >> ${script.env.WORKSPACE}/build.properties"""
    }
    steps.echo "************ Generating Sales Force Info for Deployment - END ************"
  }

  /*
  * This method will generate a signing properties for APK
  */
  def generateSigningProperties(String enviromentParam){
    def currentDate = buildTimestamp
    def projectName="${script.env.project}".toLowerCase()
    def appName=getApplicationNameGradle()
    def enviroment=enviromentParam.toLowerCase()
    String storeFileIdName="${projectName}-${appName}-android-trustore-${enviroment}"
    String storePasswordIdName="${projectName}-${appName}-android-storepassword-${enviroment}"
    String keyPasswordIdName="${projectName}-${appName}-android-keypassword-${enviroment}"
    String keyAliasIdName="${projectName}-${appName}-android-keyalias-${enviroment}"

    steps.echo """************ Generating singing properties for APK - BEGIN ************
NOTE: - The credentials should be populate on project folder credentials
Keystore credentials Id(SecretFile): ${storeFileIdName}
Keystore password credentials Id(SecretText): ${storePasswordIdName}
Key Alias credentials Id(SecretText): ${keyAliasIdName}
Key Password Credentials Id(SecretText): ${keyPasswordIdName}"""

    if(!azureKeyVaultActivated){
	      //STANDARD CREDENTIALS PLUGIN
	      steps.withCredentials([	[$class: "FileBinding", credentialsId: "${storeFileIdName}", variable: "storeFilePath"],
	    							[$class: "StringBinding", credentialsId: "${storePasswordIdName}", variable: "storePasswordValue"],
	    							[$class: "StringBinding", credentialsId: "${keyAliasIdName}", variable: "keyAliasValue"],
	    							[$class: "StringBinding", credentialsId: "${keyPasswordIdName}", variable: "keyPasswordValue"]]){
	      steps.sh """set +x
	      cp ${script.env.storeFilePath} ${script.env.WORKSPACE}/jks-${currentDate}
	      echo 'STORE_FILE=/project/data/jks-${currentDate}' > ${script.env.WORKSPACE}/signing.properties
	      echo 'STORE_PASSWORD=${script.env.storePasswordValue}' >> ${script.env.WORKSPACE}/signing.properties
	      echo 'KEY_ALIAS=${script.env.keyAliasValue}' >> ${script.env.WORKSPACE}/signing.properties
	      echo 'KEY_PASSWORD=${script.env.keyPasswordValue}' >> ${script.env.WORKSPACE}/signing.properties"""
	    }
    }else{
    	  String vaultIdName="${projectName}-vault-name"
		  steps.echo "Vault Name credentials Id: ${vaultIdName}"
	      withAzureVaultCredentials([	[azureCredentialVariable:"storeFilePath",azureCredentialId:"${storeFileIdName}",azureCredentialType:"file"],
	    								[azureCredentialVariable:"storePasswordValue",azureCredentialId:"${storePasswordIdName}"],
	    								[azureCredentialVariable:"keyAliasValue",azureCredentialId:"${keyAliasIdName}"],
	    								[azureCredentialVariable:"keyPasswordValue",azureCredentialId:"${keyPasswordIdName}"]]){
	      steps.sh """set +x
	      cp ${script.env.storeFilePath} ${script.env.WORKSPACE}/jks-${currentDate}
	      echo 'STORE_FILE=/project/data/jks-${currentDate}' > ${script.env.WORKSPACE}/signing.properties
	      echo 'STORE_PASSWORD=${script.env.storePasswordValue}' >> ${script.env.WORKSPACE}/signing.properties
	      echo 'KEY_ALIAS=${script.env.keyAliasValue}' >> ${script.env.WORKSPACE}/signing.properties
	      echo 'KEY_PASSWORD=${script.env.keyPasswordValue}' >> ${script.env.WORKSPACE}/signing.properties"""
	    }
    }
    steps.echo "************ Generating singing properties for APK - END ************"
  }

  /*
  * This method will generate a release version for Gradle
  */
  def startReleaseGradle(){
    if ("${branchName}"=="master"){
        throw new Exception("Generating of release must not be done on master")
    }
    //Check generate new version
    def applicationVersion=getApplicationVersionGradle()
    steps.echo "Application version: ${applicationVersion}"
    def nextReleaseVersion=getApplicationNextReleaseVersion(applicationVersion)
    steps.echo "Next release version: ${nextReleaseVersion}"

    def releaseBranchName="release/"+nextReleaseVersion
    def existBranchInRepository=existBranch(releaseBranchName)
    steps.echo "Exist release branch: ${existBranchInRepository}"
    if(existBranchInRepository){
      //If the branch exist an error is throw
      steps.echo "Deleting release branch: ${releaseBranchName}"
      deleteNotMergeReleaseBranch(releaseBranchName)
    }

    createNewReleaseBranch(nextReleaseVersion)
    checkoutReleaseBranch(nextReleaseVersion)
    updateToReleaseVersionGradle(nextReleaseVersion)

    def tagStateComment="Release Candicate"
    def currentDate = buildTimestamp
    def enviromentParam="CERT"
    applicationVersion=getApplicationVersionGradle()
    def tagName="RC-"+applicationVersion+"-${enviromentParam}-${currentDate}"

    tagName=updateTagNameWithACR(tagName)

    protectBranch("release/${nextReleaseVersion}")
	//Validate SAST Analisys
    if(fortifyActivated){
	   executeFortifyGradleSast()
    }
    try{
        executeSonarWithGradle(false,"UTF-8",tagName)
    }catch(Exception e){
    	steps.echo "****** WARNING: Error executing Sonar! ********"
    }
    tagBranch(tagName,tagStateComment,enviromentParam)
    deployArtifactWithGradle(enviromentParam,"tags/${tagName}")
  }

  /*
  * This method will generate a release version for Android Gradle
  */
  def startReleaseAndroidGradle(String assembleFlavor = 'assembleRelease',boolean enabledDexGuardOfuscation=false,boolean generateSigningFile=true){
    GradleAndroidJenkinsUtil gradleAndroidJenkinsUtil = this.getInstanceGradleAndroidJenkinsUtil()
    gradleAndroidJenkinsUtil.setAssembleFlavor(assembleFlavor)
    gradleAndroidJenkinsUtil.setEnabledDexGuardObfuscation(enabledDexGuardOfuscation)
    gradleAndroidJenkinsUtil.setGenerateSigningFile(generateSigningFile)
    gradleAndroidJenkinsUtil.setFortifyEnabled(this.fortifyActivated)
    gradleAndroidJenkinsUtil.startRelease()
  }

  /*
  * This method will generate a release version for Native Script
  */
  def startReleaseNativeScript(String parameters='', boolean enabledDexGuardOfuscation=false,boolean enabledJscrambler=false,boolean generateSigningFile=true,boolean enabledMutualAuth=false, boolean enabledDynatrace=true,boolean enabledFirebase=true,boolean enabledPseudoMutualAuth=true){
    if ("${branchName}"=="master"){
        throw new Exception("Generating of release must not be done on master")
    }
    steps.echo "Enabled dynatrace: ${enabledDynatrace}"
    steps.echo "Enabled dexguard: ${enabledDexGuardOfuscation}"

    //Check generate new version
    def applicationVersion=getApplicationVersionForNativeScript()
    steps.echo "Application version: ${applicationVersion}"
    def nextReleaseVersion=applicationVersion
    steps.echo "Next release version: ${nextReleaseVersion}"

    def releaseBranchName="release/"+nextReleaseVersion
    def existBranchInRepository=existBranch(releaseBranchName)
    steps.echo "Exist release branch: ${existBranchInRepository}"
    if(existBranchInRepository){
      //If the branch exist an error is throw
      steps.echo "Deleting release branch: ${releaseBranchName}"
      deleteNotMergeReleaseBranch(releaseBranchName)
    }

    createNewReleaseBranch(nextReleaseVersion)
    checkoutReleaseBranch(nextReleaseVersion)

    def tagStateComment="Release Candicate"
    def currentDate = buildTimestamp
    def enviromentParam="CERT"
    applicationVersion=getApplicationVersionForNativeScript()
    def tagName="RC-"+applicationVersion+"-${enviromentParam}-${currentDate}"

    tagName=updateTagNameWithACR(tagName)

    protectBranch("release/${nextReleaseVersion}")

    //Validate SAST Analisys
    if(fortifyActivated){
      executeFortifyTypeScriptSast()
    }

    tagBranch(tagName,tagStateComment,enviromentParam)

    deployArtifactNativeScript(enviromentParam,parameters,"tags/${tagName}","release",generateSigningFile, enabledDexGuardOfuscation,enabledJscrambler,enabledMutualAuth, enabledDynatrace, enabledFirebase,enabledPseudoMutualAuth)
  }

  /*
  * This method will generate a release version for IOS
  */
  def startReleaseIOS(String schema){
    if ("${branchName}"=="master"){
        throw new Exception("Generating of release must not be done on master")
    }

    //Check generate new version
    def applicationVersion=getApplicationVersionIOS()
    steps.echo "Application version: ${applicationVersion}"
    //The version is always major version
    def nextReleaseVersion="${applicationVersion}"
    steps.echo "Next release version: ${nextReleaseVersion}"

    def releaseBranchName="release/"+nextReleaseVersion
    def existBranchInRepository=existBranch(releaseBranchName)
    steps.echo "Exist release branch: ${existBranchInRepository}"
    if(existBranchInRepository){
      //If the branch exist an error is throw
      steps.echo "Deleting release branch: ${releaseBranchName}"
      deleteNotMergeReleaseBranch(releaseBranchName)
    }

    createNewReleaseBranch(nextReleaseVersion)
    checkoutReleaseBranch(nextReleaseVersion)

    def tagStateComment="Release Candicate"
    def currentDate = buildTimestamp
    def enviromentParam="CERT"
    applicationVersion=getApplicationVersionIOS()
    def tagName="RC-"+applicationVersion+"-${enviromentParam}-${currentDate}"

    tagName=updateTagNameWithACR(tagName)

    protectBranch("release/${nextReleaseVersion}")
    tagBranch(tagName,tagStateComment,enviromentParam)

    deployArtifactoryIOS(enviromentParam,tagName,schema)
  }

  /*
  * This method will generate a release version for Free Style Projects
  */
  def startReleaseFreeStyleProject(String relativeSrcPath = '',String packageType='zip'){
    FreeStyleJenkinsUtil freeStyleInstance = this.getInstanceFreeStyleJenkinsUtil()
    freeStyleInstance.setRelativeSrcPath(relativeSrcPath)
    freeStyleInstance.setPackageType(packageType)
    freeStyleInstance.startRelease()
  }

  /*
  * This method will generate a release version for Node with Angular Free Style Projects
  */
  def startReleaseForNode(String buildParams = '', boolean enableJscrambler=false){
    if ("${branchName}"=="master"){
        throw new Exception("Generating of release must not be done on master")
    }

    //Validate SAST Analisys
    if(fortifyActivated){
      executeFortifyTypeScriptSast()
    }
    //Check generate new version
    def applicationVersion=getApplicationVersionNode()
    steps.echo "Application version: ${applicationVersion}"
    //The version is always major version
    def nextReleaseVersion="${applicationVersion}"
    steps.echo "Next release version: ${nextReleaseVersion}"

    def releaseBranchName="release/"+nextReleaseVersion
    def existBranchInRepository=existBranch(releaseBranchName)
    steps.echo "Exist release branch: ${existBranchInRepository}"
    if(existBranchInRepository){
      //If the branch exist an error is throw
      steps.echo "Deleting release branch: ${releaseBranchName}"
      deleteNotMergeReleaseBranch(releaseBranchName)
    }

    createNewReleaseBranch(nextReleaseVersion)
    checkoutReleaseBranch(nextReleaseVersion)

    def tagStateComment="Release Candicate"
    def currentDate = buildTimestamp
    def enviromentParam="CERT"
    applicationVersion=getApplicationVersionNode()
    def tagName="RC-"+applicationVersion+"-${enviromentParam}-${currentDate}"

    tagName=updateTagNameWithACR(tagName)

    protectBranch("release/${nextReleaseVersion}")
    try{
        executeSonarForNode(tagName)
    }catch(Exception e){
    	steps.echo "****** WARNING: Error executing Sonar! ********"
    }
    tagBranch(tagName,tagStateComment,enviromentParam)

    deployArtifactForNode(enviromentParam,tagName,buildParams,enableJscrambler)
  }

  /*
  * This method will promote a release version
  */
  def promoteReleaseMaven(String releaseTagName, boolean forceRelease=false){
    this.getInstanceMavenJenkinsUtil().promoteRelease(releaseTagName, forceRelease)
  }

  /*
  * This method will promote a release version
  */
  def promoteReleaseFreeStyle(String releaseTagName,boolean forceRelease=false){
    this.getInstanceFreeStyleJenkinsUtil().promoteRelease(releaseTagName, forceRelease);
  }

   /*
  * This method will promote a release version Node
  */
  def promoteReleaseNode(String releaseTagName,String buildParameters= '',boolean forceRelease=false,boolean enableJscrambler=false){
    if(!"${branchName}".startsWith("tags/RC-")){
        throw new Exception("The release must be done on release")
    }

    def applicationVersion=getApplicationVersionNode()
    def existTag=existTag("${applicationVersion}")
    def tagStateComment="Release"
    if(existTag){
        if (forceRelease){
        steps.echo "Forcing removal and promote of release tag name: ${releaseTagName}"
        def tagName=getApplicationVersionNode()
        deleteTagBranch(tagName)
        steps.echo "Tag removed: ${releaseTagName}"
        steps.echo "${tagStateComment}-FORCED"
      } else{
        throw new Exception("The tag for this release exists: ${applicationVersion}")
      }
    }
    steps.echo "Promote release tag name: ${releaseTagName}"
    def applicationName=getApplicationNameNode()
    if(buildParameters!=null&&buildParameters!=""){
    	steps.echo "Deploy artifact to Release Repository: ${releaseTagName}"
    	deployArtifactForNode("PROD","${branchName}",buildParameters,enableJscrambler)
    }else{
    	steps.echo "Promote artifact to Release Repository: ${releaseTagName}"
        promoteRepository(applicationName, releaseTagName)
    }

    mergeOverwriteBranch(releaseTagName,"master")
    def tagName=getApplicationVersionNode()
    tagBranch(tagName,tagStateComment)
  }

  /*
  * This method will promote a release version for Android Gradle Projects
  */
  def promoteReleaseIOS(String releaseTagName,String schema,String codeSignIdentity="iPhone Distribution"){
    if(!"${branchName}".startsWith("tags/RC-")){
        throw new Exception("The release must be done on release")
    }

    def applicationVersion=getApplicationVersionIOS()
    def existTag=existTag("${applicationVersion}")
    if(existTag){
        throw new Exception("The tag for this release exists: ${applicationVersion}")
    }
    steps.echo "Promote release tag name: ${releaseTagName}"
    def applicationName=getApplicationNameIOS()
    if(schema!=null&&schema!=""){
    	steps.echo "Deploy artifact to Release Repository: ${releaseTagName}"
    	deployArtifactoryIOS("PROD","${branchName}",schema,codeSignIdentity)
    }else{
    	steps.echo "Promote artifact to Release Repository: ${releaseTagName}"
        promoteRepository(applicationName, releaseTagName,".Generic")
    }

    mergeOverwriteBranch(releaseTagName,"master")
    def tagStateComment="Release"
    def tagName=getApplicationVersionIOS()
    tagBranch(tagName,tagStateComment)
  }


  /*
  * This method will promote a release version for Gradle Projects
  */
  def promoteReleaseGradle(String releaseTagName,boolean forceRelease=false){
    if(!"${branchName}".startsWith("tags/RC-")){
        throw new Exception("The release must be done on release")
    }

    def applicationVersion = getApplicationVersionGradle()
    def tagName            = getApplicationVersionGradle()
    def existTag           = existTag("${applicationVersion}")
    def applicationName    = getApplicationNameJavaGradle()
    def tagStateComment    = "Release"

    steps.echo "Promote release tag name: ${releaseTagName}"

    if(existTag){
      if (forceRelease){
        steps.echo "Forcing removal and promote of release tag name: ${releaseTagName}"
        deleteTagBranch(tagName)
        steps.echo "Tag removed: ${releaseTagName}"
        steps.echo "${tagStateComment}-FORCED"
      } else{
        throw new Exception("The tag for this release exists: ${applicationVersion}")
      }
    }

    steps.echo "Promote artifact to Release Repository: ${releaseTagName}"
    promoteRepository(applicationName, releaseTagName)
    mergeOverwriteBranch(releaseTagName,"master")
    tagBranch(tagName,tagStateComment)
  }

  /*
  * This method will promote a release version for Android Gradle Projects
  */
  def promoteReleaseAndroidGradle(String releaseTagName,String assembleFlavor="",boolean enabledDexGuardOfuscation=false,boolean enabledSigning=true){
    GradleAndroidJenkinsUtil gradleAndroidJenkinsUtil = this.getInstanceGradleAndroidJenkinsUtil()
    gradleAndroidJenkinsUtil.setAssembleFlavor(assembleFlavor)
    gradleAndroidJenkinsUtil.setEnabledDexGuardObfuscation(enabledDexGuardOfuscation)
    gradleAndroidJenkinsUtil.setGenerateSigningFile(enabledSigning)
    gradleAndroidJenkinsUtil.promoteRelease(releaseTagName, false)
  }

  /*
  * This method will promote a release version for NativeScript Project
  */
  def promoteReleaseNativeScript(String releaseTagName,String buildParams='',boolean enabledDexGuardOfuscation=true, boolean enabledJscrambler=true, boolean signing=true, boolean enabledMutualAuth=true, boolean enabledDynatrace=true, boolean enabledFirebase=true,boolean enabledPseudoMutualAuth=true){

    if(!"${branchName}".startsWith("tags/RC-")){
        throw new Exception("The release must be done on release")
    }

    def applicationVersion = getApplicationVersionForNativeScript()
    def tagName            = getApplicationVersionForNativeScript()
    def existTag           = existTag("${applicationVersion}")
    def tagStateComment    = "Release"

    if(existTag){
      throw new Exception("The tag for this release exists: ${applicationVersion}")
    }

    steps.echo "Promote release tag name: ${releaseTagName}"
    steps.echo "Deploy artifact to Release Repository: ${releaseTagName}"
    deployArtifactNativeScript("PROD",buildParams,"${branchName}","release",signing,enabledDexGuardOfuscation,enabledJscrambler,enabledMutualAuth, enabledDynatrace, enabledFirebase,enabledPseudoMutualAuth)

    //NOTE: FIX THIS
    steps.sh "git reset --hard";

    mergeOverwriteBranch(releaseTagName,"master")
    tagBranch(tagName,tagStateComment)
  }

  /*
  * Deploy application to Sales Force
  */
  def deployToSalesForce(String enviroment,String sandboxName, String maxPoll="2000"){
    def enviromentParam="${enviroment}"
    def applicationName=getApplicationNameForSalesForce()
    def applicationVersion=getApplicationVersionForSalesForce()
    def serverURL="https://${sandboxName}.salesforce.com/"
    String zipFile="${applicationName}-${applicationVersion}.zip"

    steps.echo "enviromentParam: ${enviromentParam}"
    steps.echo "serverURL: ${serverURL}"
    steps.echo "maxPoll: ${maxPoll}"
    steps.echo "zipFile: ${zipFile}"

    generateSalesForceInfoForDeploy(enviromentParam, serverURL, maxPoll, zipFile)
    def antBuildXmlFileData=getSalesforceAntBuildXml()
    //def antBuildXmlFile = new File("${script.env.WORKSPACE}/build.xml")
    //antBuildXmlFile.append("${antBuildXmlFileData}")

    steps.echo "Ant buildXml: ${antBuildXmlFileData}"

    steps.writeFile(
      file:"${script.env.WORKSPACE}/build.xml",
      text: "${antBuildXmlFileData}"
    )

    def dockerVolumen="-v ${script.env.WORKSPACE}:/project/data -w /project/data "
	  steps.echo "dockerVolumen: ${dockerVolumen}"

    docker.withServer("${script.env.DOCKER_HOST}"){
       docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
          def dockerParameters="--network=host"
          def dockerCmd="docker run --rm ${dockerVolumen} ${dockerParameters} ${dockerSalesforceTool41Ant110Cs385Enviroment}"
	          steps.sh "${dockerCmd} ant -verbose deployZip"
       }
    }
  }

  /*
  * Deploy application to HockeyApp Store
  */
  def deployAppToHockeyAppStore(String appId, String apiTokenStringCredentialsId, String applicationRelativePath, String strategy="add"){
    steps.echo "appId: ${appId}"
    steps.echo "apiTokenStringCredentialsId: ${apiTokenStringCredentialsId}"
    def comment="branch:${branchName}<br />commit:${currentGitCommit}<br />date:${buildTimestamp}<br />build-url:${script.env.BUILD_URL}<br />build-user-id: ${buildUserId}<br />build-user: ${buildUser}"
    if(iosAppSchema){
    	comment="schema:${iosAppSchema}<br />"+comment
    }
    steps.echo "comment: ${comment}"
    steps.withCredentials([[$class: "StringBinding", credentialsId: "${apiTokenStringCredentialsId}", variable: "apiToken"]]) {
        def result=steps.sh (
       		script: """
       		# --proxy http://localhost:3128 \\
            curl -Lv \\
               -F "status=2" \\
               -F "notify=1" \\
               -F "notes=${comment}" \\
               -F "notes_type=1" \\
               -F "ipa=@${applicationRelativePath}" \\
               -F "commit_sha=${currentGitCommit}" \\
               -F "build_server_url=${script.env.BUILD_URL}" \\
               -F "repository_url=${currentGitURL}" \\
               -F "strategy=${strategy}" \\
               -H "X-HockeyAppToken: ${script.env.apiToken}" \\
               https://rink.hockeyapp.net/api/2/apps/${appId}/app_versions/upload
            """,
        returnStdout: true)
        steps.echo "result: ${result}"
    }
  }

  def deployAppToAppCenterNativeScript(String ownerName, String project, String deploymentEnviroment, String distributeGroup='Collaborators'){
      def applicationVersion=getApplicationVersionForNativeScript()
      def applicationName=getApplicationNameNativeScript()
      def appDistributionPath="platforms/android/app/build/outputs/apk/${applicationName}-${applicationVersion}.apk"
      def appName="${project}-${deploymentEnviroment}-android".toLowerCase()
      if(nativeScriptAndroidEnabled){
        deployAppToAppCenter(ownerName, appName, appDistributionPath)
      }
      appName="${project}-${deploymentEnviroment}-ios".toLowerCase()
      appDistributionPath="platforms/ios/build/device/${applicationName}-${applicationVersion}.ipa"
      if(nativeScriptIOSEnabled){
        deployAppToAppCenter(ownerName, appName, appDistributionPath, distributeGroup)
      }
  }

  /*
  * Method to help deploy to Store(IOS and Android) for NativeScript
  */
  def deployToStoreNativeScript(){
      def projectName="${script.env.project}".toLowerCase()
      def applicationName = getApplicationNameNativeScript()
      def applicationVersion = getApplicationVersionForNativeScript()
      def packageName = getApplicationIdNativeScript()
      def appDistributionPath="platforms/android/app/build/outputs/apk/${applicationName}-${applicationVersion}.apk"
      def track = 'alpha'
      if(nativeScriptAndroidEnabled){
        deployApplicationToPlayStore(projectName, applicationName, packageName, appDistributionPath, track)
      }
      if(nativeScriptIOSEnabled){
        appDistributionPath="platforms/ios/build/device/${applicationName}-${applicationVersion}.ipa"
        deployAppToTestFlight(applicationName,appDistributionPath)
      }
  }

  /*
  * DEPRECATED - DO NOT USE IT
  * Deploy application to TestFlight
  */
  def deployAppToTestFlight(String relativeDistributionApplicationPath){
      def applicationName=getApplicationNameIOS()
      deployAppToTestFlight(applicationName,relativeDistributionApplicationPath)
  }

  /*
  * Deploy application to TestFlight
  */
  def deployAppToTestFlight(String applicationName,String relativeDistributionApplicationPath){
  	def appleIdCredentials="${script.env.project}".toLowerCase()+"-${applicationName}-appleid-credentials"

    steps.node(macServerAgent) {
      steps.step([$class: 'WsCleanup', cleanWhenFailure: false])
      steps.unstash 'ios-artifacts'

	  steps.withCredentials([
	       [$class: 'UsernamePasswordMultiBinding', credentialsId: "${appleIdCredentials}",
	                       usernameVariable: 'appleIdUser', passwordVariable: 'appleIdPassword']
	       ]) {

          def proxyCredentials="${script.env.project}-${applicationName}-user-proxy-mac-server".toLowerCase()

          steps.withCredentials([
            [$class: 'UsernamePasswordMultiBinding', credentialsId:"${proxyCredentials}", usernameVariable: 'proxyUser',passwordVariable:'proxyPassword']
          ]){
              def proxyIp="${script.env.HTTP_PROXY_IP}"
              def proxyPort="${script.env.HTTP_PROXY_PORT}"
              def proxy = "http://${script.env.proxyUser}:${script.env.proxyPassword}@${proxyIp}:${proxyPort}"
              def proxyEnviroment=" export http_proxy=${proxy} && export https_proxy=${proxy} && "
              if(proxyIp!=null&&proxyIp=="null"){
                proxyIp=null
                proxy=""
                proxyEnviroment=""
              }
              steps.echo "proxyEnviroment: ${proxyEnviroment}"

              def temporalBinPath = "${macServerRvmPath}:${macServerRubyPath}:${macServerGemExecutablePath}:${macServerNodePath}:${macServerNativeScriptPath}"
    		  def paths="export GEM_PATH=${macServerGemPath} && export PATH=${temporalBinPath}:\$PATH &&"
              steps.echo "paths: ${paths}"
              steps.echo "REMEMBER TO IMPORT CERT IN(IF PROXY USED): keytool -import -alias bcp_enterprise -file /xxxx/intermediate.cer -v -keystore \"/Applications/Xcode.app/Contents/Applications/Application Loader.app/Contents/itms/java/lib/security/cacerts\" -storepass xxxx "

              if(iosTeamName!=null&&iosTeamName!=""){
                steps.sh """ ${paths} ${proxyEnviroment} export LANG=en_US.UTF-8 && export FASTLANE_ITC_TEAM_NAME="${iosTeamName}" && export FASTLANE_OPT_OUT_USAGE=YES && export FASTLANE_PASSWORD=${script.env.appleIdPassword} && \\
                  fastlane pilot builds --verbose \\
                  	  --username "${script.env.appleIdUser}" \\
                      --app_identifier "${iosAppIdentifier}"
                """
                steps.sh """ ${paths} ${proxyEnviroment} export LANG=en_US.UTF-8 && export FASTLANE_ITC_TEAM_NAME="${iosTeamName}" && export FASTLANE_OPT_OUT_USAGE=YES && export FASTLANE_PASSWORD=${script.env.appleIdPassword} && \\
                  fastlane pilot upload --verbose \\
                      --username "${script.env.appleIdUser}" \\
                      --app_identifier "${iosAppIdentifier}" \\
                      --ipa "${script.env.WORKSPACE}/${relativeDistributionApplicationPath}" \\
                      --skip_waiting_for_build_processing true """
              }else{
                steps.sh """ ${paths} ${proxyEnviroment} export LANG=en_US.UTF-8 && export FASTLANE_OPT_OUT_USAGE=YES && export FASTLANE_PASSWORD=${script.env.appleIdPassword} && \\
                  fastlane pilot builds --verbose \\
                      --username "${script.env.appleIdUser}" \\
                      --app_identifier "${iosAppIdentifier}" \\
                      --team_id "${iosTeamId}"
                """
                steps.sh """ ${paths} ${proxyEnviroment} export LANG=en_US.UTF-8 && export FASTLANE_OPT_OUT_USAGE=YES && export FASTLANE_PASSWORD=${script.env.appleIdPassword} && \\
                fastlane pilot upload --verbose \\
                    --username "${script.env.appleIdUser}" \\
                    --app_identifier "${iosAppIdentifier}" \\
                    --team_id "${iosTeamId}" \\
                    --ipa "${script.env.WORKSPACE}/${relativeDistributionApplicationPath}" \\
                    --skip_waiting_for_build_processing true
              	"""
              }
          }
	  }
    }
  }

  def configureJSCrambler(String applicationName=null,Closure body){
    def projectName = script.env.project.toLowerCase()
    def appName = null
    if(applicationName==null){
      appName = getApplicationNameNode().toLowerCase()
    }else{
      appName = applicationName
    }

    def jscramblerConfigFile  = "jscrambler.json"
    def jscramblerTempFile    = "jscrambler-tmp.json"
    def jscramlerFinalFile    = "jscrambler-final.json"
    //def mainRulesRepository   = "https://bitbucket.lima.bcp.com.pe/projects/shared/repos/jscrambler-rules/raw/angular-rules.json?raw"

    def jscramblerApiUrl      = "api.jscrambler.lima.bcp.com.pe"
    def jscramblerApiPort     = 80
    def jscramblerApiProtocol = "http"
    def jscramblerApiVersion  = 5.4

    def jscramblerApplicationId = "${projectName}-${appName}-jscrambler-app-id"
    def jscramblerAccessKey     = "${projectName}-${appName}-jscrambler-access-key"
    def jscramblerSecretKey     = "${projectName}-${appName}-jscrambler-secret-key"

    steps.echo """******************** JScrambler Configuration ********************
      NOTE: - The credentials should be populate on project folder credentials
        * Jscrambler Application ID (SecretTest) : ${jscramblerApplicationId}
        * Jscrambler Access Key (SecretTest) : ${jscramblerAccessKey}
        * Jscrambler Secret Key (SecretTest) : ${jscramblerSecretKey}

      Jscrambler INFO
        * Jscrambler version: 5.2.16
    """

    def existsConfigFileInRepo = steps.fileExists(jscramblerConfigFile);

    if(!existsConfigFileInRepo){
      throw new Exception("No existe jscrambler.json en el de proyecto");
      /*
      steps.sh """
        set +x
        curl -s -o ${jscramblerConfigFile} -k \"${mainRulesRepository}\"
      """
      */
    }

    try{
        steps.withCredentials([
            [$class: "StringBinding", credentialsId: "${jscramblerApplicationId}", variable: "jscramblerApplicationId" ],
            [$class: "StringBinding", credentialsId: "${jscramblerAccessKey}", variable: "jscramblerAccessKey" ],
            [$class: "StringBinding", credentialsId: "${jscramblerSecretKey}", variable: "jscramblerSecretKey" ]
        ]){
            def configCreds ="""
            {
                "applicationId"        : "${script.env.jscramblerApplicationId}",
                "keys": {
                    "accessKey": "${script.env.jscramblerAccessKey}",
                    "secretKey": "${script.env.jscramblerSecretKey}"
                },
                "jscramblerVersion"    : "${jscramblerApiVersion}",
                "protocol"             : "${jscramblerApiProtocol}",
                "host"                 : "${jscramblerApiUrl}",
                "proxy"                : false,
                "port"                 : "${jscramblerApiPort}",
                "useRecommendedOrder"  : true,
                "areSubscribersOrdered": false,
                "sourceMaps"           : false
            }
            """.stripIndent()

            steps.writeFile(
                file:"${script.env.WORKSPACE}/${jscramblerTempFile}",
                text: configCreds
            )

            steps.sh """
              set +x
              jq -s '.[0] * .[1]' ${jscramblerTempFile} ${jscramblerConfigFile} > ${jscramlerFinalFile}
            """

            steps.sh """
              #set +x
              mv ${jscramlerFinalFile} ${jscramblerConfigFile}
              cat ${jscramblerConfigFile}
            """

            body.call()
        }
    }catch(Exception e){
        throw e;
    }finally{

      steps.sh """
        set +x
        rm -rf ${jscramblerTempFile}
        rm -rf ${jscramlerFinalFile}
      """

      if(!existsConfigFileInRepo){
        steps.sh """
          set +x
          rm -rf ${jscramblerConfigFile}
        """
      }

    }
  }

  /*
  * With Azure keyVault value
  */
  def azureVaultSshAgent(Map credentialMap, Closure body){
    def buildTimestamp=buildTimestamp
    def httpProxyHost="${script.env.DOCKER_HTTP_PROXY_HOST}"
    def httpProxyPort="${script.env.DOCKER_HTTP_PROXY_PORT}"

    ArrayList tempCredentials=new ArrayList()
    Map sshAgentEnv=new HashMap()

    String projectName="${script.env.project}"
    projectName=projectName.toLowerCase()

    String azureVaultName="${projectName}-vault-name"
    String azureServicePrincipal="${projectName}-azure-service-principal"
    String azureCertificate="${projectName}-azure-certificate"
	String azureTenantId="${projectName}-azure-tenandId"

	steps.echo """NOTE: - The credentials should be populate on project folder credentials
azureVaultName(Credentials String): ${azureVaultName}
azureServicePrincipal(Credentials String): ${azureServicePrincipal}
azureCertificate(Credentials File): ${azureCertificate}
azureTenantId(Credentials String): ${azureTenantId}"""

	def tempWSFolder="${script.env.WORKSPACE}@tmp-${buildTimestamp}"
	def tempFolder="${tempWSFolder}/.azure-${buildTimestamp}"
	def secretCertFile="${tempFolder}/.tmp/.azure-cert-${buildTimestamp}"


    def socket
    try{
    steps.withCredentials([
	    	[$class: "StringBinding", credentialsId: "${azureVaultName}", variable: "azureVaultNameVariable"],
	    	[$class: "StringBinding", credentialsId: "${azureServicePrincipal}", variable: "azureServicePrincipalVariable"],
	    	[$class: "FileBinding", credentialsId: "${azureCertificate}", variable: "azureCertPathVariable"],
	    	[$class: "StringBinding", credentialsId: "${azureTenantId}", variable: "azureTenantIdVariable"]
    	]) {

    	int keyCount=0
    	steps.sh """
    	set +x
    	mkdir -p ${tempFolder}/.tmp
    	cp ${script.env.azureCertPathVariable} ${secretCertFile}
		"""
		ArrayList credentialList=credentialMap.get('credentials')

    	//Check credentials and set credentials, if not exist BOOM!
	    for(String azureCredentialId: credentialList){
	        def docker_proxy_param=""
	      	if(httpProxyHost && httpProxyPort){
	          docker_proxy_param="export https_proxy=http://${httpProxyHost}:${httpProxyPort} &&"
	        }

		    docker.withServer("${script.env.DOCKER_HOST}"){
		       docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
		          def dockerParameters="--network=host -v ${script.env.WORKSPACE}:/opt/jenkins/data/temp -v ${tempWSFolder}:/opt/jenkins/data/.temp"
		          //TOFIX: Bug on Azure implementation, the .azure folder can not be set with other user different to root
		          //-v ${script.env.WORKSPACE}/.azure-${buildTimestamp}/.azure:/opt/jenkins/data/.azure"
		          def dockerCmd="docker run --rm ${dockerParameters} ${dockerAzureClientEnviroment} "
		          def certPath="/opt/jenkins/data/.temp/.azure-${buildTimestamp}/.tmp/.azure-cert-${buildTimestamp}"
		          def az_cmd="${docker_proxy_param}az login --service-principal --username ${script.env.azureServicePrincipalVariable} --password \"${certPath}\" --tenant ${script.env.azureTenantIdVariable} > /opt/jenkins/data/.temp/log-${buildTimestamp}${keyCount}.log && az keyvault secret show --name ${azureCredentialId} --vault-name ${script.env.azureVaultNameVariable} --query value "

			      def azureCredentialValue
			      steps.wrap([$class: 'MaskPasswordsBuildWrapper', varPasswordPairs: [[password: certPath, var: 'certPathVariable'],[password: tempWSFolder, var: 'tempWSFolderPathVariable']]]) {
				      azureCredentialValue = steps.sh (
				          script: "${dockerCmd} bash -c \"${az_cmd}\"",
				          returnStdout: true
				      ).trim()
			      }

		          steps.sh """
		          set +x
		          cat ${tempWSFolder}/log-${buildTimestamp}${keyCount}.log
		          """

			      if(azureCredentialValue==""){
			          throw new Error("The secret "+azureCredentialId+" is not founded or the value is empty?")
			      }

	    		  def keyTimestamp=steps.sh(script: "date '+%Y%m%d%H%M%S'", returnStdout: true).trim()+"-${keyCount++}"
	    		  steps.writeFile(
                	file: "${tempFolder}/.tmp/secretfileBase-${keyTimestamp}",
                	text: azureCredentialValue
	    		  )
	    		  def keySSHFile="${tempFolder}/.tmp/${azureCredentialId}-${keyTimestamp}"

				  steps.sh """
				  set +x
				  jq --raw-output '' ${tempFolder}/.tmp/secretfileBase-${keyTimestamp} | tee ${tempFolder}/.tmp/secretfileBase-${keyTimestamp}-2 > /dev/null 2>&1
				  sed 's/\\\\n/\\n/g' ${tempFolder}/.tmp/secretfileBase-${keyTimestamp}-2 | tee ${keySSHFile} > /dev/null 2>&1
		          chmod 600 ${keySSHFile}
		          """
		          tempCredentials.add(keySSHFile)

		       }
		    }
	    }
    }

	  def sshAgent = steps.sh (
			script: "ssh-agent",
			returnStdout: true
		).trim()
		def agentPidVar=getAgentValue(sshAgent, sshAgentagentPidVar)
		def authSocketVar=getAgentValue(sshAgent, sshAgentauthSocketVar)
		steps.echo "agentPid: ${agentPidVar}"

	  sshAgentEnv.put(sshAgentagentPidVar,agentPidVar)
	  sshAgentEnv.put(sshAgentauthSocketVar,authSocketVar)
	  if (sshAgentEnv.containsKey(sshAgentauthSocketVar)) {
	    socket = sshAgentEnv.get(sshAgentauthSocketVar)
	    steps.echo "socket is set OK"
	  }else{
	      println(sshAgentauthSocketVar + " was not included")
	  }

	  def agentEnviroment = sshAgentEnv.collect {
	    key, value -> "export $key=$value"}.join(" && ")

	  //Add Keys to SSHAgent
	  for(def tempKeyPath:tempCredentials){
		  steps.sh """
		  set +x
		  ${agentEnviroment} && ssh-add ${tempKeyPath} > /dev/null 2>&1
          """
	  }

	  //Execute commands
	  steps.wrap([$class: 'MaskPasswordsBuildWrapper', varPasswordPairs: [[password: tempWSFolder, var: 'tempWSFolderPathVariable'],[password: authSocketVar, var: 'authSocketVarVariable']]]) {
		  steps.withEnv(["${sshAgentauthSocketVar}=${authSocketVar}"]){
		    body.call()
		  }
      }
    }finally{
		steps.sh """
    	set +x
		if [ -d \"${tempWSFolder}\" ]; then
		    rm -rf \"${tempWSFolder}\"
		fi
		"""
	    //Terminate SShAgent always and remote socket
		def sshAgentagentPid=sshAgentEnv.get(sshAgentagentPidVar)
		steps.sh """
		set +x
        export SSH_AGENT_PID=${sshAgentagentPid} && ssh-agent -k
		if [ -f \"${socket}\" ]; then
		    rm -f ${socket}
		fi
		"""
	}
  }

  /**
  * Parses a value from ssh-agent output.
  */
  private String getAgentValue(String agentOutput, String envVar) {
        int pos = agentOutput.indexOf(envVar) + envVar.length() + 1; // +1 for '='
        int end = agentOutput.indexOf(';', pos);
        return agentOutput.substring(pos, end);
  }

  /*
  * Deploy application to Play Store for Gradle
  * @Deprecate use deployApplicationToPlayStore, only for compatibility
  */
  def deployAppToPlayStore(String packageName, String apkAbsolutePath, String track){
    def projectName="${script.env.project}".toLowerCase()
    def appName=getApplicationNameGradle()
    deployApplicationToPlayStore(projectName,appName,packageName,apkAbsolutePath,track)
  }

  /*
  * This method will validate merge
  */
  def validateValidMergeToMaster(String releaseTagName){
    if(!"${branchName}".startsWith("tags/RC-")){
        throw new Exception("The release must be done on release")
    }
    def relationship=getRelationship("refs/tags/"+releaseTagName,"origin/master")
    steps.echo "Relationship: ${relationship}"
    if("${relationship}"=="descendant"){
        return true
    }else{
        return false
    }
  }

  /*
  * This method will merge release to master
  */
  def mergeReleaseToMaster(String releaseTagName){
    if(!"${branchName}".startsWith("tags/RC-")){
        throw new Exception("The release must be done on release")
    }
    steps.echo "Merge release tag to master branch: ${releaseTagName}"
    def currentDate = buildTimestamp
	  def targetBranch= "master"
	  def repositoryPath=getRepositoryPath(gitlabUrl,currentGitURL)

    def gitlabCommentPrefix="[ci-${targetBranch}]"
    def stateComment="Merge-release-into-master"
    def comment="${gitlabCommentPrefix} ${stateComment} \nbranch-source: ${branchName}\ncommit-source: ${currentGitCommit}\ndate: ${currentDate}\nbuild-url: ${script.env.BUILD_URL}\nbuild-user-id: ${buildUserId}\nbuild-user: ${buildUser}"
    steps.echo "comment: ${comment}"

	checkoutBranch("origin/${targetBranch}")
    steps.sh "git merge --no-ff ${releaseTagName} -m \"${comment}\""
    steps.sh "git branch --set-upstream-to \"origin/${targetBranch}\""
    steps.sh "git status"

    steps.withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: currentCredentialsId,
        usernameVariable: 'GITLAB_USER_VALUE', passwordVariable: 'GITLAB_USER_PASSWORD']]) {
        def usernameEncoded=encodeHtml('$GITLAB_USER_VALUE')
        def passwordEncoded=encodeHtml('$GITLAB_USER_PASSWORD')
		steps.wrap([$class: 'MaskPasswordsBuildWrapper', varPasswordPairs: [[password: passwordEncoded, var: 'passwordEncodedVariable'],]]) {
	        steps.sh """ set +x
	        git push ${script.env.GITLAB_PROTOCOL}://${usernameEncoded}:${passwordEncoded}@${script.env.SCM_REPOSITORY_PREFIX}${script.env.GITLAB_HOST}:${script.env.GITLAB_PORT}${script.env.SCM_REPOSITORY_PREFIX}${repositoryPath} ${targetBranch}
	        """
    	}
    }
  }

  /*
  * This method will return the git token from current credential id
  */
  def getPrivateGitToken(){
    def privateToken
    steps.withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: currentCredentialsId,
      usernameVariable: 'GITLAB_USER_VALUE', passwordVariable: 'GITLAB_USER_PASSWORD']]) {
      def usernameEncoded=encodeHtml('$GITLAB_USER_VALUE')
      def passwordEncoded=encodeHtml('$GITLAB_USER_PASSWORD')
      privateToken=steps.sh (
      script: """
      set +x
      PRIVATE_TOKEN=\$(curl -s ${gitlabUrl}/api/v3/session/ -d login=${usernameEncoded} -d password=${passwordEncoded} | sed -e 's/[{}]/''/g' | awk -v RS=',"' -F: '/^private_token/ {print \$2}')
      PRIVATE_TOKEN=\$(echo "\${PRIVATE_TOKEN}" | sed -r 's/["]+//g')
      echo \${PRIVATE_TOKEN}""", returnStdout: true
      ).trim()
    }
    return privateToken
  }

  /*
  * This method will return the current project id
  */
  def getProjectId(){
    def repositoryPath=getRepositoryPath(gitlabUrl,currentGitURL)

    repositoryPath=steps.sh (script: """
    repositoryPathTemp=${repositoryPath}
    repositoryPathTemp=\$(echo \${repositoryPathTemp#*/})
    repositoryPathTemp=\${repositoryPathTemp%.git}
    echo \${repositoryPathTemp}
    """, returnStdout: true).trim()
    steps.echo "repositoryPath: ${repositoryPath}"

    def projectNameParam=getGitProjectName()
    steps.echo "projectNameParam: ${projectNameParam}"
    def privateToken=getPrivateGitToken()

    def projectId=""
    steps.wrap([$class: 'MaskPasswordsBuildWrapper', varPasswordPairs: [[password: privateToken, var: 'privateTokenVariable'],]]) {
      projectId=steps.sh (
      script: """
      set +x
      PROJECT_ID=\$(curl -s --header 'PRIVATE-TOKEN: ${privateToken}' -X GET ${gitlabUrl}/api/v3/projects?search=${projectNameParam} | jq '.[] | select(.path_with_namespace=="${repositoryPath}").id')
      echo \${PROJECT_ID}""",    returnStdout: true
      ).trim()
    }

    return projectId
  }

  /*
  * This method will escape the branch
  */
  def escapedBranch(String branchNameParam="${branchName}"){
    def branchEscaped="${branchNameParam}"
    branchEscaped=steps.sh (script: """
          escapeText=${branchEscaped}
          textToReplace=%2F
          escapeText=\${escapeText/\\//\${textToReplace}}
          echo \${escapeText}
          """, returnStdout: true).trim()
    return branchEscaped
  }

  /*
  * This method will fetch prune repository from remote
  */
  def fetchPrune(){
    steps.echo "Executing git fetch prune"
    def repositoryPath=getRepositoryPath(gitlabUrl,currentGitURL)
    steps.withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: currentCredentialsId,
        usernameVariable: 'GITLAB_USER_VALUE', passwordVariable: 'GITLAB_USER_PASSWORD']]) {
        def timestamp=steps.sh(script: "date '+%Y%m%d%H%M%S'", returnStdout: true).trim()
        def log=steps.sh (script: """
        git config remote.origin.url ${script.env.GITLAB_PROTOCOL}://${script.env.GITLAB_USER_VALUE}:${script.env.GITLAB_USER_PASSWORD}@${script.env.GITLAB_HOST}:${script.env.GITLAB_PORT}${script.env.SCM_REPOSITORY_PREFIX}${repositoryPath}
        git fetch --prune
        git config remote.origin.url ${currentGitURL}
        """, returnStdout: true).trim()
        steps.echo "Log: ${log}"
    }
  }

  /*
  * This method will fetch repository from remote
  */
  def fetch(){
    steps.echo "Executing git fetch prune"
    def repositoryPath=getRepositoryPath(gitlabUrl,currentGitURL)
    steps.withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: currentCredentialsId,
        usernameVariable: 'GITLAB_USER_VALUE', passwordVariable: 'GITLAB_USER_PASSWORD']]) {
        def timestamp=steps.sh(script: "date '+%Y%m%d%H%M%S'", returnStdout: true).trim()
        def log=steps.sh (script: """
        git config remote.origin.url ${script.env.GITLAB_PROTOCOL}://${script.env.GITLAB_USER_VALUE}:${script.env.GITLAB_USER_PASSWORD}@${script.env.GITLAB_HOST}:${script.env.GITLAB_PORT}${repositoryPath}
        git fetch
        git config remote.origin.url ${currentGitURL}
        """, returnStdout: true).trim()
        steps.echo "Log: ${log}"
    }
  }

  /*
  * This method will update to next development version
  */
  def updateToNextDevelopmentVersion(){
    def applicationVersion=getApplicationVersion()
    def applicationNextDevelopmentVersion=getApplicationNextDevelopmentVersion(applicationVersion)
    steps.echo "Application next development version: ${applicationNextDevelopmentVersion}"
    def currentDate = buildTimestamp
	  def repositoryPath=getRepositoryPath(gitlabUrl,currentGitURL)

    steps.sh "'${mvnHome}/bin/mvn' --batch-mode versions:set -DnewVersion=${applicationNextDevelopmentVersion}"
    def gitlabCommentPrefix="[ci-${branchName}]"
    def stateComment="Update-to-next-development-version: ${applicationNextDevelopmentVersion}"
    def comment="${gitlabCommentPrefix} ${stateComment} \nbranch: ${branchName}\ndate: ${currentDate}\nbuild-url: ${script.env.BUILD_URL}\nbuild-user-id: ${buildUserId}\nbuild-user: ${buildUser}"
    steps.echo "comment: ${comment}"
    steps.sh "git ls-files --modified | xargs git add"
    steps.sh "git branch --set-upstream-to \"origin/${branchName}\""
    steps.sh "git commit -m \"${comment}\""
    steps.withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: currentCredentialsId,
        usernameVariable: 'GITLAB_USER_VALUE', passwordVariable: 'GITLAB_USER_PASSWORD']]) {
        def usernameEncoded=encodeHtml('$GITLAB_USER_VALUE')
        def passwordEncoded=encodeHtml('$GITLAB_USER_PASSWORD')
		steps.wrap([$class: 'MaskPasswordsBuildWrapper', varPasswordPairs: [[password: passwordEncoded, var: 'passwordEncodedVariable'],]]) {
	        steps.sh """ set +x
	        git push ${script.env.GITLAB_PROTOCOL}://${usernameEncoded}:${passwordEncoded}@${script.env.GITLAB_HOST}:${script.env.GITLAB_PORT}${script.env.SCM_REPOSITORY_PREFIX}${repositoryPath} ${branchName}
	        """
    	}
    }
  }

  def executeDexGuard(String applicationName,String inJars,String outJars,String libraryJars, boolean signing=false,String dockerDexGuardEnviroment=dockerNativeScript343Gradle410Android28Node8Enviroment) {
    def dexGuardLicenseFile = "dexguard-license.txt"
    def projectName          = "${script.env.project}".toLowerCase()
    def dexGuardApplicationId = "dexguard-${projectName}-${applicationName}-license"
    def timestamp=steps.sh(script: "date '+%Y%m%d%H%M%S'", returnStdout: true).trim()

    def enviromentParameters=getDockerEnviromentParameters()
    def temporalVolumes=getDockerTemporalVolumes()

    steps.echo """************ Configuring License file ************
    DexGuard License File: ${dexGuardApplicationId}
    """
    steps.withCredentials([
      [$class: "FileBinding", credentialsId: "${dexGuardApplicationId}", variable: "dexGuardApplicationId" ]
    ]){
      try{
          steps.sh """#set +x
          cp -f \"${script.env.dexGuardApplicationId}\" ${script.env.WORKSPACE}/${dexGuardLicenseFile}
          mkdir -p ${script.env.WORKSPACE}/dexguard-${script.env.BUILD_NUMBER}/mapping
          """

          steps.writeFile(
            file:"${script.env.WORKSPACE}/dexguard-parameters-${timestamp}.txt",
            text: getDexGuardParameters(inJars,outJars,libraryJars)
          )

          steps.sh "cat ${script.env.WORKSPACE}/dexguard-parameters-${timestamp}.txt"

          def dockerVolumen=" ${temporalVolumes} -v ${script.env.WORKSPACE}:/project/data -w /project/data "
          steps.echo "dockerVolumen: ${dockerVolumen}"

          docker.withServer("${script.env.DOCKER_HOST}"){
            docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
                def dockerParameters=" ${enviromentParameters} --network=host"
                def dockerCmd="docker run --rm ${dockerVolumen} ${dockerParameters} ${dockerDexGuardEnviroment}"
                if(!signing){
                  steps.sh "${dockerCmd} bash -c \"export DEXGUARD_LICENSE=dexguard-license.txt && dexguard.sh @dexguard-parameters-${timestamp}.txt\""
                }else{
                    steps.sh "${dockerCmd} bash -c \"export DEXGUARD_LICENSE=dexguard-license.txt && dexguard.sh -keystore ${script.env.keystoreFilePath} -keystorepassword ${script.env.keystorePasswordValue} -keyalias ${script.env.keyAliasValue} -keypassword ${script.env.keyPasswordValue} @dexguard-parameters-${timestamp}.txt\""
                }
            }
          }

      }catch(Exception e) {
          steps.sh "echo \"${e}\""
          throw e
      }finally {
          steps.sh """
            set +x
            rm -rf ${script.env.WORKSPACE}/${dexGuardLicenseFile}
          """
      }
    }
  }

    /*
  This method ofuscate Android Gradle
  */
  def executeDexGuardOfuscation(String appName) {
    getInstanceGradleAndroidJenkinsUtil().executeDexGuardObfuscation(appName);
  }

  def buildNsPlugin() {
    def bcp_virtual_registry = "${script.env.ARTIFACT_SERVER_URL}/api/npm/public-release-node/"
    def dockerVolumen       = "-v ${script.env.WORKSPACE}:${script.env.WORKSPACE} -w ${script.env.WORKSPACE} -v ${script.env.APPLICATION_CACHE}/node-local-repo:/opt/nodecache"

    docker.withServer("${script.env.DOCKER_HOST}"){
        docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
            def dockerParameters = "--network=host"
            def dockerCmd        = "docker run --rm ${dockerVolumen} ${dockerParameters} ${dockerNodeAngularEnviroment}"

            steps.sh """
                echo "registry=${script.env.ARTIFACT_SERVER_URL}/api/npm/public-node/\n@bcp:registry=${bcp_virtual_registry}" > ${script.env.WORKSPACE}/npmrc-jenkins
            """

            def http_proxy      = "${script.env.DOCKER_HTTP_PROXY_URL}"
            def npm_arguments   = "--strict-ssl=false --userconfig=${script.env.WORKSPACE}/npmrc-jenkins --prefer-offline"

           if(http_proxy){
             npm_arguments+=" --proxy ${http_proxy} --https-proxy ${http_proxy}"
           }

           steps.sh "${dockerCmd} /bin/sh -c 'cd ${script.env.WORKSPACE}/src; npm install ${npm_arguments}'"
           steps.sh "${dockerCmd} /bin/sh -c 'cd ${script.env.WORKSPACE}/publish ; chmod +x pack.sh ; ./pack.sh'"
        }
     }

     try {
        steps.junit allowEmptyResults: true, testResults: '**/tests_out/**/*unit.xml'
     }catch(Exception e){
        //TODO: Remove when QA are implemented
     }
  }

  def configureJscramblerAngularApp(){
    def projectName = "${script.env.project}".toLowerCase()
    def appName     = getApplicationNameNode().toLowerCase()

    def jscramblerConfigFile  = "jscrambler.json"
    def jscramblerTempFile    = "jscrambler-tmp.json"
    def jscramlerFinalFile    = "jscrambler-final.json"
    def mainRulesRepository   = "https://${script.env.GITLAB_HOST}/projects/shared/repos/jscrambler-rules/raw/angular-rules.json?raw"

    def jscramblerApiUrl      = "${script.env.JSCRAMBLER_API_HOST}"
    def jscramblerApiPort     = "${script.env.JSCRAMBLER_API_PORT}"
    def jscramblerApiProtocol = "${script.env.JSCRAMBLER_API_PROTOCOL}"
    def jscramblerApiVersion  = "5.4"
    def jscramblerNpmVersion  = "5.2.16"

    def jscramblerApplicationId = "${projectName}-${appName}-jscrambler-app-id"
    def jscramblerAccessKey     = "${projectName}-${appName}-jscrambler-access-key"
    def jscramblerSecretKey     = "${projectName}-${appName}-jscrambler-secret-key"

    steps.echo """

      JSCRAMBLER OBFUSCATION

      NOTE: - The credentials should be populate on project folder credentials
        * Jscrambler Application ID : ${jscramblerApplicationId}
        * Jscrambler Access Key     : ${jscramblerAccessKey}
        * Jscrambler Secret Key     : ${jscramblerSecretKey}

      Jscrambler INFO
        * Jscrambler version: 5.2.16
    """

    def existsConfigFileInRepo = steps.fileExists(jscramblerConfigFile);

    if(!existsConfigFileInRepo){
      steps.sh """
        set +x
        curl -s -o ${jscramblerConfigFile} -k \"${mainRulesRepository}\"
      """
    }

    try{
        steps.withCredentials([
            [$class: "StringBinding", credentialsId: "${jscramblerApplicationId}", variable: "jscramblerApplicationId" ],
            [$class: "StringBinding", credentialsId: "${jscramblerAccessKey}", variable: "jscramblerAccessKey" ],
            [$class: "StringBinding", credentialsId: "${jscramblerSecretKey}", variable: "jscramblerSecretKey" ]
        ]){
            def configCreds ="""
            {
                "applicationId"        : "${script.env.jscramblerApplicationId}",
                "keys": {
                    "accessKey": "${script.env.jscramblerAccessKey}",
                    "secretKey": "${script.env.jscramblerSecretKey}"
                },
                "jscramblerVersion"    : "${jscramblerApiVersion}",
                "protocol"             : "${jscramblerApiProtocol}",
                "host"                 : "${jscramblerApiUrl}",
                "port"                 : "${jscramblerApiPort}",
                "useRecommendedOrder"  : true,
                "areSubscribersOrdered": false,
                "sourceMaps"           : false
            }
            """.stripIndent()

            steps.writeFile(
                file:"${script.env.WORKSPACE}/${jscramblerTempFile}",
                text: configCreds
            )

            steps.sh """
              set +x
              jq -s '.[0] * .[1]' ${jscramblerTempFile} ${jscramblerConfigFile} > ${jscramlerFinalFile}
            """

            docker.withServer("${script.env.DOCKER_HOST}"){
                docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
                    def dockerParameters    = "--rm --network=host"
                    def dockerVolumen       = "-v ${script.env.WORKSPACE}:${script.env.WORKSPACE} -w ${script.env.WORKSPACE} -v ${script.env.APPLICATION_CACHE}/node-local-repo:/opt/nodecache"

                    def dockerCmd           = "docker run ${dockerVolumen} ${dockerParameters} ${dockerNodeAngularEnviroment}"
                    def bcp_virtual_registry = "${script.env.ARTIFACT_SERVER_URL}/api/npm/public-release-node/"
                    def http_proxy           = "${script.env.DOCKER_HTTP_PROXY_URL}"

                    def npm_arguments        = "--no-save --no-package-lock --quiet --no-progress --prefer-offline --strict-ssl=false --userconfig=./npmrc-jenkins"

                    steps.sh """
                        set +x
                        echo "registry=${artifact_npm_registry_url}\n@bcp:registry=${bcp_virtual_registry}" > ./npmrc-jenkins
                    """

                    if(http_proxy){
                        npm_arguments+=" --proxy ${http_proxy} --https-proxy ${http_proxy}"
                    }

                    steps.sh "${dockerCmd} npm install ${npm_arguments} jscrambler@${jscramblerNpmVersion}"
                    steps.sh "${dockerCmd} ./node_modules/.bin/jscrambler -c ${jscramlerFinalFile}"
                }
            }
        }
    }catch(Exception e){
        throw e;
    }finally{

      steps.sh """
        set +x
        rm -rf ${jscramblerTempFile}
        rm -rf ${jscramlerFinalFile}
      """

      if(!existsConfigFileInRepo){
        steps.sh """
          set +x
          rm -rf ${jscramblerConfigFile}
        """
      }

    }
  }

  /*
  This method ofuscate Android Gradle
  */
  def executeFortifyMavenSast(String buildParameters = ''){
    getInstanceMavenJenkinsUtil().executeSast(buildParameters)
  }

   /*
  This method execute Fortify - TYPESCRIPT
  */
  def executeFortifyTypeScriptSast(){
    if(fortifyActivated){
      def fortifyVersion = getGitProjectName()
      def projectLowerCase="${script.env.project}".toLowerCase()
      def fortifyCloudScanCredId = "${projectLowerCase}-jenkins-fortify-cloudscan-token"
      def fortifyAuthCredId = "${projectLowerCase}-jenkins-fortify-ssc-token"
      def buildID = "${script.env.project}-${fortifyVersion}-${script.env.BUILD_NUMBER}"

        steps.withCredentials([[$class: 'StringBinding', credentialsId: "${fortifyCloudScanCredId}", variable: 'cloudScanToken' ],
                  [$class: 'StringBinding', credentialsId: "${fortifyAuthCredId}", variable: 'authToken' ]]){

        try{
          //Docker execute
          steps.sh """
            #set -x
            docker run -d -it --cpus ${execFortifyCpu} --network=host -w /project/data ${dockerFortifyTypescript} bash > .dockerId
            docker cp ${script.env.WORKSPACE}/. `cat .dockerId`:/project/scan
            if [ -d ${script.env.WORKSPACE}/node_modules ]; then
              docker exec `cat .dockerId` rm -r /project/scan/node_modules
            fi
            if [ -d ${script.env.WORKSPACE}/platforms ]; then
              docker exec `cat .dockerId` rm -r /project/scan/platforms
            fi
            docker exec `cat .dockerId` sourceanalyzer -b "${buildID}" -verbose "/project/scan/**/*.ts" -clean
            docker exec `cat .dockerId` sourceanalyzer -b "${buildID}" -verbose "/project/scan/**/*.ts" -Dcom.fortify.sca.Phase0HigherOrder.Languages=typescript
            docker exec `cat .dockerId` cloudscan -sscurl ${fortifyUrl} -ssctoken ${script.env.cloudScanToken} start  -block -log typescript.log -upload --application "${script.env.project}" --application-version "${fortifyVersion}" -b "${buildID}" -uptoken ${script.env.cloudScanToken} -scan ${execFortifyMemory}
            docker rm -f `cat .dockerId`
            docker run -d -it --cpus ${execFortifyCpu} --network=host -v ${script.env.WORKSPACE}:/project/data -w /project/data ${dockerFortifyTypescript} bash > .dockerId
            docker exec `cat .dockerId` fortifyclient downloadFPR -file result.fpr -url ${fortifyUrl} -authtoken ${script.env.authtoken} -project "${script.env.project}" -version "${fortifyVersion}"
            docker exec `cat .dockerId` BIRTReportGenerator -template "OWASP Top 10" -source result.fpr -format HTML --Version "OWASP Top 10 2017" -output MyOWASP_Top10_Report.html
            docker exec `cat .dockerId` fortifyclient -url ${fortifyUrl} -authtoken ${script.env.authtoken} listApplicationVersions > .listProject.txt
          """
        }catch(Exception e){
          steps.sh """
                  set +e
                  docker exec `cat .dockerId` cat /opt/jenkins/.fortify/cloudscan/log/cloudscan.log
          """
          throw e
        }finally{
              steps.sh """
            docker rm -f `cat .dockerId`
          """
              }

        generateReportFortify()
      }
    }else{
      steps.echo "********************* FORTIFY IS DISABLED!!!!! *********************"
    }
  }

  /*
  This method look for vulnerabilities in Gradle with fortify
  */
  def executeFortifyGradleSast(String buildParameters = ''){
	def fortifyVersion = getGitProjectName()
	def projectLowerCase="${script.env.project}".toLowerCase()
	def fortifyCloudScanCredId = "${projectLowerCase}-jenkins-fortify-cloudscan-token"
	def fortifyAuthCredId = "${projectLowerCase}-jenkins-fortify-ssc-token"
	def buildID = "${script.env.project}-${fortifyVersion}-${script.env.BUILD_NUMBER}"

    steps.withCredentials([[$class: 'StringBinding', credentialsId: "${fortifyCloudScanCredId}", variable: 'cloudScanToken' ],
						   [$class: 'StringBinding', credentialsId: "${fortifyAuthCredId}", variable: 'authToken' ]]){
		def gradleInitScript = getGradleInitScript()

        steps.writeFile(
          file:"${script.env.WORKSPACE}/gradle-jenkins",
          text: gradleInitScript
        )
		try{
			//Docker execute
			steps.sh """
				set -x
        if [ ! -d ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER} ]; then
					mkdir -p ${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}
				fi
        docker run -d -it --cpus ${execFortifyCpu} --network=host -v ${script.env.APPLICATION_CACHE}/gradle-local-repo:/gradle-home -v '${script.env.WORKSPACE}:/project/data' -w /project/data -v '${script.env.WORKSPACE}/project-cache-${script.env.BUILD_NUMBER}:/gradle-project-cache'  ${dockerFortifyGradleRunner} bash > .dockerId
				docker exec `cat .dockerId` sourceanalyzer -b "${buildID}" gradle ${buildParameters} install --project-cache-dir /gradle-project-cache --stacktrace -Dorg.gradle.daemon=false --gradle-user-home=/gradle-home --init-script ./gradle-jenkins
				docker exec `cat .dockerId` cloudscan -sscurl ${fortifyUrl} -ssctoken ${script.env.cloudScanToken} start -block -log resultado.log -upload --application "${script.env.project}" --application-version "${fortifyVersion}" -b "${buildID}" -uptoken ${script.env.cloudScanToken} -scan ${execFortifyMemory}
        docker exec `cat .dockerId` fortifyclient downloadFPR -file result.fpr -url ${fortifyHost} -authtoken ${script.env.authtoken} -project "${script.env.project}" -version "${fortifyVersion}"
        docker exec `cat .dockerId` BIRTReportGenerator -template 'OWASP Top 10' -source result.fpr -format HTML --Version 'OWASP Top 10 2017' -output MyOWASP_Top10_Report.html
				docker exec `cat .dockerId` fortifyclient -url ${fortifyHost} -authtoken ${script.env.authtoken} listApplicationVersions > .listProject.txt
      """
		}catch(Exception e){
			steps.sh """
        set +e
        docker exec `cat .dockerId` cat /opt/jenkins/data/.fortify/cloudscan/log/cloudscan.log
			"""
			throw e
		}finally{
      steps.sh """
				docker rm -f `cat .dockerId`
			"""
          }

		generateReportFortify()
	}
  }

 /*
  This method look for vulnerabilities in Android with fortify
  */
  def executeFortifyAndroidGradleSast(String buildParameters = ''){
    getInstanceGradleAndroidJenkinsUtil().executeSast(buildParameters)
  }

  /*
   * DEPRECATED redicrect to method executeFortifyAndroidGradleSast()
   This method look for vulnerabilities in Android with fortify
  */
  def executeFortifyAndroidGradleNDKSast(String buildParameters = ''){
    executeFortifyAndroidGradleSast(buildParameters)
  }


  def executeFortifyIosSast(String schema){
    if(fortifyActivated){
      //Pack data to send to slave node

	  steps.stash includes: '**/*', name: 'workspace'
      steps.node(macServerAgent) {
		steps.step([$class: 'WsCleanup'])
		steps.unstash 'workspace'
        def fortifyVersion = getGitProjectName()
        def projectLowerCase="${script.env.project}".toLowerCase()
        def fortifyCloudScanCredId = "${projectLowerCase}-jenkins-fortify-cloudscan-token"
        def fortifyAuthCredId = "${projectLowerCase}-jenkins-fortify-ssc-token"
        def buildID = "${script.env.project}-${fortifyVersion}-${script.env.BUILD_NUMBER}"

        steps.withCredentials([[$class: 'StringBinding', credentialsId: "${fortifyCloudScanCredId}", variable: 'cloudScanToken' ],
                    [$class: 'StringBinding', credentialsId: "${fortifyAuthCredId}", variable: 'authToken' ]]){
          try{
              steps.sh """
              	  #COCOA PODS
                  bash -l -c 'xcodebuild clean -workspace bcp.xcworkspace -scheme \"${schema}\" '
                  bash -l -c 'ls'
                  bash -l -c 'rm -rf Pods'
                  bash -l -c  'pod install'
                  bash -l -c  'xcodebuild -alltargets clean'
                  #BUILD & ANALYSIS
                  bash -l -c ' sourceanalyzer -b \"${buildID}\" xcodebuild clean -workspace \"${xcodeWorkspace}\" -scheme \"${schema}\" '
                  bash -l -c ' sourceanalyzer -b \"${buildID}\" -clean'
                  bash -l -c ' sourceanalyzer -b \"${buildID}\" xcodebuild build -workspace \"${xcodeWorkspace}\" -scheme \"${schema}\" -sdk iphonesimulator '
                  bash -l -c ' cloudscan -sscurl \"${fortifyUrl}\" -ssctoken \"${script.env.cloudScanToken}\" start -block -log ios.log -upload --application \"${script.env.project}\" --application-version \"${fortifyVersion}\" -b \"${buildID}\" -uptoken \"${script.env.cloudScanToken}\" -scan \"${execFortifyMemory}\" '
                  bash -l -c ' fortifyclient downloadFPR -file result.fpr -url \"${fortifyUrl}\" -authtoken \"${script.env.authtoken}\" -project \"${script.env.project}\" -version \"${fortifyVersion}\" '
                  bash -l -c ' BIRTReportGenerator -template \"OWASP Top 10\" -source \"result.fpr\" -format HTML --Version \"OWASP Top 10 2017\" -output MyOWASP_Top10_Report.html '
                  bash -l -c ' fortifyclient -url ${fortifyHost} -authtoken ${script.env.authtoken} listApplicationVersions > .listProject.txt'
              """
          }catch(Exception e){
            steps.sh """
                  set +e
                  bash -l -c 'cat /Users/usrconsmbbk/.fortify/cloudscan/log/cloudscan.log'
          	"""
            throw e
          }finally{
             steps.sh """
                  set +e
                  bash -l -c 'rm /Users/usrconsmbbk/.fortify/cloudscan/log/cloudscan.log'
          	 """
          }
          //generateReportFortify()
        }

      }
    }else{
      steps.echo "********************* FORTIFY IS DISABLED!!!!! *********************"
    }
  }



  def configureNativeScriptMutualAuth(applicationName,applicationVersion,enviroment="", version="1.0.0",platform="android", Closure body){
    def projectName          = "${script.env.project}".toUpperCase();
    def projectNameLowercase = projectName.toLowerCase();
    //TODO: CHANGE TO BMDL->SCRP
    def artifact_npm_registry_url = "${script.env.ARTIFACT_SERVER_NAME_URL}/api/npm/public-node/"
    def bcp_virtual_registry      = "${script.env.ARTIFACT_SERVER_NAME_URL}/api/npm/public-release-node/"
    def securityProjectKey        = "SCRP";
    def adapterFolder             = "ADAPTER";
    def adapterArtifactName       = "mutual-adapter-nativescript.tar.gz";
    def pluginArtifactName        = "nativescript-yourplugin-${version}.tgz";
    def pluginsFolderName         = "plugins";
        enviroment                = enviroment.toUpperCase();

    repositoryId = "${securityProjectKey}.npm.Snapshot.private"

    if ("${branchName}"=="master"){
      repositoryId = "${securityProjectKey}.npm.Release.private"
    }

    if(enviroment && enviroment=="PROD"){
      repositoryId = "${securityProjectKey}.npm.Release.private"
    }

    if(enviroment && enviroment!="PROD" && enviroment!="DEV"){
      repositoryId = "${securityProjectKey}.npm-${enviroment}.private"
    }

    def artifactoryPluginPath  = "${script.env.ARTIFACT_SERVER_NAME_URL}/${repositoryId}/${projectName}/${pluginArtifactName}"
    def artifactoryAdapterPath = "${script.env.ARTIFACT_SERVER_NAME_URL}/${securityProjectKey}.npm.Release.private/${adapterFolder}/${adapterArtifactName}"

    def srcFolder          = "${script.env.WORKSPACE}/src/app";
    def appResourcesFolder = "${script.env.WORKSPACE}/App_Resources";
    def existsAppFolder    = steps.sh(script: "test -d ${script.env.WORKSPACE}/app && echo true || echo false ", returnStdout: true).trim()
        existsAppFolder    = existsAppFolder.toBoolean()

    if(existsAppFolder){
      srcFolder          = "${script.env.WORKSPACE}/app";
      appResourcesFolder = "${script.env.WORKSPACE}/app/App_Resources"
    }

    def srcFolderEnv = steps.sh(script:"echo \$SRC_FOLDER",returnStdout:true).trim();
    def appResourcesFolderEnv = steps.sh(script:"echo \$APP_RESOURCES_FOLDER",returnStdout:true).trim();

    if(srcFolderEnv != null && srcFolderEnv != ""){
      srcFolder = srcFolderEnv
    }

    if(appResourcesFolderEnv != null && appResourcesFolderEnv != ""){
      appResourcesFolder = appResourcesFolderEnv;
    }

    steps.echo """
      ******** START MUTUAL AUTHENTICATION ********
      SOURCE FOLDER      : ${srcFolder}
      APP RESOURCE FOLDER: ${appResourcesFolder}
    """;

    try{

      steps.sh "mkdir -p ${pluginsFolderName}"

      steps.echo """
        *** DOWNLOADING SECURITY PLUGINS ***
      """

      steps.withCredentials([
        [
          $class: 'UsernamePasswordMultiBinding', credentialsId: projectNameLowercase+'-jenkins-artefactory-token',usernameVariable: 'ARTIFACT_USER_NAME', passwordVariable: 'ARTIFACT_USER_PASSWORD'
        ]
      ]){
        steps.sh """
              curl --fail -o ${pluginsFolderName}/${pluginArtifactName} -u \"${script.env.ARTIFACT_USER_NAME}:${script.env.ARTIFACT_USER_PASSWORD}\" \"${artifactoryPluginPath}\"
              curl --fail -o ${pluginsFolderName}/${adapterArtifactName} -u \"${script.env.ARTIFACT_USER_NAME}:${script.env.ARTIFACT_USER_PASSWORD}\" \"${artifactoryAdapterPath}\"
          """
      }

      steps.echo """
        *** INSTALLING SECURITY PLUGIN ***
      """

      def npmUserConfig = """
        registry=${artifact_npm_registry_url}
        @bcp:registry=${bcp_virtual_registry}
        strict-ssl=false
        cache=/opt/nodecache
      """.stripIndent()

      steps.writeFile(
          file:"${script.env.WORKSPACE}/.npmrc",
          text: npmUserConfig
      )

      if(platform == "android"){
         docker.withServer("${script.env.DOCKER_HOST}"){
          docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
            def enviromentParameters      = getDockerEnviromentParameters()
            def temporalVolumes           = getDockerTemporalVolumes()
            def cacertsVolume             = "-v ${script.env.APPLICATION_TOOL}/oracle_java/jdk1.8.0_172/jre/lib/security/cacerts:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/security/cacerts"
            def initGradleVolume          = "-v ${script.env.WORKSPACE}/.gradle:/opt/android-sdk-linux/.gradle"
            def dockerVolumen             = "${temporalVolumes} -v ${script.env.WORKSPACE}:${script.env.WORKSPACE} -w ${script.env.WORKSPACE} ${initGradleVolume} ${cacertsVolume} -v ${script.env.APPLICATION_CACHE}/node-local-repo:/opt/nodecache"
            def dockerParameters          = "--network=host -e PROJECT_ARTIFACTID=${applicationName} -e PROJECT_VERSION=${applicationVersion}"
            def dockerCmd                 = "docker run --rm ${dockerVolumen} ${dockerParameters} ${dockerNativeScriptEnviroment}"

            steps.sh "${dockerCmd} tns plugin add ${script.env.WORKSPACE}/${pluginsFolderName}/${pluginArtifactName}"
          }
        }
      }else if(platform=="ios"){
        def temporalBinPath = "${macServerRubyPath}:${macServerNodePath}:${macServerNativeScriptPath}"
        steps.sh "export PATH=${temporalBinPath}:\$PATH && tns plugin add ${script.env.WORKSPACE}/${pluginsFolderName}/${pluginArtifactName}"
      }

      steps.sh """
        set +x
        tar -C ${script.env.WORKSPACE}/plugins -xf ${script.env.WORKSPACE}/plugins/mutual-adapter-nativescript.tar.gz
        cp ${script.env.WORKSPACE}/plugins/package/network-request-adapter-safe.ts ${srcFolder}/libs/network-request-adapter/network-request-adapter.ts
        cp ${script.env.WORKSPACE}/plugins/package/https-response.ts ${srcFolder}/libs/network-request-adapter/https-response.ts
        sed -i -e  s/NetworkRequestAdapterSafe/NetworkRequestAdapter/ ${srcFolder}/libs/network-request-adapter/network-request-adapter.ts
      """

      if(platform == "android"){
        def certificateCredentialId    = "${projectNameLowercase}-${applicationName}-mutual-cert-file";
        steps.echo """
          *** INSTALLING MUTUAL AUTHENTICATION CERTIFICATE ***
          * Certificate Credential ID: ${certificateCredentialId}
        """;

        steps.withCredentials([
          [
            $class: "FileBinding", credentialsId: "${certificateCredentialId}", variable: "mutualCertificateSecretFilePath"
          ]
        ]){
          steps.sh """
            set +x
            cp ${script.env.mutualCertificateSecretFilePath} ${srcFolder}/client.p12
          """
        }
      }else if(platform == "ios"){
        def certificateCredentialIosId = "${projectNameLowercase}-${applicationName}-mutual-cert-file-ios";

        steps.echo """
          *** INSTALLING MUTUAL AUTHENTICATION CERTIFICATE ***
          * Certificate Credential ID: ${certificateCredentialIosId}
        """;

        steps.withCredentials([
          [
            $class: "FileBinding", credentialsId: "${certificateCredentialIosId}", variable: "mutualCertificateSecretFilePath"
          ]
        ]){
          steps.sh """
            set +x
            cp ${script.env.mutualCertificateSecretFilePath} ${appResourcesFolder}/iOS/client.p12
          """
        }
      }

      steps.echo """
        ******** END MUTUAL AUTHENTICATION ********
      """

      body.call()
    }catch(Exception e){
      throw e;
    }finally{
      steps.sh """
        set +x
        rm -rf ${script.env.WORKSPACE}/${pluginsFolderName}/${adapterArtifactName}
        rm -rf ${script.env.WORKSPACE}/${pluginsFolderName}/${pluginArtifactName}
      """
    }

  }

  def setAngularEnvironmentFromVault(credentials=[:],envPathFile="src/environments/environment.ts"){
    angularEnvVault         = credentials;
    angularEnvVaultPathFile = envPathFile;
  }

  def configureAngularEnvironmentFromVault(Closure body){
    if(angularEnvVault==null || angularEnvVaultPathFile==null){
      body()
    }else{
      steps.echo "******* CONFIGURACION DE VARIABLES DE ENTORNO-VAULT ANGULAR *******"

      if(angularEnvVault.size()<1 || angularEnvVaultPathFile.trim().length()<1){
        throw new Error("Debe usar la funcion setAngularEnvironmentFromVault correctamente");
      }

      steps.echo """
        KEYS             : ${angularEnvVault}
        ENVIRONMENT FILE : ${angularEnvVaultPathFile}
      """

      def credentialsMap = [:];
      def buildTimestamp = getBuildTimestamp();
      def envFile        = "env-${buildTimestamp}.json"
      def tsFile         = "new-environment-${buildTimestamp}.ts";

      angularEnvVault.each {
        def secretKey   = "${it.key}".toString();
        def secretValue = "${it.value}".toString();

        steps.withCredentials([
          [$class: 'StringBinding', credentialsId: "${secretKey}", variable: 'secretTempVar' ]
        ]){
          credentialsMap["${secretValue}"] = "${script.env.secretTempVar}".toString()
        }
      }

      def json_result_parsed = JsonOutput.toJson(credentialsMap)

      steps.writeFile(
        file: envFile,
        text: json_result_parsed.toString()
      )

      def bcp_virtual_registry = "${script.env.ARTIFACT_SERVER_URL}/api/npm/public-release-node/"
      def dockerVolumen       = "-v ${script.env.WORKSPACE}:/project/data1 -w /project/data1 -v ${script.env.APPLICATION_CACHE}/node-local-repo:/opt/nodecache"

      def npm_arguments = """"
        registry = ${artifact_npm_registry_url}
        @bcp:registry = ${bcp_virtual_registry}
        quiet = true
        strict-ssl = false
        progress = false
      """.stripIndent()

      steps.writeFile(
        text: npm_arguments,
        file: "${script.env.WORKSPACE}/.npmrc"
      )

      def angularEnvVaultPathFileTmp = angularEnvVaultPathFile.replace(".ts", "");
      def tmpScriptFile              = "vaultscript-${buildTimestamp}.ts";
      def vaultScript = """
        import {environment} from './${angularEnvVaultPathFileTmp}'
        let data = require('./${envFile}');

        let result = {...environment,...data};

        console.log("export const environment =",result);
      """.stripIndent();

      steps.writeFile(
        file:tmpScriptFile,
        text:vaultScript
      )

      docker.withServer("${script.env.DOCKER_HOST}"){
        docker.withRegistry("http://${script.env.DOCKER_REGISTRY_URL}") {
          def dockerParameters = "--network=host"                                                                         //"--cap-add=SYS_ADMIN -e DISPLAY --net=host -u root"
          def dockerCmd        = "docker run --rm ${dockerVolumen} ${dockerParameters} ${dockerNodeAngularEnviroment}"

          steps.sh "${dockerCmd} npm install --no-save --no-package-lock ts-node typescript @types/node"
          steps.sh "${dockerCmd} ./node_modules/.bin/ts-node --skip-project true ${tmpScriptFile} > ${tsFile}"
        }
      }

      steps.sh """
        set +x
        mv ${tsFile} ${angularEnvVaultPathFile}
      """

      steps.sh """
        set +x
        rm -rf ${envFile}
        rm -rf ${tmpScriptFile}
        rm ${script.env.WORKSPACE}/.npmrc
      """

      steps.echo "******* FIN CONFIGURACION DE VARIABLES DE ENTORNO-VAULT ANGULAR *******"

      body()

      steps.sh "git checkout ${angularEnvVaultPathFile}"

    }
  }

  /**
  * Metodo despliega hacia Azure Webapp For Containers
  */
  def deployToAzureWebappContainer(String appName,String appVersion,String azureResourceGroupName = '',String azureWebAppName = '',boolean debugMode=false) {

    if(!appName || !appVersion || !azureResourceGroupName || !azureWebAppName){
      throw new Exception("Paramters appName, appVersion,azureResourceGroupName, azureWebAppName are required")
    }

    def projectName    = "${script.env.project}".toLowerCase();
    def environment    = "${script.env.deploymentEnvironment}";
    def buildTimestamp = buildTimestamp;

    //SP para hacer PUSH de la imagen hacia ACR
    //KV de DevSecOps o Carpeta principal de Jenkins
    def azureSpPushId         = "${projectName}-acr-push-sp-id"
    def azureSpPushPassword   = "${projectName}-acr-push-sp-password"

    def dockerRegistryUrl = azureContainerRegistries[environment];
    def imageTag          = "${dockerRegistryUrl}/${projectName}/${appName}:${appVersion}"

    steps.echo """
      Azure Webapp Container Deployment INFO
        * Webapp Container Resource Group: ${azureResourceGroupName}
        * Webapp Container Name: ${azureWebAppName}
        * Debug Mode: ${debugMode}
    """

    def buildArgs = getDockerBuildArgs();
    def buildInfo = getDockerLabelBuildInfo(appName,appVersion);

    steps.echo """
      ******** BUILDING DOCKER IMAGE ********
    """

    steps.withEnv(dockerBuildEnvironmentVariables){
      steps.sh "docker build --network=host --no-cache ${buildArgs} ${buildInfo} -t ${imageTag} ."
    }

    steps.echo """
      ******** PUSHING DOCKER IMAGE ********
    """

    /**
      * Docker login
      * Docker push
      * Docker logout
    */
    steps.withCredentials([
      [$class: "StringBinding", credentialsId: "${azureSpPushId}", variable: "azureSpPushIdValue" ],
      [$class: "StringBinding", credentialsId: "${azureSpPushPassword}", variable: "azureSpPushPasswordValue" ]
    ]){
      try{
        script.env.azureSpPushPasswordValue = script.env.azureSpPushPasswordValue.replaceAll( /([^a-zA-Z0-9])/, '\\\\$1' )
        steps.sh """
          set +x
          docker login ${dockerRegistryUrl} --username ${script.env.azureSpPushIdValue} --password ${script.env.azureSpPushPasswordValue}
        """
        steps.sh "docker push ${imageTag}"
        steps.sh "docker logout ${dockerRegistryUrl}"
        steps.sh "docker rmi ${imageTag}"
      }catch(Exception e){
        throw e;
      }
    }

    //SP para PULL ACR
    //KV de DevSecOps o Carpeta principal de Jenkins
    def azureSpPullId       = "${projectName}-acr-pull-sp-id"
    def azureSpPullPassword = "${projectName}-acr-pull-sp-password"

    //SP para configurar WebApp
    //En KV de equipo o carpeta de jenkins
    def azureSpWebappId       = "${projectName}-${appName}-webapp-id"
    def azureSpWebappPassword = "${projectName}-${appName}-webapp-password"
    def azureSpWebappTenant   = "${projectName}-${appName}-webapp-tenant"

    def dockerVolumen    = "-v ${script.env.WORKSPACE}@tmp-${buildTimestamp}:${script.env.WORKSPACE}@tmp-${buildTimestamp} -v ${script.env.WORKSPACE}:${script.env.WORKSPACE} -v ${script.env.WORKSPACE}@tmp:${script.env.WORKSPACE}@tmp"
    def dockerParameters = "-d -it --network=host"

    steps.echo """
      Azure Webapp Container Deployment Credentials ID
        * Azure Container Registry SP ID: ${azureSpPullId}
        * Azure Container Registry SP ID: ${azureSpPullPassword}
        *
        * Azure Webapp for Container SP ID: ${azureSpWebappId}
        * Azure Webapp for Container SP PASSWORD: ${azureSpWebappPassword}
        * Azure Webapp for Container SP TENANT: ${azureSpWebappTenant}
    """

    /**
    * Configurar Webapp Container
    * Configurar WebApp AppSettings
    */
    docker.withRegistry(script.env.DOCKER_REGISTRY_URL) {
      docker.withServer(script.env.DOCKER_HOST){

        if(!azureKeyVaultActivated){
          steps.withCredentials([
            [$class: "StringBinding", credentialsId: "${azureSpPullId}", variable: "azureSpPullIdValue" ],
            [$class: "StringBinding", credentialsId: "${azureSpPullPassword}", variable: "azureSpPullPasswordValue" ],
            [$class: "StringBinding", credentialsId: "${azureSpWebappId}", variable: "azureSpWebappIdValue" ],
            [$class: "FileBinding", credentialsId: "${azureSpWebappPassword}", variable: "azureSpWebappPasswordValue" ],
            [$class: "StringBinding", credentialsId: "${azureSpWebappTenant}", variable: "azureSpWebappTenantValue" ]
          ]){

            script.env.azureSpPullPasswordValue = script.env.azureSpPullPasswordValue.replaceAll( /([^a-zA-Z0-9])/, '\\\\$1' )

            def azCliDockerId = "";

            try{
              azCliDockerId = steps.sh(script:"docker run ${dockerParameters} ${dockerVolumen} ${dockerAzureClientEnviroment} bash",returnStdout:true).trim();
              steps.sh "docker exec ${azCliDockerId} az login --service-principal --username ${script.env.azureSpWebappIdValue} --password ${script.env.azureSpWebappPasswordValue} --tenant ${script.env.azureSpWebappTenantValue}"

              steps.echo """
                ******** CONFIGURING WEBAPP APP SETTINGS ********
              """

              //CONFIGURAR APP SETTINGS
              if(webAppSettingsEnvironmentVariables.size()>0){
                for(def item in webAppSettingsEnvironmentVariables){
                  steps.sh "docker exec ${azCliDockerId} az webapp config appsettings set -n ${azureWebAppName} -g ${azureResourceGroupName} --settings ${item}"
                }
              }

              steps.echo """
                ******** CONFIGURING WEBAPP CONTAINER SETTINGS ********
              """

              //CONFIGURAR CONTAINER SETTINGS
              steps.sh "docker exec ${azCliDockerId} az webapp config container set -n ${azureWebAppName} -g ${azureResourceGroupName} -c ${imageTag} -r ${dockerRegistryUrl} -u ${script.env.azureSpPullIdValue} -p ${script.env.azureSpPullPasswordValue}"
              steps.sh "docker exec ${azCliDockerId} az webapp restart -g ${azureResourceGroupName} -n ${azureWebAppName}"

            }catch(Exception e){
              throw e;
            }finally{
              if(azCliDockerId){
                steps.sh "docker rm -f ${azCliDockerId}"
              }

              steps.sh "rm ${script.env.azureSpWebappPasswordValue}"
            }

          }
        }else{
          steps.withCredentials([
              [$class: "StringBinding", credentialsId: "${azureSpPullId}", variable: "azureSpPullIdValue" ],
              [$class: "StringBinding", credentialsId: "${azureSpPullPassword}", variable: "azureSpPullPasswordValue" ]
          ]){
            withAzureVaultCredentials([
              [azureCredentialId: "${azureSpWebappId}", azureCredentialVariable: "azureSpWebappIdValue" ],
              [azureCredentialId: "${azureSpWebappPassword}", azureCredentialVariable: "azureSpWebappPasswordValue",azureCredentialType:"file"],
              [azureCredentialId: "${azureSpWebappTenant}", azureCredentialVariable: "azureSpWebappTenantValue" ]
            ]){
              script.env.azureSpPullPasswordValue = script.env.azureSpPullPasswordValue.replaceAll( /([^a-zA-Z0-9])/, '\\\\$1' )

              def azCliDockerId = "";

              try{
                azCliDockerId = steps.sh(script:"docker run ${dockerParameters} ${dockerVolumen} ${dockerAzureClientEnviroment} bash",returnStdout:true).trim();
                steps.sh "docker exec ${azCliDockerId} az login --service-principal --username ${script.env.azureSpWebappIdValue} --password ${script.env.azureSpWebappPasswordValue} --tenant ${script.env.azureSpWebappTenantValue}"

                steps.echo """
                  ******** CONFIGURING WEBAPP APP SETTINGS ********
                """

                //CONFIGURAR APP SETTINGS
                if(webAppSettingsEnvironmentVariables.size()>0){
                  for(def item in webAppSettingsEnvironmentVariables){
                    steps.sh "docker exec ${azCliDockerId} az webapp config appsettings set -n ${azureWebAppName} -g ${azureResourceGroupName} --settings ${item}"
                  }
                }

                steps.echo """
                  ******** CONFIGURING WEBAPP CONTAINER SETTINGS ********
                """

                //CONFIGURAR CONTAINER SETTINGS
                steps.sh "docker exec ${azCliDockerId} az webapp config container set -n ${azureWebAppName} -g ${azureResourceGroupName} -c ${imageTag} -r ${dockerRegistryUrl} -u ${script.env.azureSpPullIdValue} -p ${script.env.azureSpPullPasswordValue}"
                steps.sh "docker exec ${azCliDockerId} az webapp restart -g ${azureResourceGroupName} -n ${azureWebAppName}"

              }catch(Exception e){
                throw e;
              }finally{
                if(azCliDockerId){
                  steps.sh "docker rm -f ${azCliDockerId}"
                }

                steps.sh "rm ${script.env.azureSpWebappPasswordValue}"
              }
            }
          }
        }
      }
    }
  }

  def getDockerLabelBuildInfo(appName,appVersion){
    def startedTimestamp = steps.sh(script: "echo \$(date +\"%Y-%m-%dT%H:%M:%S.%3N%z\")", returnStdout: true).trim()
    def projectKey       = script.env.project;
    def buildUrl         = script.env.BUILD_URL;

    def labels=" --label " + [
      "name=${appName}",
      "version=${appVersion}",
      "startedTimestamp=\"${startedTimestamp}\"",
      "projectKey=${projectKey}",
      "buildUrl=\"${buildUrl}\"",
      "currentGitCommit=${currentGitCommit}",
      "buildUser=\"${buildUser}\"",
      "currentGitURL=${currentGitURL}",
      "branchName=${branchName}"
    ].join(" --label ");

    return labels;
  }

  def getDockerBuildArgs(){
    def buildArgs = "";
    if(dockerBuildEnvironmentVariables){
      buildArgs = "--build-arg " + dockerBuildEnvironmentVariables.join(" --build-arg ")
    }
    return buildArgs;
  }

  //IOS MODULES METHODS

  def getXCodeWorkspace(){
    def result = steps.sh (script : "find . -name '*.xcworkspace' -not -regex '.*xcodeproj.*'", returnStdout: true).trim()
    return result
  }

  def getXCodeWorkspaceName(){
    def result = steps.sh (script : "basename ${getXCodeWorkspace()}", returnStdout: true).trim()
    return result
  }

  def getXCodeWorkspaceDir(){
    def workspace = getXCodeWorkspace()
    return workspace.substring(0, workspace.lastIndexOf(getXCodeWorkspaceName()))
  }

  def getApplicationNamePod(){
    def workspaceName = getXCodeWorkspaceName()
    return workspaceName.substring(0, workspaceName.lastIndexOf(".")).trim()
  }

  def getXCodeSchema(String schemeQuery = ''){
    if(schemeQuery == ''){
      schemeQuery = getApplicationNamePod()
    }
    def scheme_available = steps.sh (script:"""
      xcodebuild -workspace ${getXCodeWorkspace()} -list
    """, returnStdout: true).trim()

    scheme_available = steps.sh (script:"""
      echo "${scheme_available}" | sed -e '1,2d' -e 's/        //' -e '/^Pods-/d'
    """, returnStdout: true).trim()

    def schema = steps.sh (script:"""
      echo "${scheme_available}" | grep "${schemeQuery}" | head -1
    """, returnStdout: true).trim()
    return schema == schemeQuery ? schema : schemeQuery
  }

  def getApplicationVersionPod() {
    def podSpecFile     = steps.readFile getApplicationPodSpec()
    def versionLine     = podSpecFile.split("\n").find { it.contains('.version') }.trim()
    def version         = versionLine.substring(versionLine.indexOf('\'')+1, versionLine.lastIndexOf('\''))
    return version
  }

  def getApplicationPodSpec() {
    def podSpecFile = steps.sh (script:"find *.podspec", returnStdout: true).trim()
    if(podSpecFile == null || podSpecFile == ""){
      throw new Exception("No PodSpec file")
    }
    return podSpecFile
  }

  def getPodFile() {
    def podFile = steps.sh (script:"find . -name Podfile", returnStdout: true).trim()
    if(podFile == null || podFile == ""){
      throw new Exception("No PodFile")
    }
    return podFile
  }

  def getPodFileLockPath() {
    def podFileLockPath = steps.sh (script:"find . -name Podfile.lock", returnStdout: true).trim()
    return podFileLockPath
  }

  def getPodRepoList(String repoType) {
    def podRepoList = steps.sh (script: """
      bash -l -c 'pod ${repoType} list'
    """, returnStdout: true).trim()
    return podRepoList
  }

  def getCurrentHomeDir() {
    def currentHomeDir = steps.sh (script: """
      echo ~
    """, returnStdout: true).trim()
    return currentHomeDir
  }

  def existsPodRepo(String repoName, String repoType) {
    def podRepoList = getPodRepoList(repoType)
    def findRepo = steps.sh (script:"""
      echo "${podRepoList}" | grep "${repoName}" | head -1
    """, returnStdout: true).trim()
    steps.echo "find: ${findRepo}"
    steps.echo "source: ${repoName}"
    return findRepo == repoName
  }

  def removePodRepo(String repoName) {
    if (existsPodRepo(repoName, "repo-art")) {
      steps.sh (script: """
        bash -l -c 'pod repo-art remove ${repoName}'
      """)
    }
    if (existsPodRepo(repoName, "repo")) {
      steps.sh (script: """
        bash -l -c 'pod repo remove ${repoName}'
      """)
    }
  }

  def addPodRepo(String repoName){
    if (!existsPodRepo(repoName, "repo-art") && !existsPodRepo(repoName, "repo")) {
      def podRepoSource = "\"https://artifactory.lima.bcp.com.pe/artifactory/api/pods/${repoName}\""
      steps.echo "source pod: ${podRepoSource}"
      steps.sh (script: """
        bash -l -c 'pod repo-art add ${repoName} ${podRepoSource}'
      """)
    }
  }

  private boolean existCredentialById(String credentialId) {
    def list = CredentialsProvider.lookupCredentials(UsernamePasswordCredentials.class);
    for (UsernamePasswordCredentials c : list) {
      steps.echo "search credential id: ${c.getId()}"
      if (c.getId().equals(credentialId)) {
        return true;
      }
    }
    return false;
  }

  private def printWithNoTrace(cmd) {
    steps.sh (script: '#!/bin/sh -e\n'+ cmd, returnStdout: true)
  }

  private def withProxyAvailable(String proxyCredentials, Closure body){
    //TO FIX move to global properties
    def noProxy = "10.79.0.0/16,127.0.0.1,.lima.bcp.com.pe,localhost"
    def proxyIps = ["10.80.193.113","10.80.193.115", "10.80.129.173", "10.80.129.175"]
    def proxyPort = "8080"
    def proxyAvailable = ""
    steps.withCredentials([
        [$class: 'UsernamePasswordMultiBinding', credentialsId:"${proxyCredentials}", usernameVariable: 'proxyUser',passwordVariable:'proxyPassword']
    ]){
      for(String proxyIp: proxyIps) {
        def proxy = "http://${script.env.proxyUser}:${script.env.proxyPassword}@${proxyIp}:${proxyPort}"
        try{
          steps.withEnv(["HTTP_PROXY=${proxy}","HTTPS_PROXY=${proxy}","http_proxy=${proxy}","https_proxy=${proxy}","no_proxy=${noProxy}","NO_PROXY=${noProxy}","LC_ALL=en_US.UTF-8","LANG=en_US.UTF-8"]) {
            printWithNoTrace("""
              curl --silent --max-time 5 -I https://www.google.com > /dev/null
            """)
            proxyAvailable = proxy
          }
        }
        catch(Exception e){
        }
      }
      if(proxyAvailable == ""){
        throw new Exception("No proxy available")
      }
      steps.echo "------------------------Initialize with proxy---------------------------------------"
      steps.echo "proxy used: ${proxyAvailable}"
      steps.withEnv(["HTTP_PROXY=${proxyAvailable}","HTTPS_PROXY=${proxyAvailable}","http_proxy=${proxyAvailable}","https_proxy=${proxyAvailable}","no_proxy=${noProxy}","NO_PROXY=${noProxy}","LC_ALL=en_US.UTF-8","LANG=en_US.UTF-8"]) {
        body.call()
      }
    }
  }

  private def podInstall(boolean updateRepo = false) {
    steps.echo "clean pod cache"
    steps.sh (script: """
      bash -l -c 'pod cache clean --all'
    """)

    def updateRepoCmd = ''
    if(updateRepo) {
      updateRepoCmd = '--repo-update'
    }

    deleteFileIfExists(getPodFileLockPath())

    def proxyCredentials = "${script.env.project}-user-proxy-mac-server".toLowerCase()
    def xCodeWorkspaceDir = getXCodeWorkspaceDir()

    def podfile = getPodFile()
    def startRepos = steps.sh (script: "grep -n \"cocoapods-art\" ${podfile} || true", returnStdout: true).trim()
    def podRepoList = []
    if(startRepos != ""){
      steps.echo "-----------------------------Get repos cocoapods---------------------------------"
      def startLine = Integer.parseInt(startRepos.split(":")[0]) + 1
      def endLine = Integer.parseInt(steps.sh (script: "grep -n \"\\]\" ${podfile}", returnStdout: true).split(":")[0]) - 1
      podRepoList = steps.sh (script: """
        sed -n '${startLine},${endLine}p' ${podfile}
      """, returnStdout: true).split("\\n")
    }
    steps.echo "-----------------------------Add repo cocoapods---------------------------------"
    for(String repo: podRepoList){
      addPodRepo(repo.replaceAll("[^a-zA-Z0-9._-]|^", ""))
    }
    steps.echo "-----------------------------End repo cocoapods---------------------------------"
    steps.echo "credential id: ${proxyCredentials}"
    try{
      withProxyAvailable(proxyCredentials){
        steps.sh (script: """
          bash -l -c 'cd ${xCodeWorkspaceDir} && pod install ${updateRepoCmd}'
        """)
      }
    }
    catch(Exception e){
      steps.sh (script: """
          bash -l -c 'cd ${xCodeWorkspaceDir} && pod install ${updateRepoCmd}'
      """)
    }
    for(String repo: podRepoList){
      removePodRepo(repo.replaceAll("[^a-zA-Z0-9._-]|^", ""))
    }
  }

  /*
  * This method save the results for test and generate and the deploy artifact by extension
  */
  def saveResultPod() {
    steps.archiveArtifacts(artifacts: '*tar.gz', allowEmptyArchive: true)
  }

  def deleteFileIfExists(String filePath){
    if (filePath&&filePath!=''&&steps.fileExists(filePath)) {
      steps.sh (script: """
          rm ${filePath}
      """)
    }
  }

  def createDefaultCredentialsFile() {
    steps.withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: currentCredentialsId,usernameVariable: 'GIT_USER_VALUE', passwordVariable: 'GIT_USER_PASSWORD']]) {
      def usernameEncoded = "${script.env.GIT_USER_VALUE}"
      def passwordEncoded = "${script.env.GIT_USER_PASSWORD}"
      def currentHomeDir  = getCurrentHomeDir()
      def credentialsFile = "$currentHomeDir/.netrc"

      deleteFileIfExists(credentialsFile)

      def netrc = """
        machine bitbucket.lima.bcp.com.pe
        login $usernameEncoded
        password $passwordEncoded

        machine artifactory.lima.bcp.com.pe
        login $usernameEncoded
        password $passwordEncoded
      """.stripIndent()

      steps.writeFile(
        file: credentialsFile,
        text: netrc
      )
    }
  }

  def setPodRepo(String branchName = '', String repoName = "${podSpecsRepoName}"){
    removePodRepo(repoName) //juntar

    steps.sh (script: """
      bash -l -c 'pod repo add ${repoName} ${bitbucketUrl}/${podSpecsRepoGITURL} ${branchName}'
    """)
  }

  def buildPod(String enviroment = '', boolean enabledIxGuardOfuscation = false) {
    //Pack data to send to slave node
    steps.stash includes: '**/*', name: 'workspace'
    steps.node(getMacServerAgent()) {
      steps.step([$class: 'WsCleanup'])
      steps.unstash 'workspace'

      def podName = getApplicationNamePod()
      def podSpecFilePath = getApplicationPodSpec()
      def podVersion = getApplicationVersionPod()
      def xcodeWorkspace = getXCodeWorkspaceDir()

      steps.echo "pod name: $podName"
      steps.echo "podSpec file path: $podSpecFilePath"
      steps.echo "pod version: $podVersion"
      steps.echo "pod workspace path $xcodeWorkspace"

      podInstall()

      def podSchema = getXCodeSchema()
      def folderDevices = "build/devices"
      def folderSimulator = "build/simulator"
      def folderUniversal = "build/universal"
      steps.echo "pod schema $podSchema"

      steps.echo "create folder where we place built frameworks"
      steps.sh "mkdir build"

      steps.echo "build framework for simulators"
      steps.sh (script: """
        xcodebuild clean build -project ${xcodeWorkspace}${podsXCodeProject} -scheme ${podSchema} -configuration Release OTHER_CFLAGS="-fembed-bitcode" ENABLE_BITCODE=YES BITCODE_GENERATION_MODE="bitcode" -sdk iphonesimulator -derivedDataPath derived_data
      """)

      steps.echo "create folder to store compiled framework for simulator"
      steps.sh "mkdir ${folderSimulator}"

      steps.echo "copy compiled framework for simulator into our build folder"
      steps.sh "cp -r ${xcodeWorkspace}build/Release-iphonesimulator/${podSchema}/${podSchema}.framework ${folderSimulator}"

      steps.echo "build framework for devices"
      steps.sh (script: """
        xcodebuild clean build -project ${xcodeWorkspace}${podsXCodeProject} -scheme ${podSchema} -configuration Release OTHER_CFLAGS="-fembed-bitcode" ENABLE_BITCODE=YES BITCODE_GENERATION_MODE="bitcode" -sdk iphoneos -derivedDataPath derived_data
      """)

      steps.echo "create folder to store compiled framework for simulator"
      steps.sh "mkdir ${folderDevices}"

      steps.echo "copy compiled framework for simulator into our build folder"
      steps.sh "cp -r ${xcodeWorkspace}build/Release-iphoneos/${podSchema}/${podSchema}.framework ${folderDevices}"

      steps.echo "create folder to store compiled universal framework"
      steps.sh "mkdir ${folderUniversal}"

      steps.echo "copy device framework into universal folder"
      steps.sh "cp -r ${folderDevices}/${podSchema}.framework ${folderUniversal}"

      steps.echo "create framework binary compatible with simulators and devices, and replace binary in unviersal framework"
      steps.sh "lipo -create ${folderSimulator}/${podSchema}.framework/${podSchema} ${folderDevices}/${podSchema}.framework/${podSchema} -output ${folderUniversal}/${podSchema}.framework/${podSchema}"

      steps.echo "copy simulator Swift public interface to universal framework"
      steps.sh "cp ${folderSimulator}/${podSchema}.framework/Modules/${podSchema}.swiftmodule/* ${folderUniversal}/${podSchema}.framework/Modules/${podSchema}.swiftmodule"

      if(enabledIxGuardOfuscation == true){
        steps.echo "remove the architectures i386 and x86_64 for the Simulators"
        removeArchitectureiOS("i386", "${folderUniversal}/${podSchema}.framework/${podSchema}")
        removeArchitectureiOS("x86_64", "${folderUniversal}/${podSchema}.framework/${podSchema}")
        executeIxGuard("${folderUniversal}/${podSchema}.framework")
      }

      steps.echo "Check the architectures from framework"
      steps.sh "lipo -info ${folderUniversal}/${podSchema}.framework/${podSchema}"

      steps.echo "remove folder store compiled framework for simulator and devices"
      steps.sh "rm -R ${folderSimulator} && rm -R ${folderDevices}"

      steps.echo "change pod spec source files"
      steps.sh (script: """
        sed -i '' 's~s.source_files.*~s.source_files = '"'"'${folderUniversal}/${podName}.framework/Headers/*'"'"' ~g' ${podName}.podspec
        sed -i '' '/s.source_files.*/a\\'\$'\\n   s.public_header_files = '"'"'${folderUniversal}/${podName}.framework/Headers/*'"'"' ' ${podName}.podspec
        sed -i '' '/s.public_header_files.*/a\\'\$'\\n   s.vendored_frameworks = '"'"'${folderUniversal}/${podName}.framework'"'"'\n ' ${podName}.podspec
      """)

      steps.echo "view podspec"
      steps.sh "cat ${podName}.podspec"

      steps.echo "Building a tgz artifact"
      steps.sh (script: """
        tar -zcvf ${podName}-${podVersion}.tar.gz build/ ${podName}.podspec
        ls -al ${podName}-${podVersion}.tar.gz
      """)
      steps.stash includes: '*.tar.gz', name: 'pod-artifact'
    }
    steps.unstash 'pod-artifact'
  }

  def executeIxGuard(String applicationFile){
    def applicationName = FilenameUtils.getBaseName(applicationFile)
    def applicationPath = FilenameUtils.getFullPath(applicationFile)
    def applicationType = FilenameUtils.getExtension(applicationFile)
    def ixGuardLicenseFile = "ixguard-license.txt"
    def ixGuardConfig = "${applicationPath}ixguard.yml"
    def projectName          = "${script.env.project}".toLowerCase()
    def ixGuardApplicationId = "ixguard-${projectName}-${applicationName.toLowerCase()}-license"
    def timestamp=steps.sh(script: "date '+%Y%m%d%H%M%S'", returnStdout: true).trim()

    steps.echo """************ Initialize IxGuard ************
    Application name: ${applicationName}
    Application path: ${applicationPath}
    Application type: ${applicationType}
    """

    steps.echo """************ Configuring License file ************
    IxGuard License File: ${ixGuardApplicationId}
    """
    steps.withCredentials([[$class: "FileBinding", credentialsId: "${ixGuardApplicationId}",
    variable: "ixGuardApplicationId" ]]) {
        steps.sh """set +x
          cp -f ${script.env.ixGuardApplicationId} ${applicationPath}
        """
        def ixguardConfig = steps.sh (script: """
          find . -name ixguard.yml -print
        """, returnStdout: true).trim()
        if(ixguardConfig != ""){
          steps.sh "use custom ${ixguardConfig} to build ${applicationFile}"
        }
        else{
          def ixguardConfigText = """
export:
  identity:        ''
build:
  bcsymbolmaps-path: "dSYMs/"
protection:
  names:
    enabled:         false
  arithmetic-operations:
    enabled:         true
  control-flow:
    enabled:         true
    logic:
      enabled:         true
    calls:
      enabled:         false
  data:
    enabled:         true
  code-integrity:
    enabled:         true
    functions:
      enabled:         true
    objc-calls:
      enabled:         true
  app-integrity:
    enabled:         false
    spray:           false
  environment-integrity:
    enabled:         false
    jailbreak:
      enabled:         true
    debugger:
      enabled:         true
...
          """

          ixguardConfig = "ixguard.yml"
          steps.writeFile(
            file: ixguardConfig,
            text: ixguardConfigText
          )
        }
      	steps.sh "mv ${ixguardConfig} ${ixGuardConfig}"
      	steps.sh "cat ${ixGuardConfig}"
        steps.echo "execute ixguard run"
        def obfuscatedFile = "obfuscated.${applicationType}"
      	steps.sh "bash -l -c 'cd ${applicationPath} && ls -lha && ixguard -config ixguard.yml -o ${obfuscatedFile} ${FilenameUtils.getName(applicationFile)}'"
      	steps.sh "rm -R ${applicationFile} && rm ${applicationPath}${ixGuardLicenseFile} && mv ${applicationPath}${obfuscatedFile} ${applicationFile} && ls -lha ${applicationPath}"
    }
  }

  def removeArchitectureiOS(String architecture, String sourceFile){
    steps.sh "lipo -remove ${architecture} ${sourceFile} -o ${sourceFile}"
  }

  def executeFortifyPodSast(){
    steps.node(getMacServerAgent()) {
      steps.echo "Interrupt the script if any step here fails"
      steps.sh (script: """
        set -e
        set -o pipefail
      """)

      def xcodeWorkspace = getXCodeWorkspaceDir()
      def podName = getApplicationNamePod()
      def podSchema = getXCodeSchema()

      def buildID = "${script.env.project}-${podName}-${script.env.BUILD_NUMBER}"

      steps.echo "Clean project build cache (soft clean)"
      steps.sh (script: """
        sourceanalyzer -b ${buildID} xcodebuild clean -project ${xcodeWorkspace}${podsXCodeProject} -scheme ${podSchema}
      """)
      steps.echo "Clean project build cache (hard clean)"
      def CACHEPATH="${getCurrentHomeDir()}/Library/Developer/Xcode/DerivedData"
      steps.sh (script: """
        set +e
        find "${CACHEPATH}" -name "${podName}*" -type d -maxdepth 1 -exec rm -rf {} + > /dev/null
        ls ${CACHEPATH}
        sourceanalyzer -b ${buildID} -clean
      """)

      steps.echo "Build project"
      def output_raw = "${podName}/${podSchema}.fpr"

      steps.sh (script: """
        sourceanalyzer -b ${buildID} –Xmx3G xcodebuild build -project ${xcodeWorkspace}${podsXCodeProject} -scheme ${podSchema}
        sourceanalyzer -b ${buildID} –Xmx3G -scan -f "${output_raw}"
      """)

      def projectLowerCase="${script.env.project}".toLowerCase()
      def fortifyCloudScanCredId = "${projectLowerCase}-jenkins-fortify-cloudscan-token"
      def fortifyAuthCredId = "${projectLowerCase}-jenkins-fortify-ssc-token"
      def fortifyVersion = getGitProjectName()

      steps.withCredentials([[$class: 'StringBinding', credentialsId: "${fortifyCloudScanCredId}", variable: 'cloudScanToken' ],
                  [$class: 'StringBinding', credentialsId: "${fortifyAuthCredId}", variable: 'authToken' ]]){

        steps.echo "Execute scan with project ${script.env.project}"
        steps.sh (script: """
          cloudscan -sscurl ${fortifyUrl} -ssctoken ${script.env.cloudScanToken} start -block -log ios.log -upload --application "${script.env.project}" --application-version "${fortifyVersion}" -b "${buildID}" -uptoken ${script.env.cloudScanToken} -scan ${execFortifyMemory}
          fortifyclient downloadFPR -file result.fpr -url ${fortifyHost} -authtoken ${script.env.authtoken} -project "${script.env.project}" -version "${fortifyVersion}"
          BIRTReportGenerator -template 'OWASP Top 10' -source result.fpr -format HTML --Version 'OWASP Top 10 2017' -output MyOWASP_Top10_Report.html
        """)
      }


      steps.echo "Analysis done. Your results are at"
      //steps.echo output_raw
    }
  }

  /*
  * This method deploy artifact on Artifactory for Native IOS projects
  */
  def deployArtifactoryPod(String enviroment = '',String tagName = '', boolean enabledIxGuardOfuscation = false){
    def buildUniqueId

    if(tagName != null && tagName != ''){
      //Clean up to prevent unclean data
      steps.step([$class: 'WsCleanup'])
      buildUniqueId = steps.sh (script: "echo ${tagName} | cut -d '/' -f2 ", returnStdout: true).trim()
      checkoutBranch(tagName)
      buildPod(enviroment, enabledIxGuardOfuscation)
    }else{
      buildUniqueId = "${buildTimestamp}"
    }

    def repository
    enviroment=enviroment.toUpperCase()
    def repoKey
    if ("${branchName}" == "master"){
      repoKey = "${script.env.project}.cocoapods.Release"
    }else if (enviroment){
      if (enviroment=="DEV") {
        repoKey="${script.env.project}.cocoapods.Snapshot"
      } else if (enviroment!="PROD") {
        repoKey="${script.env.project}.cocoapods-${enviroment}"
      }
    }

    steps.echo "Enviroment: ${enviroment}"
    steps.echo "Branch name: ${branchName}"

    def packageType = "tar.gz"
    def podName = getApplicationNamePod()
    def podVersion = getApplicationVersionPod()
    def projectNameLowercase = "${script.env.project}".toLowerCase()
    def buildTimestamp = getBuildTimestamp()

    def applicationGitName     = getGitProjectName()
    def projectKeyName         = "${script.env.project}".toLowerCase()+"-${podName}"
    def actionURL              = "${artefactoryUrl}/${repoKey}/${podName}/${podVersion}/"
    steps.echo "Repository Path: ${actionURL}"
    def actionBuildURL         = "${artefactoryUrl}/api/build"
    steps.echo "Artifactory Build Api URL: ${actionBuildURL}"

    steps.echo "buildTimestamp: $buildTimestamp"
    steps.echo "buildUniqueId: $buildUniqueId"
    steps.echo "applicationGitName: $applicationGitName"
    steps.echo "projectKeyName: $projectKeyName"

    steps.withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: projectNameLowercase+'-jenkins-artefactory-token',
                        usernameVariable: 'ARTIFACT_USER_NAME', passwordVariable: 'ARTIFACT_USER_PASSWORD']]) {

      def artifactSha1 = steps.sh (script: "sha1sum ${podName}-${podVersion}.${packageType} | cut -d ' ' -f 1", returnStdout: true).trim()
      def artifactMd5 = steps.sh (script: "md5sum ${podName}-${podVersion}.${packageType} | cut -d ' ' -f 1", returnStdout: true).trim()
      steps.echo "artifactSha1: ${artifactSha1}"
      steps.echo "artifactMd5: ${artifactMd5}"

      def result = steps.sh (script: """
      curl -o response-build-info-${buildTimestamp}.txt -X PUT --header \"X-Checksum-MD5:${artifactMd5}\" --header \"X-Checksum-Sha1:${artifactSha1}\" -u ${script.env.ARTIFACT_USER_NAME}:${script.env.ARTIFACT_USER_PASSWORD} -T ${podName}-${podVersion}.${packageType} "${actionURL}${podName}-${podVersion}.${packageType};build.name=${projectKeyName};build.number=${buildUniqueId};build.user=${script.env.BUILD_USER_ID}"
      """, returnStdout: true)
      def callback = steps.sh (script: "cat response-build-info-${buildTimestamp}.txt", returnStdout: true).trim()
      steps.echo "Callback: ${callback}"

      def errorMessageArtifactory = steps.sh (script:  "cat response-build-info-${buildTimestamp}.txt | jq --raw-output '.errors | .[0] | .message'", returnStdout: true).trim()

      //Artefact has promoted or moved
      if(errorMessageArtifactory != "null"){
         throw new Exception("Error deploying the artifact to the artifact server: ${errorMessageArtifactory}")
      }

      //Nanoseconds not supporte for MAC, set to 0
      def timestamp = steps.sh (script: "echo \$(date +\"%Y-%m-%dT%H:%M:%S.000%z\")", returnStdout: true).trim()
      def artifactoryBuildInfoJson = getArtifactoryBuildInfo("${script.env.ARTIFACT_USER_NAME}",projectNameLowercase,podName,podVersion,buildUniqueId,timestamp,packageType,artifactSha1,artifactMd5)
      steps.echo "artifactoryBuildInfoJson: ${artifactoryBuildInfoJson}"

      steps.writeFile(
       file: "artifactory-json-${buildTimestamp}.json",
       text: artifactoryBuildInfoJson
      )

      result = steps.sh (script: """
      curl -X PUT -I -i -u ${script.env.ARTIFACT_USER_NAME}:${script.env.ARTIFACT_USER_PASSWORD} -H \"Content-Type: application/json\" -T artifactory-json-${buildTimestamp}.json "${actionBuildURL}"
      """, returnStdout: true)
      steps.echo "Result: ${result}"
    }
  }

  def startReleasePod(boolean enabledIxGuardOfuscation = false){
    if ("${branchName}"=="master"){
        throw new Exception("Generating of release must not be done on master")
    }

    //Check generate new version
    def applicationVersion=getApplicationVersionPod()
    steps.echo "Application version: ${applicationVersion}"
    def nextReleaseVersion=getApplicationNextReleaseVersion(applicationVersion)
    steps.echo "Next release version: ${nextReleaseVersion}"

    def releaseBranchName="release/"+nextReleaseVersion
    def existBranchInRepository=existBranch(releaseBranchName)
    steps.echo "Exist release branch: ${existBranchInRepository}"
    if(existBranchInRepository){
      //If the branch exist an error is throw
      steps.echo "Deleting release branch: ${releaseBranchName}"
      deleteNotMergeReleaseBranch(releaseBranchName)
    }

    createNewReleaseBranch(nextReleaseVersion)
    checkoutReleaseBranch(nextReleaseVersion)
    updateToReleaseVersionPod(nextReleaseVersion)

    def tagStateComment="Release Candidate"
    def currentDate = buildTimestamp
    def enviromentParam="CERT"
    applicationVersion=getApplicationVersionPod()
    def tagName = "RC-"+applicationVersion+"-${enviromentParam}-${currentDate}"

    tagName=updateTagNameWithACR(tagName)

    steps.echo "Tag name: ${tagName}"
    protectBranch(releaseBranchName)

    //Validate SAST Analisys
    if(fortifyActivated){
      executeFortifyPodSast()
    }

    tagBranch(tagName,tagStateComment,enviromentParam)
    deployArtifactoryPod(enviromentParam, tagName, enabledIxGuardOfuscation)
  }

  def updateToReleaseVersionPod(String version){
    def podSpecFilePath = getApplicationPodSpec()
    def podFilePath     = getPodFile()
    def podVersion      = getApplicationVersionPod()
    steps.sh (script: """
    sed -i -e 's/${podVersion}/${version}/' ${podSpecFilePath}
    sed -i -e 's/-SNAPSHOT//' ${podSpecFilePath}
    sed -i -e 's/-SNAPSHOT//' ${podFilePath}
    """, returnStdout: true)
    updateToReleaseVersion(version)
  }

  def getResponseFile() {
    def responseBIF = steps.sh (script:"find . -name \"response-build-info-${buildTimestamp}.txt\"", returnStdout: true).trim()
    return responseBIF
  }

  /*
  * This method will promote a release version
  */
  def promoteReleasePod(String releaseTagName, boolean forceRelease = false){
    if(!"${branchName}".startsWith("tags/RC-")){
        throw new Exception("The release must be done on release")
    }

    def applicationVersion = getApplicationVersionPod()
    def existTag           = existTag("${applicationVersion}")
    def tagStateComment    = "Release"
    def tagName            = applicationVersion
    def applicationName        = getApplicationNamePod()

    if(existTag){
      if (forceRelease){
        steps.echo "Forcing removal and promote of release tag name: ${releaseTagName}"
        deleteTagBranch(tagName)
        steps.echo "Tag removed: ${releaseTagName}"
        steps.echo "${tagStateComment}-FORCED"
      } else{
        throw new Exception("The tag for this release exists: ${applicationVersion}")
      }
    }

    steps.echo "Promote release tag name: ${releaseTagName}"
    promoteRepository(applicationName, releaseTagName, ".cocoapods")
    //mergeReleaseToMaster(releaseTagName)
    mergeOverwriteBranch(releaseTagName, "master")
    tagBranch(tagName,tagStateComment)
  }

  public void setMappingFilePath(String path){
    getInstanceGradleAndroidJenkinsUtil().setMappingFilePath(path)
  }

  public void setGradleCertEnabled(boolean gradleCertParam){
    getInstanceGradleAndroidJenkinsUtil().setGradleCertEnabled(gradleCertParam)
  }
  
  /*
  Method for deploy api in Azure API Management - Release 1 (Api PU)
  */
  def deployApiLifeCycleAzure(Map mapParameters){
    steps.echo """
      NOTE: - The parameters should be populate in jenkins file before invoke this method
        * swaggerName: Archivo swagger
        * appCode: Código de aplicación
        * clientIdBackend: Id cliente de backend
        * productName: Nombre de Producto
        * productDisplayName: Nombre para mostrar del producto
        * productDescription: Descripción del producto
    """
    String swaggerName = mapParameters.apiParameters.swaggerName
    String appCode = mapParameters.policyParameters.appCode
    String clientIdBackend = mapParameters.policyParameters.clientIdBackend
    String productName = mapParameters.productParameters.productName
    String productDisplayName = mapParameters.productParameters.productDisplayName
    String productDescription = mapParameters.productParameters.productDescription

    String deploymentEnviroment = "${script.env.deploymentEnviroment}"
    String deployFolder = "/deploy/template-json" //imported folder where is located json templates
    String policyFolder = "/deploy/policy-xml" //imported folder where is located xml policies

    // Reading swagger to obtain parameters
    String swaggerFolder = "/devops/swagger" //local folder where is located swagger to be deployed
    String strPath = steps.sh script: "jq -rj '.paths | keys[0]' ${script.env.WORKSPACE}${swaggerFolder}/${swaggerName}", returnStdout: true

    def lstPath = strPath.split('/')
    String resource = lstPath[3]
    String version = lstPath[2]
    String apiUniqueId = resource + '-' + version

    steps.echo "strPath Path : ${strPath}"
    steps.echo "resource: ${resource}"
    steps.echo "version: ${version}"

    // bcp-api-type = PU|BS|UX|...
    String apiType = steps.sh script: "jq -rj '.\"x-bcp-api-type\"' ${script.env.WORKSPACE}${swaggerFolder}/${swaggerName}", returnStdout: true
    String apiDescription = steps.sh script: "jq -rj '.info.description' ${script.env.WORKSPACE}${swaggerFolder}/${swaggerName}", returnStdout: true
    String apiDisplayName = steps.sh script: "jq -rj '.info.title' ${script.env.WORKSPACE}${swaggerFolder}/${swaggerName}", returnStdout: true

    String idTag = productDisplayName.replaceAll(" ","-")

    String apiSwagger = steps.sh script: "jq -rj '.host = \"${script.env.azingress}\"' ${script.env.WORKSPACE}${swaggerFolder}/${swaggerName}", returnStdout: true
    apiSwagger=apiSwagger.trim()
    apiSwagger=apiSwagger.replaceAll("\\r\\n|\\r|\\n", "")
    apiSwagger=apiSwagger.replaceAll("\\t", " ")
    apiSwagger=apiSwagger.replaceAll("\"","\\\\\"")

    steps.echo "Archivo Swagger: ${apiSwagger}"

    String apiTypeName = "${apiType}".toLowerCase()

    String azureSpId = "${script.env.projectName}-az-${apiTypeName}-sp-app-id-${deploymentEnviroment}"
    String azureSpSecret = "${script.env.projectName}-az-${apiTypeName}-sp-secret-${deploymentEnviroment}"
    String azureSpTenant = "${script.env.projectName}-az-${apiTypeName}-sp-tenant-${deploymentEnviroment}"
    String azureResourceGroup = "${script.env.projectName}-az-${apiTypeName}-resource-group-${deploymentEnviroment}"
    String azureApimName = "${script.env.projectName}-az-${apiTypeName}-apim-name-${deploymentEnviroment}"

    steps.echo """
      NOTE: - The credentials should be populate on project folder credentials
        * Service Principal ID: ${azureSpId}
        * Service Principal Password: ${azureSpSecret}
        * Service Principal Tenant: ${azureSpTenant}
        * Apim Resource Group: ${azureResourceGroup}
        * Apim Instance Name: ${azureApimName}

      Azure Deployment INFO
        * Api Name: ${apiUniqueId}
        * Api Display Name: ${apiDisplayName}
        * Api Description: ${apiDescription}
    """

    String loginArgs  = "--service-principal"
    String dockerVolumes    = "-v ${script.env.WORKSPACE}:${script.env.WORKSPACE} -v ${script.env.WORKSPACE}@tmp:${script.env.WORKSPACE}@tmp"
    String dockerParameters = "-d -it --network=host"
    String debug = deploymentEnviroment == "dev" ? "--debug " : ""

    docker.withRegistry(script.env.DOCKER_REGISTRY_URL) {
      docker.withServer(script.env.DOCKER_HOST){
        steps.withCredentials([
          [$class: "StringBinding", credentialsId: "${azureSpId}", variable: "azureSpIdValue" ],
          [$class: "FileBinding", credentialsId: "${azureSpSecret}", variable: "azureSpSecretValue" ],
          [$class: "StringBinding", credentialsId: "${azureSpTenant}", variable: "azureSpTenantValue" ],
          [$class: "StringBinding", credentialsId: "${azureResourceGroup}", variable: "azureResourceGroupValue" ],
          [$class: "StringBinding", credentialsId: "${azureApimName}", variable: "azureApimNameValue" ]
        ]){

          String azCliDockerId = ""
          String policyXml = ""

          String deployArgs1 = "--parameters apimInstanceName=${script.env.azureApimNameValue} apiName=${apiUniqueId} apiDescription=\"${apiDescription}\" apiDisplayName=\"${apiDisplayName}\" apiPath=\"${apiUniqueId}\" apiSwagger=\"${apiSwagger}\" idTag=\"${idTag}\" productDisplayName=\"${productDisplayName}\""
          String deployArgs2 = ""
          String deployArgs3 = "--parameters apimInstanceName=${script.env.azureApimNameValue} productName=${productName} productDisplayName=\"${productDisplayName}\" productDescription=\"${productDescription}\""
          String deployArgs4 = "--parameters apimInstanceName=${script.env.azureApimNameValue} productName=${productName} apiName=${apiUniqueId}"

          String azStep1 = "az group deployment create ${debug}--resource-group ${script.env.azureResourceGroupValue} --template-file ${script.env.WORKSPACE}${deployFolder}/create_api_pu.json ${deployArgs1}"
          String azStep2 = ""
          String azStep3 = "az group deployment create ${debug}--resource-group ${script.env.azureResourceGroupValue} --template-file ${script.env.WORKSPACE}${deployFolder}/create_product.json ${deployArgs3}"
          String azStep4 = "az group deployment create ${debug}--resource-group ${script.env.azureResourceGroupValue} --template-file ${script.env.WORKSPACE}${deployFolder}/associate_api_product.json ${deployArgs4}"
              
          try{

            azCliDockerId = steps.sh(script:"docker run ${dockerParameters} ${dockerVolumes} ${dockerAzureClientEnviroment} bash",returnStdout:true).trim();

            if (apiType == "PU"){

              policyXml = steps.sh script: "cat ${script.env.WORKSPACE}${policyFolder}/policy_pu.xml", returnStdout: true
              policyXml=policyXml.trim()
              policyXml=policyXml.replaceAll("\\r\\n|\\r|\\n", "")
              policyXml=policyXml.replaceAll("\\t", " ")
              policyXml=policyXml.replaceAll("\"","\\\\\"")
              steps.echo "Archivo xml: ${policyXml}"

              deployArgs2 = "--parameters apimInstanceName=${script.env.azureApimNameValue} apiName=${apiUniqueId} apiPolicy=\"${policyXml}\" clientIdBackend=\"${clientIdBackend}\" appCode=\"${appCode}\""
              azStep2 = "az group deployment create ${debug}--resource-group ${script.env.azureResourceGroupValue} --template-file ${script.env.WORKSPACE}${deployFolder}/apply_policy_api_pu.json ${deployArgs2}"

              steps.sh "docker exec ${azCliDockerId} az login ${loginArgs} ${debug}--username ${script.env.azureSpIdValue} --password ${script.env.azureSpSecretValue} --tenant ${script.env.azureSpTenantValue}"
              steps.sh "docker exec ${azCliDockerId} ${azStep1}"
              steps.sh "docker exec ${azCliDockerId} ${azStep2}"
              steps.sh "docker exec ${azCliDockerId} ${azStep3}"
              steps.sh "docker exec ${azCliDockerId} ${azStep4}"
            }

          }catch(Exception e){
                throw e;
          }finally{
            if(azCliDockerId){
              steps.sh "docker rm -f ${azCliDockerId}"
            }

            steps.sh "rm -rf ${script.env.azureSpSecretValue}"
          }
        }
      }
    }
  }

  def executeXrayMavenSCA(String tagName='') {
    MavenJenkinsUtil mavenJenkinsUtil = this.getInstanceMavenJenkinsUtil()
    mavenJenkinsUtil.setXrayEnabled(this.xrayActivated)
    mavenJenkinsUtil.setXrayFailBuild(this.xrayFailBuild)
    mavenJenkinsUtil.executeXraySCA(tagName)
  }

  def executeXrayAndroidGradleSCA(String tagName='') {
    GradleAndroidJenkinsUtil gradleAndroidJenkinsUtil = this.getInstanceGradleAndroidJenkinsUtil()
    gradleAndroidJenkinsUtil.setXrayEnabled(this.xrayActivated)
    gradleAndroidJenkinsUtil.setXrayFailBuild(this.xrayFailBuild)
    gradleAndroidJenkinsUtil.executeXraySCA(tagName)
  }
}
