package sharedlib;

public interface DevopsUtil {

  public String getApplicationName()

  public String getApplicationVersion()

  public void build(final String buildParams)
 
  public void executeQA(final String releaseName, final String encoding)
  
  public void executeSast(final String buildParams)

  public void uploadArtifact(final String releaseName)

  public void saveResult(final String extension)

  public void startRelease(final String buildParams)

  public void promoteRelease(final String releaseTagName, final boolean forceRelease)  
}
