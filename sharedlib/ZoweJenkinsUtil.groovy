package sharedlib;

import org.jenkinsci.plugins.docker.workflow.*;
import groovy.json.JsonSlurper;
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*

class ZoweJenkinsUtil extends BaseUtil implements Serializable {
  private String dockerZoweEnvironment = "bcp/zowe-cli-bcp:1.0"
  
  private ZoweJenkinsUtil() {}
    
  public ZoweJenkinsUtil(script, String type = ''){
    super(script, type)

    this.script.steps.echo "[BCP-DevSecOps] script: ${script}"
    script.properties.each { println "$it.key -> $it.value" }
  }
  
  public String getApplicationName(){
    return this.getGitProjectName();
  }
  public String getApplicationVersion(){
    return this.script.steps.sh(script: "cat version", returnStdout: true).trim();
  }
  
  def entryValidation(description, transactionName, cicsName, secured){
      this.printMessage("""
        NOTE: - The parameters should be populate in jenkins file before invoke this method
          * transactionName: Nombre de la transacción
          * cicsName: nombre de cics
          * secured: para saber si la transacción esta protegida
      """)
      if (secured){
        return description.length() != 0 && transVal(transactionName) && cicsVal(cicsName) 
      }
        return transVal(transactionName) && cicsVal(cicsName) 
    }

    // Validación del transactionName en la function def validarDatos()
    def transVal(String transactionName) {
      if (transactionName.length() == 0 || transactionName.matches("C[0-9A-Z][0-9][0-9]")) {
      return false
      }
      return !(transactionName.length() != 4 || !( transactionName.matches("[A-Z][0-9A-Z][0-9][0-9]")))
    }

   // Validación del cicsName en la function def validarDatos()
    def cicsVal(String cicsName) {
      String[] cicsList
      cicsList = cicsName.split(',')
      def flag = cicsList[0].substring(0, 2)

      for( String cics : cicsList ) {
        if(cics.length() == 0 || (cics.length() != 4 || !( cics.matches("[A-Z][A-Z0-9][0-9][0-9]") ) || cics.substring(0, 2) != flag ) ) {
          return false
        }
      }
      return true
    }

    // RE-Install Transaction to CICS without RACF
    public void reInstallTransactionToCICS(String transactionName = '', String cicsRawList = '') {
      this.printMessage("""
        NOTE: - The parameters should be populate in jenkins file before invoke this method
          * transactionName: Nombre de la transacción
          * cicsRawList: Lista de cics.
      """)
      docker.withRegistry(script.env.DOCKER_REGISTRY_URL){
        docker.withServer(script.env.DOCKER_HOST){
          this.script.steps.withCredentials([
		  [
			$class: 'UsernamePasswordMultiBinding', credentialsId: 'OPBK_APP_USER_HOST', 
			usernameVariable: 'OPBK_APP_USER_USR', passwordVariable: 'OPBK_APP_USER_PSW'
			]
		  ]) {

            def dockerParameters = "--network=host --rm --cpus=2"
            def deploymentEnvironment = "${script.env.deploymentEnvironment}" 
            
            def hostCICS = ""
            def insCixPlex = ""
            def insPortCics = ""
            def csdGroup = ""
			if (deploymentEnvironment == "dev") {
              	csdGroup = "BCZWE@" + cicsRawList.substring(1, 2) + "5"   
               	hostCICS = "bc23.lima.bcp.com.pe"
            	insCixPlex= "CPLXA501"
            	insPortCics="32757"
			}else if (deploymentEnvironment == "cert"){
                csdGroup = "BCZWE@" + cicsRawList.substring(1, 2) + "7"  
				hostCICS = "bc13.lima.bcp.com.pe"
            	insCixPlex= "CPLXA701"
            	insPortCics="32756"
			}else if(deploymentEnvironment == "prod"){
                csdGroup = "BCZWE@" + cicsRawList.substring(1, 2) + "2"  
				hostCICS = "bc12.lima.bcp.com.pe"
            	insCixPlex= "CPLXA201"
            	insPortCics="32753"
			}

            // Create a CISC Transaction
              // Install a Transaction
              def cicsList = cicsRawList.split(",").toList()

              for (String cics : cicsList) {
                try{
                  def response = this.script.steps.sh (script: """ docker run ${dockerParameters} ${dockerZoweEnvironment} bash -c 'zowe cics install transaction ${transactionName} ${csdGroup} --cics-plex ${insCixPlex} --region-name "${cics}CICS" --host ${hostCICS} --port ${insPortCics} --user \"${script.env.OPBK_APP_USER_USR}\" --password \"${script.env.OPBK_APP_USER_PSW}\" --protocol https --rfj' > response.json""", returnStdout: true).trim()
                  def stdout = this.script.steps.sh(script: "cat response.json | jq .stdout",returnStdout:true).trim()
                  if (stdout.contains("successfully")) { 
					this.script.steps.echo "LA TRANSACCIÓN SE HA INSTALADO CORRECTAMENTE EN EL ${cics}." 
				  }
                }catch(Exception e){
                  def stderr = this.script.steps.sh(script: "cat response.json | jq .stderr",returnStdout:true).trim()
                      if (stderr.contains("NODATA")) { 
						this.script.steps.echo "TIPO DE ERROR: NODATA. ES PROBABLE QUE SU TRANSACCIÓN NO ESTE DEFINIDA." 
					  }else{
                          this.script.steps.echo "${stderr}"
                          this.script.steps.echo "OCURRIO UN ERROR INTERNO EN EL CICS ${cics}, POR FAVOR CONTACTAR AL EQUIPO DE MAINFRAME-IBM."
                      }
                  throw e  
                }
                sleep(5)
              }
        
          }
        }
      }
    }

    // Install Transaction to CICS
    public void deployTransactionToCICS(String transactionName = '', String cicsRawList = '') {
      this.printMessage("""
        NOTE: - The parameters should be populate in jenkins file before invoke this method
          * transactionName: Nombre de la transacción
          * cicsRawList: Lista de cics.
      """)
      docker.withRegistry(script.env.DOCKER_REGISTRY_URL) {
        docker.withServer(script.env.DOCKER_HOST){
		  this.script.steps.withCredentials([
		  [
			$class: 'UsernamePasswordMultiBinding', credentialsId: 'OPBK_APP_USER_HOST', 
			usernameVariable: 'OPBK_APP_USER_USR', passwordVariable: 'OPBK_APP_USER_PSW'
			]
		  ]) {
            def dockerParameters = "--network=host --rm --cpus=2"
            def deploymentEnvironment = "${script.env.deploymentEnvironment}" 
			def hostCICS = ""
            def defRegionName = ""
			def defPortCentralCics = ""
            def insCixPlex = ""
            def insPortCics = ""
            def csdGroup = ""
			if (deploymentEnvironment == "dev") {
              	csdGroup = "BCZWE@" + cicsRawList.substring(1, 2) + "5"   
               	hostCICS = "bc23.lima.bcp.com.pe"
                defRegionName= "A050CICS"
				defPortCentralCics = "30550"
            	insCixPlex= "CPLXA501"
            	insPortCics="32757"
			}else if (deploymentEnvironment == "cert"){
                csdGroup = "BCZWE@" + cicsRawList.substring(1, 2) + "7"  
				hostCICS = "bc13.lima.bcp.com.pe"
                defRegionName= "A070CICS"
				defPortCentralCics = "30770"
            	insCixPlex= "CPLXA701"
            	insPortCics="32756"
			}else if(deploymentEnvironment == "prod"){
                csdGroup = "BCZWE@" + cicsRawList.substring(1, 2) + "2"  
				hostCICS = "bc12.lima.bcp.com.pe"
                defRegionName= "A020CICS"
				defPortCentralCics = "30220"
            	insCixPlex= "CPLXA201"
            	insPortCics="32753"
			}

            // Create a CISC Transaction

              //DEFINIR LA TRANSACCIÓN
               try{
                  def response = this.script.steps.sh (script: """ docker run ${dockerParameters} ${dockerZoweEnvironment} bash -c 'zowe cics define transaction ${transactionName} DFHMIRS ${csdGroup} --region-name ${defRegionName} --host ${hostCICS} --port ${defPortCentralCics} --user \"${script.env.OPBK_APP_USER_USR}\" --password \"${script.env.OPBK_APP_USER_PSW}\" --protocol https --rfj' > response.json""", returnStdout: true).trim() 
                  def stdout = this.script.steps.sh(script: "cat response.json | jq .stdout",returnStdout:true).trim()
                      if (stdout.contains("successfully")) { this.script.steps.echo "LA TRANSACCIÓN SE HA DEFINIDO CORRECTAMENTE." }  
              }catch(Exception e){
                  def stderr = this.script.steps.sh(script: "cat response.json | jq .stderr",returnStdout:true).trim()
                      if (stderr.contains("DUPRES")) { this.script.steps.echo "TIPO DE ERROR: DUPRES. DUPLICIDAD CON LA TRANSACCIÓN, ESTA YA HA SIDO DEFINIDA."}
                      else if (stderr.contains("CSDERR")){ this.script.steps.echo "TIPO DE ERROR: CSDERR. UN USUARIO HA REALIZADO DEFINICIONES MANUALMENTE Y HA DEJADO ABIERTO SU ENTORNO DE TRABAJO EN EL CICS. POR FAVOR CONTACTAR AL EQUIPO DE MAINFRAME-IBM."}
                      else{
                          this.script.steps.echo "${stderr}"
                          this.script.steps.echo "OCURRIO UN ERROR INTERNO EN EL CICS, POR FAVOR CONTACTAR AL EQUIPO DE MAINFRAME-IBM."
                      }
                      throw e
                  }

              // Install a Transaction
              def cicsList = cicsRawList.split(",").toList()
              def cont=0//Contador para mapear las transacciones que no se instalen en determinados cics

              for (String cics : cicsList) {
                try{
                  def response = this.script.steps.sh (script: """ docker run ${dockerParameters} ${dockerZoweEnvironment} bash -c 'zowe cics install transaction ${transactionName} ${csdGroup} --cics-plex ${insCixPlex} --region-name "${cics}CICS" --host ${hostCICS} --port ${insPortCics} --user \"${script.env.OPBK_APP_USER_USR}\" --password \"${script.env.OPBK_APP_USER_PSW}\" --protocol https --rfj' > response.json""", returnStdout: true).trim()
                  def stdout = this.script.steps.sh(script: "cat response.json | jq .stdout",returnStdout:true).trim()
                  if (stdout.contains("successfully")) {
					this.script.steps.echo "LA TRANSACCIÓN SE HA INSTALADO CORRECTAMENTE EN EL ${cics}." 
				  }
                }catch(Exception e){
                  def stderr = this.script.steps.sh(script: "cat response.json | jq .stderr",returnStdout:true).trim()
                      if (stderr.contains("NODATA")) { 
						this.script.steps.echo "TIPO DE ERROR: NODATA. ES PROBABLE QUE SU TRANSACCIÓN NO ESTE DEFINIDA." 
					  }else{
                          this.script.steps.echo "${stderr}"
                          this.script.steps.echo "OCURRIO UN ERROR INTERNO EN EL CICS ${cics}, POR FAVOR CONTACTAR AL EQUIPO DE MAINFRAME-IBM."
                      }
                  cont++
                  throw e  
                }
                sleep(5)
              }
            if(cicsList.size() == cont){
                  this.script.steps.sh """docker run ${dockerParameters}  ${dockerZoweEnviroment} bash -c 'zowe cics delete transaction ${transactionName} ${csdGroup} --region-name ${defRegionName} --host ${hostCICS} --port ${defPortCentralCics} --user \"${script.env.OPBK_APP_USER_USR}\" --password \"${script.env.OPBK_APP_USER_PSW}\" --protocol https' """
            }

          }
        }
      }
    }

    // Permiss to RACF by Transaction
    public void installPermissionRACF(String transactionName='', String description='') {
      this.printMessage("""
        NOTE: - The parameters should be populate in jenkins file before invoke this method
          * transactionName: Nombre de la transacción
      """)
      def jenkinsBuild = "DevSecOps:jenkins"
      def dockerParameters = "-d -it --network host --cpus=2"
      def data = "data(\'${description}\')"
      def appldata = "appldata(\'${script.env.JOB_NAME} [${script.env.BUILD_NUMBER}]\')"
      def zoweDockerId = ""

      docker.withRegistry(script.env.DOCKER_REGISTRY_URL) {
        docker.withServer(script.env.DOCKER_HOST){
          this.script.steps.withCredentials([
            [
              $class: 'UsernamePasswordMultiBinding', 
              credentialsId: 'OPBK_APP_USER_HOST',
              usernameVariable: 'OPBK_APP_USER_USR', 
              passwordVariable: 'OPBK_APP_USER_PSW'
            ]
          ]) {
            def deploymentEnvironment = "${script.env.deploymentEnvironment}"
			def idRACF = ""
			def hostRACF = ""
			def ownerRACF = "BCRCICSC"
			def transactionNameStr = "BCCICSC.${transactionName}"
			def portRACF = "10443"
			if (deploymentEnvironment == "dev") {
				idRACF = "SPMBDES"
				hostRACF = "bc23.lima.bcp.com.pe"
			}else if (deploymentEnvironment == "cert"){
				idRACF = "SPMBCER"
				hostRACF = "bc13.lima.bcp.com.pe"
			}else if(deploymentEnvironment == "prod"){
				idRACF = "SPMBPRO"
				hostRACF = "bc12.lima.bcp.com.pe"
				ownerRACF = "RCICS"
				transactionNameStr = "${transactionName}"
			}
			try{           
                zoweDockerId = this.script.steps.sh(script: "docker run ${dockerParameters} ${dockerZoweEnvironment} bash",returnStdout: true).trim()
                this.script.steps.sh "docker exec ${zoweDockerId} zowe zos-tso issue command \"rdefine TCICSTRN ${transactionNameStr} owner(${ownerRACF}) uacc(NONE) ${data} ${appldata} audit(all(READ))\" --host ${hostRACF} --port ${portRACF} --user ${script.env.OPBK_APP_USER_USR} --password ${script.env.OPBK_APP_USER_PSW} --account IZUACCT --ssm true"
                this.script.steps.sh "docker exec ${zoweDockerId} zowe tso issue command \"permit BCCICSC.${transactionName} class(TCICSTRN) id(${idRACF}) access(READ)\" --host ${hostRACF} --port ${portRACF} --user ${script.env.OPBK_APP_USER_USR} --password ${script.env.OPBK_APP_USER_PSW} --account IZUACCT --ssm true"
                this.script.steps.sh "docker exec ${zoweDockerId} zowe tso issue command \"setropts refresh generic(TCICSTRN)\" --host ${hostRACF} --port ${portRACF} --user ${script.env.OPBK_APP_USER_USR} --password ${script.env.OPBK_APP_USER_PSW} --account IZUACCT --ssm true"
                this.script.steps.sh "docker exec ${zoweDockerId} zowe tso issue command \"setropts refresh raclist(TCICSTRN)\" --host ${hostRACF} --port ${portRACF} --user ${script.env.OPBK_APP_USER_USR} --password ${script.env.OPBK_APP_USER_PSW} --account IZUACCT --ssm true"

            }catch(Exception e) {                      
               	throw e;
            }finally {
               	if(zoweDockerId){
					this.script.steps.sh "docker rm -f ${zoweDockerId}"
				}
            }
			
          }
        }
      }
    }


    public void getInfoTransaction(String transactionName='', String cicsRawList='') {
      this.printMessage(""")
        NOTE: - The parameters should be populate in jenkins file before invoke this method
          * transactionName: Nombre de la transacción
          * cicsRawList: Lista de cics.
      """)
      docker.withRegistry(script.env.DOCKER_REGISTRY_URL) {
        docker.withServer(script.env.DOCKER_HOST){
          this.script.steps.withCredentials([
			[
				$class: 'UsernamePasswordMultiBinding', credentialsId: 'OPBK_APP_USER_HOST', 
				usernameVariable: 'OPBK_APP_USER_USR', passwordVariable: 'OPBK_APP_USER_PSW'
			]
		  ]){	
              def dockerParameters = "--network=host --rm --cpus=2"
              def deploymentEnvironment = "${script.env.deploymentEnvironment}" 
              def cicsList = cicsRawList.split(",").toList()
            
              def hostCICS = ""
              def insCixPlex = ""
              def insPortCics = ""
              def portRACF = "10443"
              if (deploymentEnvironment == "dev") {
                  hostCICS = "bc23.lima.bcp.com.pe"
                  insCixPlex= "CPLXA501"
                  insPortCics="32757"
              }else if (deploymentEnvironment == "cert"){
                  hostCICS = "bc13.lima.bcp.com.pe"
                  insCixPlex= "CPLXA701"
                  insPortCics="32756"
              }else if(deploymentEnvironment == "prod"){
                  hostCICS = "bc12.lima.bcp.com.pe"
                  insCixPlex= "CPLXA201"
                  insPortCics="32753"
              }

         		//Consultar Transacción
                for (String cicsName : cicsList){
                  try{
                    def response =	this.script.steps.sh (script: """ docker run ${dockerParameters} ${dockerZoweEnvironment} bash -c 'zowe cics get resource CICSLocalTransaction --cics-plex ${insCixPlex} --region-name "${cicsName}CICS" --criteria \"TRANID=${transactionName}\" --host ${hostCICS} --port ${insPortCics} --user ${script.env.OPBK_APP_USER_USR} --password ${script.env.OPBK_APP_USER_PSW} --protocol https --rfj' > response.json""", returnStdout: true).trim()
                    def existe = this.script.steps.sh(script: "cat response.json | jq .success",returnStdout:true).trim()
                    this.script.steps.sh "${existe}"
                      if (existe) {
                          def tranid = this.script.steps.sh(script: "cat response.json | jq .data.response.records.cicslocaltransaction.tranid",returnStdout:true).trim()
                          this.script.steps.echo "TRANSACCIÓN ENCONTRADA: ${tranid}. SI ESTA ASEGURADA, A CONTINUACIÓN SE LISTAN LOS DETALLES DE SU DEFINICIÓN EN EL RACF. "
                      }
                  }catch(Exception e){
                    def stderr = this.script.steps.sh(script: "cat response.json | jq .stderr",returnStdout:true).trim()
                      if (stderr.contains("NODATA")) { 
						this.script.steps.echo "TIPO DE ERROR: NODATA. LA TRANSACCIÓN NO ESTA INSTALADA." 
					  }else{
                          this.script.steps.echo "${stderr}"
                          this.script.steps.echo "OCURRIÓ UN ERROR INTERNO EN EL CICS, POR FAVOR CONTACTAR AL EQUIPO DE MAINFRAME-IBM."
                      }
                      throw e
                  }
                  sleep(5)
                }
                this.script.steps.sh """ docker run ${dockerParameters} ${dockerZoweEnvironment} bash -c 'zowe tso issue command \"rlist tcicstrn bccicsc.${transactionName}\" --host ${hostCICS} --port ${portRACF} --user ${script.env.OPBK_APP_USER_USR} --password ${script.env.OPBK_APP_USER_PSW} --account IZUACCT --ssm true'"""             
          }
       }
    }
  } 

}
