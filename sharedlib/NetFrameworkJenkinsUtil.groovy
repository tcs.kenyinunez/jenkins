package sharedlib;

import org.jenkinsci.plugins.docker.workflow.*;
import groovy.json.JsonSlurper;
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*

class NetFrameworkJenkinsUtil extends BaseUtil implements Serializable, DevopsUtil {

  private final String FIRMA_MENSAJES             = "[BCP-DevSecOps]"
  private final String DEFAULT_TESTFILE_PATTERN   = '*/bin/Release/*.Tests*.dll'

  private final static String DOCKER_MAVEN_JAVA_ENVIRONMENT = "bcp/maven339-java180_172:1.0"
  private String packageType = 'zip'
  private docker
  private currentCredentialsId
  private artefactoryUrl

  def buildProperties = [
    WINDOWS_NODE          : "",
    ARTIFACT_WILDCARD     : "",
    NUGET_PATH            : "C:\\jenkins\\tools\\nuget\\",
    MSBUILD_PATH          : "C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\BuildTools\\MSBuild\\15.0\\Bin",
    VISUALSTUDIO_VERSION  : "15.0",
    MSBUILD_SONAR_PATH    : "C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\BuildTools\\MSBuild\\15.0\\Bin",
    SONARSCANNER_PATH     : "C:\\jenkins\\tools\\hudson.plugins.sonar.MsBuildSQRunnerInstallation\\SONAR_SCANNER_4.2.0.1214\\",
    NUNIT_CONSOLE_PATH    : "C:\\jenkins\\tools\\NUnit.ConsoleRunner.3.6.1\\tools\\",
    OPENCOVER_PATH        : "C:\\jenkins\\tools\\OpenCover.4.6.519\\tools\\",
    REPORTGENERATOR_PATH  : "C:\\jenkins\\tools\\ReportGenerator.2.5.6\\tools\\",
    NUNITORANGE_PATH      : "C:\\jenkins\\tools\\NOrange\\"
  ]

  def msBuildVersions = [
    MSBUILD_15         : "C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\BuildTools\\MSBuild\\15.0\\Bin",
    MSBUILD_14         : "C:\\Program Files (x86)\\MSBuild\\14.0\\bin",
    MSBUILD_4_7        : "C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319",
    MSBUILD_3_5        : "C:\\Windows\\Microsoft.NET\\Framework64\\v3.5",
    MSBUILD_2_0        : "C:\\Windows\\Microsoft.NET\\Framework64\\v2.0.50727"
  ]

  private NetFrameworkJenkinsUtil(){}

  NetFrameworkJenkinsUtil(script,String type = ''){
    super(script, type)

    def remoteConfigs = this.script.scm.getUserRemoteConfigs()
    for (Object remoteConfig : remoteConfigs) {
      this.currentCredentialsId = remoteConfig.getCredentialsId();
      this.script.steps.echo "[BCP-DevSecOps] CurrentCredentialsId para ${remoteConfig} --> ${currentCredentialsId}";
    }
    script.steps.echo "script: ${script}"
    this.docker = new Docker(this.script);
    this.artefactoryUrl = this.script.env.ARTIFACT_SERVER_URL;

    script.properties.each { println "$it.key -> $it.value" }
  }

  public void setMsBuildVersion(String msBuildVersion="MSBUILD_15"){
    if(!msBuildVersions.containsKey(msBuildVersion)){
      throwException("${msBuildVersion} not supported")
    }
    
    buildProperties.MSBUILD_PATH = msBuildVersions["${msBuildVersion}"]

    script.steps.echo """ ${FIRMA_MENSAJES}
      TOOLS_SET             : ${msBuildVersion}
      NUGET_PATH            : ${buildProperties.NUGET_PATH} 
      MSBUILD_PATH          : ${buildProperties.MSBUILD_PATH} 
      VISUALSTUDIO_VERSION  : ${buildProperties.VISUALSTUDIO_VERSION}
      MSBUILD_SONAR_PATH    : ${buildProperties.MSBUILD_SONAR_PATH} 
      SONARSCANNER_PATH     : ${buildProperties.SONARSCANNER_PATH} 
      NUNIT_CONSOLE_PATH    : ${buildProperties.NUNIT_CONSOLE_PATH} 
      OPENCOVER_PATH        : ${buildProperties.OPENCOVER_PATH} 
      REPORTGENERATOR_PATH  : ${buildProperties.REPORTGENERATOR_PATH} 
      NUNITORANGE_PATH      : ${buildProperties.NUNITORANGE_PATH} 
    """
  }

  private static boolean isNullOrEmpty(String... strArr) {
    for (String st : strArr) {
      if  (st==null || st.equals(""))
        return true;
    } 
    return false;
  }

  public void setWindowsNode(String windowsNode){
    
    if(isNullOrEmpty(windowsNode)){
      throwException("Windows Node is empty, please specify a valid string")
    }
    buildProperties.WINDOWS_NODE = windowsNode

    printMessage("Windows Node is set to: ${windowsNode}")
  }

  public void setArtifactWildcard(String artifactWildcard){
    if(isNullOrEmpty(artifactWildcard)){
      throwException("Artifact Wildcard is empty, please specify a valid string")
    }

    buildProperties.ARTIFACT_WILDCARD = artifactWildcard
    printMessage("Artifact Wildcard is set to: ${artifactWildcard}")
  }

  private String getSolutionFile(){
    def solutionFile = script.steps.sh(script:"find . -name '*.sln' | cut -d \"/\" -f2", returnStdout: true).trim()

    if (!isNullOrEmpty(solutionFile)){
      printMessage("Solution file : ${solutionFile}")
      return solutionFile
    }
    throwException("No se encontró el archivo de la solución en la raíz del proyecto")
  }
  
  public String getApplicationVersion(){
    def applicationVersion = script.steps.sh(script:"awk -F\"[<>]\" '/Version/ {print \$3;exit}' ${script.env.WORKSPACE}/Directory.Build.props", returnStdout: true).trim()
    if (!isNullOrEmpty(applicationVersion)){
      printMessage("Application Version : ${applicationVersion}")
      return applicationVersion
    }
    throwException("Set <Version> TAG in Directory.Build.props")
  }

  public String getApplicationName(){
    def applicationName = getGitProjectName()
    printMessage("Application Name : ${applicationName}")
    return applicationName
  }

  private void executeMsBuildCmd(String... buildParams) {
    def toolsSet = buildProperties

    for (String st : buildParams) {
      script.steps.withEnv(["path+=${toolsSet.MSBUILD_PATH}"]){
        script.steps.bat([script:"MSBuild.exe ${st}"])
        //script.steps.bat([script:"MSBuild.exe ${st} /p:VisualStudioVersion=${toolsSet.VISUALSTUDIO_VERSION}"])
      }
    }
  }

  public void build(final String  buildParams= ''){

    script.steps.stash includes: '**/*', name: 'workspace'
    
    def windowsNode        = this.buildProperties.WINDOWS_NODE
    def solutionFile       = this.getSolutionFile();
    def testFilesPattern   = this.DEFAULT_TESTFILE_PATTERN
    def artifactsPattern   = this.buildProperties.ARTIFACT_WILDCARD
    def projectName        = "${script.env.project}"
    def applicationName    = this.getApplicationName()
    def applicationVersion = this.getApplicationVersion()
    def buildTimestamp     = buildTimestamp

    def toolsSet = buildProperties

    script.steps.node(windowsNode){
      script.steps.step([$class: 'WsCleanup'])
      script.steps.unstash 'workspace'

      //def script = "${solutionFile} /t:Clean;Build /p:Configuration=Release /p:Platform=\"Any CPU\" ${buildParams}"
      def defaultScript = "${solutionFile} /t:Clean;Build /p:Configuration=Release /p:Platform=\"Any CPU\""

      script.steps.withEnv(["PATH+nuget=${toolsSet.NUGET_PATH}"
                    ,"PATH+nunit=${toolsSet.NUNIT_CONSOLE_PATH}"
                    ,"PATH+nunitorange=${toolsSet.NUNITORANGE_PATH}"
                    ,"PATH+opencover=${toolsSet.OPENCOVER_PATH}"]){

        script.steps.bat(script:"nuget.exe restore ${solutionFile}")

        executeMsBuildCmd(defaultScript)

        if(!isNullOrEmpty(buildParams)){
          executeMsBuildCmd(buildParams.split(","))
        }

        try{
          def testFiles = this.getFilesPathByPattern(testFilesPattern)
          script.steps.bat([script:"nunit3-console.exe ${testFiles}"])
          script.steps.bat([script:"NUnitOrange.exe TestResult.xml TestResult.html"])
          script.steps.bat([script:"md .build"])
          script.steps.bat([script:"OpenCover.Console.exe -register:Path64 -target:nunit3-console.exe -targetargs:${testFiles} -filter:\"+[*]* -[*.Tests*]*\" -output:.build\\${projectName}_Tests.coverage.xml"])
        }catch(Exception e){
          printMessage("Error en pruebas unitarias")
        }
      }
      script.steps.stash includes: "${artifactsPattern}", name: 'artifacts'
    }

    script.steps.dir('target'){
      script.steps.unstash 'artifacts'
      script.steps.sh "find . -path ./devops -prune -o -type f -name 'Web.config' -exec rm {} +"
    }

    zipArtifacts()
  }

  private void zipArtifacts(){
    printMessage("Building zip artifact")
    def comment = "ic: JENKINS build-url: ${script.env.BUILD_URL} branch: ${branchName} commit: ${currentGitCommit} date: ${buildTimestamp}."
    script.steps.sh "echo ${FIRMA_MENSAJES} ${comment} > comments-jks-${buildTimestamp}"
    script.steps.sh "zip -z -x comments-jks-${buildTimestamp} -r ${script.env.WORKSPACE}/${applicationName}-${applicationVersion}.zip target < comments-jks-${buildTimestamp}"
    script.steps.sh "ls -al ${applicationName}-${applicationVersion}.zip"

    printMessage("Printing Zip File")
    script.steps.sh "ls -lrtA ${script.env.WORKSPACE}"

    saveResult("zip")
  }

  private String getFilesPathByPattern(String pattern){
    
    def paths = []
        paths = script.steps.powershell (script: "ls -r ${pattern} | % FullName | sort-object -Unique", returnStdout: true).split("\r?\n")
    if(paths.size()>0){
      def result = ""
      for (def i = 0; i <paths.size(); i++) {
        result += "\"${paths[i]}\" "
      }
      return result;
    }
    //throw new Exception("Test files not found with pattern: ${pattern}")
  }

  public void executeQA(final String releaseName = "", final String encoding = "UTF-8"){
    this.executeSonarNet(releaseName,encoding)
  }

  private void executeSonarNet(String releaseName = "", String encoding = "UTF-8") {
    def applicationName        = this.getApplicationName()
    def applicationVersion     = this.getApplicationVersion()
    def projectName            = "${this.script.env.project}".toLowerCase()
    def windowsNode            = this.buildProperties.WINDOWS_NODE
    def solutionFile           = this.getSolutionFile()
    def buildTimestamp         = buildTimestamp
    def toolsSet               = buildProperties

    printMessage("executeSonarWithSonarRunner inicio")

    script.steps.node(windowsNode){
      def groupIdName         = "com.bcp.${projectName}"
      def projectKey          = "${projectName}-${groupIdName}:${applicationName}"
      def sonarProjectVersion = "${applicationVersion}-${branchName}"

      if(releaseName!=''){
        sonarProjectVersion = "${applicationVersion}-${releaseName}"
      }

      def coverageParameter = "/d:sonar.cs.opencover.reportsPaths=.\\.build\\${projectName}_Tests.coverage.xml"

      def traceabilityParams=" /d:sonar.analysis.version=\"${applicationVersion}\" \
      /d:sonar.analysis.userId=\"${buildUserId}\" /d:sonar.analysis.commitId=\"${currentGitCommit}\" /d:sonar.analysis.tek=\"net-framework\" \
      /d:sonar.analysis.projectName=\"${projectName}\" /d:sonar.analysis.buildTimestamp=\"${buildTimestamp}\" /d:sonar.analysis.branch=\"${branchName}\" \
      /d:sonar.links.scm=\"${currentGitURL}\" /d:sonar.analysis.jobname=\"${script.env.JOB_NAME}\" "

      script.steps.withSonarQubeEnv{
        script.steps.withEnv(["PATH+sonarscanner=${toolsSet.SONARSCANNER_PATH}","PATH+msbuild=${toolsSet.MSBUILD_SONAR_PATH}"]){
          script.steps.bat "SonarQube.Scanner.MSBuild.exe begin /k:${projectKey} /n:${projectKey} /v:${applicationVersion} /d:sonar.verbose=true /d:sonar.sourceEncoding=${encoding} /d:sonar.host.url=${script.env.SONAR_HOST_URL} /d:sonar.login=${script.env.SONAR_AUTH_TOKEN} ${traceabilityParams} ${coverageParameter}"
          script.steps.bat "MSBuild.exe ${solutionFile} /t:Rebuild"
          script.steps.bat "SonarQube.Scanner.MSBuild.exe /d:sonar.login=${script.env.SONAR_AUTH_TOKEN} end "
        }
      }
    }

    printMessage("executeSonarWithSonarRunner Fin")
  }
  
  // fortify
  public void executeSast(final String buildParams){
    this.executeFortifyNet(buildParams)
  }

  public void uploadArtifact(final String releaseName = "", final String buildParams = ""){
    def buildTimestamp = buildTimestamp
    def buildUniqueId  = "${buildTimestamp}"
    def projectName    = "${script.env.project}"
    def environment    = "${script.env.deploymentEnvironment}".toUpperCase()
    def repositoryId   = ""

    printMessage("Environment: ${environment}")

    if(releaseName!=null && releaseName!=""){
      script.steps.step([$class: 'WsCleanup'])
      buildUniqueId = script.steps.sh(script: "echo ${releaseName} | cut -d '/' -f2 ", returnStdout: true).trim()
      checkoutBranch(releaseName)
      build(buildParams)
    }

    repositoryId = "${script.env.project}.Generic.Snapshot"
    
    if ("${branchName}" == "master"){
      repositoryId = "${script.env.project}.Generic.Release"
    }

    if(environment == "CERT"){
      repositoryId = "${script.env.project}.Generic-${environment}"
    } else if(environment == 'PROD' && "${branchName}".startsWith("tags/RC-")){
      repositoryId  = "${script.env.project}.Generic.Release"
      buildUniqueId = "${buildUniqueId}-PROMOTED"
    }

    printMessage("Deploy to repository: ${repositoryId}")
    buildInfo(buildUniqueId,repositoryId)
  }

  private void buildInfo(String buildUniqueId = "", String repositoryId){
    def projectName          = "${script.env.project}"
    def projectNameLowercase = projectName.toLowerCase()
    def applicationName      = getApplicationName()
    def applicationVersion   = getApplicationVersion()
    def buildTimestamp       = buildTimestamp
    def projectKeyName       = "${projectNameLowercase}-${applicationName}"
    def deployPackage        = "${applicationName}-${applicationVersion}.zip"
    def repositoryUrl        = "${script.env.ARTIFACT_SERVER_URL}/${repositoryId}/${projectNameLowercase}/${applicationName}/${applicationVersion}/"
    def buildUrl             = "${script.env.ARTIFACT_SERVER_URL}/api/build"

    printMessage("ProjectName: ${projectNameLowercase}")

    script.steps.withCredentials([[$class: "UsernamePasswordMultiBinding", credentialsId: "${projectNameLowercase}-jenkins-artefactory-token",usernameVariable: "ARTIFACT_USER_NAME", passwordVariable: "ARTIFACT_USER_PASSWORD"]]) {
      def artifactSha1 = script.steps.sh(script: "sha1sum ${deployPackage} | cut -d ' ' -f 1", returnStdout: true).trim()
      def artifactMd5  = script.steps.sh(script: "md5sum ${deployPackage} | cut -d ' ' -f 1", returnStdout: true).trim()
      printMessage("artifactSha1: ${artifactSha1}")
      printMessage("artifactMd5: ${artifactMd5}")

      def result = script.steps.sh (script: """
      curl -o response-build-info-${buildTimestamp}.txt -X PUT --header \"X-Checksum-MD5:${artifactMd5}\" --header \"X-Checksum-Sha1:${artifactSha1}\" -u ${script.env.ARTIFACT_USER_NAME}:${script.env.ARTIFACT_USER_PASSWORD} -T ${deployPackage} "${repositoryUrl}${deployPackage};build.name=${projectKeyName};build.number=${buildUniqueId}"
      """, returnStdout: true)
          
      def callback = script.steps.sh(script: "cat response-build-info-${buildTimestamp}.txt", returnStdout: true).trim()
      printMessage("Callback: ${callback}")

      def errorMessageArtifactory = script.steps.sh (script: "cat response-build-info-${buildTimestamp}.txt | jq --raw-output '.errors | .[0] | .message'", returnStdout: true).trim()
      
      //Artefact has promoted or moved
      if(errorMessageArtifactory!="null"){
        throwException("Error deploying the artifact to the artifact server")
      }
      
      def timestamp                = script.steps.sh(script: "echo \$(date +\"%Y-%m-%dT%H:%M:%S.%3N%z\")", returnStdout: true).trim()
      def artifactoryBuildInfoJson = getArtifactoryBuildInfo("${script.env.ARTIFACT_USER_NAME}",projectNameLowercase,applicationName,applicationVersion,buildUniqueId,timestamp,"zip",artifactSha1,artifactMd5)

      printMessage("artifactoryBuildInfoJson: ${artifactoryBuildInfoJson}")
      script.steps.writeFile(
        file: "${script.env.WORKSPACE}/artifactory-json-${buildTimestamp}.json",
        text: artifactoryBuildInfoJson
      )
      
      result = script.steps.sh (script: """
      curl -X PUT -I -i -u ${script.env.ARTIFACT_USER_NAME}:${script.env.ARTIFACT_USER_PASSWORD} -H \"Content-Type: application/json\" -T artifactory-json-${buildTimestamp}.json "${buildUrl}"
      """, returnStdout: true)
      printMessage("Result: ${result}")
    }
  }

  public void saveResult(final String extension){
    script.steps.archiveArtifacts(artifacts:'*.'+extension,allowEmptyArchive:true)
  }

  public void startRelease(final String buildParams = "",final String fortifyParams = ""){
    if ("${branchName}"=="master"){
      throwException("Generating of release must not be done on master")
    }

    def applicationVersion      = this.getApplicationVersion()
    def nextReleaseVersion      = "${applicationVersion}"
    def releaseBranchName       = "release/"+nextReleaseVersion
    def existBranchInRepository = existBranch(releaseBranchName)
    def tagStateComment         = "Release Candicate"
    def currentDate             = buildTimestamp
    def environmentParam        = "CERT"
    def releaseTagName          = "RC-${applicationVersion}-${environmentParam}-${currentDate}"

    printMessage("Application version: ${applicationVersion}")
    printMessage("Next release version: ${nextReleaseVersion}")
    printMessage("Exist release branch: ${existBranchInRepository}")

    if(existBranchInRepository){
      printMessage("Deleting release branch: ${releaseBranchName}")
      deleteNotMergeReleaseBranch(releaseBranchName)
    }

    createNewReleaseBranch(nextReleaseVersion)
    checkoutReleaseBranch(nextReleaseVersion)
   
    build(buildParams)
    
    if(fortifyActivated){
      executeSast(fortifyParams)
    }
    
    try{
      executeQA(releaseTagName)
    }catch(Exception e){
      printMessage("WARNING: Error executing SonarQube!")
    }

    protectBranch("release/${nextReleaseVersion}")
    tagBranch(releaseTagName,tagStateComment,environmentParam)
    uploadArtifact(releaseTagName,buildParams)
    executeXraySCA(releaseTagName)
  }

  public void promoteRelease(final String releaseTagName, final boolean forceRelease){
    if(!"${branchName}".startsWith("tags/RC-")){
      this.throwException("The release must be done on release")
    }

    def applicationVersion = this.getApplicationVersion()
    def existTag = existTag("${applicationVersion}")
    def tagStateComment = "Release"
    def tagName = this.getApplicationVersion()
    def applicationName = this.getApplicationName()

    if(existTag){
      if (!forceRelease){
        this.throwException("The tag for this release exists: ${applicationVersion}")
      }
      this.printMessage("Forcing removal and promote of release tag name: ${releaseTagName}")
      this.deleteTagBranch(tagName)
      this.printMessage("Tag removed: ${releaseTagName}")
      this.printMessage("${tagStateComment}-FORCED")
    }
    this.printMessage("Promote release tag name: ${releaseTagName}")
    promoteRepository(applicationName,releaseTagName,".Generic")
    mergeOverwriteBranch(releaseTagName,"master")
    tagBranch(tagName,tagStateComment)
  }

  public void deployApplicationToIIS(def deployProperties = [:]){
    def applicationVersion    = this.getApplicationVersion()
    def applicationName       = this.getApplicationName()
    def windowsNode           = this.buildProperties.WINDOWS_NODE
    def projectName           = "${script.env.project}"
    def projectNameLowercase  = projectName.toLowerCase()
    def environment           = "${script.env.deploymentEnvironment}".toUpperCase()
    def repositoryId          = "${script.env.project}.Generic.Snapshot"
    def deploymentEnvironment = "${script.env.deploymentEnvironment}"
    def buildTimestamp        = buildTimestamp
    def deployCredentialsId   = "${projectNameLowercase}-${deployProperties.SERVER.toLowerCase()}-deploy-user-credentials"

    if ("${branchName}" == "master"){
      repositoryId = "${script.env.project}.Generic.Release"
    }

    if(environment == "CERT"){
      repositoryId = "${script.env.project}.Generic-${environment}"
    } else if(environment == 'PROD' && "${branchName}".startsWith("tags/RC-")){
      repositoryId = "${script.env.project}.Generic.Release"
    }

    script.steps.sh(script: "rm -rf release", returnStdout: true).trim()
    script.steps.sh(script: "mkdir release", returnStdout: true).trim()
    script.steps.dir('release'){
      script.steps.sh(script:"wget https://artifactory.lima.bcp.com.pe/artifactory/${repositoryId}/${projectNameLowercase}/${applicationName}/${applicationVersion}/${applicationName}-${applicationVersion}.zip")
      script.steps.sh(script:"unzip ${applicationName}-${applicationVersion}.zip")
      if(deployProperties.CONFIG_FILES_REQUIRED){
        script.steps.sh(script:"cp -fr ./target/devops/config/${deploymentEnvironment}/${deployProperties.CONFIG_FOLDER}/. ${script.env.WORKSPACE}/release/target/${deployProperties.ARTIFACT_FOLDER}/")
      }
      script.steps.stash includes: "target/${deployProperties.ARTIFACT_FOLDER}/**", name: "DeployPackage"
    }

    script.steps.node(windowsNode){
      script.steps.step([$class: 'WsCleanup'])
      script.steps.unstash "DeployPackage"
      script.steps.withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId:deployCredentialsId, passwordVariable: 'PASS', usernameVariable: 'USERNAME']]) {
        printMessage("Stop Site")
        script.steps.powershell([script: "\$server = New-PSSession -Name AppServer -ComputerName ${deployProperties.SERVER} -Credential (New-object System.Management.Automation.PSCredential -ArgumentList @('${script.env.USERNAME}',('${script.env.PASS}'|ConvertTo-secureString -AsPlainText -Force)));Invoke-Command -Session \$server -ScriptBlock {Stop-WebSite -Name '${deployProperties.SITE_NAME}';}"])
        printMessage("Copy Files")
        script.steps.powershell([script: "\$server = New-PSSession -Name server -ComputerName ${deployProperties.SERVER} -Credential (New-object PSCredential -ArgumentList @('${script.env.USERNAME}', ('${script.env.PASS}'|ConvertTo-secureString -AsPlainText -Force)));Copy-Item –Path ${script.env.WORKSPACE}\\target\\${deployProperties.ARTIFACT_FOLDER}\\** –Destination '${deployProperties.SITE_PATH}' -Recurse -force –ToSession \$server;"])
        printMessage("Starting Site")
        script.steps.powershell([script: "\$server = New-PSSession -Name AppServer -ComputerName ${deployProperties.SERVER} -Credential (New-object System.Management.Automation.PSCredential -ArgumentList @('${script.env.USERNAME}',('${script.env.PASS}'|ConvertTo-secureString -AsPlainText -Force)));Invoke-Command -Session \$server -ScriptBlock {Start-WebSite -Name '${deployProperties.SITE_NAME}';}"])
      }
    }
  }
  private void executeFortifyNet(String buildParameters = '', String encoding = "UTF-8") {
    if(fortifyActivated){
      def fortifyVersion = getGitProjectName()
      def solutionFile = this.getSolutionFile()
      def projectLowerCase="${script.env.project}".toLowerCase()
      def fortifyCloudScanCredId = "${projectLowerCase}-jenkins-fortify-cloudscan-token"
      def fortifyAuthCredId = "${projectLowerCase}-jenkins-fortify-ssc-token"
      def buildID = "${script.env.project}-${fortifyVersion}-${script.env.BUILD_NUMBER}"
      def windowsNode = this.buildProperties.WINDOWS_NODE
      def toolsSet = buildProperties

      script.steps.echo "${FIRMA_MENSAJES} executeFortifyRunner inicio ${buildID}"

      script.steps.node(windowsNode){
                script.steps.withCredentials([[$class: 'StringBinding', credentialsId: "${fortifyCloudScanCredId}", variable: 'cloudScanToken' ],
                    [$class: 'StringBinding', credentialsId: "${fortifyAuthCredId}", variable: 'authToken' ]]){

        script.steps.withEnv(["PATH+msbuild=${toolsSet.MSBUILD_PATH}","PATH+nuget=${toolsSet.NUGET_PATH}"]){ 
            script.steps.bat "sourceanalyzer -b \"${buildID}\" -clean"
            script.steps.bat(script:"nuget.exe restore ${solutionFile}")
            script.steps.bat "sourceanalyzer -b \"${buildID}\" msbuild \"${solutionFile}\" /t:rebuild /p:Configuration=Release"
            script.steps.bat "sourceanalyzer -b \"${buildID}\" -scan -f \"result.fpr\" \"${execFortifyMemory}\""
            script.steps.bat "fortifyclient -url ${fortifyUrl} -authtoken ${script.env.authtoken} listApplicationVersions > .listProject.txt"
            script.steps.bat "cloudscan -sscurl \"${fortifyUrl}\" -ssctoken \"${script.env.cloudScanToken}\" start -block -log NetFramework.log -upload --application \"${script.env.project}\" --application-version \"${fortifyVersion}\" -b \"${buildID}\" -uptoken \"${script.env.cloudScanToken}\" -scan \"${execFortifyMemory}\""
            script.steps.bat "fortifyclient downloadFPR -file result.fpr -url \"${fortifyUrl}\" -authtoken \"${script.env.authtoken}\" -project \"${script.env.project}\" -version \"${fortifyVersion}\""
            script.steps.bat "BIRTReportGenerator -template \"OWASP Top 10\" -source \"result.fpr\" -format HTML --Version \"OWASP Top 10 2017\" -output MyOWASP_Top10_Report.html"
          	script.steps.stash includes: ".listProject.txt,MyOWASP_Top10_Report.html", name: 'fileStashed'
          }
        }
      }
      script.steps.unstash 'fileStashed'
      
      generateReportFortify()
      
      script.steps.echo "${FIRMA_MENSAJES} executeFortifyRunner Fin ${buildID}"
    }else{
      script.steps.echo "********************* FORTIFY IS DISABLED!!!!! *********************"
    }
  }

  private String getArtifactoryBuildInfo(
          String user,
          String projectKeyName,
          String applicationName,
          String applicationVersion,
          String buildtimestamp,
          String startedTimeStamp,
          String artifactType,
          String artifactSha1,
          String artifactMd5
  ){
    def result = """{
  "version" : "1.0",
  "name" : "${projectKeyName}-${applicationName}",
  "number" : "${buildtimestamp}",
  "type" : "GENERIC",
  "agent" : {
    "name" : "Jenkins"
  },
  "started" : "${startedTimeStamp}",
  "artifactoryPrincipal" : "${user}",
  "url" : "${this.script.env.BUILD_URL}",
  "vcsRevision" : "${currentGitCommit}",
  "vcsUrl" : "${currentGitURL}",
  "modules" : [ {
    "properties" : {
      "project.build.sourceEncoding" : "UTF-8"
    },
    "id" : "${applicationName}:${applicationVersion}",
    "artifacts" : [ {
      "type" : "${artifactType}",
      "sha1" : "${artifactSha1}",
      "md5" : "${artifactMd5}",
      "name" : "${applicationName}-${applicationVersion}.${artifactType}"
    }]
  } ]
}
""".stripIndent()
    return result
  }
}
